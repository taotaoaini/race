#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls u<<1
#define rs u<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;


void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	int n;
	cin >> n;
	auto get = [&](int n)->vector<int> {
		vector<int> vis(n + 1);
		vector<int> pr;
		for (int i = 2; i <= n; i++) {
			if (!vis[i]) {
				pr.push_back(i);
			}
			for (int j = 0; j < (int)pr.size() && 1LL * i * pr[j] <= n; j++) {
				vis[i*pr[j]]=true;
				if(i%pr[j]==0) break;
			}
		}
		return pr;
	};
	auto re=[&](int n,int p)->int{
		int res=0;
		while(n){
			res+=n/p;
			n/=p;
		}
		return res;
	};
	vector<int> pr=get(n);
	// for(auto & x: pr) cout<<x<<" ";
	for(int i=0;i<(int)pr.size();i++){
		int x=re(n,pr[i]);
		if(x!=0) cout<<pr[i]<<" "<<x<<endl;
	}
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	//cin>>t;

	while (t--) {
		solve();
		//init();
	}

	return 0;
}