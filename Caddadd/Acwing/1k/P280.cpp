#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls u<<1
#define rs u<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 1010;

int f[210][810][30];
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	int n, m;
	int idx = 0;
	cin >> n >> m;
	idx += 1;
	vector<pii> a(n + 1);
	vector<int> v(n + 1);
	vector<int> w(n + 1);
	for (int i = 1; i <= n; i++) cin >> a[i].fi >> a[i].se;
	for (int i = 1; i <= n; i++) v[i] = a[i].fi - a[i].se, w[i] = a[i].fi + a[i].se;
	const int base = 400; //偏量
	// vector<vector<vector<int>>> f(n + 10, vector<vector<int>>(N + 10, vector<int>(m + 10, -inf)));
	memset(f, -0x3f, sizeof(f));
	f[0][base][0] = 0; //向右偏移400
	for (int i = 1; i <= n; i++) { //01背包转移
		for (int j = 0; j <= 2 * base ; j++) {
			for (int k = 0; k <= m; k++) {
				f[i][j][k] = f[i - 1][j][k];
				if (k >= 1 && j + v[i] <= 2 * base && j + v[i] >= 0)
					f[i][j][k] = max(f[i][j][k], f[i - 1][j + v[i]][k - 1] + w[i]);
			}
		}
	}
	int res = 0;
	int s = 0;
	for (int i = base, j = base;; i--, j++) {
		if (f[n][i][m] >= 0 || f[n][j][m] >= 0) {
			if (f[n][i][m] > f[n][j][m]) s = i;
			else s = j;
			res = max(f[n][i][m], f[n][j][m]);
			break;
		}
	}
	vector<int> ans;
	int i = n, j = s, k = m;
	int sp = 0, sd = 0;
	while (k) { //解包,通过最优解逆推具体方案
		if (f[i][j][k] == f[i - 1][j][k]) i--;
		else {
			ans.push_back(i);
			sp += a[i].fi;
			sd += a[i].se;
			j += v[i];
			i--, k--;
		}
	}
	sort(ans.begin(), ans.end());
	cout << "Jury #" << idx << endl;
	cout << "Best jury has value " << sp << " for prosecution and value " << sd << " for defence:" << endl;
	for (int i = 0; i < (int)ans.size(); i++) cout << ans[i] << " ";
	cout << endl;
	cout << endl;
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	//cin>>t;

	while (t--) {
		solve();
		//init();
	}

	return 0;
}