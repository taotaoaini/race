#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls p<<1
#define rs p<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;

int h[N], ne[N * 2], w[N * 2], to[N * 2], idx = 0;
// h:头，ne:下一个指针，to：指向的节点
int d[N], f[N], b[N]; //存路径：链式前向星的下标
int n, k;
void add(int u, int v, int c) { //链式前向星
	to[++idx] = v; ne[idx] = h[u]; h[u] = idx; w[idx] = c;
}

void dfs(int u, int fa) {
	for (int i = h[u]; i; i = ne[i]) {
		int v = to[i];
		if (v == fa) continue;
		d[v] = d[u] + 1;
		f[v] = u; b[v] = i;
		if (d[v] > d[0]) {
			f[0] = v;
			d[0] = d[v];
		}
		dfs(v, u);
	}
}
void dfs1(int u, int fa) {
	for (int i = h[u]; i; i = ne[i]) {
		int v = to[i];
		if (v == fa) continue;
		dfs1(v, u);
	}
}
void work(int& p, int& q) {
	d[0] = -inf; //不可达状态
	d[1] = 0; dfs(1, 0); p = f[0];
	d[p] = 0; dfs(p, 0); q = f[0]; //此时p为根节点
}
void dpfind(int u, int fa, int& mx) { //路径存在负的 ，如果子树都是负的，父亲节点可以选择不要
	for (int i = h[u]; i; i = ne[i]) {
		int v = to[i];
		if (v == fa) continue;
		dpfind(v, u, mx);
		mx = max(mx, d[u] + d[v] + w[i]);
		d[u] = max(d[u], d[v] + w[i]);
	}
	d[u] = max(d[u], 0);
	mx = max(mx, d[u]);
}
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	cin >> n >> k;
	for (int i = 1; i <= n - 1; i++) {
		int u, v, c;
		cin >> u >> v;
		add(u, v, 1); add(v, u, 1);
	}
	int p, q, res = 2 * (n - 1);
	work(p, q);
	res -= (d[0] - 1);
	if (k == 2) {
		for (int i = q; i != p; i = f[i]) {
			w[b[i]] = -1;
			w[b[i] + ((b[i] & 1) ? 1 : - 1)] = -1;
		}
		memset(d, -0x3f, sizeof(d));
		int mx = 0;
		dpfind(p, 0, mx);
		res -= (mx - 1);
	}
	cout << res << endl;
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	//cin>>t;

	while (t--) {
		solve();
		//init();
	}

	return 0;
}