/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;

string+string
to_string(i32)
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second
#define mid ((l+r)>>1)

using i8 = signed char;
using u8 = unsigned char;
using i16 = signed short int;
using u16 = unsigned short int;
using i32 = signed int;
using u32 = unsigned int;
using f32 = float;
using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using i128 = __int128_t;
using u128 = __uint128_t;
using f128 = long double;

using pii = std::pair<i32, i32>;
using pll = std::pair<i64, i64>;
using namespace std;

/*
快读代码模板
a=read<i32>(),b=read<i64>(),c=read<int128>();
*/

template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}

template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}

const i32 inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的

const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;

void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	int n;
	cin >> n;
	set<i8> d;
	vector<pair<char, int>> a(26);
	string s[n + 1];
	for (int i = 0; i < 26; i++) a[i].fi = 'a' + i;
	for (int i = 1; i <= n; i++) {
		cin >> s[i];
		int slen = s[i].size();
		d.insert(s[i][0]);
		for (int j = slen-1,sum=1; j>=0; j--) {
			a[s[i][j] - 'a'].se += sum;
			sum*=10;
		}
	}
	sort(a.begin(), a.end(), [&](pair<char, int> x, pair<char, int> y) {return x.se > y.se;});
	vector<i32> ans(26);
	bool flag = false;
	int x = 1;
	i64 res = 0;
	for (int i = 0; i < 26; i++) {
		if (!flag && d.count(a[i].fi) == 0) flag = true;
		else res+=x*a[i].se,x++;
	}
	cout<<res<<endl;
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	cout << fixed << setprecision(12);

	int t = 1;
	//init();//全局初始化
	//cin>>t;

	while (t--) {
		solve();
	}

	return 0;
}