// /*
// i64=9.22*10^18 ((1LL << 63) - 1)
// int=2.1*10^9 ((1<<32)-1)
// u64=(1.844*10^19)

// double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
// double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
// double round(double x);//四舍五入,-2.7->-3,-2.2->-2

// 二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
// 三行四列的数组:arr[1][2]=arr[1*4+2]

// % + -
// << >>
// > >= < <=
// != & ^ | && ||

// string 从下标1开始读入
// char s[N];
// cin>>s+1;

// str+str
// to_string(int)
// */
// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题
// #include<functional>//编写内部函数

// #define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
// #define endl '\n'
// #define ls u<<1
// #define rs u<<1|1
// #define fi first
// #define se second
// #define mid ((l+r)/2)

// using i8 = signed char;
// using u8 = unsigned char;
// using i16 = signed short int;
// using u16 = unsigned short int;
// using i32 = signed int;
// using u32 = unsigned int;
// using f32 = float;
// using i64 = signed long long;
// using u64 = unsigned long long;
// using f64 = double;
// using i128 = __int128_t;
// using u128 = __uint128_t;
// using f128 = long double;

// using pii = std::pair<int, int>;
// using pll = std::pair<long long, long long>;
// using pci = std::pair<char, int>;
// using namespace std;

// /*
// 快读代码模板
// a=read<int>(),b=read<ll>(),c=read<int128>();
// */

// template<typename T>
// void read(T &x) {
// 	x = 0; bool flag(0); char ch = getchar();
// 	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
// 	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
// 	flag ? x = -x : 0;
// }

// template<typename T>
// void write(T x, bool mode = 1) {//mode=1为换行，0为空格
// 	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
// 	do stk[++top] = x % 10, x /= 10; while (x);
// 	while (top) putchar(stk[top--] | 48);
// 	mode ? putchar('\n') : putchar(' ');
// }

// priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
// priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

// //int[]:6.7e7
// const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
// const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
// const int mod = 1e9 + 7;
// const int M = 100010;
// const int N = 100010;


// void solve() {
// 	//freopen("a.in","r",stdin);
// 	//freopen("a.out","w",stdout);
// 	int n;
// 	string s;
// 	cin >> n;
// 	cin >> s;
// 	vector<int> a(n + 10, 0);
// 	vector<int> b(n + 10, 0);
// 	for (int i = 1; i <= n; i++) a[i] = s[i - 1] - '0';
// 	cin >> s;
// 	for (int i = 1; i <= n; i++) b[i] = s[i - 1] - '0';
// 	auto add = [&](int x) {
// 		int t = 1;
// 		while (t && x >= 1) {
// 			a[x] += 1;
// 			t = 0;
// 			if (a[x] == 10) {
// 				a[x] = 0;
// 				t = 1;
// 			}
// 			x -= 1;
// 		}
// 	};
// 	auto sub = [&](int x) {
// 		int t = 1;
// 		while (t && x >= 1) {
// 			a[x] -= 1;
// 			t = 0;
// 			if (a[x] == -1) {
// 				a[x] = 9;
// 				t = 1;
// 			}
// 			x -= 1;
// 		}
// 	};
// 	i64 res = 0;
// 	// sub(5);
// 	// for(int i=1;i<=n;i++) cout<<a[i];
// 	vector<int> f(3, inf);
// 	int t1 = 0, t2 = 0;
// 	for (int i = n; i >= 1; i--) {
// 		int x = a[i];
// 		int y = b[i];
// 		int tmp1 = y - x >= 0 ? y - x : 10 - x + y;
// 		int tmp2 = x - y >= 0 ? x - y : x + 10 - y;
// 		// cout<<tmp1<<"::::"<<tmp2<<endl;
// 		// for(int i=1;i<=n;i++) cout<<a[i];
// 		cout << endl;
// 		if (tmp1 < tmp2) {
// 			res += tmp1;
// 			if (tmp1 + x >= 10) {
// 				add(i - 1);
// 				f[1]
// 			}
// 		} else if (tmp1 > tmp2) {
// 			res += tmp2;
// 			if (x - tmp2 < 0) sub(i - 1);
// 		} else {

// 		}
// 	}
// 	cout << res << endl;
// }

// int main() {
// 	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
// 	cout << fixed << setprecision(12);

// 	//init();//全局初始化

// 	int t = 1;
// 	//cin>>t;

// 	while (t--) {
// 		solve();
// 		//init();
// 	}

// 	return 0;
// }





/*
i64=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
u64=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;

str+str
to_string(int)
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls u<<1
#define rs u<<1|1
#define fi first
#define se second
#define mid ((l+r)/2)

using i8 = signed char;
using u8 = unsigned char;
using i16 = signed short int;
using u16 = unsigned short int;
using i32 = signed int;
using u32 = unsigned int;
using f32 = float;
using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using i128 = __int128_t;
using u128 = __uint128_t;
using f128 = long double;

using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<int128>();
*/

template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}

template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

//int[]:6.7e7
const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;


void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	int n;
	cin >> n;
	string s, t;
	cin >> s >> t;
	vector<vector<int>> f(n + 10, vector<int>(3, inf));
	//f[i][j]表示当前到达i时，3种状态的不同代价,(0,1,2)
	//w的公式：这个是显然可以看出的
	// f[i][0]:表示当前i时,不进位直接计算:abs(y-x,x-y);
	// f[i][1]:表示当前i时,通过进位进行计算:t[i]+(10-s[i]);
	// f[i][2]:表示当前i时,通过退位进行计算：(s[i]+10-t[i]);

	// f[n-1][0]=abs(s[n-1]-t[n-1]);
	// f[n-1][1]=t[n-1]+10-s[n-1];
	// f[n-1][2]=s[n-1]+10-t[n-1];

	//f[i][0]=max(f[i+1][0]+w0,f[i+1][1]+w1,f[i+1][2]+w2);
	//f[i][1]=max(f[i+1][0]+w0,f[i+1][1]+w1,f[i+1][2]+w2);
	//f[i][2]=max(f[i+1][0]+w0,f[i+1][1]+w1,f[i+1][2]+w2);
	f[n][0] = 0;//状态可达的代价，为什么f[n][1]=f[n][2]=inf(因为状态不可达)
	for (int i = n - 1; i >= 0; i--) {
		int x = s[i] - '0', y = t[i] - '0';
		f[i][0] = f[i + 1][0] + abs(x - y);
		f[i][0] = min(f[i][0], f[i + 1][1] + abs(x + 1 - y));
		f[i][0] = min(f[i][0], f[i + 1][2] + abs(x - 1 - y));

		f[i][1] = f[i + 1][0] + y + 10 - x;
		f[i][1] = min(f[i][1], f[i + 1][1] + y + 10 - x - 1);
		f[i][1] = min(f[i][1], f[i + 1][2] + y + 10 - x + 1);

		f[i][2] = f[i + 1][0] + x + 10 - y;
		f[i][2] = min(f[i][2], f[i + 1][1] + x + 1 + 10 - y);
		f[i][2] = min(f[i][2], f[i + 1][2] + x - 1 + 10 - y);
	}
	cout << min(min(f[0][0], f[0][1]), f[0][2]) << endl;
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	//cin>>t;

	while (t--) {
		solve();
		//init();
	}

	return 0;
}