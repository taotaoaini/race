// 我们知道当所有儿子节点排好序的时候，父节点就排好序了。 
// 这里我们定义一种状态：状态s的二进制位上的1表示此点已经排好序了。 
// 例如：s=6时，化为二进制s=110，表示第2、3个点已经排好序了。 
// 所以父节点的状态可以由子节点转移而来。
 // 用son[i]表示节点i可以进行转移的合法状态，f[s]表示状态为s的方法数。 
// 然后枚举所有的状态，然后在此状态中找二进制位上是0的点，如果这个点要求的合法状态是当前状态的子状态，那么可以由当前状态转移到把第i位设为1的状态。 
#include<bits/stdc++.h>
using namespace std;
#define int long long
const int N = 17; int son[N], n, dp[1 << N], m;
signed main() {
	cin >> n >> m;
	while(m--) {
		int x, y; cin >> x >> y; x--, y--;
		son[x] |= (1 << y);
	}
	dp[0] = 1;
	for(int i = 0; i < (1 << n); i++) {
		for(int j = 0; j < n; j++) {
			if(i & (1 << j)) continue;
			if((i & son[j]) != son[j]) continue;
			dp[i | (1 << j)] += dp[i];
		}
	}
	cout << dp[(1 << n) - 1];
	return 0;
}