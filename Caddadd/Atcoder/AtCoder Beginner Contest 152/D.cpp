#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const int N=200010;
ll a[10][10];
int s(int x){
	while(x>9){
		x/=10;
	}
	return x;
}
int e(int x){
	return x%10;
}
void solve(){
	int n;
	cin>>n;
	for(int i=1;i<=n;i++){
		a[s(i)][e(i)]++;
	}
	ll res=0;
	for(int i=1;i<=n;i++){
		res+=a[e(i)][s(i)];
	}
	cout<<res<<endl;
}

int main(){
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	
	int t=1;
	//cin>>t;
	while(t--){
		solve();
	}

	return 0;
}