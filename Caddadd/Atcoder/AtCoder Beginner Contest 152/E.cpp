#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const ll mod = 1e9 + 7;
const int N = 1e6 + 5;
ll vis[N], pri[N], cnt = 0;
ll a[N], v[N];
ll qmi(ll a,ll b){
	ll res=1;
	while(b){
		if(b&1) res=res*a%mod;
		a=a*a%mod;
		b>>=1;
	}
	return res;
}
void solve(){
	for(int i = 2; i < N; i++){
		if(vis[i] == 0) pri[++cnt] = i;
		for(int j = 1; j <= cnt && i * pri[j] < N; j++){
			vis[i * pri[j]] = 1;
			if(i % pri[j] == 0) break;
		} 
	}

	int n;
	cin >> n;
	for(int i = 1; i <= n; i++) cin >> a[i];
	for(int i = 1; i <= n; i++){
		ll x = a[i];
		for(int j = 1; pri[j] * pri[j] <= x; j++){
			ll num = 0;
			while(x % pri[j] == 0) num++, x /= pri[j];
			v[pri[j]] = max(num, v[pri[j]]);
		}
		if(x != 0) v[x] = max(v[x], 1ll);
	}

	ll lcm = 1, res = 0;
	for(int i = 1; i <= cnt; i++) lcm = lcm * qmi(pri[i], v[pri[i]]) % mod;
	for(int i = 1; i <= n; i++) res = (res + lcm * qmi(a[i], mod-2) % mod) % mod;
	
	cout << res << endl;
}

int main(){
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	
	int t=1;
	//cin>>t;
	while(t--){
		solve();
	}

	return 0;
}