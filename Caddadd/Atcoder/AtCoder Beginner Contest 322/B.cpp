#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}

void solve(){
	int n,m;
    string s1,s2;
	cin>>n>>m;
    cin>>s1>>s2;
	bool f1=true,f2=true;
    // cout<<n.size()<<endl;
	for(int i=0;i<s1.size();i++){
		if(s1[i]!=s2[i]) f1=false;
        // cout<<s2.size()+i-s1.size()<<endl;
		if(s1[i]!=s2[s2.size()+i-s1.size()]) f2=false;
	}
	if(f1&&f2) cout<<0<<endl;
	else if(f1&&!f2) cout<<1<<endl;
	else if(!f1&&f2) cout<<2<<endl;
	else cout<<3<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}