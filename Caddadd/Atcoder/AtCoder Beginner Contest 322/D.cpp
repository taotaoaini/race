#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
#define SIZE 4
#define endl '\n'
//#define int long long
using namespace std;
//逆时针旋转90度
void rotate(string s[]){
    char temp[SIZE][SIZE];//中间变量
    for(int i=0;i<SIZE;i++){
        for(int j=0;j<SIZE;j++){
            temp[SIZE-j-1][i]=s[i][j];
        }
    }
    for(int i=0;i<SIZE;i++){
        for(int j=0;j<SIZE;j++){
            s[i][j]=temp[i][j];
        }
    }
}
//判断合法性
bool valid(int x){
    return x>=0&&x<SIZE;
}
//平移
int get(string s[],int dx,int dy){
    int ret=0;//ret存储二进制串
    for(int x=0;x<SIZE;x++){
        for(int y=0;y<SIZE;y++){
            if(s[x][y]=='#'){
                int tx=x+dx;
                int ty=y+dy;
                if(!valid(tx)||!valid(ty)) return -1;
                ret|=1<<(tx*4+ty);
            }
        }
    }
    return ret;//返回图案所对应的二进制串
}
vector<int>add(int id){
    vector<int>ret;
    string s[SIZE];
    for(int i=0;i<SIZE;i++) cin>>s[i];
    for(int i=0;i<4;i++){
        for(int dx=-3;dx<=3;dx++){
            for(int dy=-3;dy<=3;dy++){
                int v=get(s,dx,dy);
                if(v>=0) ret.push_back(v);
            }
        }
        rotate(s);
    }
    return ret;
}
void solve() {
    vector<int>mask[3];
    for(int id=0;id<3;id++) mask[id]=add(id);
    for(int x:mask[0]){
        for(int y:mask[1]){
            for(int z:mask[2]){
                if((x|y|z)!=(1<<SIZE*SIZE)-1) continue;
                if(x&y) continue;
                if(x&z) continue;
                if(y&z) continue;
                cout<<"Yes"<<endl;
                return;
            }
        }
    }
    cout<<"No"<<endl;
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);
    int t=1;
//    cin>>t;
    while(t--) {
        solve();
    }
    return 0;
}