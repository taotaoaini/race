#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
const int N=110,M=110;
// int a[N];
int ans[N];
struct A{
	int sc,id;
}a[N];
struct Sc{
	int sc,id;
}sc[M];
int n,m;
bool vis[N][M];
void solve(){
	cin>>n>>m;
	char c;
	for(int i=1;i<=n;i++) a[i].id=i,sc[i].id=i;
	for(int i=1;i<=m;i++) cin>>sc[i].sc;
	for(int i=1;i<=n;i++){
		int cnt=0;
		for(int j=1;j<=m;j++){
			cin>>c;
			if(c=='o') {cnt+=1;a[i].sc+=sc[j].sc;vis[i][j]=true;}
		}
		a[i].sc+=i;
	}
	auto cmp1=[&](const Sc& a1,const Sc& a2){
		return a1.sc>a2.sc;
	};
	auto cmp2=[&](const A& a1,const A& a2){
		return a1.sc<a2.sc;		
	};
	sort(sc+1,sc+m+1,cmp1);
	sort(a+1,a+n+1,cmp2);
	int mx1=a[n].sc,mx2=a[n-1].sc;
	// cout<<mx1<<" "<<mx2<<endl;
	for(int i=1;i<=n;i++){
		int x=a[i].sc;
		int id=a[i].id;
		int cnt=0;
		for(int j=1;j<=m;j++){
			if(i==n&&x>mx2) break;
			if(i<n&&x>mx1) break;
			if(vis[id][sc[j].id]) continue;
			x+=sc[j].sc;
			vis[id][sc[j].id]=true;
			cnt+=1;
		}
		ans[id]=cnt;
	}
	for(int i=1;i<=n;i++) cout<<ans[i]<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}