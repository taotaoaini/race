/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;

string+string
to_string(int)
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second
#define mid ((l+r)>>1)

using ll = long long;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<int128>();
*/

template<typename T>
void read(T &x) {
    x = 0; bool flag(0); char ch = getchar();
    while (!isdigit(ch)) flag = ch == '-', ch = getchar();
    while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
    flag ? x = -x : 0;
}

template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
    x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
    do stk[++top] = x % 10, x /= 10; while (x);
    while (top) putchar(stk[top--] | 48);
    mode ? putchar('\n') : putchar(' ');
}

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const ll dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;
struct full_permutation {

    std::vector<std::vector<int>> get(int n) {
        std::vector<int> nums;
        for (int i = 1; i <= n; ++i) {
            nums.push_back(i);
        }

        std::vector<std::vector<int>> result;
        generate(nums, 0, result);
        return result;
    }
    void generate(std::vector<int>& nums, int index, std::vector<std::vector<int>>& result) {
        if (index == nums.size()) {
            result.push_back(nums);
            return;
        }

        for (int i = index; i < nums.size(); ++i) {
            std::swap(nums[index], nums[i]);
            generate(nums, index + 1, result);
            std::swap(nums[index], nums[i]); // backtrack
        }
    }
};
int n, m;
int a[10][10];
int b[10][10];
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    cin >> n >> m;
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++) {
            cin >> a[i][j];
        }
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++) {
            cin >> b[i][j];
        }
    full_permutation fp;
    vector<vector<int>> t1 =fp.get(n);
    vector<vector<int>> t2=fp.get(m);
    int res=inf;
    for(int i=0;i<t1.size();i++){
        for(int j=0;j<t2.size();j++){
            bool flag=true;
            for(int k=1;k<=n;k++){
                for(int z=1;z<=m;z++){
                    if(a[t1[i][k-1]][t2[j][z-1]]!=b[k][z]) flag=false;
                }
            }
            if(!flag) continue;
            int cnt1=0,cnt2=0;
            for(int k=0;k<n;k++)
                for(int z=0;z<n;z++){
                    if(k<z&&t1[i][k]>t1[i][z]) cnt1++;
                }
            for(int k=0;k<m;k++)
                for(int z=0;z<m;z++){
                    if(k<z&&t2[j][k]>t2[j][z]) cnt2++;
                }
            res=min(res,cnt1+cnt2);
        }
    }
    if(res==inf) cout<<-1<<endl;
    else cout<<res<<endl;

}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
    }

    return 0;
}