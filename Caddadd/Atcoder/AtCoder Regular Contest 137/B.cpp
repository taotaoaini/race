// 提示 2-2：把子数组内的 0 视作 1，1 视作 -1。
// 最大子数组和就是最大增量 maxInc。

// 提示 2-3：把子数组内的 0 视作 -1，1 视作 1。
// 最大子数组和就是最大减少量 maxDec。
// 注：也可以在提示 2-2 的基础上计算最小子数组和。

// 提示 3：答案就是 maxInc+maxDec+1。
// 证明：我们可以在任意子数组的基础上「微调」，也就将子数组的长度加一或者减一。例如把 0 包含入子数组，那么变化量仅会 +1。同理，把 0 移出子数组，那么变化量仅会 -1。对于把 1 包含入/移出子数组的情况同理。
// 因此可以把变化量从 -maxDec 不断微调到 maxInc，所以变化量可以取到 [-maxDec, maxInc] 中的任意整数，这一共有 maxInc+maxDec+1 个数。
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
void solve(){
	int n;
	cin>>n;
	vector<int> a(n);
	vector<int> b(n);
	vector<int> c(n);
	for(int i=0;i<n;i++){
		cin>>a[i];
		if(a[i]==0) b[i]=1,c[i]=-1;else b[i]=-1,c[i]=1;
	}
	ll res=0;
	ll s1=0,s2=0,ans1=0,ans2=0;
	for(int i=0;i<n;i++){
		if(b[i]+s1<b[i]) s1=b[i];
		else s1+=b[i];
		ans1=max(s1,ans1);
		if(c[i]+s2<c[i]) s2=c[i];
		else s2+=c[i];
		ans2=max(s2,ans2); 
	}
	cout<<ans1+ans2+1<<endl;
}
int main(){
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
		solve();
	return 0;
}