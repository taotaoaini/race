/*1.set:存不存在值，st:存已有值*/
// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>

// #define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;

// void solve(){
// 	int n,m;
// 	cin>>n>>m;
// 	vector<int> a(n);
// 	unordered_map<int,int> st;
// 	set<int> s;
// 	for(int i=0;i<n;i++) cin>>a[i];
// 	// priority_queue<int> q;
// 	for(int i=0;i<=n;i++) s.insert(i);
// 	for(int i=0;i<m;i++) {st[a[i]]++;if(st[a[i]]==1) s.erase(a[i]);}
// 	int res=*(s.begin());
// 	for(int i=m;i<n;i++){
// 		st[a[i-m]]--; if(st[a[i-m]]==0) s.insert(a[i-m]);
// 		st[a[i]]++; if(st[a[i]]==1) s.erase(a[i]);
// 		res=min(res,*(s.begin()));
// 	}
// 	cout<<res<<endl;
// }
// int main() {
//     ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
// 	solve();    
//     return 0;
// }


/*滑动窗口,大顶堆,哈希表*/
// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<deque>
// #define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;
	
// void solve(){
// 	int n,m;
// 	cin>>n>>m;
// 	vector<int> a(n);
// 	vector<int> c(n+1);
// 	for(int i=0;i<n;i++) cin>>a[i];
// 	priority_queue<int,vector<int>,greater<int>> q;
// 	for(int i=0;i<m;i++) {c[a[i]]++;}
// 	for(int i=0;i<=n;i++) {if(c[i]==0) q.push(i);}
// 	int res=q.top();
// 	for(int i=m;i<n;i++){
// 		if(--c[a[i-m]]==0) q.push(a[i-m]);
// 		c[a[i]]++;
// 		while(c[q.top()]) q.pop();
// 		res=min(res,q.top());
// 	}
// 	cout<<res<<endl;
// }
// int main() {
//     ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
//     solve();
//     return 0;
// }