/*3^3=27,暴力枚举每一种情况*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
void solve(){
    int n;
    cin>>n;
    vector<int> a(n);
    string s;
    for(int i=0;i<n;i++) cin>>a[i];
    cin>>s;
    auto f=[](int i,int j,int k)->int{
            int t[4]{0};
            t[i]++;
            t[j]++;
            t[k]++;
            for(int i=0;i<4;i++) if(!t[i]) return i;
    };
    ll res=0;
    for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
            for(int k=0;k<3;k++){
                ll k1=0,k2=0,k3=0;
                for(int z=0;z<s.size();z++){
                    if(a[z]==i&&s[z]=='M') k1++;
                    if(a[z]==j&&s[z]=='E') k2+=k1;
                    if(a[z]==k&&s[z]=='X') k3+=k2;
                }
                res+=k3*f(i,j,k);
            }
            cout<<res<<endl;
}
int main(){
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    solve();
    return 0;
}