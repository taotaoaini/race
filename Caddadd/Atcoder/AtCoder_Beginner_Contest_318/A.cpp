#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;


int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
 	int n,m,p;
 	cin>>n>>m>>p;
 	int res=0;
 	for(int i=m;i<=n;i+=p){
 		res++;
 	}
 	cout<<res<<endl;
    return 0;
}