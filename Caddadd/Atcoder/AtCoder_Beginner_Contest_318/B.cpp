#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

bool vis[110][110];
int res=0;

int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int n;
    cin>>n;
    int a,b,c,d;
    while(n--){
 	cin>>a>>b>>c>>d;
    for(int i=a;i<b;i++){
    	for(int j=c;j<d;j++){
    		if(!vis[i][j]) res++,vis[i][j]=true;
    	}
    }
    }
    cout<<res<<endl;
    return 0;
}