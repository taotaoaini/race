#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;


int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int n,d,p;
    cin>>n>>d>>p;
    vector<int> a(n);
    auto cmp=[&](int a,int b)->bool{return a>b;};
    for(int i=0;i<n;i++) cin>>a[i];
    sort(a.begin(),a.end(),cmp);
	vector<ll> sum(n+1);
	for(int i=1;i<=n;i++) sum[i]=sum[i-1]+a[i-1];
	ll res=sum[n];
	for(int i=0;i<=n;i+=d){
		if(i+d<=n){
			if(sum[i+d]-sum[i]>p)  res-=(sum[i+d]-sum[i])-p;
		}else{	
			if(sum[n]-sum[i]>p)	res-=(sum[n]-sum[i])-p;  
		}
	}
	cout<<res<<endl;

    return 0;
}