// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>

// #define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数


// b[i] > mx 的时候 a[i] 可能为0，但是为零不代表就一定合法
// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;
// const int mod=1e9+7;
// ll qmi(ll x,ll n){
// 	ll res=1;
// 	while(n){
// 		if(n&1) res=res*x%mod;
// 		n>>=1;
// 		x=x*x%mod;
// 	}
// 	return res;
// }
// void solve(){
// 	int n;
// 	cin>>n;
// 	vector<ll> b(n);
// 	vector<ll> a(n);
// 	ll x=0,mx=0;
// 	bool flag=true;
// 	for(int i=0;i<n;i++){
// 		cin>>x;
// 		if(mx<x) a[i]=x,mx=x;
// 	}
// 	mx=0;
// 	ll mi=0;
// 	// cout<<a[4]<<endl;
// 	for(int i=0;i<n;i++) cin>>b[i];
// 	for(int i=n-1;i>=0;i--){
// 		if(mx<mi) {flag=false;break;}
// 		if(b[i]>mx){
// 			if(a[i]&&a[i]!=b[i]) {flag=false;break;}
// 			else a[i]=b[i],mx=b[i];
// 		}
// 		if(!a[i]) mi=max(a[i],mi);
// 	}
// 	ll cnt=0;
// 	ll res=1;
// 	if(flag){
// 		for(ll i=0,l=1;i<n;i++){
// 			// cout<<a[i]<<" ";
// 			if(!a[i]) {cnt++;continue;}
// 			if(cnt){
// 				// cout<<"#cbt:"<<cnt<<endl;
// 				if(l<a[i]) res=res*qmi(l,cnt)%mod;
// 				else res=res*qmi(a[i],cnt)%mod;
// 				cnt=0;
// 			}
// 			l=a[i];
// 		}
// 		// cout<<endl;
// 		cout<<res<<endl;
// 	}
// 	else cout<<0<<endl;  
// }
// int main() {
//     ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
//     solve();
    
//     return 0;
// }


// 如果 b[i]>b[i-1]，那么 a[i] 一定是 b[i]。如果此时 c[i] < b[i]，矛盾，输出 0。
// 同理，如果 c[i]>c[i+1]，那么 a[i] 一定是 c[i]。如果此时 b[i] < c[i]，矛盾，输出 0。
// 如果 b[i]=b[i-1] 且 c[i]=c[i-1]，那么 a[i] 可以填不超过 min(b[i],c[i]) 的正整数，这有 min(b[i],c[i]) 种方案。
// 所有 a[i] 的方案数相乘即为答案。



#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const int mod=1e9+7;
void solve(){
	int n;
	cin>>n;
	vector<int> a(n+2);
	vector<int> b(n+2);
	for(int i=1;i<=n;i++) cin>>a[i];
	for(int i=1;i<=n;i++) cin>>b[i];
	ll res=1;
	for(int i=1;i<=n;i++){
		if(a[i]>a[i-1]&&b[i]<a[i])   {cout<<0<<endl;return;}
		if(b[i]>b[i+1]&&a[i]<b[i]) {cout<<0<<endl;return;}
		if(b[i]==b[i+1]&&a[i]==a[i-1]) res=res*min(b[i],a[i])%mod;
	}
	cout<<res<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);

    solve();
    return 0;
}