#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const ll INF = 1e16;
const int maxn = 1e5+5;

typedef struct Node{ 
	int v;		// 点	 
	ll w;		// 权值 
}node;

bool operator < (const node &a, const node &b){     // 重载 < ，使优先队列按照权值排序 
	return a.w > b.w;
}  
	
vector<node>mp[maxn];	// 邻图表 
ll dis[maxn];		// 1 到 i号点的最短距离 
int path[maxn] = {0};	// 路径，x点是从path[x]到达的
	
void Pr(int x){       		   // 递归路径 
	if(x == 1){		   // x = 1 时就结束 
		cout << 1 << " ";
		return;
	}
		
	Pr(path[x]);		   // 递推前一个点 
	cout << x << " ";
		
}
void solve(){
			
	int n, m;
	cin >> n >> m;
		
	int a, b, w;
	node c;
	for(int i = 1; i <= m; i++){    // 邻图表，存边和边的长度 
		cin >> a >> b >> w;
		c.v = b; c.w = w;
		mp[a].push_back(c);
		c.v = a; c.w = w;
		mp[b].push_back(c);
	}
		
	for(int i = 1; i < maxn; i++) dis[i] = INF;  //初始化最大值 
		
	priority_queue<node>qu;          // 算法的核心：优先队列(贪心的实现)
	node now, next;			 // 队列中的 w 是指 1 号点到 v 号点的距离（队列最前端的就是最短距离）
	now.v = 1; now.w = 0;
	qu.push(now);                    // 插入 1 号点 
		
	while(!qu.empty()){				// 当不能再插入的时候就结束 
		now = qu.top();
		qu.pop();
			
		int len = mp[now.v].size();
		for(int i = 0; i < len; i++){            // 遍历当前点可以到达的下一个点 
			next.v = mp[now.v][i].v;
			next.w = now.w + mp[now.v][i].w; 
			if(next.w < dis[next.v]){     	        // 如果当前距离小于储存的最短距离时 
				path[next.v] = now.v;		// 更新路径 
				dis[next.v] = next.w;		// 更新最短距离 
			}
				qu.push(next);			// 下一个点入列 
		}
			
			mp[now.v].clear();			// 这个点已经走过了，储存的点清空，在进入这个点的时候就不用再次循环，当这样可以省去一个标记数组
	}
		
	if(path[n] == 0) cout << -1 << endl;    // 没有可以到达 n 的点 
	else Pr(n);				// 递归路径 
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}