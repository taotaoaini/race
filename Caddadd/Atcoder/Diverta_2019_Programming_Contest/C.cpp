#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

int f(const std::string& str, const std::string& sub)
{
	int num = 0;
	size_t len = sub.length();
	if (len == 0)len=1;//应付空子串调用
	for (size_t i=0; (i=str.find(sub,i)) != std::string::npos; num++, i+=len);
	return num;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
 	int x;
 	cin>>x;
 	ll res=0;
 	int a=0,b=0,c=0;
 	for(int i=0;i<x;i++){
 		string s;
 		cin>>s;
 		if(s[0]=='B'&&s[s.size()-1]=='A') c+=1;
 		else if(s[0]=='B') a+=1;
 		else if(s[s.size()-1]=='A') b+=1;
 		res+=f(s,"AB");
 	}
 	int t=min(a,b);
 	cout<<res+c+t-(a==0&&b==0)<<endl;
    return 0;
}



