#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const int N=200010;
int n,k,a[N];
struct E{
	int v,ne;
}e[N*2];
int idx,h[N];
int son[N];
ll f[N],dep[N];
void add(int x,int y){
	e[++idx].v=y;
	e[idx].ne=h[x];
	h[x]=idx;
}
void dfs(int u,int fa){
	for(int i=h[u];i;i=e[i].ne){
		int v=e[i].v;
		if(v==fa) continue;
		f[v]=dep[v]=dep[u]+a[v];
		dfs(v,u);
		if(f[v]>f[u]) f[u]=f[v],son[u]=v;
	}
}
int top[N];
void dfs1(int u,int fa,int topf){
	top[u]=topf;
	if(son[u]) dfs1(son[u],u,topf);
	for(int i=h[u];i;i=e[i].ne){
		int v=e[i].v;
		if(v==fa||v==son[u]) continue;
		dfs1(v,u,v);
	}
}


void solve() {
	cin>>n>>k;
	for(int i=1;i<=n;i++) cin>>a[i];
	for(int i=1;i<n;i++){
		int x,y,z;
		cin>>x>>y;
		add(x,y);
		add(y,x);
	}
	dep[1]=f[1]=a[1];
	dfs(1,0);
	dfs1(1,0,1);
	vector<ll> ans;
	for(int i=1;i<=n;i++){
		if(top[i]==i) ans.push_back(f[i]-dep[i]+a[i]);
	}
	sort(ans.begin(),ans.end(),greater<ll>());
	ll res=0;
	for(int i=0;i<min(k,(int)ans.size());i++) res+=ans[i];
	cout<<res<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    solve();
    return 0;
}