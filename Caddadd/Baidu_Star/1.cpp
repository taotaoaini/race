#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;
int n,q,k,x;
int a[100010],t[100010*4];
void build(int p,int l,int r){
    if(l==r) t[p]=a[l];
    int mid=(l+r)>>1;
    build(p<<1,l,mid);
    build(p<<1|1,mid+1,r);
    t[p]=t[p<<1]+t[p<<1|1];
}
int query(int p,int l,int r,int st,int ed){
    if(st>r || ed<l) return 0;
    if(st<=l&&ed>=r) return t[p];
    int mid=(l+r)>>1;
    return query(p<<1,l,mid,st,ed)+query(p<<1|1,mid+1,r,st,ed);    
}
int query(int p,int l,int r,int st,int ed,int cnt){
    if(st>r || ed<l) return 0;
    if(st<=l&&ed>=r&&cnt) return t[p];
    int mid=(l+r)>>1;
    return query(p<<1,l,mid,st,ed)+query(p<<1|1,mid+1,r,st,ed);    
}

int main() {
    cin>>n>>q>>k>>x;
    for(int i=1;i<=n;i++) cin>>a[i];
    build(1,1,n);
    while(q--){
        int l,r;
        cin>>l>>r;
        if(l-r+1<=k) cout<<query(1,1,n,l,r)<<endl;
        else cout<<query(l,r,n,1,r,0)<<endl;
    }
    return 0;
}