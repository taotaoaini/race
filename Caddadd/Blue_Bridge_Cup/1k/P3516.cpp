/*
double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;

str+str
to_string(int)
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls u<<1
#define rs u<<1|1
#define fi first
#define se second
#define mid ((l+r)/2)

using i8 = signed char;
using u8 = unsigned char;
using i16 = signed short int;
using u16 = unsigned short int;
using i32 = signed int;
using u32 = unsigned int;
using f32 = float;
using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
//using i128 = __int128_t;
//using u128 = __uint128_t;
using f128 = long double;

using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<int128>();
*/

template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}

template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

//i64=9.22*10^18 ((1LL << 63) - 1)
//int=2.1*10^9 ((1<<32)-1)
//64=(1.844*10^19)
//int[]:6.7e7
//64mb:4*1e6 (64*1024*1024)/4
//1 bytes=8 bit

// queue:push,pop,front,back
// deque:push_back,push_front,front,back,pop_front,pop_back
// priority_queue: top,push,pop
// stack: push,top,pop

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;
const int LOGN = 20;
//这题就是求s(u,v)
vector<pii> e[N];
int n, k;
i64 sum[N];
int dep[N], fa[N][LOGN + 2];

void dfs1(int u, int f) {
	dep[u] = dep[f] + 1;
	fa[u][0] = f;
	for (int i = 1; i <= LOGN; i++) {
		fa[u][i] = fa[fa[u][i - 1]][i - 1];
	}
	for (auto p : e[u]) {
		int v = p.fi;
		int w = p.se;
		if (v == f) continue;
		sum[v] = sum[u] + w;
		dfs1(v, u);
	}
	// cout<<u<<":::"<<sum[u]<<endl;
}
int lca(int u, int v) {
	if (dep[u] < dep[v]) swap(u, v);
	if (u == v) return u;
	for (int i = LOGN; i >= 0; i--) {
		if (dep[fa[u][i]] >= dep[v]) u = fa[u][i];
	}
	if (u == v) return u;
	for (int i = LOGN; i >= 0; i--) {
		if (fa[u][i] != fa[v][i]) u = fa[u][i], v = fa[v][i];
	}
	return fa[u][0];
}
i64 get(int u, int v) {
	return sum[u] + sum[v] - 2*sum[lca(u, v)];
}
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	cin >> n >> k;
	for (int i = 1, x, y, w; i <= n - 1; i++) {
		cin >> x >> y >> w;
		e[x].push_back({y, w});
		e[y].push_back({x, w});
	}
	dfs1(1, 0);
	vector<int> a(k + 1);
	i64 ss = 0;
	for (int i = 1; i <= k; i++) {
		cin >> a[i];
		if (i >= 2) {
			ss += get(a[i], a[i - 1]);
		}
	}
	for (int i = 1; i <= k; i++) {
		i64 res = 0;
		if (i == 1) {
			res=ss-get(a[i],a[i+1]);
			cout << res << " ";
		} else if (i == k) {
			res=ss-get(a[i],a[i-1]);
			cout << res << " ";
		} else {
			res=ss-get(a[i],a[i+1])-get(a[i],a[i-1])+get(a[i-1],a[i+1]);
			cout << res << " ";
		}
	}
	cout << endl;
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	//cin>>t;

	while (t--) {
		solve();
		//init();
	}

	return 0;
}