#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

int pto (string time){
	int h,m,s;
	sscanf(time.c_str(),"%d:%d:%d",&h,&m,&s);
	return h*3600+m*60+s;
}
int f(string s){
	string res;
	stack<char> st;
	for(char c:s){
		if(c=='('){
			st.push(c);
		}else if(c==')'&& !st.empty()){
			while(!st.empty()&&st.top()!='('){
				res+=st.top();
				st.pop();
			}
			reverse(res.begin(),res.end());
			break;
		}else if(!st.empty()){
			st.push(c);
		}
	}
	return res==""?0:res[1]-'0';
}
string sto(int seconds){
	int h=seconds/3600;
	seconds%=3600;
	int m=seconds/60;
	int s=seconds%=60;
	char buffer[9];
	snprintf(buffer,sizeof(buffer),"%02d:%02d:%02d",h,m,s);
	return string(buffer);
}
int f1(string s1,string s2,int op){
	int seconds1=pto(s1);
	int seconds2=pto(s2);
	int t=24*3600;//86400
	int timeDiff=seconds2+t*op-seconds1;
	return timeDiff;
}
string f2(int d1,int d2){
	int m=d1+d2>>1;
	return sto(m);
}
void solve(){
	string s;
	getline(cin,s);
	int op=f(s);
	string s1=s.substr(0,8);
	string s2=s.substr(9,8);
	int d1=f1(s1,s2,op);
	getline(cin,s);
	op=f(s);
	s1=s.substr(0,8);
	s2=s.substr(9,8);
	int d2=f1(s1,s2,op);
	cout<<f2(d1,d2)<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    cin>>t;
    cin.ignore();
    // getchar();
    while(t--){
    	solve();
    }
    
    return 0;
}