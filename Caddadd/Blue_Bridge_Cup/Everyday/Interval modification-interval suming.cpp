#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

const int N=100010;
ll a[N],st[N*4],f[N*4];
int n,q;
void pushup(ll u){
    st[u]=st[lc]+st[rc];
}
void pushdown(ll u,ll l,ll r,ll mid){
    if(f[u]){
        st[lc]+=f[u]*(mid-l+1);
        st[rc]+=f[u]*(r-mid);
        f[lc]+=f[u];
        f[rc]+=f[u];
        f[u]=0;
    }
}
void build(ll u,ll l,ll r){
    if(l==r){
        st[u]=a[l];
        return;
    }
    ll mid=l+r>>1;
    build(lc,l,mid);
    build(rc,mid+1,r);
    pushup(u);
}
void update(ll u,ll l,ll r,ll x,ll y,ll k){
    if(x>r||y<l) return;
    if(x<=l&&y>=r){
        st[u]+=(r-l+1)*k;
        f[u]+=k;
        return;
    }
    ll mid=l+r>>1;
    pushdown(u,l,r,mid);
    update(lc,l,mid,x,y,k);
    update(rc,mid+1,r,x,y,k);
    pushup(u);
}
ll query(ll u,ll l,ll r,ll x,ll y){
    if(x>r||y<l) return 0;
    if(x<=l&&r<=y) return st[u];
    ll mid=l+r>>1;
    pushdown(u,l,r,mid);
    return query(lc,l,mid,x,y)+query(rc,mid+1,r,x,y);
}
void solve(){
    cin>>n>>q;
    for(int i=1;i<=n;i++) cin>>a[i];
    build(1,1,n);
    while(q--){
        ll f,l,r,k;
        cin>>f;
        if(f==1){
            cin>>l>>r>>k;
            update(1,1,n,l,r,k);
        }else{
            cin>>l>>r;
            cout<<query(1,1,n,l,r)<<endl;
        }
    }
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
   solve();
    return 0;
}