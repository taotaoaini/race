#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
struct A{
	int ts,id;
	bool operator<(const A& a1) const{
		return ts<a1.ts;} 
}a[100010];
void solve(){
	int n,d,k;
	cin>>n>>d>>k;
	for(int i=0;i<n;i++) cin>>a[i].ts>>a[i].id;
	sort(a,a+n);
	unordered_map<int,int> mp;
	set<int> res;
	int l=0,r=0;
	while(r<n){
		mp[a[r].id]+=1;
		while(a[r].ts-a[l].ts>=d){
			mp[a[l].id]-=1;
			l++;
		}
		if(mp[a[r].id]>=k) res.insert(a[r].id);
		r++; 
	}
	for(auto x:res){cout<<x<<endl;}
	}

int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}