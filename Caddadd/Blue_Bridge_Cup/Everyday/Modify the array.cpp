#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
int fa[1100010];
int n;
int a[1000010];
void init(){
	for(int i=1;i<1100010;i++){
		fa[i]=i;
	}
}
int find(int x){
	while(x!=fa[x]) x=fa[x]=fa[fa[x]];
	return x;
}
void solve(){
	cin>>n;
	init();
	for(int i=1;i<=n;i++) cin>>a[i];
	fa[a[1]]=a[1]+1;
	for(int i=2;i<=n;i++){
		int fi=find(a[i]);
		a[i]=fi;fa[fi]=fi+1;
	}
	for(int i=1;i<=n;i++) cout<<a[i]<<" ";
	cout<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}