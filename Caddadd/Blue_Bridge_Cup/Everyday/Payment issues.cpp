// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include <iomanip>
// #define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;

// void solve(){
// 	int n;
// 	double S;
// 	// ll S;
// 	// cin>>n>>s;
// 	cin>>n>>S;
// 	double m=S/n;
// 	// cout<<m<<endl;
// 	vector<double> a(n);
// 	for(int i=0;i<n;i++) cin>>a[i];
// 	sort(a.begin(),a.end());
// 	double res=0.0;
// 	for(int i=0;i<n-1;i++){
// 		if(a[i]>=m) {S-=m;}
// 		else S-=a[i],res+=(m-a[i])*(m-a[i]);
// 	}
// 	// cout<<S<<endl;
// 	// cout<<res<<endl;
// 	res+=abs(m-S)*abs(m-S);
// 	// cout<<res<<endl;
// 	res=sqrt(res/n);
// 	cout<<fixed<<setprecision(4)<<res<<endl;
// }
// int main() {
//     ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
//     solve();
    
//     return 0;
// }




// 贪心法：
// 准备贪心：先将花费数组从小到大排序
// 贪心策略：
// 从数组最小的元素开始，每次做判断
// 若当前元素小于剩余花费平均值，则取该元素不改变值，将平均成本转嫁到后续元素上
// 若当前元素大于等于剩余花费平均值，则后续元素也大于该平均值，能够承接前较小元素的成 本，将当前元素之后的所有元素取剩余花费平均值
// 结束贪心，解出标准差
// 精确度
// 本题对于精确度要求较高，总结如下提升准确度的方法

// long long提升整数的准确度
// double可以承接18位左右有效数字
// 尽量减少除法，转换成乘法
// 尽量减少会导致误差的计算的次数
#include <cstdio>
#include <iostream>
#include <cmath>
#include <algorithm>
using namespace std;
typedef long long ll;

int main(){
  ll N;
  double S, avg = 0;
  cin >> N >> S;
  int A[500005];
  
  for(int i = 0; i < N; i++)
    cin >> A[i];
  avg = S*1.0/N;
  sort(A, A+N);

  double ans = 0;
  for(int i = 0 ;  i < N; i++){
    if(A[i]*(N-i) < S){
        ans += (avg - A[i]) * (avg - A[i]);
        S -= A[i];
    }
    else{
      double cur = S*1.0/(N-i);
      ans += (cur-avg)*(cur-avg)*(N-i);
      break;
    }
  }

  printf("%.4lf", sqrt(ans/N));
  return 0;
}