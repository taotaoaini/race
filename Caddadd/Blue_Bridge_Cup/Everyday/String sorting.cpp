#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read() {
	__int128 x = 0, f = 1;
	char ch = getchar();
	while (ch < '0' || ch > '9') {
		if (ch == '-') f = -1;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9') {
		x = x * 10 + ch - '0';
		ch = getchar();
	}
	return x * f;
}
//__int128的输出
inline void print(__int128 x) {
	if (x < 0) {
		putchar('-');
		x = -x;
	}
	if (x > 9)
		print(x / 10);
	putchar(x % 10 + '0');
}
/*
贪心的思想，增加字符串长度相当于插入一个字符，增加的逆序对 = 大于自己 + 小于自己的，
最大构造逆序对的情况即不等于自己的字符数，于是有字符串中所有字符数量越接近越好，
在字符数 > 26 以后就又是从abc的顺序开始新增（满足字典序小）,每次插入新字符增加的逆序对即为原字符长度 - 与自己相等的字符数量
于是我们可以暴力枚举字符，判断在选择此字符的情况下能否构造出逆序对数 >= n的
*/
int n;
int f[1010];
int get_max() { // 获取长度为m的字符串的最大逆序对数
	for (int i = 2; i <= 26; i ++) f[i] = f[i - 1] + i - 1; // 长度小于26的字符串最大逆序对数
	int sum = 26, vis[30];
	for (int i = 0; i < 26; i ++) vis[i] = 1; // 记录当前字符串已经各个字符串各一个了
	for (int i = 27; f[i - 1] < n; i ++, sum ++) {
		int ch = (i % 26 - 1 + 26) % 26; // 新增的字符按abc……的顺序新增，插入到逆序的位置，例如zyx……a,下一个接着插入a zyx……aa
		f[i] = f[i - 1] + sum - vis[ch]; vis[ch] ++; //新增逆序对字符总数 - 和自己相同的字符数
	}
}
int cnt[30], vis[30]; // cnt 代表已经确定的构造字符，vis代表后续按最大方法构造的字符
int get_add(int ch){
    int add = 0;
    for(int i = 0; i < ch; i ++) add += vis[i]; // vis 是还未确定的可以按任意顺序排列所以都可以计算进来
    for(int i = ch + 1; i < 26; i ++) add += cnt[i] + vis[i]; // 因为cnt已经确定了，后续字符只能在其后，所以新增的只能是 > ch 的字符数
    return add; 
}
bool check(int id, int m, int ch, int sum) {
	for (int i = id + 1; i <= m; i ++) {
		int maxadd = 0, ch1 = 0;
		for (int j = 0; j < 26; j ++) { // 和上述fi的求解过程同理，只是枚举字符选择最优解的那一个
			int add = get_add(j);
			if (maxadd < add) {
				maxadd = add;
				ch1 = j;
			}
		}
		vis[ch1] ++;
		sum += maxadd;
	}
	memset(vis, 0, sizeof vis);
	if (sum >= n) return true; // 当剩余字符能构造出 >= n 的即返回true
	return false;
}
void solve() {
	cin >> n;
	get_max();
	int m = 0;
	for (int i = 2; i <= n; i ++) {
		if (f[i] >= n) {
			m = i;
		}
	}
	int sum = 0;
	string ans;
	for (int i = 1; i <= m; i ++) {
		for (int j = 0; j < 26; j ++) { // 每个位置都从'a' 开始枚举，看是否剩下的字符最大情况下仍然能构造出大于等于n的字符，若可以则使用当前的ch
			int initadd = get_add(j);
			cnt[j] ++;
			sum += initadd;
			if (check(i, m, j, sum)) {
				ans += ('a' + j);
				break;
			}
			cnt[j] --; // 不满足，于是回溯枚举新的字符
			sum -= initadd;
		}
	}
	cout << ans << endl;
}
int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}