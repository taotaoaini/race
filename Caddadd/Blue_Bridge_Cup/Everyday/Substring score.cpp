#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read() {
    __int128 x = 0, f = 1;
    char ch = getchar();
    while (ch < '0' || ch > '9') {
        if (ch == '-') f = -1;
        ch = getchar();
    }
    while (ch >= '0' && ch <= '9') {
        x = x * 10 + ch - '0';
        ch = getchar();
    }
    return x * f;
}
//__int128的输出
inline void print(__int128 x) {
    if (x < 0) {
        putchar('-');
        x = -x;
    }
    if (x > 9)
        print(x / 10);
    putchar(x % 10 + '0');
}
vector<int> a[26];
void solve() {
    string s;
    cin >> s;
    int n = s.size();
    for (int i = 0; i < 26; i++) a[i].push_back(-1);
    for (int i = 0; i < s.size(); i++) {
        a[s[i] - 'a'].push_back(i);
    }
    for (int i = 0; i < 26; i++) {
        a[i].push_back(n);
    }
    for(int i=0;i<26;i++){
        if(a[i].size()==2) continue;
        for(auto x:a[i]){
            cout<<x<<" ";
        }
        cout<<endl;
    }
    ll res = 0;
    for (int i = 0; i < 26; i++) {
        if (a[i].size() == 2) continue;
        for (int j = 1; j < a[i].size() - 1; j++) {
            // res += (1ll) * max(1, (a[i][j] - a[i][j - 1])) * max(1, (a[i][j + 1] - a[i][j]));
            //在头插入-1，在末尾插入n，可以减少边界判断
            //a...a..a:枚举中间值的贡献度：4*3
            res+=(1ll)*(a[i][j]-a[i][j-1])*(a[i][j+1]-a[i][j]);
        }
    }
    cout << res << endl;

}
int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    //cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}