#include <iostream>
#include <algorithm>
#include <cstring>
#include <cmath>
using namespace std;

typedef long long LL;

const int N = 100010 * 2;
int a[N];
int n, m;

int main()
{
    cin >> n >> m;
    int k = n + m + 1;
    for(int i = 0;i < k;i ++) scanf("%d",&a[i]);
    
    sort(a,a + k);  // 找最小值和最大值
    LL res = 0;
    if(!m) {
        for(int i = 0;i < k;i ++) res += a[i];
    }else {
        res = a[k - 1] - a[0];
        for(int i = 1;i < k - 1;i ++) res += abs(a[i]);
    }
    
    printf("%lld\n",res);
    return 0;
}