#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
int dp[1<<22];
int cnt[110];//利用二进制使得cnt[i]表示第i个袋子里都有哪些糖果，例如cnt[1]=3(011),就表示袋子i中有2中糖果分别为：第一种和第二种
void solve(){
	int n,m,k;
    cin>>n>>m>>k;
    memset(dp,0x3f,sizeof (dp));
    for (int i=1;i<=n;i++){
        for (int j=1;j<=k;j++){
            int a;
            cin>>a;
            cnt[i]|=(1<<(a-1));
        }
    }
    dp[0]=0;//dp[j],j表示包含哪些糖果，dp[j]表示最少需要买多少袋
    for (int j=0;j<(1<<m);j++){
        if (dp[j]<n){//如果n个糖果袋都买过了，那就没必要在卖了。
            for (int i=1;i<=n;i++){
                dp[j|cnt[i]]=min (dp[j|cnt[i]],dp[j]+1);
                //如果第i个袋子的糖果我都已经有了，那么dp[j|cnt[i]]=dp[j|cnt[i]]，相当于没买，也就做到了一种袋子最多买一次，不会重复。
            }
        }
    }
    if (dp[(1<<m)-1]>n){
        cout<<-1;
    }else cout<<dp[(1<<m)-1]<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}