#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

void solve(){
	int n;
	cin>>n;
	vector<int> a(n+10);
	vector<int> f;
	for(int i=1;i<=n;i++) cin>>a[i];
	for(int i=1,j=1;;){
		// cout<<i<<endl;
		int sum=0;
		if(i+j-1>=n){
			for(int z=i;z<=n;z++) {sum+=a[z];}
			f.push_back(sum);
			break;
		}
		for(int z=0;z<j;z++) sum+=a[i+z];
		f.push_back(sum);
		i+=j;
		j<<=1;
	}
	int res[2]{-1,-1};
	// for(int i=0;i<f.size();i++) cout<<f[i]<<" ";
	for(int i=f.size()-1;i>=0;i--) if(res[0]<=f[i]) res[0]=f[i],res[1]=i+1;
	cout<<res[1]<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}