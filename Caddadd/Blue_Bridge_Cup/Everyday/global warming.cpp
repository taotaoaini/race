#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
int n;
int dis[4][2]{{0,1},{0,-1},{1,0},{-1,0}};
char a[1010][1010];
bool vis[1010][1010];
int res=0,cnt=0;
bool flag=false;
bool check(int x,int y){
	return x>=0&&x<n&&y>=0&&y<n;
}
void dfs(int i,int j){
	bool f1=true;
	if(!flag){
		for(auto &[x1,y1]:dis){
		int x=x1+i;
		int y=y1+j;
		if(a[x][y]=='.') f1=false;
		}
	}
	if(f1) flag=true;
	for(auto &[x1,y1]:dis){
		int x=x1+i;
		int y=y1+j;
		if(check(x,y)&&!vis[x][y]&&a[x][y]=='#') {vis[x][y]=true;dfs(x,y);}
	}
}
void solve(){
		cin>>n;
		for(int i=0;i<n;i++)
			for(int j=0;j<n;j++) cin>>a[i][j];
		for(int i=0;i<n;i++)
			for(int j=0;j<n;j++){
				flag=false;
				if(a[i][j]=='.'||vis[i][j]) continue;
				vis[i][j]=true;
				dfs(i,j);
				res+=1;
				if(flag) cnt+=1;
			}
		cout<<res-cnt<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    return 0;
}