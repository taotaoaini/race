#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef unsigned long long ull;
int n,m,a,b;
vector<vector<ll>> mx;
vector<vector<ll>> mi;
vector<vector<ll>> nums(1010,vector<ll>(1010));
const int mod=998244353;



vector<ll> fmin(vector<ll> a,int k,int len){
    deque<int> q;
    vector<ll> v;
    for(int i=0;i<k;i++){
        while(q.size()&&a[q.back()]>=a[i]) q.pop_back();
        q.push_back(i);
    }
    v.push_back(a[q.front()]);
    for(int i=k;i<len;i++){
        if(q.front()==i-k) q.pop_front();
        while(q.size()&&a[q.back()]>=a[i]) q.pop_back();
        q.push_back(i);
        v.push_back(a[q.front()]);
    }
    // for(auto x:v) cout<<x<<endl;
    return v;
}



vector<ll> fmax(vector<ll> a,int k,int len){
    deque<int> q;
    vector<ll> v;
    for(int i=0;i<k;i++){
        while(q.size()&&a[q.back()]<=a[i]) q.pop_back();
        q.push_back(i);
    }
    v.push_back(a[q.front()]);
    for(int i=k;i<len;i++){
        if(q.front()==i-k) q.pop_front();
        while(q.size()&&a[q.back()]<=a[i]) q.pop_back();
        q.push_back(i);
        v.push_back(a[q.front()]);
    }
    return v;
}


int main() {
    cin>>n>>m>>a>>b;
    for(int i=0;i<n;i++){
        for(int j=0;j<m;j++){
            cin>>nums[i][j];
        }
    }
    for(int i=0;i<n;i++){
       mi.push_back(fmin(nums[i],b,m));
        mx.push_back(fmax(nums[i],b,m));
    }
    // for(auto x:mx){
    //     for(auto y:x) {cout<<y<<" ";}
    //     cout<<endl;
    //     }
    // cout<<endl; 
    vector<vector<ll>> mi2(mi[0].size());
    vector<vector<ll>> mx2(mx[0].size());
    for(int i=0;i<n;i++){
        for(int j=0;j<mx[i].size();j++){
            mx2[j].push_back(mx[i][j]);
            mi2[j].push_back(mi[i][j]);
        }
    }
    //     for(auto x:mx2){
    // for(auto y:x) {cout<<y<<" ";}
    // cout<<endl;
    // }
    // cout<<mx2[0][0]<<endl; 
    mx.clear();
    mi.clear();
       for(int i=0;i<mx2.size();i++){
        mi.push_back(fmin(mi2[i],a,mi2[i].size()));
        mx.push_back(fmax(mx2[i],a,mx2[i].size()));
    }
    // for(auto x:mx){
    // for(auto y:x) {cout<<y<<" ";}
    // cout<<endl;
    // }
    ll res=0;
    for(int i=0;i<mx.size();i++){
        for(int j=0;j<mx[i].size();j++){
            // cout<<mx[i][j]<<"::::"<<mi[i][j]<<"::::"<<endl;
            res=(res+mx[i][j]%mod*mi[i][j]%mod)%mod;
        }
    }
    cout<<res<<endl;
    return 0;
}


