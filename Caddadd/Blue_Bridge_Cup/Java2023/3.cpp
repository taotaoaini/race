#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef unsigned long long ull;
int T;
int phi(int n){
    int res=n;
    for(int i=2;i*i<=n;i++){
        if(n%i==0){
            res=res/i*(i-1);
            while(n%i==0) n/=i;
        }
    }
    if(n>1) res=res/n*(n-1); 
}
const int N=1e5;
int phi[N];
void init(int n){
    for(int i=1;i<=n;i++) phi[i]=i;
    for(int i=2;i<=n;i++)
        if(phi[i]==i)
            for(int j=i;j<=n;j+=i){
                phi[j]=phi[j]/i*(i-1);
            }
}
int main() {
    cin>>T;
    while(T--){
        int x;
        cin>>x;
        if(x==1) cout<<"YES"<<endl;
        else cout<<"NO"<<endl;
    }
    return 0;
}