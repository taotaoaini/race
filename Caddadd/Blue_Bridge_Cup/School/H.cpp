#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second
#define mid ((l+r)>>1)

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

const int inf=0x3f3f3f3f;//1061109567,是10^9级别的
const ll dinf=0x7f7f7f7f;
//const int M=100010;
//const int N=100010;


ll f(ll x){
	ll res=1;
	while(x){
        ll t=x%10;
        if(t!=0) res*=t;
		x/=10;
	}
	return res;
}

void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout); 
    ll n;
    cin>>n;
    while(n>=10){
        n=f(n);
        cout<<n<<endl;
    }
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    //cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}