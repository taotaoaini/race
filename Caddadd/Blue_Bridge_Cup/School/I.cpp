#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second
#define mid ((l+r)>>1)

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

const int inf=0x3f3f3f3f;//1061109567,是10^9级别的
const ll dinf=0x7f7f7f7f;
//const int M=100010;
const int N=1010;

int a[N][N];
int n,m;
bool vis[N][N];
int cnt=0;
int gcd(int a,int b){
	return b==0?a:gcd(b,a%b);
}
int dis[4][2]{{-1,0},{1,0},{0,-1},{0,1}};
void dfs(int i,int j){
	vis[i][j]=1;
	cnt+=1;
	for(int k=0;k<4;k++){
		int x=i+dis[k][0];
		int y=j+dis[k][1];
		if(x>=1&&x<=n&&y>=1&&y<=m&&!vis[x][y]&&gcd(a[x][y],a[i][j])>1) dfs(x,y);
	}
}
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout); 
   	cin>>n>>m;
   	for(int i=1;i<=n;i++){
   		for(int j=1;j<=m;j++){
   			cin>>a[i][j];
   		}
   	}
   	int x0,y0;
   	cin>>x0>>y0;
   	dfs(x0,y0);
   	cout<<cnt<<endl;
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    //cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}