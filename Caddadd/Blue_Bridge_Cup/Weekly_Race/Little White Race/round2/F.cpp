/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;

string+string
to_string(int)
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls u<<1
#define rs u<<1|1
#define fi first
#define se second
#define mid ((l+r)/2)

using i8 = signed char;
using u8 = unsigned char;
using i16 = signed short int;
using u16 = unsigned short int;
using i32 = signed int;
using u32 = unsigned int;
using f32 = float;
using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using i128 = __int128_t;
using u128 = __uint128_t;
using f128 = long double;

using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<int128>();
*/

template<typename T>
void read(T &x) {
    x = 0; bool flag(0); char ch = getchar();
    while (!isdigit(ch)) flag = ch == '-', ch = getchar();
    while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
    flag ? x = -x : 0;
}

template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
    x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
    do stk[++top] = x % 10, x /= 10; while (x);
    while (top) putchar(stk[top--] | 48);
    mode ? putchar('\n') : putchar(' ');
}

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 998244353;
const int M = 100010;
const int N = 100010;
int sum[N << 2][3], add[N << 2][3][3], now[3][3];
void mul33(int a[3][3], int b[3][3]) {
    int c[3][3];
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            c[i][j] = 0;
            for (int k = 0; k < 3; k++) {
                c[i][j] = (c[i][j] + 1LL * a[i][k] * b[k][j]) % mod;
            }
        }
    }
    for (int i = 0; i < 3; i++) for (int j = 0; j < 3; j++) a[i][j] = c[i][j];
}
void mul13(int a[3], int b[3][3]) {
    int c[3];
    for (int i = 0; i < 3; i++) {
        c[i] = 0;
        for (int j = 0; j < 3; j++) {
            c[i] = (c[i] + 1LL * a[j] * b[j][i]) % mod;
        }
    }
    for (int i = 0; i < 3; i++) a[i] = c[i];
}
void build(int u, int l, int r) {
    for (int i = 0; i < 3; i++) for (int j = 0; j < 3; j++) add[u][i][j] = (i == j); //单位矩阵
    if (l == r) {
        int c;
        cin >> c;
        sum[u][c - 1] = 1;
        return;
    }
    build(ls, l, mid);
    build(rs, mid + 1, r);
    for (int i = 0; i < 3; i++) sum[u][i] = (sum[ls][i] + sum[rs][i]) % mod;
}
void change(int u, int l, int r, int ql, int qr) {
    if (ql <= l && qr >= r) {
        mul33(add[u], now);
        mul13(sum[u], now);
        return;
    }
    mul33(add[ls], add[u]);
    mul33(add[rs], add[u]);
    mul13(sum[ls], add[u]);
    mul13(sum[rs], add[u]);
    for (int i = 0; i < 3; i++) for (int j = 0; j < 3; j++) add[u][i][j] = (i == j);
    if (ql <= mid) change(ls, l, mid, ql, qr);
    if (qr > mid) change(rs, mid + 1, r, ql, qr);
    for (int i = 0; i < 3; i++) sum[u][i] = (sum[ls][i] + sum[rs][i]) % mod;
}

int n, m;
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    cin >> n >> m;
    build(1, 1, n);
    for (int i = 1, op, l, r, a, b; i <= m; i++) {
        memset(now, 0, sizeof(now));
        now[0][0] = now[1][1] = now[2][2] = 1;
        cin >> l >> r >> op;
        if (op == 1) {
            cin >> a >> b;
            a--;
            b--;
            now[a][a] = 0;
            now[b][b] = 0;
            now[a][b] = now[b][a] = 1;
        } else if (op == 2) {
            cin >> a >> b;
            a--;
            b--;
            now[a][a] = 0;
            now[a][b] = 1;
        } else {
            cin >> a;
            a -= 1;
            now[a][a] = 2;
        }
        change(1, 1, n, l, r);
        cout << sum[1][0] << " " << sum[1][1] << " " << sum[1][2] << endl;
    }
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
    }

    return 0;
}