/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
    x = 0; bool flag(0); char ch = getchar();
    while (!isdigit(ch)) flag = ch == '-', ch = getchar();
    while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
    flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
    //mode=1为换行，0为空格
    x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
    do stk[++top] = x % 10, x /= 10; while (x);
    while (top) putchar(stk[top--] | 48);
    mode ? putchar('\n') : putchar(' ');
}
const int inf = 0x3f3f3f3f;
const ll dinf = 0x7f7f7f7f;
const int N = 1010;
int a[N][N];
ll f[N][N][4];
int g[N][N][4];
int gcd(int a, int b) {
    return b == 0 ? a : gcd(b, a % b);
}
int n, m, q;
ll dfs(int x, int y, int k) {
    if (x == n && y == m) return a[n][m];
    ll res = -dinf;
    if (f[x][y][k] != -1) return f[x][y][k];
    if (x + 1 <= n) {
        if (gcd(a[x][y], a[x + 1][y]) == 1) {
            if (k + 1 <= q) res = max(res, dfs(x + 1, y, k + 1) + a[x][y]);
        } else res = max(res, dfs(x + 1, y, k) + a[x][y]);

    }
    if (y + 1 <= m) {
        if (gcd(a[x][y], a[x ][y + 1]) == 1) {
            if (k + 1 <= q) res = max(res, dfs(x , y + 1, k + 1) + a[x][y]);
        } else res = max(res, dfs(x , y + 1, k) + a[x][y]);

    }
    return f[x][y][k] = res;
}
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    cin >> n >> m >> q;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) cin >> a[i][j];
    }
    // for (int i = 1; i <= n; i++) {
    //     for (int j = 1; j <= m; j++) {
    //         int x = 0;
    //         for (int k = 0; k <= q; k++) g[i][j][k] = 1;
    //         if (gcd(a[i][j], a[i - 1][j]) == 1) x = 1;
    //         for (int k = x; k <= q; k++) {
    //             f[i][j][k] = max(f[i][j][k], f[i - 1][j][k - x] + a[i][j]);
    //         }
    //         x = 0;
    //         if (gcd(a[i][j], a[i][j - 1]) == 1) x = 1;
    //         for (int k = x; k <= q; k++) {
    //             if (f[i][j - 1][k - x] == 0) continue;
    //             f[i][j][k] = max(f[i][j][k], f[i][j - 1][k - x] + a[i][j]);
    //         }
    //     }
    // }
    // ll res = 0;
    // for (int i = 0; i <= q; i++) {
    //     cout << g[n][m][i] << endl;

    //     if (g[n][m][i] == n + m - 2)res = max(res, f[n][m][i]);
    // }
    // if (res == 0) cout << -1 << endl;
    // else cout << res << endl;
    memset(f,-1,sizeof(f));
    ll res=dfs(1,1,0);
    if(res>=0) cout<<res<<endl;
    else cout<<-1<<endl;
    // cout<<dfs(1,1,0)<<endl;
    // cout << res << endl;
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    //cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}