#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls p<<1
#define rs p<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;

int n, m, k;

void solve() {
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	cin >> n >> m >> k;
	vector<vector<int>> a(n + 10, vector<int>(m + 10));
	for (int i = 0; i < n; i++) {
		string s;
		cin >> s;
		for (int j = 0; j < m; j++) a[i][j] = s[j] - '0';
	}
	int dx[4] {0, 0, 1, -1}, dy[4] {1, -1, 0, 0};
	i64 x = 0, y = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			if (a[i][j] == 1) {
				for (int kk = 0; kk < 4; kk++) {
					int xi = i + dx[kk];
					int yj = j + dy[kk];
					if (xi >= 0 && yj >= 0 && xi < n && yj < m && a[xi][yj] == 1) {
						a[xi][yj] = 0;
						a[i][j] = 0;
						x += 1;
						break;
					}
				}
			}
		}
	}
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			if (a[i][j] == 1) {
				x -= 1;
				y += 1;
			}
		}
	}
	// for(int i=0;i<n;i++){
	// 	for(int j=0;j<m;j++) cout<<a[i][j]<<" ";
	// 	cout<<endl;
	// }
	cout<<y<<"::"<<x<<endl;
	while (x + y > k) {
		x -= 3;
		y += 2;
	}
	cout << y << " " << x << endl;
	cout<<x+y<<endl;
}


// void solve() {
// 	cin >> n >> m >> k;
// 	vector<vector<int>> a(n + 10, vector<int>(m + 10));
// 	for (int i = 0; i < n; i++) {
// 		string s;
// 		cin >> s;
// 		for (int j = 0; j < m; j++) a[i][j] = s[j] - '0';
// 	}
// 	int cnt=0;
// 	for(int i=0;i<n;i++)
// 		for(int j=0;j<m;j++) if(a[i][j]==1) cnt+=1;
// 	int x=3*k-cnt;
// 	int y=cnt-2*k;
// 	cout<<y<<" "<<x<<endl;
// }

int main() {
	ios::sync_with_stdio(false); cin.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	//cin>>t;

	while (t--) {
		solve();
		//init();
	}

	return 0;
}