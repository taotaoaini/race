#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls p<<1
#define rs p<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;
// 1-9 9 m=1
// 10-99 90  m=2
// 100-999 900  m=3
// (x-1)*m*10

i64 find(i64 n, i64 m) {
    i64 digit = 1;
    i64 cnt = m - 1;  //当前位数的数量
    i64 start = 1;
    while (n >= digit * cnt) { //digit位数*cnt=数量
        n -= digit * cnt;
        digit += 1;
        cnt *= m;
        start *= m;
        // cout << n << "::" << digit*cnt << endl;
    }
    // 查看在第几组
    // n=5  10 11 12
    i64 v = (n - 1) / digit; //n-1为偏移
    // int v=(n-digit+digit-1) / digit;
    start += v; //表示在第几个数,但代表的是十进制的
    n -= v * digit;
    // cout << "::" << v << endl;
    for (i64 i = 0; i < digit - n; i++) { //取第n位
        start /= m;
    }
    // cout << start % m << endl;
    return start % m ;
}

void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    i64 n, m;
    cin >> n >> m;
    cout << find(n, m) << endl;
    // cout << find(17, 10) << endl;
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    cin >> t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}