#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls p<<1
#define rs p<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;

i64 qmi(i64 a, i64 b) {
	i64 res = 1;
	while (b) {
		if (b & 1) res = res * a;
		b /= 2;
		a *= a;
	}
	return res;
}
i64 find(i64 n, i64 m) {
	i64 len = 1;
	while (len * (m - 1) * qmi(m, len - 1) < n) { //可以看作成循环结
		n -= len * (m - 1) * qmi(m, len - 1);
		len++;
	}
	i64 s = qmi(m, len - 1); // 开始位置
	// (x-s+1)*len>=n
	i64 x = n / len - 1 + s; // 公式推导，算出最小合法位置
	n -= (x - s + 1) * len;
	return n == 0 ? x % m : (x + 1) / qmi(m, len - n) % m;
}
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	i64 n, m;
	cin >> n >> m;
	cout << find(n, m) << endl;
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	cin >> t;

	while (t--) {
		solve();
		//init();
	}

	return 0;
}