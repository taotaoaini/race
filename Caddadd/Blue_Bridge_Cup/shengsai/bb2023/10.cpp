#include<bits/stdc++.h>
using namespace std;
#define fi first
#define se second
using pii=pair<int,int>;
const int inf=0x3f3f3f3f;
int n,k,m;
vector<vector<pii>> e;
vector<array<int,16>> f;
void dijkstra(int st,int en){
	for(int i=0;i<n;i++){
		for(int j=0;j<=k;j++){
			f[i][j]=inf;
		}
	}
	queue<int> q;
	q.push(st);
	f[st][0]=0;
	while(!q.empty()){
		int u=q.front();
		q.pop();
		for(int i=0;i<e[u].size();i++){
			bool vis=false;
			int vw=e[u][i].fi;
			int v=e[u][i].se;
			if(f[u][0]+vw<f[v][0]){
				vis=true;
				f[v][0]=f[u][0]+vw;
			}
			for(int j=1;j<=k;j++){
				if(f[u][j-1]<f[v][j]){
					vis=true;
					f[v][j]=f[u][j-1];
				}
			}
			if(f[u][k]+vw<f[v][k]){
				vis=true;
				f[v][k]=f[u][k]+vw;
			}
			if(vis) q.push(v);
		}
	}	
}
void solve(){
	cin>>n>>k>>m;
	f.resize(n+1);
	e.resize(n+1);
	for(int i=0,x,y,w;i<m;i++){
		cin>>x>>y>>w;
		e[x].emplace_back(w,y);
		e[y].emplace_back(w,x);
	}
	int st=0,en=n-1; 
	dijkstra(st,en);
	cout<<f[n-1][k]<<endl;
}
int main() {
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	int t=1;
	while(t--) {
		solve();
	}
}
