#include<bits/stdc++.h>
#include<iostream>
#include<cstring>
#include<vector>
#include<queue>
#include<bitset>
using namespace std;
using i64 = long long;
using f64 = long double;
void solve() {
	int n,m,x;
	cin>>n>>m>>x;
	vector<int> a(n+1);
	vector<int> L(n+1);
	for(int i=1; i<=n; i++) cin>>a[i];
	map<int,int> mp;
	for(int i=1; i<=n; i++) {
		if(mp.count(a[i]^x)>=1) L[i]= mp[a[i]^x];
		mp[a[i]]=i;
	}
	for(int i=1;i<=n;i++) L[i]=max(L[i],L[i-1]);
	for(int i=1; i<=m; i++) {
		int l,r;
		cin>>l>>r;
		if(L[r]>=l) cout<<"yes"<<endl;
		else cout<<"no"<<endl; 
	}
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout << fixed << setprecision(12);
	int t=1;
	// cin>>t;
	while(t--) {
		solve();
	}
}

