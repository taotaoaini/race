#include<bits/stdc++.h>
using namespace std;
#define fi first
#define se second
int n,m;
void solve() {
	int n,m;
	cin>>n>>m;
//	vector<pii> a(m+10);
	vector<int> b(n+1,0);
	for(int i=1; i<=n; i++) b[i]=i;
	for(int i=1,p,q; i<=m; i++) {
		cin>>p>>q;
		if(p==0)
			sort(b.begin()+1,b.begin()+q+1,[&](int x,int y) {
			return x>y;
		});
		else sort(b.begin()+q,b.end());
	}
	for(int i=1;i<=n;i++) cout<<b[i]<<" ";
	cout<<endl;
}
int main() {
	int t=1;
	while(t--) {
		solve();
	}
	return 0;
}
