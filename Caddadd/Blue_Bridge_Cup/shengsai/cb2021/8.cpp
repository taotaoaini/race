#include<bits/stdc++.h>
using namespace std;
using i64=long long;
using pll =pair<long long,long long>;
#define fi first
#define se second
// (k*(x+1)+b)*(x+1)-(k*x+b)*x=2*k*x+k+b
priority_queue<array<i64,3>,vector<array<i64,3>>,less<array<i64,3>>> q;
void solve(){
	int n,m;
	cin>>n>>m;
	i64 k,b;
	vector<pll> a(m);
	for(int i=0;i<m;i++){
		cin>>k>>b;
		a[i].fi=k;
		a[i].se=b;
		i64 y=b+k;
		if(y<=0) continue;
		q.push({y,1,i});
	}
	i64 res=0;
	for(int i=0;i<n;i++){
		if(q.empty()) break;
		auto p=q.top();
		q.pop();
		i64 y=p[0];
		i64 x=p[1];
		i64 idx=p[2];
		res+=y;
		y=2*a[idx].fi*x+a[idx].fi+a[idx].se;
		if(y<=0) continue;
		q.push({y,x+1,idx});
	}
	cout<<res<<endl;
}
int main(){
	int t=1;
	while(t--){
		solve();
	}
	return 0;
}
