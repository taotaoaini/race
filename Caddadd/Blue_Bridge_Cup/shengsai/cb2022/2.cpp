#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
using f64 = long double;
int mm[13] {0,31,28,31,30,31,30,31,31,30,31,30,31};
string get(int x) {
	if(x<10) return "0"+to_string(x);
	else return to_string(x);
}
string tt[8] {"012","123","234","345","456","567","678","789"};
void solve() {
	i64 res=0;
	for(int i=1; i<=12; i++) {
		for(int j=1; j<=mm[i]; j++) {
			string s="2022"+get(i)+get(j);
			for(int k=0; k<8; k++) {
				if(s.find(tt[k])!=-1) {
//					cout<<s<<endl;
					res++;
					break;
				}
			}
//				cout<<s<<endl;
		}
	}
//	cout<<get(1)<<endl;
//	cout<<res<<endl;
	cout<<14<<endl;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout << fixed << setprecision(12);
	int t=1;
	// cin>>t;
	while(t--) {
		solve();
	}
}

