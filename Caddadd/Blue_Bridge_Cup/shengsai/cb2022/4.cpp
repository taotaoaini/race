#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
using f64 = long double;
void solve(){
		int n;
		cin>>n;
		vector<int> a(n+1);
		for(int i=1;i<=n;i++){
			a[i]=max({1,(i-1)*2,(n-i)*2});
		}
		for(int i=1;i<=n;i++){
			cout<<a[i]<<endl;
		}
}
int main(){
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
	int t=1;
	// cin>>t;
	while(t--){
	   solve();
	}
}

