#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
using f64 = long double;
const i64 mod=1000000007;
void solve() {
	// 321 -> 3*10*2+2*2+1
	int n;
	cin>>n;
	int na;
	cin>>na;
	vector<int> a(na+2);
	for(int i=1; i<=na; i++) cin>>a[i];
	int nb;
	cin>>nb;
	vector<int> b(nb+2);
	for(int i=1; i<=nb; i++) cin>>b[i];
	// A>=B
//	int i=1;
	i64 s=0,t=0;
	vector<int> ans(na+2);
	for(int i=1; i<=nb; i++) {
		if(t*n+b[i]>s*n+a[i]) {
			ans[i]=n;
		}else ans[i]=max({a[i]+1,b[i]+1,2});
		s+=a[i];
		t+=b[i];
	}
//	for(int i=1;i<=na;i++){
//		cout<<ans[i]<<" ";
//	}
	i64 res=0;
	i64 sa=0,sb=0;
	i64 x=1;
	for(int i=na;i>=1;i--){
		cout<<"LL"<<ans[i]<<endl;
		 sa=(sa+a[i]*x)%mod;
		 sb=(sb+b[i]*x)%mod;
		 x*=ans[i];
//		cout<<sa<<" "<<sb<<endl;
	}
	cout<<(sa-sb+mod)%mod<<endl;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout << fixed << setprecision(12);
	int t=1;
	// cin>>t;
	while(t--) {
		solve();
	}
}

