#include<bits/stdc++.h>
using namespace std;
using i64=long long;
void solve(){
	i64 a,b,n;
	cin>>a>>b>>n;
	i64 x=n/(a*5+b*2)*7;
	i64 y=n%(a*5+b*2);
	for(int i=1;i<=5&&y>0;i++){
		y-=a;
		x+=1;
		if(y<=0) break;
	}
	for(int i=1;i<=2&&y>0;i++){
		y-=b;
		x+=1;
		if(y<=0) break;
	}
	cout<<x<<endl;
}
int main(){
	int t=1;
	while(t--){
		solve();
	}
	return 0;
}
