#include<bits/stdc++.h>
using namespace std;
void solve(){
	int n;
	cin>>n;
	vector<int> ans(n+1);
	for(int i=1;i<=n;i++){
		ans[i]=max(i-1,(n-i))*2;
	}
	for(int i=1;i<=n;i++){
		cout<<ans[i]<<endl;
	}
	cout<<endl;
}
int main(){
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	int t=1;
	while(t--){
		solve();
	}
	return 0;
}
