#include<bits/stdc++.h>
using namespace std;
const int mod=1000000007;
using i64=long long;
void solve() {
	// A.size()==B.size()
	int n;
	cin>>n;
	int ma;
	cin>>ma;
	vector<i64> a(ma+10,0);
	for(int i=1; i<=ma; i++) cin>>a[i];
	int mb;
	cin>>mb;
	vector<i64> b(mb+10,0);
	for(int i=1; i<=mb; i++) cin>>b[i];
	i64 x=1;
	i64 res=a[ma]-b[ma];
	for(int i=ma-1; i>=1; i--) {
			x=x*(max(2LL,max(a[i+1],b[i+1])+1))%mod;
			res=(res+a[i]*x-b[i]*x)%mod;
		}
	cout<<(res+mod)%mod<<endl;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);
	int t=1;
	while(t--) {
		solve();
	}
	return 0;
}
