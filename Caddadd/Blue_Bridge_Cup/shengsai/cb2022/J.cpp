#include<bits/stdc++.h>
using namespace std;
const int mod=1000000007;
using i64=long long;
const int N=600;
i64 a[N][N];
int n,m;
i64 k;

void solve() {
	cin>>n>>m>>k;
	for(int i=1; i<=n; i++) {
		for(int j=1; j<=m; j++) {
			cin>>a[i][j];
		}
	}
	for(int i=1; i<=m; i++) {
		for(int j=2; j<=n; j++) {
			a[j][i]+=a[j-1][i];
		}
	}
	i64 res=0;
	for(int i=0; i<=n-1; i++) {
		for(int j=i+1; j<=n; j++) {
			int l=1,r=1;
			i64 sum=0;
			while(r<=m) {
				sum+=a[j][r]-a[i][r];
				while(sum>k){
					sum-=a[j][l]-a[i][l];
					l+=1;
				}
//				cout<<l<<" "<<r<<endl;
				res+=r-l+1;
				r++;
			}
		}
	}
	cout<<res<<endl;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);
	int t=1;
	while(t--) {
		solve();
	}
	return 0;
}
