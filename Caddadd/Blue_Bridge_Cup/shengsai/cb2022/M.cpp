#include<bits/stdc++.h>
using namespace std;
using i64=long long;
using pll=pair<long long,long long>;
#define fi first
#define se second
const int N=200010;
int mod=1000000007;
// 砍竹子
// [l,r] hi
// 连续的一段相同高度，
// 每次就是判断周围是否有相同的，相同的话就忽略了

priority_queue<pll,vector<pll>,less<pll>>  q;
int R[N];
void solve() {
	int n;
	cin>>n;
	i64 x;
	vector<i64> a(n+10,0);
	for(int i=1;i<=n;i++){
		cin>>a[i];
		q.push({a[i],i});
	}
	i64 res=0;
	while(q.size()){
		auto p=q.top();
		q.pop();
		i64 pos=p.se;
		i64 x=p.fi;
//		x=sqrt(x/2+1);
//		if(pos!=1&&a[pos-1]==x){
//			R[pos-1]=R[pos];
//			continue;
//		}
//		a[R[pos]]=x;
//		res+=1;
//		if(x>1)
//		q.push({x,pos});
	cout<<x<<" "<<pos<<endl;
	}
	cout<<res<<endl;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);
	int t=1;
	while(t--) {
		solve();
	}
	return 0;
}
