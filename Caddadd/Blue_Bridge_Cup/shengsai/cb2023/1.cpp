#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
char a[110];
int n=100;
int d[13] {0,31,28,31,30,31,30,31,31,30,31,30,31};
string t="2023";
set<string> st;
bool check(string s) {
		int x=stoi(s.substr(4,2));
		int y=stoi(s.substr(6,2));
		return x>=1&&x<=12&&y<=d[x]&&y>=1;
}
void dfs(int x,string s) {
	if(s.length()>8) return ;
	if(x>100) {
		if(s.length()==8&&check(s)){
			 st.insert(s);
			 return;
		}
		else return ;
	}
	if(s.length()==4&&s!=t) return;
	dfs(x+1,s);
	dfs(x+1,s+a[x]);
}
void dfs(int x,string s,int k) {
	if(k>8) return ;
	if(x>100) {
		if(k==8&&check(s)){
			 st.insert(s);
		}
		return;
	}
	dfs(x+1,s);
	if(k>=4||a[x]==t[k]){
		dfs(x+1,s+a[x],k+1);	
	}
}
void solve() {
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	for(int i=1; i<=100; i++) cin>>a[i];
	dfs(1,"",0);
	cout<<st.size()<<endl;
//cout<<235<<endl;
}
int main() {
	solve();
	return 0;
}
