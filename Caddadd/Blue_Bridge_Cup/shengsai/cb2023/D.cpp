//#include<bits/stdc++.h>
//using namespace std;
//using i64=long long;
//const int N=20;
//int T[N],D[N],L[N];
//int n;
//bool vis[N]; //vis=flag
//bool dfs(int k,int t) {
//	if(k==n) return true;
//	bool res=false;
//	for(int i=1; i<=n; i++) {
//		if(!vis[i]){
//			if(t>T[i]+D[i]){
//				return false;
//			}
//			vis[i]=true;
//			res=res||dfs(k+1,max(T[i],t)+L[i]);
//			vis[i]=false;
//		}
//	}
//	return res;
//}
//void init(){
//	for(int i=1;i<=n;i++) vis[i]=false,T[i]=0,D[i]=0,L[i]=0;
//}
//void solve() {
//	cin>>n;
//	for(int i=1;i<=n;i++) cin>>T[i]>>D[i]>>L[i];
//	if(dfs(0,0)){
//		cout<<"YES"<<endl;
//	}else{
//		cout<<"NO"<<endl;
//	}
//	init();
//}
//int main() {
//	ios::sync_with_stdio(false);
//	int t=1;
//	cin>>t;
//	while(t--) {
//		solve();
//	}
//	return  0;
//}



#include<bits/stdc++.h>
using namespace std;
const int N=12;
int T[N],D[N],L[N];
bool vis[N];
bool res=false;
int n;
void dfs(int cnt,int last) {
	if(cnt==n) {
		res=true;
		return;
	}
	if(res) return;
	for(int i=0; i<n; i++) {
		if(!vis[i]&&T[i]+D[i]>=last){
		vis[i]=true;
		// 不能写last=max(last,T[i])+L[i]; 因为回溯到这个位置的时候，last已经改变了,不能改变Last的值
		//需要注意回溯问题，之前改变的值，再回到这个位置的时候，值是不能发生改变的 
		int t=max(last,T[i])+L[i]; // (第一种写法)
//		dfs(cnt+1,max(last,T[i])+L[i]); (第二种写法)
		dfs(cnt+1,t);
		vis[i]=false;
	}
}
}
void solve() {
	cin>>n;
	for(int i=0; i<n; i++) {
		cin>>T[i]>>D[i]>>L[i];
	}
	dfs(0,0);
	if(res) {
		cout<<"YES"<<endl;
	} else {
		cout<<"NO"<<endl;
	}
	res=false;
//	init();
}
int main() {
	int t=1;
	cin>>t;
	while(t--) {
		solve();
	}
}
