#include<bits/stdc++.h>
using namespace std;
void solve(){
	int n;
	cin>>n;
	vector<string> a(n+10);
	vector<int> f(10,0);
	for(int i=1;i<=n;i++){
		cin>>a[i];
	}
	int res=0;
	for(int i=1;i<=n;i++){
		int x=a[i][0]-'0';
		int y=a[i][a[i].length()-1]-'0';
		f[y]=max(f[x]+1,f[y]);
		res=max(res,f[y]);
	}
	cout<<n-res<<endl;
	
}
int main(){
	int t=1;
	while(t--){
		solve();
	}
	return 0;
}
