#include<bits/stdc++.h>
using namespace std;
const int N=60;
int c[N][N];
int e[N][N];
bool vis[N][N];
int n,m;
int dx[] {-1,1,0,0},dy[] {0,0,-1,1};
int dxx[8] {-1,1,0,0,-1,1,-1,1},dyy[] {0,0,-1,1,-1,1,1,-1};
int cnt=0;
void dfs(int i,int j) {
	for(int k=0; k<4; k++) {
		int x=i+dx[k];
		int y=j+dy[k];
		if(x<0||x>=n||y<0||y>=m) continue;
		if(c[x][y]==0&&e[x][y]==1) {
			c[x][y]=cnt;
			dfs(x,y);
		}
	}
}
set<int> d;
void dfs1(int i,int j) {
	for(int k=0; k<8; k++) {
		int x=i+dxx[k];
		int y=j+dyy[k];
		if(x<0||x>=n||y<0||y>=m) continue;
		if(vis[x][y]==1) continue;
		if(c[x][y]!=0) d.insert(c[x][y]);
		else {
			vis[x][y]=1;
			dfs1(x,y);
		}
	}
}
void init() {
	d.clear();
	cnt=0;
	for(int i=0; i<n; i++) {
		for(int j=0; j<m; j++) {
			vis[i][j]=0;
			c[i][j]=0;
		}
	}
}
void solve() {
	cin>>n>>m;
	string s;
	for(int i=1; i<=n; i++) {
		cin>>s;
		for(int j=1; j<=m; j++) {
			e[i][j]=s[j-1]-'0';
		}
	}
	n=n+2;
	m=m+2;
	for(int i=0; i<n; i++) {
		for(int j=0; j<m; j++) {
			if(c[i][j]!=0||e[i][j]==0) continue;
			++cnt;
			c[i][j]=cnt;
			dfs(i,j);
		}
	}
	for(int i=0; i<n; i++) {
		for(int j=0; j<m; j++) {
			if(i==0||j==0||i==n-1||j==m-1) {
				dfs1(i,j);
			}
		}
	}
	cout<<d.size()<<endl;
	init();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);
	int t=1;
	cin>>t;
	while(t--) {
		solve();
	}
	return 0;
}
