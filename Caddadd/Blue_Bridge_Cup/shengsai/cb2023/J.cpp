#include<bits/stdc++.h>
using namespace std;
using i64=long long;
void solve(){
	int n;
	string s;
	char c,d;
	cin>>n>>s>>c>>d;
	vector<int> top,last;
	for(int i=0;i<s.length();i++){
		if(s[i]==c) top.push_back(i);
		if(s[i]==d) last.push_back(i);
	}
	i64 res=0;
	for(auto& x:top){
		int idx=lower_bound(last.begin(),last.end(),max(2,x+n-1))-last.begin();
		cout<<x<<" "<<idx<<" "<<(int)last.size()-idx+1<<endl;
		res+=max(0,(int)last.size()-idx);
	}
	cout<<res<<endl;
}
int main(){
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	int t=1;
	while(t--){
		solve();
	}
	return 0;
}
