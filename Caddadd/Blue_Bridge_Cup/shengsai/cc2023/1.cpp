#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
i64 get(i64 x){
	i64 res=0;
	while(x){
		res+=x%10;
		x/=10;
	}
	return res;
}
void solve(){
	i64 res=0;
	for(i64 i=1;i<=20230408;i++){
		res+=get(i);
	}
//	cout<<res<<endl;
	cout<<645697135<<endl;
//	cout<<get(1234)<<endl;
}
int main(){
	solve();
	return 0;
}
