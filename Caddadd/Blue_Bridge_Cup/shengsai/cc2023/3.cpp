#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
void solve() {
	int n;
	cin>>n;
	vector<array<i64,3>> a(n);
	for(int i=0; i<n; i++) cin>>a[i][0];
	for(int i=0; i<n; i++) cin>>a[i][1];
	for(int i=0; i<n; i++) cin>>a[i][2];
	auto get=[&](const int i,const int j,const int k)->int {
		sort(a.begin(),a.end(),[&](array<i64,3> x,array<i64,3> y) {
			return  x[i]-(x[j]+x[k])>y[i]-(y[j]+y[k]);
		});
		i64 s=0;
		int t=0;
		for(auto& x:a) {
			if(s+x[i]>x[j]+x[k]) {
				t++;
				s+=x[i]-x[j]-x[k];
			} else  break;
		}
		return t;
	};
	int res=-1;
	res=max(res,max({get(0,1,2),get(1,0,2),get(2,0,1)}));
	if(res==0) cout<<-1<<endl;
	else  cout<<res<<endl;
}
int main() {
	solve();
	return 0;
}

