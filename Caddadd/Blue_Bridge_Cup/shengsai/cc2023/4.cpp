#include<bits/stdc++.h>
using namespace std;
void solve() {
	string s;
	cin>>s;
	int res=0;
	int n=s.length();
	vector<int> vis(n+1);
	for(int i=1;i<n;i++){
		if(vis[i]) continue;
		if(!vis[i-1]&&(s[i]==s[i-1]||s[i]=='?')){
			res++;
			vis[i]=1;
			vis[i-1]=1;
		}else if(i+1<n&&!vis[i+1]&&(s[i]==s[i+1]||s[i]=='?')){
			res++;
			vis[i]=1;
			vis[i+1]=1;
		}
	}
	cout<<res<<endl;
}
int main() {
	int t=1;
//	cin>>t;
	while(t--) {
		solve();
	}
	return 0;
}
