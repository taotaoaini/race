#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
using f64 = long double;
const i64 mod=998244353;
i64 qmi(i64 a,i64 b){
	i64 res=1;
	while(b){
		if(b&1) res=res*a%mod;
		b/=2;
		a=a*a%mod;
	}
	return res;
}
vector<int> get(i64 x) {
	vector<int> ans;
	for(int i=2; i<=x/i; i++) {
		if(x%i==0) {
			ans.push_back(i);
			while(x%i==0) {
				x/=i;
			}
		}
	}
	if(x>1) ans.push_back(x);
	return ans;
}
void solve() {
	i64 a,b;
	cin>>a>>b;
	vector<int> v=get(a);
	for(auto x:v) cout<<x<<" ";
//	i64 sum=qmi(a,b);
//	i64 res=sum;
//	for(auto x:v) {
//		res=(res-sum*qmi(x,mod-2)%mod+mod)%mod;
//	}
//	cout<<res<<endl;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout << fixed << setprecision(12);
	int t=1;
	// cin>>t;
	while(t--) {
		solve();
	}
}

