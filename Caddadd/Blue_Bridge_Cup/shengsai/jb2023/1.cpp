#include<bits/stdc++.h>
using namespace std;
int f0(int x){
	int res=0;
	while(x){
		res+=x%2;
		x/=2;
	}
	return res;
}
int f1(int x) {
	int res=0;
	while(x) {
		res+=x%10;
		x/=10;
	}
	return res;
}
int f2(int x) {
	int res=0;
	while(x) {
		res+=x%8;
		x/=8;
	}
	return res;
}
int f3(int x) {
	int res=0;
	while(x) {
		res+=x%16;
		x/=16;
	}
	return res;
}
void solve() {
//	cout<<f2(126)<<endl;
//	cout<<f3(126)<<endl;
//	if(126%f1(126)==0&&126%f2(126)==0&&126%f3(126)==0) cout<<1<<endl;
//	for(int i=1,cnt=0;; i++) {
//		if(i%f0(i)==0&&i%f1(i)==0&&i%f2(i)==0&&i%f3(i)==0) {
//			if(++cnt==2023) {
//				cout<<i<<endl;
//				break;
//			}
////			cout<<i<<endl;
//		}
//	}
	cout<<215040<<endl;
}
int main() {
	solve();
	return 0;
}
