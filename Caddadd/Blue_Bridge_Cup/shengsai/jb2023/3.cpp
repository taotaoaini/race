#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
using f64 = long double;
const int mod=1000000007;
i64 qmi(i64 a,i64 b) {
	if(b<=0) return 1;
	i64 res=1;
	while(b) {
		if(b&1) res=res*a%mod;
		a=a*a%mod;
		b/=2;
	}
	return res;
}
void solve() {
	int n;
	cin>>n;
	i64 x=0,y=0;
	for(int i=1;i<=n;i++){
		int a;
		cin>>a;
		if(a&1) x++;
		else y++;
	}
	if(x&1) {
		cout<<0<<endl;
		return;
	}
	cout<<qmi(2LL,y)*qmi(2LL,x-1)<<endl;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout << fixed << setprecision(12);
	int t=1;
	 cin>>t;
	while(t--) {
		solve();
	}
}

