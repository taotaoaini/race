#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
using f64 = long double;
i64 a[4][2];
void solve(){
	for(int i=0;i<4;i++){
		cin>>a[i][0];
		cin>>a[i][1];
	}		
	i64 a1=min(a[1][1],a[3][1]);
	i64 a2=max(a[0][1],a[2][1]);
	i64 b1=min(a[1][0],a[3][0]);
	i64 b2=max(a[0][0],a[2][0]);
//	cout<<a1<<":"<<a2<<":"<<b1<<"::"<<b2<<endl;
	i64 s1=(a[1][0]-a[0][0])*(a[1][1]-a[0][1]);
	i64 s2=(a[3][0]-a[2][0])*(a[3][1]-a[2][1]);
//	cout<<s1<<endl;
//	cout<<s2<<endl;
	cout<<s1+s2-max(0LL,a1-a2)*max(0LL,b1-b2)<<endl;
}
int main(){
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
	int t=1;
	// cin>>t;
	while(t--){
	   solve();
	}
}

