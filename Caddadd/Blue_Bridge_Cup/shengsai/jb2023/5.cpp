#include<bits/stdc++.h>
using namespace std;
#define fi first
#define se second
using pii = pair<int,int>;
using i64 = long long;
using f64 = long double;
const int N=60;
int a[N][N];
int b[N][N];
bool visa[N][N];
bool visb[N][N];
int n;
int dx[4] {0,0,-1,1},dy[4] {-1,1,0,0};
int ans[2];
int bfs(int t0,int t1) {
	queue<pii> q;
	visa[t0][t1]=1;
	q.push({t0,t1});
	int cnt=0;
	while(!q.empty()) {
		auto p=q.front();
		q.pop();
		cnt++;
		int x=p.fi,y=p.se;
		for(int k=0; k<4; k++) {
			int xk=dx[k]+x;
			int yk=dy[k]+y;
			if(!visa[xk][yk]&&a[xk][yk]==1) {
				visa[xk][yk]=1;
				q.push({xk,yk});
			}
		}
	}
	return cnt;
}
int bfs2(int t0,int t1) {
	queue<pii> q;
	visb[t0][t1]=1;
	q.push({t0,t1});
	int cnt=0;
	while(!q.empty()) {
		auto p=q.front();
		q.pop();
		cnt++;
		int x=p.fi,y=p.se;
		for(int k=0; k<4; k++) {
			int xk=dx[k]+x;
			int yk=dy[k]+y;
			if(!visb[xk][yk]&&b[xk][yk]==1) {
				visb[xk][yk]=1;
				q.push({xk,yk});
			}
		}
	}
	return cnt;
}
void solve() {
	cin>>n;
	for(int i=1; i<=n; i++) {
		for(int j=1; j<=n; j++) {
			cin>>a[i][j];
		}
	}
	for(int i=1; i<=n; i++) {
		for(int j=1; j<=n; j++) {
			cin>>b[i][j];
		}
	}
	for(int i=1; i<=n; i++) {
		for(int j=1; j<=n; j++) {
			if(i==1||j==1||i==n||j==n) {
				if(!visa[i][j]&&a[i][j]==1) {
					ans[0]=max(ans[0],bfs(i,j));
				}
			}
		}
	}
//	cout<<ans[0]<<endl;
	for(int i=1; i<=n; i++) {
		for(int j=1; j<=n; j++) {
			if(i==1||j==1||i==n||j==n) {
				if(!visb[i][j]&&b[i][j]==1) {
					ans[1]=max(ans[1],bfs2(i,j));
				}
			}
		}
	}
	cout<<ans[0]+ans[1]<<endl;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout << fixed << setprecision(12);
	int t=1;
	// cin>>t;
	while(t--) {
		solve();
	}
}

