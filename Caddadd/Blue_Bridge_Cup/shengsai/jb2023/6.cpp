#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
using f64 = long double;
const int N=5e5+10;
int n;
int a[N];
bool vis[N];
void solve() {
	cin>>n;
	i64 sum=0;
	for(int i=1; i<=n; i++) cin>>a[i],sum+=a[i];
	sort(a+1,a+n+1);
	int x=n-1,y=n;
	for(int i=n-2; i>=1; i--) {
		while(y>=1&&vis[y]) y--;
		while(x>=1&&(vis[x]||x==y)) x--;
		if(x>=y) break;
		if(a[i]<=a[x]/2){
				sum-=a[i];
				vis[x]=true;
				vis[y]=true;
				vis[i]=true;
		}
	}
	cout<<sum<<endl;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout << fixed << setprecision(12);
	int t=1;
	// cin>>t;
	while(t--) {
		solve();
	}
}

