#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
using f64 = long double;
const int N=100010;
f64 a[N];
f64 b[N];
f64 c[N];
f64 f[N][2]; // 0: (a[i],0) 1: (x,c[i-1])
int n;
void solve() {
	cin>>n;
	for(int i=1; i<=n; i++) cin>>a[i];
	for(int i=1; i<=n-1; i++) cin>>b[i]>>c[i];
	f[1][0]=a[1]; // Ԥ��1��λ��Ϊ(a[1],0)
	f[1][1]=a[1];
	for(int i=1; i<=n-1; i++) {
		f64 x=c[i-1]<b[i]?(b[i]-c[i-1])/0.7:(c[i-1]-b[i])/1.3;
		f64 y=b[i]>0?b[i]/0.7:b[i]/1.3;
		f[i+1][1]=min(f[i][1]+x,f[i][0]+y);
		f64 z=c[i]>0?c[i]/1.3:c[i]/0.7;
		f[i+1][0]=min(f[i][0]+a[i+1]-a[i],f[i+1][1]+z);
	}
	cout<<f[n][0]<<endl;
}
int main() {
	cout<<fixed<<setprecision(2);
	solve();
	return 0;
}
