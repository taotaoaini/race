#include<bits/stdc++.h>
using namespace std;
const int N=110;
vector<int> init(string s) {
	vector<int> res;
	for(int i=s.length()-1; i>=0; i--) {
		if(s[i]=='-') continue;
		res.push_back(s[i]-'0');
	}
	return res;
}
vector<int> add(vector<int> a,vector<int> b) {
	vector<int> ans;
	int t=0;
	int n=a.size(),m=b.size();
	int i=0;
	for(i=0; i<min(n,m); i++) {
		t+=a[i]+b[i];
		ans.push_back(t%10);
		t/=10;
	}
	for(int j=i; j<n; j++) {
		if(t) {
			ans.push_back(a[j]+t);
			t=0;
		} else ans.push_back(a[j]);
	}
	for(int j=i; j<m; j++) {
		if(t) {
			ans.push_back(a[j]+t);
			t=0;
		} else ans.push_back(a[j]);
	}
	return ans;
}
vector<int> del(vector<int> a,vector<int> b) { //大减小
	vector<int> ans;
	int t=0;
	int n=a.size(),m=b.size();
	int i=0;
	int s=0;
	for(i=0; i<min(n,m); i++) {
		s=t+a[i]-b[i];
		t=0;
		if(s<0) {
			s+=10;
			t=-1;
		}
		ans.push_back(s);
	}
	s=0;
	for(int j=i; j<n; j++) {
		s=t+a[j];
		t=0;
		if(s<0){
			s+=10;
			t=-1;
		}
		ans.push_back(s);		
	}
	while(ans.back()==0) ans.pop_back(); //可能存在前导零，当最高位为1，并且被借位的时候，最高位至0 
	return ans;
}
bool cmp(vector<int> a,vector<int> b) {
	int n=a.size();
	int m=b.size();
//	cout<<n<<":"<<m<<endl;
	if(n>m) return true;
	if(n<m) return false;
	for(int i=n-1; i>=0; i--) {
//		if(i==n-1) cout<<a[i]<<"::::"<<b[i]<<endl;
		if(a[i]<b[i]) return false;
		if(a[i]>b[i]) return true;
	}
	return true;
}
vector<int> mul(vector<int> a,vector<int> b) {
	int n=a.size();
	vector<int> ans(n*2+10);
	int t=0;
	for(int i=0; i<n; i++) {
		for(int j=0; j<n; j++) {
			t+=a[i]*a[j];
			ans[i+j]+=t%10;
			t/=10;
		}
//		for(int i=0;i<3;i++) cout<<ans[i];
//		cout<<endl;
		if(t) {
			ans[i+n]+=t;
			t=0;
		}
	}
	t=0;
	for(int i=0; i<ans.size(); i++) {
		t+=ans[i];
		ans[i]=t%10;
		t/=10;
	}
	while(ans.back()==0) ans.pop_back();
	return ans;
}
void solve() {
	string s,t;
	cin>>s>>t;
	vector<int> a=init(s);
	vector<int> b=init(t);
//	vector<int> ans=add(a,b);
//	for(auto x: ans) cout<<x<<" ";
//	for(auto x:a) cout<<x<<" ";
//	cout<<endl;
//	for(auto x:b) cout<<x<<" ";
//	cout<<endl;
//	cout<<cmp(a,b)<<endl;
	a=mul(a,a);
//	reverse(a.begin(),a.end());
	b=mul(b,b);
//	reverse(b.begin(),b.end());
//	for(auto x:a) cout<<x;
//	cout<<endl;
	vector<int> ans;
//	cout<<cmp(a,b)<<endl;
	if(cmp(a,b))  ans=del(a,b);
	else {
//		cout<<"-";
//		cout<<"L:L:";
		ans=del(b,a);
	}
	reverse(ans.begin(),ans.end());
	for(auto x:ans) cout<<x;
	cout<<endl;
//	for(int i=0;i<4;i++) cout<<ans[i];
//	cout<<endl;
}
int main() {
	solve();
	return 0;
}
