#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
string s;
bool check(int i,int j) {
	while(i<j) {
		if(s[i]<s[j]) return false;
		if(s[i]>s[j]) return true;
		i++,j--;
	}
	return false;
}
void solve() {
//	string s;
	i64 res=0;
	cin>>s;
	int n=s.length();
	for(int i=0; i<n; i++) {
		for(int j=i+1;j<n;j++){
			if(check(i,j)) res++;
		}
	}
//	cout<<check(1,3)<<endl;
	cout<<res<<endl;
}
int main() {
	solve();
	return 0;
}
