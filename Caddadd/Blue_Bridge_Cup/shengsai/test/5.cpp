#include<bits/stdc++.h>
using namespace std;
#define fi first
#define se second
const int N=200010;
vector<int> e[N];
int n;
int c[N];
int res=0;
unordered_map<int,int> mp[N]; 
void dfs(int u,int fa){
	mp[u][c[u]]+=1;
	for(auto& v:e[u]){
		if(v==fa) continue;
		dfs(v,u);
		for(auto& x:mp[v]){
			mp[u][x.fi]+=x.se;
		}
	}
	int cnt=mp[u][c[u]];
	bool flag=true;
	for(auto &x:mp[u]){
		if(cnt!=x.se){
			flag=false;
			break;
		}
	}
	if(flag) res++;
}
void solve(){
	cin>>n;
	for(int i=1;i<=n;i++){
		int x;
		cin>>c[i]>>x;
 	   e[x].push_back(i);
	}
	dfs(1,0);
	cout<<res<<endl;
}
int main(){
	solve();
	return 0;
}
