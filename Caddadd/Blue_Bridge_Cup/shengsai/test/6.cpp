#include<bits/stdc++.h>
using namespace std;
const int inf=0x3f3f3f3f;
using f64 = long double;
void solve() {
	int n;
	f64 m;
	cin>>n>>m;
	vector<int> a(n);
	for(int i=0; i<n; i++) cin>>a[i];
	int res=inf;
	for(int i=0; i<(1<<(n+1)); i++) {
		int cnt=0;
		f64 s=0;
		for(int j=0; j<n; j++) {
			if(i&(1<<j)) {
				cnt++;
				s+=1LL*a[j]*1.0/2;
			} else {
				s+=a[j];
			}
		}
    cout<<s<<endl;
		if(fabsl(s-m)<=1e-6) res=min(res,cnt);
	}
	if(res==inf) cout<<-1<<endl;
	else  cout<<res<<endl;
}
int main() {
	solve();
	return 0;
}
