#include<bits/stdc++.h>
using namespace std;
const int N=100010;
const int inf=0x3f3f3f3f;
int n,m,q;
//vector<int> e[N];
int e[510][510];
void solve() {
	cin>>n>>m>>q;
	for(int i=1; i<=n; i++) {
		for(int j=i+1; j<=n; j++) {
			e[i][j]=e[j][i]=inf;
		}
	}
	for(int i=1;i<=m;i++){
		int x,y,w;
		cin>>x>>y>>w;
		e[x][y]=e[y][x]=w;
	}
	for(int k=1;k<=n;k++){
		for(int i=1;i<=n;i++){
			for(int j=i+1;j<=n;j++){
				int x=min(e[i][k],e[k][j]);
				if(e[i][j]==inf) e[i][j]=x;
				else e[i][j]=max(x,e[i][j]);
			}
		}
	}
	for(int i=1;i<=q;i++){
		int x,y;
		cin>>x>>y;
		if(e[x][y]==inf) cout<<-1<<endl;
		else cout<<e[x][y]<<endl;
	}
}
int main() {
	solve();
	return 0;
}
