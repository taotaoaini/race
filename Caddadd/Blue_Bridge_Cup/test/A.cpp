#include<bits/stdc++.h>
using namespace std;
using i64 = long long;

void solve(){
	int n=2023;
	int x=0;
	int res=0;
	for(int i=1;i<=2023/i;i++){
		if(n%i==0){
			if(i*i==n) res+=1;
			else res+=2;
		}
	}
	cout<<res<<endl;
}
int main(){
	ios::sync_with_stdio(false);cin.tie(0);
	int t=1;
	while(t--){
		solve();
	}
	return 0;
}
