#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const int N=1e5+10;
int n;
int d[N],cnt=0,r;
void solve(){
	cin>>n;
	for(int i=1;i<n;i++){
		int u,v;
		cin>>u>>v;
		if(++d[u]==3) cnt+=1,r=u;
		if(++d[v]==3) cnt+=1,r=v;
	}
	if(cnt==0){
		cout<<"Yes"<<endl;
		cout<<1<<endl;
		for(int i=1;i<=n;i++) if(d[i]==1) cout<<i<<" ";
		cout<<endl;
	}
	else if(cnt==1){
		cout<<"Yes"<<endl;
		for(int i=1;i<=n;i++) if(d[i]==1) cnt++;
		cout<<cnt-1<<endl;
		for(int i=1;i<=n;i++) if(d[i]==1) cout<<i<<" "<<r<<endl;
	}else{
		cout<<"No"<<endl;
	}
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    solve();
    
    return 0;
}