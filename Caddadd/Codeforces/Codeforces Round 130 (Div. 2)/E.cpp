#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
/*
统计u的k级儿子
*/

const int N=1e5+10;
struct E{
	int v,ne;
}e[N<<1];
int h[N],idx;
void add(int x,int y){
	e[++idx].v=y;e[idx].ne=h[x],h[x]=idx;
}
int n,m,rt;
int dep[N],sz[N],son[N],st[N][22];
//分别为深度、子树大小、重儿子编号、倍增数组
int ans[N],cnt[N];
vector<pii> q[N];
void dfs0(int u,int fa){
	dep[u]=dep[fa]+1,sz[u]=1;
	st[u][0]=fa;
	for(int i=1;i<20;i++) st[u][i]=st[st[u][i-1]][i-1];
	for(int i=h[u];i;i=e[i].ne){
		int v=e[i].v;
		if(v==fa) continue;
		dfs0(v,u);
		sz[u]+=sz[v];
		if(son[u]==-1||sz[v]>sz[son[u]]) son[u]=v;
	}
}
void change(int u,int fa,int k){
	cnt[dep[u]]+=k;
	for(int i=h[u];i;i=e[i].ne){
		int v=e[i].v;
		if(v==fa) continue;
		change(v,u,k);
	}
}
void dfs(int u,int fa,bool del){
	for(int i=h[u];i;i=e[i].ne){
		int v=e[i].v;
		if(v==fa||v==son[u]) continue;
		dfs(v,u,true);
	}
	if(son[u]!=-1) dfs(son[u],u,false);
	cnt[dep[u]]++;
	for(int i=h[u];i;i=e[i].ne){
		int v=e[i].v;
		if(v==fa||v==son[u]) continue;
		change(v,u,1);
	}
	for(auto i:q[u]){
		ans[i.second]=cnt[dep[u]+i.first]-1;
	}
	if(del) change(u,fa,-1);
}
void solve(){
	cin>>n;
	for(int i=1;i<=n;i++){
		int x;cin>>x;
		add(x,i);
	}
	memset(son,-1,sizeof(son));
	dfs0(rt,rt);// 预处理深度数组，重儿子，以及倍增数组
	cin>>m;
	for(int i=1;i<=m;i++){
		int v,p;
		cin>>v>>p;
		for(int j=0;(1<<j)<=p;j++){
			if((p>>j)&1) v=st[v][j];
		}
		if(!v) ans[i]=0;
		else q[v].push_back(make_pair(p,i));
	}
	dfs(rt,rt,false);
	for(int i=1;i<=m;i++) cout<<ans[i]<<" ";
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}





