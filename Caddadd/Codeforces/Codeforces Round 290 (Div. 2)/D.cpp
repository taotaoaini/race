#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second
using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read() {
    __int128 x = 0, f = 1;
    char ch = getchar();
    while (ch < '0' || ch > '9') {
        if (ch == '-') f = -1;
        ch = getchar();
    }
    while (ch >= '0' && ch <= '9') {
        x = x * 10 + ch - '0';
        ch = getchar();
    }
    return x * f;
}
//__int128的输出
inline void print(__int128 x) {
    if (x < 0) {
        putchar('-');
        x = -x;
    }
    if (x > 9)
        print(x / 10);
    putchar(x % 10 + '0');
}
const int N = 310;
int n;
int l[N], c[N];
unordered_map<int, int> dp;
int gcd(int a,int b){
    return b==0?a:gcd(b,a%b);
}
void solve() {
    cin >> n;
    for (int i = 1; i <= n; i++) {
        cin >> l[i];
    }
    for (int i = 1; i <= n; i++) {
        cin >> c[i];
    }
    //小技巧，这里我们通过判断是否为0来进行操作,为0为第一次出现的位置
    for (int i = 1; i <= n; i++) {
        dp[l[i]] = dp[l[i]] ? min(dp[l[i]], c[i]) : c[i];
    }
    for (int i = 1; i <= n; i++) {
        for (auto d : dp) {
            int x = gcd(l[i],d.first);
            dp[x]=dp[x]?min(dp[x],d.second+c[i]):d.second+c[i];
        }
    }
    if(dp[1]) cout<<dp[1]<<endl;
    else cout<<-1<<endl;
}
int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    //cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}