#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

const int maxn = 1e6 + 5;
int a[maxn] = {0};
bool dp[1005][1005] = {false};
void solve(){
	int n, m;
	cin >> n >> m;
	for(int i = 1; i <= n; i++){
		cin >> a[i];
		a[i] %= m;
	}
	
	bool ok = false;
	if(n >= m) ok = true, n = 0;  // 这里让 n = 0，下边的循环就不会执行
	
	for(int i = 1; i <=n; i++){
		dp[i][a[i]] = true;       	// 初始化
		for(int j = 0; j <m; j++){
			if(dp[i-1][j]) dp[i][j] = true;                 // 传递上一位的状态
			if(dp[i][j]) dp[i+1][(j + a[i+1]) % m] = true;  // 状态转移
		}
	}
	if(dp[n][0]) ok = true;   // 判断答案
	
	if(ok) cout << "YES" << endl;
	else cout << "NO" << endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}