#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
int a[250010];
int n;
int ans[510],idx=0;
unordered_map<int,int> mp;
int gcd(int a,int b){
	return b==0?a:gcd(b,a%b);
}
void solve(){
	cin>>n;
	for(int i=1;i<=n*n;i++){
		cin>>a[i];
		mp[a[i]]+=1;
	}
	if(n==1) {cout<<a[1]<<endl;return;}
	sort(a+1,a+n*n+1);
	int mx1=a[n*n];
	int mx2=a[n*n-1];
	ans[idx++]=mx1;
	ans[idx++]=mx2;
	mp[mx1]-=1;
	mp[mx2]-=1;
	int t=gcd(mx1,mx2);
	mp[t]-=2;
	for(int i=n*n-2;i>=1;i--){
		if(mp[a[i]]>0){
			for(int j=0;j<idx;j++){
				mp[gcd(a[i],ans[j])]-=2;
			}
			ans[idx++]=a[i];
			mp[a[i]]-=1;
		}
		if(idx==n) break;
	}
	for(int i=0;i<idx;i++) cout<<ans[i]<<" ";
		cout<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}