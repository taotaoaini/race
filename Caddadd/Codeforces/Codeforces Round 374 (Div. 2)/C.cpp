#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const int N=5010;
const int inf=0x3f3f3f3f;
int n,m,T;
int f[N][N],pre[N][N];
int res,ans[N],cnt;
struct edge{
	int u,v,w;
	void read(){
		cin>>u>>v>>w;
	}
}e[N];
//f[i][j]代表从1节点出发，当前走到节点i，途径j个节点的最小代价
void solve(){
	cin>>n>>m>>T;
	for(int i=1;i<=m;i++){
		e[i].read();
	}
	memset(f,0x3f,sizeof(f));
	f[1][1]=0;
	int u,v,w;
	for(int i=2;i<=n;i++){
		for(int j=1;j<=m;j++){
			u=e[j].u;
			v=e[j].v;
			w=e[j].w;
			if(f[v][i]>f[u][i-1]+w){
				f[v][i]=f[u][i-1]+w;
				pre[v][i]=u;
			}
		}
	}
	for(int i=n;i>=1;i--){
		if(f[n][i]<=T){
			res=i;
			break;
		}
	}
	cout<<res<<endl;
	for(int i=n;i!=1;i=pre[i][res--]){
		ans[cnt++]=i;
	}
	ans[cnt++]=1;
	for(int i=cnt-1;i>=0;i--) cout<<ans[i]<<" ";
	cout<<endl;
}

int main(){
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	
	int t=1;
	//cin>>t;
	while(t--){
		solve();
	}

	return 0;
}