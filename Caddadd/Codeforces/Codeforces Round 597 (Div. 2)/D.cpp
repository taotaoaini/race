#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
int n;
const int N=2010,M=4000010;
struct edge{
	int u,v;
	ll w;
}e[M];
int idx=0;
int d[N][2];
int c[N],k[N];
int fa[N];
void init(int n){
	for(int i=1;i<=n;i++) fa[i]=i;
}
int find(int x){
	while(x!=fa[x]) x=fa[x]=fa[fa[x]];
	return x;
}
bool cmp(const edge& a,const edge& b){
	return a.w<b.w;
}
void solve(){
	cin>>n;
	init(n);
	for(int i=1;i<=n;i++) cin>>d[i][0]>>d[i][1];
	for(int i=1;i<=n;i++) cin>>c[i],e[++idx]={i,n+1,(ll)c[i]};
	for(int i=1;i<=n;i++) cin>>k[i];
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			e[++idx]={i,j,((ll)abs(d[i][0]-d[j][0])+abs(d[i][1]-d[j][1]))*((ll)k[i]+k[j])};
		}
	}
	sort(e+1,e+idx+1,cmp);
	ll res=0;
	int cnt=0;
	vector<int> ans1;
	vector<pii> ans2;
	for(int i=1;i<=idx;i++){
		int fx=find(e[i].u);
		int fy=find(e[i].v);
		if(fx==fy) continue;
		if(e[i].v==n+1)  ans1.push_back(e[i].u);
		else ans2.push_back({e[i].v,e[i].u});
		res+=e[i].w;
		// cout<<res<<endl;
		fa[fx]=fy;
		cnt++;
		if(cnt==n) break;
	}
	cout<<res<<endl;
	cout<<ans1.size()<<endl;
	for(int i=0;i<ans1.size();i++) cout<<ans1[i]<<" ";
	cout<<endl;
	cout<<ans2.size()<<endl;
	for(int i=0;i<ans2.size();i++) cout<<ans2[i].first<<" "<<ans2[i].second<<endl;

}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}