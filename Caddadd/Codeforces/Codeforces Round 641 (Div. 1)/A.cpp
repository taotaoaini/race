#define _CRT_SECURE_NO_WARNINGS 01

#include<iostream>
#include<algorithm>
#include<cstring>
#include<map>
#include<vector>


using namespace std;

typedef long long LL;

const int N = 1e5 + 10, MAXI = 0x3f3f3f3f;

int t, n, m, k, z;
int a[N];
int f[N][6];//f[i][j]表示当前位置i并且使用了j次左移的最大价值

int main()
{
  cin >> t;
  while (t--)
  {
  	int ans = 0;
  	memset(f, 0, sizeof f);
  	memset(a, 0, sizeof a);
  	cin >> n >> k >> z;
  	for (int i = 1; i <= n; i++)
  	{
  		cin >> a[i];
  	}
  	for (int i = 2; i <= n; i++)
  	{
  		f[i][0] = f[i - 1][0] + a[i];
  		if (i - 1 == k)ans = max(ans, f[i][0]);//更新一步向左都不走的情况

  		for (int j = 1; j <= z; j++)//枚举一共向左走了j次
  		{
  			for (int h = 0; h <= j; h++)//枚举上一个点向左走了h次
  			{
  				int t = h * (a[i] + a[i - 1]);
  				f[i][j] = max(f[i][j], f[i - 1][j - h] + t + a[i]);

  				// 如果在本节点已经用完了步数
  				if (i - 1 + j * 2 == k)ans = max(ans, f[i][j]);
  				// 如果在上本节点已经用完了步数
  				if (i - 1 + j * 2 - 1 == k)ans = max(ans, f[i][j] - a[i]);
  			}
  		}
  	}
  	cout << ans + a[1] << endl;// 一开始有a[1]分
  }
}
