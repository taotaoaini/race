/*
i64=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
u64=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;

str+str
to_string(int)
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls u<<1
#define rs u<<1|1
#define fi first
#define se second
#define mid ((l+r)/2)

using i8 = signed char;
using u8 = unsigned char;
using i16 = signed short int;
using u16 = unsigned short int;
using i32 = signed int;
using u32 = unsigned int;
using f32 = float;
using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using i128 = __int128_t;
using u128 = __uint128_t;
using f128 = long double;

using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<int128>();
*/

template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}

template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

//int[]:6.7e7
const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 110;

vector<string> a;
int n, m;
int dx[4] {0, 0, 1, -1}, dy[4] {1, -1, 0, 0};
bool vis[N][N];
int cnt = 0;
int res = 0;
// 看上去很麻烦,但是要从这种题目中学会分类,学会找题目规律的

//Ⅰ . 当 好 人 和 坏 人 相 邻 , 直 接 输 出 N O \color{Red}{Ⅰ.当好人和坏人相邻,直接输出NO}Ⅰ.当好人和坏人相邻,直接输出NO

// 因 为 按 照 要 求 好 人 一 定 能 到 ( n , m ) 因为按照要求好人一定能到(n,m)因为按照要求好人一定能到(n,m)

// 那 么 坏 人 一 定 可 以 先 走 到 好 人 的 位 置 , 然 后 像 好 人 一 样 走 到 终 点 那么坏人一定可以先走到好人的位置,然后像好人一样走到终点那么坏人一定可以先走到好人的位置,然后像好人一样走到终点

// Ⅱ . 否 则 , 考 虑 加 一 些 障 碍 . \color{Red}Ⅱ.否则,考虑加一些障碍.Ⅱ.否则,考虑加一些障碍.

// 这 里 分 享 一 个 小 技 巧 , 加 的 障 碍 一 定 是 有 明 显 的 规 律 的 , 毕 竟 只 是 D 题 啊 ! ! 这里分享一个小技巧,加的障碍一定是有明显的规律的,毕竟只是D题啊!!这里分享一个小技巧,加的障碍一定是有明显的规律的,毕竟只是D题啊!!

// 因 为 我 们 想 让 坏 人 出 不 去 , 所 以 我 们 用 障 碍 围 住 坏 人 的 上 下 左 右 因为我们想让坏人出不去,所以我们用障碍围住坏人的上下左右因为我们想让坏人出不去,所以我们用障碍围住坏人的上下左右

// 这 样 是 最 优 的 。 因 为 不 管 怎 样 你 一 定 要 把 坏 人 围 成 一 圈 围 起 来 , 否 则 坏 人 就 能 出 去 这样是最优的。因为不管怎样你一定要把坏人围成一圈围起来,否则坏人就能出去这样是最优的。因为不管怎样你一定要把坏人围成一圈围起来,否则坏人就能出去

// 那 么 围 大 圈 肯 定 不 如 围 小 圈 , 小 圈 影 响 范 围 更 小 那么围大圈肯定不如围小圈,小圈影响范围更小那么围大圈肯定不如围小圈,小圈影响范围更小

// 那 么 围 完 所 有 坏 人 后 , 用 b f s 判 断 每 个 好 人 是 否 能 走 到 ( n , m ) 那么围完所有坏人后,用bfs判断每个好人是否能走到(n,m)那么围完所有坏人后,用bfs判断每个好人是否能走到(n,m)

// 只 要 有 1 个 走 不 到 就 输 出 N O 只要有1个走不到就输出NO只要有1个走不到就输出NO

// 这 里 有 一 个 小 技 巧 这里有一个小技巧这里有一个小技巧

// 每 个 好 人 都 去 b f s 太 麻 烦 , 时 间 复 杂 度 太 高 每个好人都去bfs太麻烦,时间复杂度太高每个好人都去bfs太麻烦,时间复杂度太高

// 不 如 我 们 从 ( n , m ) 开 始 b f s , 标 记 终 点 能 到 达 的 点 不如我们从(n,m)开始bfs,标记终点能到达的点不如我们从(n,m)开始bfs,标记终点能到达的点

// 就 可 以 O ( 1 ) 判 断 好 人 能 否 到 终 点 就可以O(1)判断好人能否到终点就可以O(1)判断好人能否到终点
void dfs(int i, int j) {
	if (a[i][j] == 'G') res += 1;
	vis[i][j] = true;
	for (int k = 0; k < 4; k++) {
		int x = i + dx[k];
		int y = j + dy[k];
		if (x >= 0 && x < n && y >= 0 && y < m && !vis[x][y] && ( a[x][y] == '.' || a[x][y] == 'G')) {
			dfs(x, y);
		}
	}
}
void init() {
	a.clear();
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			vis[i][j] = false;
		}
	}
	res = 0;
	cnt = 0;
}
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	cin >> n >> m;
	string s;
	for (int i = 0; i < n; i++)
	{cin >> s; a.push_back(s);}
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			if (a[i][j] == 'G') cnt += 1;
			if (a[i][j] == 'B') {
				for (int k = 0; k < 4; k++) {
					int x = i + dx[k];
					int y = j + dy[k];
					if (x >= 0 && x < n && y >= 0 && y < m && a[x][y] == '.') {
						a[x][y] = '#';
					} else if (x >= 0 && x < n && y >= 0 && y < m && a[x][y] == 'G') {
						cout << "No" << endl; return;
					}
				}
			}
		}
	}
	if (a[n - 1][m - 1] != '#')
		dfs(n - 1, m - 1);
	if (res == cnt) {
		cout << "Yes" << endl;
	} else {
		cout << "No" << endl;
	}
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	cin >> t;

	while (t--) {
		solve();
		init();
	}

	return 0;
}