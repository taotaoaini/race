/*
把所有的数字都插入字典树中，并且插入的每个节点都存一下插入的下标，那么对于一个节点来说，如果只有一个分支，那么挂在这个点上的所有数字从根到这个点为止数值都是一样的，那么就不存在逆序对，否则如果有两个分支，那么就会有一部分数字大于另一部分，我们不妨枚举0的那一边数字，如果这一边的数字的下标比1的那一边要大的话，肯定就是存在逆序对的。递归处理就能算出对于每一个二进制位来说，这一位取0和取1分别会得到的逆序对数量，由于字典树的性质，挂在一个点的数字更高的二进制位都是一样的，所以没有后效性。


*/



/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
    x = 0; bool flag(0); char ch = getchar();
    while (!isdigit(ch)) flag = ch == '-', ch = getchar();
    while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
    flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
    //mode=1为换行，0为空格
    x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
    do stk[++top] = x % 10, x /= 10; while (x);
    while (top) putchar(stk[top--] | 48);
    mode ? putchar('\n') : putchar(' ');
}
const int inf=0x3f3f3f3f;
const ll dinf=0x7f7f7f7f;
const int N=300010;

int tr[N*31][2],idx;
ll cnt[31][2];
vector<int> index[N*31];
void insert(int x,int pos){
	int p=0;
	for(int i=30;i>=0;i--){
		int t=x>>i&1;
		if(!tr[p][t]) tr[p][t]=++idx;
		p=tr[p][t];
		index[p].push_back(pos);
	}
}
void dfs(int x,int p){
	if(x==-1) return;
	int ls=tr[p][0],rs=tr[p][1],id=0;
	if(!ls&&!rs) return;
	ll sum=0;
	for(auto v:index[ls]){
		while(id<index[rs].size()&&v>index[rs][id]) ++id;
		sum+=id;
	}
	cnt[x][0]+=sum;
	cnt[x][1]+=(1ll)*index[ls].size()*index[rs].size()-sum;
	if(ls) dfs(x-1,ls);
	if(rs) dfs(x-1,rs);
}
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout); 
    int n;
    cin>>n;
    for(int i=1,x;i<=n;i++){
    	cin>>x;
    	insert(x,i);
    }
    dfs(30,0);
    ll res=0,val=0;
    for(int i=30;i>=0;i--){
    	if(cnt[i][0]<=cnt[i][1]) res+=cnt[i][0];
    	else{
    		res+=cnt[i][1];
    		val^=(1<<i);
    	}
    }
    cout<<res<<" "<<val<<endl;
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    //cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}