/*
BFS+拓扑+并查集
*/
// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题

// #define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;
// //__int128的输入
// inline __int128 read() {
// 	__int128 x = 0, f = 1;
// 	char ch = getchar();
// 	while (ch < '0' || ch > '9') {
// 		if (ch == '-') f = -1;
// 		ch = getchar();
// 	}
// 	while (ch >= '0' && ch <= '9') {
// 		x = x * 10 + ch - '0';
// 		ch = getchar();
// 	}
// 	return x * f;
// }
// //__int128的输出
// inline void print(__int128 x) {
// 	if (x < 0) {
// 		putchar('-');
// 		x = -x;
// 	}
// 	if (x > 9)
// 		print(x / 10);
// 	putchar(x % 10 + '0');
// }
// const int N = 200010;
// int n;
// vector<int> e[N];
// int deg[N];
// int BFS[N], cnt = 0;//数组模拟BFS，cnt：当前节点个数
// int fa[N], num[N];//num：统计一个x为根节点的子树大小
// int find(int x) {
// 	while (x != fa[x]) x = fa[x] = fa[fa[x]];
// 	return x;
// }
// //因为不止一个测试数据，需要初始化数据
// void init(int n) {
// 	for (int i = 1; i <= n; i++) {
// 		deg[i] = 0;
// 		num[i] = 1;
// 		fa[i] = i;
// 	}
// 	cnt = 0;
// }
// void merge(int x, int y) {
// 	int fx = find(x);
// 	int fy = find(y);
// 	if (fx == fy) return;
// 	if (num[fx] < num[fy]) {
// 		num[fy] += num[fx];
// 		fa[fx] = fy;
// 	} else {
// 		num[fx] += num[fy];
// 		fa[fy] = fx;
// 	}
// }
// bool is_root(int x) {
// 	return fa[x] == x;
// }
// void solve() {
// 	cin >> n;
// 	//初始化工作
// 	init(n);
// 	int u, v;
// 	for (int i = 0; i < n; i++) {
// 		cin >> u >> v;
// 		deg[u]++, deg[v]++;
// 		e[u].push_back(v);
// 		e[v].push_back(u);
// 	}
// 	//从deg=1的叶子节点开始向上删去，向上合并，最后得到以环内节点为根节点的一颗颗树
// 	for (int i = 1; i <= n; i++) {
// 		if (deg[i] == 1) {
// 			BFS[++cnt] = i;
// 		}
// 	}
// 	for (int i = 1; i <= cnt; i++) {
// 		int u = BFS[i];
// 		for (int v : e[u]) {
// 			merge(u, v);
// 			deg[v] -= 1;
// 			if (deg[v] == 1) BFS[++cnt] = v;
// 		}
// 	}
// 	ll res = (ll)n * (n - 1);
// 	for (int i = 1; i <= n; i++) {
// 		if (is_root(i)) res -= (ll)num[i] * ((num[i]) - 1) / 2;
// 	}
// 	cout << res << endl;
// 	for (int i = 1; i <= n; i++) {
// 		e[i].clear();
// 	}
// }
// int main() {
// 	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
// 	int t = 1;
// 	cin >> t;
// 	while (t--) {
// 		solve();
// 	}
// 	return 0;
// }

