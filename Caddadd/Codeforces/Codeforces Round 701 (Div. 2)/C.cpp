#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
ll x, y;
ll ans;
void solve(){
	 ans = 0;
        cin >> x >> y;
        for (ll b = 2; b <= min(y, (ll)sqrt(x + 1)); b++)
            ans += b - 1;
        ll l, r;
        for (l = sqrt(x + 1) + 1; l <= min(x, y); l = r + 1)
        {
            if (x / (l + 1) <= 0) 
                break;
            r = min(y, min(x, x / (x / (l + 1))-1));
            //这里-1的原因是：除的是l+1而不是l
            //r＝max（i）中的i是满足i+1≤n/k的，也就是i≤n/k-1
            ans += (r - l + 1) * (x / (l + 1));
        }
        cout << ans << endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}