#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
const int N = 2e5 + 10, M = 1e6 + 10, MAXI = 0x3f3f3f3f;

int n, m, t, k;
int a[N];

void solve(int n, int k)
{
  for (int i = 0; i < k - 3; i++)
  {
      printf("1 ");
  }
  n = n - k + 3;
  if (n & 1)
  {
      printf("1 %d %d", n / 2, n / 2);
  }
  else
  {
      if (n % 4 != 0)
      {
          printf("2 %d %d", n / 2 - 1, n / 2 - 1);
      }
      else
      {
          printf("%d %d %d", n / 4, n / 4, n / 4 * 2);
      }
  }
}

int main()
{
  cin >> t;
  while (t--)
  {
      cin >> n >> k;
      solve(n, k);
      cout << endl;
  }
  return 0;
}