#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

void solve(){
	int n,r,b;
	cin>>n>>r>>b;
	vector<char> ans;
	int x=r/(b+1),y=r%(b+1);
	for(int i=1;i<=y;i++){
		for(int j=1;j<=x+1;j++){
			ans.push_back('R');
		}
		ans.push_back('B');
	}
	for(int i=1;i<=b-y+1;i++){
		for(int j=1;j<=x;j++){
			ans.push_back('R');
		}
		ans.push_back('B');
	}
	for(int i=0;i<ans.size()-1;i++){
		cout<<ans[i]<<" ";
	}
	cout<<endl;
}

int main(){
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	
	int t=1;
	cin>>t;
	while(t--){
		solve();
	}

	return 0;
}