
// 若 n>1
//  且 m>1
// ，那么路径上一定至少有一次转折，比如右→下，把它改成下→右能使价值 +2/不变/-2
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

void solve(){
	int n,m;
	cin>>n>>m;
	vector<vector<int>> a(n,vector<int>(m));
	vector<vector<int>> mi(n,vector<int>(m));
	vector<vector<int>> mx(n,vector<int>(m));
	for(int i=0;i<n;i++)
		for(int j=0;j<m;j++) cin>>a[i][j];
	mi[0][0]=mx[0][0]=a[0][0];
	for(int i=1;i<n;i++) mx[i][0]=mi[i][0]=a[i][0]+mx[i-1][0];
	for(int i=1;i<m;i++) mx[0][i]=mi[0][i]=a[0][i]+mx[0][i-1];
	for(int i=1;i<n;i++)
		for(int j=1;j<m;j++){
			mi[i][j]=min(mi[i-1][j],mi[i][j-1])+a[i][j];
			mx[i][j]=max(mx[i-1][j],mx[i][j-1])+a[i][j];
		}
	if(mx[n-1][m-1]%2==0&&mi[n-1][m-1]%2==0&&mi[n-1][m-1]<=0&&mx[n-1][m-1]>=0) cout<<"YES"<<endl;
	else cout<<"NO"<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t;
    cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}


