#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

void solve(){
	int n;
	cin>>n;
	vector<int> s(n);
	vector<int> ans(n+1);
	for(int i=0;i<n;i++){
		char c;
		cin>>c;
		s[n-i-1]=c-'0';
	}
	int h=s[n-1];
	if(h==9){
		int t=0;
		for(int i=0;i<n;i++){
			ans[i]=1-s[i]+t;
			t=0;
			if(ans[i]<0){
				ans[i]+=10;
				t=-1;
			}
		}	
	}else{
		for(int i=0;i<n;i++){
			ans[i]=9-s[i];
		}
	}
	int i;
	for(i=n;i>=0;i--){
		if(ans[i]!=0) break;
	}
	for(;i>=0;i--){
		cout<<ans[i];
	}
	cout<<endl;
}

int main(){
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	
	int t=1;
	cin>>t;
	while(t--){
		solve();
	}

	return 0;
}