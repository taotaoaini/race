	#include<iostream>
	#include<cstring>
	#include<algorithm>
	#include<map>
	#include<set>
	#include<vector>
	#include<queue>
	#include<sstream>
	#include<cmath>
	#include<list>
	#include<bitset>
	#include<unordered_map>
	#include<unordered_set>
	#include<stack>

	#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
	#define endl '\n'
	#define lson pos<<1
	#define rson pos<<1|1

	using ll = long long;
	using ull = unsigned long long;
	using pii = std::pair<int, int>;
	using pll = std::pair<ll, ll>;
	using namespace std;

	void solve(){
		int n,m;
		cin>>n>>m;
		unordered_set<int> d;
		vector<int> ans(m+1);
		int x;
		for(int i=1;i<=n;i++){
			cin>>x;
			int t1=max(x,m+1-x),t2=min(x,m+1-x);
			if(d.find(t2)==d.end()){
				ans[t2]=1;
				d.insert(t2);
			}else{
				ans[t1]=1;
				// d.insert(t1);
			}
		}
		for(int i=1;i<=m;i++){
			if(ans[i]) cout<<"A";
			else cout<<"B";
		}
		cout<<endl;
	}

	int main(){
		ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
		
		int t=1;
		cin>>t;
		while(t--){
			solve();
		}

		return 0;
	}