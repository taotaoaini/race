/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second
#define mid ((l+r)>>1)

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
    x = 0; bool flag(0); char ch = getchar();
    while (!isdigit(ch)) flag = ch == '-', ch = getchar();
    while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
    flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
    x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
    do stk[++top] = x % 10, x /= 10; while (x);
    while (top) putchar(stk[top--] | 48);
    mode ? putchar('\n') : putchar(' ');
}
const int inf=0x3f3f3f3f;//1061109567,是10^9级别的
const ll dinf=0x7f7f7f7f;
// const int M=100010;
const int N=1000010;
int a[N];
int n;
ll f[N];
const int m=1e5;
vector<int> b[N];
int cnt[N];
void init(){
	for(int i=1;i<=m;i++){
		for(int j=i;j<=m;j+=i){
			b[j].push_back(i);
		}
	}
}
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout); 
    cin>>n;
    for(int i=1;i<=n;i++) cin>>a[i];
    sort(a+1,a+n+1);
	for(int i=1;i<=n;i++){
		for(int j=0;j<b[a[i]].size();j++){
			f[b[a[i]][j]]+=cnt[b[a[i]][j]]*(n-i);
			cnt[b[a[i]][j]]++;
		}
	}
	ll res=0;
	for(int i=m;i>=1;i--){
		for(int j=i+i;j<=m;j+=i){
			f[i]-=f[j];
		}
		res+=f[i]*i;
	}
	cout<<res<<endl;
	memset(f,0,sizeof(f));
	memset(cnt,0,sizeof(cnt));
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    cin>>t;
    init();
    while (t--) {
        solve();
    }

    return 0;
}