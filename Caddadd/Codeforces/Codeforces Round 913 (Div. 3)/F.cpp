/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题
// #include<functional>//编写内部函数

// #define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1
// #define fi first
// #define se second
// #define mid ((l+r)>>1)

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;

// /*
// 快读代码模板
// a=read<int>(),b=read<ll>(),c=read<__int128>();
// */
// template<typename T>
// void read(T &x) {
//     x = 0; bool flag(0); char ch = getchar();
//     while (!isdigit(ch)) flag = ch == '-', ch = getchar();
//     while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
//     flag ? x = -x : 0;
// }
// template<typename T>
// void write(T x, bool mode = 1) {//mode=1为换行，0为空格
//     x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
//     do stk[++top] = x % 10, x /= 10; while (x);
//     while (top) putchar(stk[top--] | 48);
//     mode ? putchar('\n') : putchar(' ');
// }
// const int inf=0x3f3f3f3f;//1061109567,是10^9级别的
// const ll dinf=0x7f7f7f7f;
// //const int M=100010;
// const int N=100010;


// void solve() {
//     //freopen("a.in","r",stdin);
//     //freopen("a.out","w",stdout); 
//     int n;
//     cin>>n;
//     vector<int> a(n+1);
//     for(int i=1;i<=n;i++) cin>>a[i];
//     int l=1,r=n;
// 	while(l<n&&a[l]<=a[l+1]) l++;
// 	while(r>=1&&a[r]>=a[r-1]) r--;	
// 	// cout<<l<<"::"<<r<<endl;
// 	int res=inf;
// 	if(l>=n||r<=1) {cout<<0<<endl;return;}
// 	if(l+1==r&&a[n]<=a[1]){cout<<n-r+1<<endl;return;}
// 	l=1,r=n;
// 	while(l<n&&a[l]>=a[l+1]) l++;
// 	while(r>=1&&a[r]<=a[r-1]) r--;
// 	if(l>=n||r<=1) {cout<<1<<endl;return;}
// 	if(l+1==r&&a[n]>=a[1]) {cout<<n-r+2<<endl;return;}
// 	cout<<-1<<endl;	
// }

// int main() {
//     ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
//     int t = 1;
//     // init();//全局初始化
//     cin>>t;
//     while (t--) {
//         solve();
//     }

//     return 0;
// }



// 思路：将数组放到环上考虑， 假设环起初按顺时针排 ， 且有一个指针指向了环的第一位。现考虑操作变成了什么：

// 移位操作：将指针往前移动一格。

// 反转：将指针往前一格，且访问顺序调换方向。

// 接下来考虑题目意思：即从环上某一点出发，绕一圈是非递减排序。其中指针位置可以变，指针访问方向也可以变，接下来就是一道模拟题了，模拟出最小操作数即可。
#include <bits/stdc++.h>
using namespace std;
#define LL long long
#define pb push_back
#define x first
#define y second 
#define endl '\n'
const LL maxn = 4e05+7;
const LL N = 5e05+10;
const LL mod = 1e09+7;
const int inf = 0x3f3f3f3f;
const LL llinf = 5e18;
typedef pair<int,int>pl;
priority_queue<LL , vector<LL>, greater<LL> >mi;//小根堆
priority_queue<LL> ma;//大根堆
LL gcd(LL a, LL b){
	return b > 0 ? gcd(b , a % b) : a;
}
 
LL lcm(LL a , LL b){
	return a / gcd(a , b) * b;
}
int n , m;
vector<int>a(N , 0);
void init(int n){
	for(int i = 0 ; i <= n ; i ++){
		a[i] = 0;
	}
}
void solve() 
{
	cin >> n;
	for(int i = 0 ; i < n ; i ++)
		cin >> a[i];
	vector<int>v(2 * n , 0);
	for(int i = 0 ; i < n ; i ++){
		v[i] = a[i];
		v[i + n] = a[i];
	}
	int ans = -1;
	//递减
	for(int i = 0 ; i < n ; ){
		int st = i;
		int cnt = 1;
		while(i < 2 * n - 1 && v[i] >= v[i + 1]){
			cnt++;
			i++;
		}
		if(cnt >= n){
			ans = min(1 + st, 1 + (n - st));
		}
		i ++;
	}
	//递增
	for(int i = 0 ; i < n ; ){
		int st = i;
		int cnt = 1;
		while(i < 2 * n - 1 && v[i] <= v[i + 1]){
			cnt++;
			i++;
		}
		if(cnt >= n){
			if(ans == -1){
				ans = min(n - st , st + 2);
			}
			else{
				ans = min(ans ,min(n - st , st + 2) );
			}
			if(st == 0)
			ans = 0;
		}
		i ++;
	}	
	cout << ans << endl;
}            
int main() 
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);
    cout.precision(10);
    int t=1;
	cin>>t;
    while(t--)
    {
    	solve();
    }
    return 0;
}