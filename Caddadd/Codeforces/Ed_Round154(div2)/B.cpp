#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

bool check(string a,string b){
	int n1=a.size();
	int n2=b.size();
	if(n1!=n2) return false;
	for(int i=n1-1;i>=1;i--){
		if(a[i]=='1'&&a[i-1]=='0'&&b[i]=='1'&&b[i-1]=='0') return true;
	}
	return false;
}
int main() {
    ios::sync_with_stdio(false);
    int n;
    cin>>n;
    while(n--){
    	string a,b;
    	cin>>a>>b;
    	if(check(a,b)) cout<<"YES"<<endl;
    	else cout<<"NO"<<endl;
    }
    return 0;
}