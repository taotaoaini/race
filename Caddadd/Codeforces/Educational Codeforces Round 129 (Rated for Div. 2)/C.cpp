#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read() {
    __int128 x = 0, f = 1;
    char ch = getchar();
    while (ch < '0' || ch > '9') {
        if (ch == '-') f = -1;
        ch = getchar();
    }
    while (ch >= '0' && ch <= '9') {
        x = x * 10 + ch - '0';
        ch = getchar();
    }
    return x * f;
}
//__int128的输出
inline void print(__int128 x) {
    if (x < 0) {
        putchar('-');
        x = -x;
    }
    if (x > 9)
        print(x / 10);
    putchar(x % 10 + '0');
}
int ans[10010][2], idx = 0;
void solve() {
    idx = 0;
    int n;
    cin >> n;
    vector<int> a(n + 1);
    vector<int> b(n + 1);
    for (int i = 1; i <= n; i++) cin >> a[i];
    for (int i = 1; i <= n; i++) cin >> b[i];
    for (int i = 1; i <= n-1; i++) {
        for (int j = 1; j <= n - 1; j++) {
            if (a[j] > a[j + 1]) {
                swap(a[j], a[j + 1]);
                swap(b[j], b[j + 1]);
                ans[idx][0] = j;
                ans[idx++][1] = j + 1;
            }
        }
    }
    int l = 1;
    for (int i = 1; i <= n;) {
        while (i <= n && a[i] == a[l]) i += 1;
        for (int k = 1; k <= i - l-1; k++) {
            for (int j = l; j < i - 1; j++) {
                if (b[j] > b[j + 1]) {
                    swap(b[j], b[j + 1]);
                    ans[idx][0] = j;
                    ans[idx++][1] = j + 1;
                }
            }
        }
        l = i;
    }
    //  for(int i=1;i<=n;i++) cout<<a[i]<<" ";
    // cout<<endl;
    // for(int i=1;i<=n;i++) cout<<b[i]<<" ";
    // cout << endl;
    for (int i = 1; i <= n; i++) if (b[i] < b[i - 1]) {cout << -1 << endl; return;}
    cout << idx << endl;
    for (int i = 0; i < idx; i++) {
        cout << ans[i][0] << " " << ans[i][1] << endl;
    }
}
int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    cin >> t;
    while (t--) {
        solve();
    }

    return 0;
}