/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/


//过15个点，错误贪心

// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题
// #include<functional>//编写内部函数

// #define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1
// #define fi first
// #define se second
// #define mid ((l+r)>>1)

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;

// /*
// 快读代码模板
// a=read<int>(),b=read<ll>(),c=read<__int128>();
// */
// template<typename T>
// void read(T &x) {
// 	x = 0; bool flag(0); char ch = getchar();
// 	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
// 	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
// 	flag ? x = -x : 0;
// }
// template<typename T>
// void write(T x, bool mode = 1) {//mode=1为换行，0为空格
// 	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
// 	do stk[++top] = x % 10, x /= 10; while (x);
// 	while (top) putchar(stk[top--] | 48);
// 	mode ? putchar('\n') : putchar(' ');
// }
// const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
// const ll dinf = 0x7f7f7f7f;
// //const int M=100010;
// const int N = 300010;
// int n;
// ll a[N];
// ll f(vector<ll>& mx){
// 	ll res = mx[1];
// 	// cout<<mx[0]<<"::"<<mx[1]<<endl;
// 	ll l = mx[0] - 1, r = mx[0] + 1;
// 	ll t = mx[1] - 1;
// 	while (l >= 1 && r <= n) {
// 		if (a[l] <= a[r]) {
// 			if (t < a[l]) res += a[l] - t, t = a[l]-1;
// 			else t -= 1;
// 			l -= 1;
// 		} else {
// 			if (t < a[r]) res += a[r] - t, t = a[r]-1;
// 			else t -= 1;
// 			r += 1;
// 		}
// 	}
// 	while (l >= 1) {
// 		if (t < a[l]) res += a[l] - t, t = a[l]-1;
// 		else t -= 1;
// 		l -= 1;
// 	}
// 	// cout<<"r::"<<r<<endl;
// 	// cout<<t<<endl;
// 	while (r <= n) {
// 		// cout<<t<<":t::"<<a[r]<<endl;
// 		if (t < a[r]) res += a[r] - t, t = a[r]-1;
// 		else t -= 1;
// 		r += 1;
// 	}
// 	return res;
// }
// void solve() {
// 	//freopen("a.in","r",stdin);
// 	//freopen("a.out","w",stdout);
// 	cin >> n;
// 	vector<ll> mx(2,0);
// 	for (int i = 1; i <= n; i++) {
// 		cin >> a[i];
// 		if (a[i] > mx[1]) mx[0] = i, mx[1] = a[i];
// 	}
// 	ll res=f(mx);
// 	for(int i=n;i>=1;i--){
// 		if(mx[1]==a[i]){
// 			mx[0]=i;
// 			break;
// 		}
// 	}
// 	res=min(res,f(mx));
// 	cout << res << endl;
// }

// int main() {
// 	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
// 	int t = 1;
// 	//cin>>t;
// 	while (t--) {
// 		solve();
// 	}
// 	return 0;
// }


/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second
#define mid ((l+r)>>1)

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
    x = 0; bool flag(0); char ch = getchar();
    while (!isdigit(ch)) flag = ch == '-', ch = getchar();
    while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
    flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
    x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
    do stk[++top] = x % 10, x /= 10; while (x);
    while (top) putchar(stk[top--] | 48);
    mode ? putchar('\n') : putchar(' ');
}
const int inf=0x3f3f3f3f;//1061109567,是10^9级别的
const ll dinf=0x7f7f7f7f;
//const int M=100010;
const int N=100010;

//前缀后缀分解
int n;
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout); 
   	cin>>n;
   	vector<int> a(n+2,0);
   	vector<int> b(n+2,0);
   	vector<int> c(n+2,0);
   	for(int i=1;i<=n;i++){
   		cin>>a[i];
   		b[i]=n-i+a[i];
   		c[i]=i-1+a[i];
   	}
   	for(int i=1;i<=n;i++){
   		b[i]=max(b[i-1],b[i]);
   	}
   	for(int i=n;i>=1;i--){
   		c[i]=max(c[i],c[i+1]);
   	}
   	int res=2*0x3f3f3f3f;
   	for(int i=1;i<=n;i++){
   		res=min(res,max({a[i],b[i-1],c[i+1]}));
   	}
   	cout<<res<<endl;
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    //cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}