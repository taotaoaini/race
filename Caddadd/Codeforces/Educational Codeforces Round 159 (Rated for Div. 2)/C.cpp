/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/


// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题
// #include<functional>//编写内部函数

// #define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1
// #define fi first
// #define se second
// #define mid ((l+r)>>1)

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;

// /*
// 快读代码模板
// a=read<int>(),b=read<ll>(),c=read<__int128>();
// */
// template<typename T>
// void read(T &x) {
//     x = 0; bool flag(0); char ch = getchar();
//     while (!isdigit(ch)) flag = ch == '-', ch = getchar();
//     while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
//     flag ? x = -x : 0;
// }
// template<typename T>
// void write(T x, bool mode = 1) {//mode=1为换行，0为空格
//     x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
//     do stk[++top] = x % 10, x /= 10; while (x);
//     while (top) putchar(stk[top--] | 48);
//     mode ? putchar('\n') : putchar(' ');
// }
// const int inf=0x3f3f3f3f;//1061109567,是10^9级别的
// const ll dinf=0x7f7f7f7f;
// //const int M=100010;
// const int N=100010;

// int n;
// int gcd(int x,int y){
//     return y==0?x:gcd(y,x%y);
// }
// void solve() {
//     //freopen("a.in","r",stdin);
//     //freopen("a.out","w",stdout); 
//     cin>>n;
//     vector<int> a(n);
//     set<int> st;
//     for(int i=0;i<n;i++){
//         cin>>a[i];
//         st.insert(a[i]);
//     }
//     vector<int> b;
//     sort(a.begin(),a.end());
//     int mx=a.back();
//     for(int i:a){
//         if(mx-i!=0) b.push_back(mx-i);
//     }
//     //有问题
//     if(!b.size()) {
//         cout<<1<<endl;
//         return;
//     }
//     int gc=b[0];
//     for(int i=1;i<b.size();i++){
//         gc=gcd(gc,b[i]);
//     }
//     int res=0;
//     for(int i=1;i<=n;i++){
//         if(!st.count(mx-gc*i)){
//             res=i;
//             break;
//         }
//     }
//     if(res==0) res=n;
//     for(int i:b){
//         res+=i/gc;
//     }
//     cout<<res<<endl;
	
// }

// int main() {
//     ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
//     int t = 1;
//     cin>>t;
//     while (t--) {
//         solve();
//     }

//     return 0;
// }

/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second
#define mid ((l+r)>>1)

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
    x = 0; bool flag(0); char ch = getchar();
    while (!isdigit(ch)) flag = ch == '-', ch = getchar();
    while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
    flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
    x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
    do stk[++top] = x % 10, x /= 10; while (x);
    while (top) putchar(stk[top--] | 48);
    mode ? putchar('\n') : putchar(' ');
}
const int inf=0x3f3f3f3f;//1061109567,是10^9级别的
const ll dinf=0x7f7f7f7f;
//const int M=100010;
const int N=100010;

ll gcd(ll a,ll b){
    return b==0?a:gcd(b,a%b);
}
int n,m;
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout); 
    cin>>n;
    vector<ll> a(n+1);
    a[0]=-1e18;
    ll sum=0;
    for(int i=1;i<=n;i++) cin>>a[i],sum+=a[i];
    sort(a.begin()+1,a.end());
    if(n==1) {cout<<1<<endl;return;}
    ll x=a[2]-a[1];
    for(int i=3;i<=n;i++){
        x=gcd(x,a[i]-a[i-1]);
    }
    cout<<"x::"<<x<<endl;
    for(int i=n;i>=1;i--){
        if(a[i]-a[i-1]!=x){
            // 加上a[n+1],如果a[i]-a[i-1]!=x:说明a[i]-x是可取的
            //如果如果到1了
            sum+=a[i]-x;
            cout<<"a[n+1]::"<<a[i]-x<<endl;
            break;
            //????
        }
    }
    cout<<(a[n]*(n+1)-sum)/x<<endl;

}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}