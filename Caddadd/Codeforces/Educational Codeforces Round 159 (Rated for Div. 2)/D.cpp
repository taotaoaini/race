/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second
#define mid ((l+r)>>1)

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
    x = 0; bool flag(0); char ch = getchar();
    while (!isdigit(ch)) flag = ch == '-', ch = getchar();
    while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
    flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
    x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
    do stk[++top] = x % 10, x /= 10; while (x);
    while (top) putchar(stk[top--] | 48);
    mode ? putchar('\n') : putchar(' ');
}
const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const ll dinf = 0x7f7f7f7f;
//const int M=100010;
const int N = 200010;
string s;
int n, q;
map<pii, vector<int>> mp;
pii f[N];
int dx[4]={0,1,0,-1},dy[4]={1,0,-1,0};
string ss="URDL";
//f(i)=(xi,yi)表示从坐标(0,0)开始执行字符串s的前i条命令所能到达的坐标(（前i个x的偏移量的和与前i个y的偏移量的和)
//f(i)=f(i-1)+s[i]
//f(0)=(0,0)
//查询(x,y,l,r)
//将s的区间[l,r]反转s’，再对s’求f(i)，是否经过(x,y)===1<=i<=n::f(i)=(x,y),时间复杂度：O(n*q)
//能否只用s求得的f(i)来处理每一个询问

//s'===[0,l-1],[l,r],[r+1,n]
//对[0,l-1],[r+1,n]没有影响
//map<pair<int,int>,vector<int>> mp;//存储(x,y)第一次出现的i和最后一次出现的i

//定义g(i)=(xi,yi)表示s[i,n]中x的偏移量的和与y的偏移量的和
//g(i)=g(i+1)+s[i]
//f(i)=f(n)-f(i-1)
//l<=i<=r，可以理解从点f(l-1)走i-(l-1)
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    cin >> n >> q;
    cin >> s;
    s = "" + s;
    mp[{0,0}].push_back(0);
    for(int i=1;i<=n;i++){
        f[i]=f[i-1];
        for(int j=0;j<4;j++){
            if(s[i]==ss[j]){
                f[i].fi+=dx[j];
                f[i].se+=dy[j];
            }
        }
        mp[f[i]].push_back(i);
    }
    for (int i = 1, x, y, l, r; i <= q; i++) {
        cin << x>>y >> l >> r;
        //pass
        pii t=make_pair(x,y);
        if(mp.count(t)&&mp[t][0]<=l-1){
            cout<<"YES"<<endl;
        }else if(mp.count(t)&&mp[t][0]>=r+1){
            cout<<"YES"<<endl;
        }else{

        }
    }
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    //cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}