// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>

// #define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;

// const int N=1e5+10;
// int n,color[N],son[N],sz[N],cnt[N];
// ll ans[N],s;
// int mx;
// vectro<int> e[N];
// void dfs(int u,int fa){
//  sz[u]=1;
//  for(int v:e[x]){
//      if(v==fa) continue;
//      dfs(v,u);
//      sz[u]+=sz[v];
//      if(sz[son[u]]<sz[v]) son[u]=v;
//  }
// }
// void update(int u,int fa,int k,int s){
//  int c=color[u];
//  cnt[c]+=k;
//  if(mx<cnt[c]) mx=cnt[c],s=c;
//  else if(cnt[c]==mx) s+=c;
//  for(int v:e[u]){
//      if(v==fa||v==s) continue;
//      update(u,v,k,0);
//  }
// }
// void dfs1(int u,int fa,int op){
//  for(int v:e[u]){
//      if(v==fa||v=son[u]) continue;
//      dfs1(v,u,0);
//  }
//  if(son[u]) dfs(son[u],u,1);
//  update(u,fa,1,son[u]);//更新轻儿子
//  ans[x]=s;
//  if(!op) update(x,fa,-1,0),s=mx=0;
// }
// void solve(){
//  cin>>n;
//  for(int i=1;i<=n;i++) cin>>color[i];
//  for(int i=1;i<n;i++){
//      int x,y;
//      cin>>x>>y;
//      e[x].push_back(y);
//      e[y].push_back(x);
//  }
//  dfs(1,0);
//  dfs1(1,0,1);
//  for(int i=1;i<=n;i++) cout<<ans[i]<<" ";
//  cout<<endl;
// }
// int main() {
//     ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
//     solve();

//     return 0;
// }



// #include <iostream>
// #include <cstring>
// #include <algorithm>

// using namespace std;
// #define int long long
// const int N = 1e5 + 10, M = 2 * N, INF = 1e9;
// int n, m, a[N];
// int h[N], e[M], ne[M], idx;
// int son[N], l[N], r[N], rw[N], sz[N], ts;
// int cnt[N], sum, maxv;
// int ans[N];

// void add(int a, int b)
// {
//     e[idx] = b, ne[idx] = h[a], h[a] = idx ++;
// }

// void update(int u, int v)
// {
//     cnt[a[u]] += v;
//     if(cnt[a[u]] > maxv) maxv = cnt[a[u]], sum = a[u];
//     else if(cnt[a[u]] == maxv) sum += a[u];
// }

// void dfs1(int u, int fa)
// {
//     sz[u] = 1, l[u] = ++ ts, rw[ts] = u;
//     for(int i = h[u]; ~i ; i = ne[i])
//     {
//         int j = e[i];
//         if(j == fa) continue;
//         dfs1(j, u);
//         sz[u] += sz[j];
//         if(sz[j] > sz[son[u]]) son[u] = j;
//     }
//     r[u] = ts;
// }

// void dfs2(int u, int fa, int del)
// {
//     for(int i = h[u]; ~i ; i = ne[i])
//     {
//         int j = e[i];
//         if(j == son[u] || j == fa) continue;
//         dfs2(j, u, 1);
//     }

//     if(son[u]) dfs2(son[u], u, 0);

//     for(int i = h[u]; ~i ; i = ne[i])
//     {
//         int j = e[i];
//         if(j == son[u] || j == fa) continue;
//         for(int k = l[j] ; k <= r[j] ; k ++ )
//             update(rw[k], 1); // 子树合并
//     }
//     update(u, 1);
//     ans[u] = sum;

//     if(del)
//     {
//         for(int k = l[u]; k <= r[u] ; k ++ )
//             update(rw[k], -1);
//         maxv = sum = 0;
//     }
// }

// signed main()
// {
//     ios::sync_with_stdio(0), cin.tie(0);
//     cin >> n;
//     memset(h, -1, sizeof h);
//     for(int i = 1; i <= n ; i ++ ) cin >> a[i];
//     for(int i = 1 ; i < n ; i ++ )
//     {
//         int a, b;
//         cin >> a >> b;
//         add(a, b), add(b, a);
//     }

//     dfs1(1, 0);
//     dfs2(1, 0, 1);
//     for(int i = 1; i <= n ; i ++ ) cout << ans[i] << " ";
//     cout << endl;
// }



// //题意：给出一棵 n 个结点的树，每个结点都有一种颜色编号，求该树中每棵子树里的出现次数最多的颜色的编号和。
// //思路：dsu on tree，对树做轻重链剖分，先跑轻儿子，最后一遍跑重儿子的时候不用清空数组，复杂度降低为O(nlogn)
// #include<bits/stdc++.h>
// using namespace std;
// typedef long long LL;
// const int maxn = 1e5+10;

// LL c[maxn], ans[maxn];

// vector<int>G[maxn];
// LL siz[maxn], son[maxn];
// void dsu(int x, int fa){ //轻重链剖分
//  siz[x] = 1;
//  for(int to : G[x]){
//      if(to==fa)continue;
//      dsu(to,x);
//      siz[x] += siz[to];
//      if(siz[to]>siz[son[x]])son[x]=to;
//  }
// }

// LL cnt[maxn], tmp, tmpv, Son;
// void add(int x, int fa, int val){//统计x节点的贡献
//  cnt[c[x]] += val;
//  if(cnt[c[x]]>tmpv)tmpv=cnt[c[x]],tmp=c[x];
//  else if(cnt[c[x]]==tmpv)tmp +=c[x];
//  for(int to : G[x]){
//      if(to==fa)continue;
//      if(to==Son)continue;//统计x节点的轻儿子时不统计重儿子
//      add(to,x,val);
//  }
// }
// void dfs(int x, int fa, int op){//遍历每个节点
//  for(int to : G[x]){
//      if(to==fa)continue;
//      if(to!=son[x])dfs(to,x,0);//暴力统计轻边的贡献，op=0表示递归完成后消除对该点的影响
//  }
//  if(son[x])dfs(son[x],x,1), Son=son[x];//统计重儿子的贡献，不消除影响
//  add(x,fa,1); Son=0;   //暴力统计所有轻儿子的贡献
//  ans[x] = tmp;
//  if(!op)add(x,fa,-1),tmp=0,tmpv=0;//消除对该点的影响
// }

// int main(){
//  ios::sync_with_stdio(false);
//  int n;  cin>>n;
//  for(int i = 1; i <= n; i++)cin>>c[i];
//  for(int i = 1; i <= n-1; i++){
//      int x, y;  cin>>x>>y;
//      G[x].push_back(y);
//      G[y].push_back(x);
//  }
//  dsu(1,0);
//  dfs(1,0,0);
//  for(int i = 1; i <= n; i++)
//      cout<<ans[i]<<" ";
//  return 0;
// }


// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>

// #define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;

// const int N=
// int main() {
//     ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);


//     return 0;
// }








// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>

// #define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;

// const int N=2e5+10;
// vector<int>e[N];
// int col[N],son[N],sz[N],ma,Son;
// ll sum=0,ans[N],cnt[N];
// //cnt记载个数，son记载重儿子
// void dfs1(int u,int fa)
// {
//     sz[u]=1;
//     for(auto v:e[u])
//     {
//         if(v==fa)continue;
//         dfs1(v,u);
//         sz[u]+=sz[v];
//         if(sz[v]>sz[son[u]])son[u]=v;
//     }
// }
// /*var==1统计答案数目函数，var==-1将轻儿子的数目减去，
//  * 如果var==-1代表这个儿子是轻儿子，此时需要将这个儿子的贡献减去（包括该儿子中的重儿子）
//  * 如果var==1代表统计这个儿子的贡献
//  * 在统计某点答案时，只有该点下的重儿子不用统计
//  * */
// void update(int u,int fa,int var)
// {
//     int c=col[u];
//     cnt[c]+=var;
//     if(cnt[c]>ma) ma=cnt[c],sum=col[u];
//     else if(cnt[c]==ma) sum+=c;
//     for(auto v:e[u])
//     {
//         if(v==fa||v==Son)continue;
//         update(v,u,var);
//     }
// }
// /*首先去完成轻儿子的答案统计，如果存在重儿子则最后统计重儿子
//  * 该点完成后要判断这一点是父亲的轻儿子还是重儿子，如果是轻儿子则将贡献删掉，重儿子则不用
//  * */
// void dfs2(int u,int fa,int op)
// {
//     for(auto v:e[u])
//     {
//         if(v==fa||v==son[u])continue;
//         dfs2(v,u,0);
//     }
//     if(son[u]) dfs2(son[u],u,1);
//     Son=son[u];
//     update(u,fa,1);
//     Son=0;
//     ans[u]=sum;
//     if(!op)update(u,fa,-1),sum=ma=0;
// }
// void solve()
// {
//    int n;
//    cin>>n;
//    for(int i=1;i<=n;i++)cin>>col[i];
//    for(int i=1;i<n;i++)
//    {
//        int x,y;
//        cin>>x>>y;
//        e[x].push_back(y);
//        e[y].push_back(x);
//    }
//    dfs1(1,0);
//    dfs2(1,0,1);
//    for(int i=1;i<=n;i++)cout<<ans[i]<<" ";
// }
// signed main ()
// {
//     std::ios::sync_with_stdio(false);
//     std::cin.tie(0);
//     std::cout.tie(0);
//     int t=1;
//     // cin>>t;
//     while(t--) solve();
// }


/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题

// #define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1
// #define fi first
// #define se second
// #define mid ((l+r)>>1)

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;

// /*
// 快读代码模板
// a=read<int>(),b=read<ll>(),c=read<__int128>();
// */
// template<typename T>
// void read(T &x) {
//     x = 0; bool flag(0); char ch = getchar();
//     while (!isdigit(ch)) flag = ch == '-', ch = getchar();
//     while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
//     flag ? x = -x : 0;
// }
// template<typename T>
// void write(T x, bool mode = 1) {//mode=1为换行，0为空格
//     x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
//     do stk[++top] = x % 10, x /= 10; while (x);
//     while (top) putchar(stk[top--] | 48);
//     mode ? putchar('\n') : putchar(' ');
// }
// const int inf = 0x3f3f3f3f;
// const ll dinf = 0x7f7f7f7f;
// //const int M=100010;
// const int N = 100010;
// int n, m;
// int color[N];
// int ls[N * 50], rs[N * 50], tot, root[N];
// ll sum[N * 50], mx[N * 50];
// //mx:主导颜色数，sum：主导颜色的编号和
// ll ans[N];
// vector<int> e[N];
// void pushup(int u) {//上传
//     if (mx[ls[u]] > mx[rs[u]]) {
//         mx[u] = mx[ls[u]];
//         sum[u] = sum[ls[u]];
//     } else if (mx[ls[u]] < mx[rs[u]]) {
//         mx[u] = mx[rs[u]];
//         sum[u] = sum[rs[u]];
//     } else {
//         sum[u] = sum[ls[u]] + sum[rs[u]];
//         mx[u] = mx[ls[u]];
//     }
// }
// void change(int &u, int l, int r, int p, int k) {//点修：时间复杂度：O（2*nlogn）
//     if (!u) u = ++tot;
//     if (l == r) {mx[u] += k; sum[u] = l; return;}
//     if (p <= mid) change(ls[u], l, mid, p, k);
//     else change(rs[u], mid + 1, r, p, k);
//     pushup(u);
// }
// int merge(int x, int y, int l, int r) {//合并：时间复杂度：O（2*nlogn）
//     if (!x || !y) return x + y;
//     if (l == r) {mx[x] += mx[y]; sum[x] = l; return x;}
//     ls[x] = merge(ls[x], ls[y], l, mid);
//     rs[x] = merge(rs[x], rs[y], mid + 1, r);
//     pushup(x);
//     return x;
// }
// void dfs(int u, int father) {//递归合并
//     for (int v : e[u]) {
//         if (v == father) continue;
//         dfs(v, u);
//         root[u] = merge(root[u], root[v], 1, N);
//     }
//     change(root[u], 1, N, color[u], 1);
//     ans[u] = sum[root[u]];
//     //因为是共用一个线段树的，所以求出当前子树的答案，需要及时保存，防止被破坏
// }
// void solve() {
//     //freopen("a.in","r",stdin);
//     //freopen("a.out","w",stdout);
//     cin >> n;
//     for (int i = 1; i <= n; i++) {
//         cin >> color[i];
//     }
//     for (int i = 1, x, y; i < n; i++) {
//         cin >> x >> y;
//         e[x].push_back(y);
//         e[y].push_back(x);
//     }
//     dfs(1, 0);
//     for (int i = 1; i <= n; i++) {
//         cout << ans[i] << " ";
//     }
//     cout << endl;
// }

// int main() {
//     ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
//     int t = 1;
//     //cin>>t;
//     while (t--) {
//         solve();
//     }

//     return 0;
// }


/*
i64=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
u64=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;

str+str
to_string(int)
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls u<<1
#define rs u<<1|1
#define fi first
#define se second
#define mid ((l+r)/2)

using i8 = signed char;
using u8 = unsigned char;
using i16 = signed short int;
using u16 = unsigned short int;
using i32 = signed int;
using u32 = unsigned int;
using f32 = float;
using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using i128 = __int128_t;
using u128 = __uint128_t;
using f128 = long double;

using pii = std::pair<int,int>;
using pll = std::pair<long long,long long>;
using pci=std::pair<char,int>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<int128>();
*/

template<typename T>
void read(T &x) {
    x = 0; bool flag(0); char ch = getchar();
    while (!isdigit(ch)) flag = ch == '-', ch = getchar();
    while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
    flag ? x = -x : 0;
}

template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
    x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
    do stk[++top] = x % 10, x /= 10; while (x);
    while (top) putchar(stk[top--] | 48);
    mode ? putchar('\n') : putchar(' ');
}

priority_queue<int,vector<int>,less<int> > mx_hp;//大顶堆
priority_queue<int,vector<int>,greater<int> > mi_hp;//小顶堆

//int[]:6.7e7
const int inf=0x3f3f3f3f;//1061109567,是10^9级别的
const i64 dinf=0x3f3f3f3f3f3f3f3f;//4.62*10^18级别的
const int mod=1e9+7;
const int M=100010;
const int N=100010;


void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout); 
    
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    cout << fixed << setprecision(12);
    
    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}