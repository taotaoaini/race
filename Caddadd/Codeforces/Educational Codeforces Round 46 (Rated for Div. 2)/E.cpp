/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
	//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}
const int inf = 0x3f3f3f3f;
const ll dinf = 0x7f7f7f7f;
const int N = 300010, M = 600010;
int n, m;
struct edge {int v, ne;} e[M];
vector<int> ne[N];
int h[N], idx = 1; //2,3开始配对
int dfn[N], low[N], tot;
stack<int> stk;
int dcc[N], cnt;
int bri[M];
int d1[N], d2[N], d = 0;

void add(int a, int b) {
	e[++idx].v = b; e[idx].ne = h[a];
	h[a] = idx;
}
void tarjan(int x, int in_edg) {
	dfn[x] = low[x] = ++tot;
	stk.push(x);
	for (int i = h[x]; i; i = e[i].ne) {
		int y = e[i].v;
		if (!dfn[y]) { //若y尚未访问
			tarjan(y, i);
			low[x] = min(low[x], low[y]);
			if (low[y] > dfn[x])
				bri[i] = bri[i ^ 1] = true;
		} else if (i != (in_edg ^ 1)) //不是反边
			low[x] = min(low[x], dfn[y]);
	}
	if (dfn[x] == low[x]) {
		++cnt;
		while (1) {
			int y = stk.top(); stk.pop();
			dcc[y] = cnt; //记录eDCC
			if (y == x)break;
		}
	}
}
void dfs(int u, int fa) {
	d1[u] = d2[u] = 0;
	for (int v : ne[u]) {
		if (v == fa) continue;
		dfs(v, u);
		int t = d1[v] + 1;
		if (t > d1[u]) {
			d2[u] = d1[u], d1[u] = t;
		} else if (t > d2[u]) {
			d2[u] = t;
		}
	}
	d = max(d, d1[u] + d2[u]);
}
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	cin >> n >> m;
	for (int i = 1, x, y; i <= m; i++) {
		cin >> x >> y;
		add(x, y);
		add(y, x);
	}
	tarjan(1, 0);
	for (int u = 1; u <= n; u++) {
		for (int i = h[u]; i; i = e[i].ne) {
			int v = e[i].v;
			if (dcc[u] != dcc[v]) {
				ne[dcc[u]].push_back(dcc[v]);
				ne[dcc[v]].push_back(dcc[u]);
			}
		}
	}
	for (int i = 1; i <= cnt; i++) {
		sort(ne[i].begin(), ne[i].end());
		ne[i].erase(unique(ne[i].begin(), ne[i].end()), ne[i].end());
	}
	dfs(1, 0);
	cout << d << endl;
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}