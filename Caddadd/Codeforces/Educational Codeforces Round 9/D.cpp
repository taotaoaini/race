#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
//这题值域很小，从值域出发，暴力枚举值域，mlogm
//空数组的 LCM 等于 1
const int N=1000010;
int a[N],b[N],c[N],n,m;
int res[2]{0,1};
void solve(){
	cin>>n>>m;
	int x;
	for(int i=1;i<=n;i++){
		cin>>a[i];
		if(a[i]>m) continue; 
		b[a[i]]++;
	}
	for(int i=1;i<=m;i++){
		if(!b[i]) continue;
		for(int j=i;j<=m;j+=i){
			c[j]+=b[i];
		}
	}
	for(int i=1;i<=m;i++){
		if(c[i]>res[0]){
			res[0]=c[i],res[1]=i;
		}
	}
	cout<<res[1]<<" "<<res[0]<<endl;
	for(int i=1;i<=n;i++){
		if(res[1]%a[i]==0){
			cout<<i<<" ";
		}
	}
	cout<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}