#include<iostream>
#include<cstring>
#include<vector>
using namespace std;
using LL = long long;

int main(){

    cin.tie(0);
    cout.tie(0);
    ios::sync_with_stdio(0);

    const string s = "vika";

    int T;
    cin >> T;
    while(T--){
        int n, m;
        cin >> n >> m;
        vector<string> g(m);
        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                char c;
                cin >> c;
                g[j] += c;
            }
        }
        int ans = 0;
        for(int i = 0; i < m && ans != 4; i++){
            if (g[i].find(s[ans]) != string::npos){
                ans++;
            }
        }
        cout << (ans == 4 ? "Yes" : "No") << '\n';
    }

}