/*差分数组*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<stack>
#include<unordered_map>
#include<unordered_set>
using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef unsigned long long ull;
void solve(){
    int n;cin>>n;
    vector<int>a(n+1);
    for(int i=1;i<=n;i++) cin>>a[i];
    if(a[1]!=n){
        cout<<"NO\n";
        return;
    }
    vector<int>b(n+2);
    for(int i=1;i<=n;i++){
        b[1]++,b[a[i]+1]--;
    }
    for(int i=1;i<=n;i++) b[i]+=b[i-1];
    bool ok=1;
    for(int i=1;i<=n;i++){
        if(a[i]!=b[i]) ok=0;
    }
    if(ok) cout<<"YES\n";
    else cout<<"NO\n";
}
int main() {
	ios::sync_with_stdio(false);
    int T;
    cin>>T;
    while(T--){
        solve();
    }
    return 0;
}