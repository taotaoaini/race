// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<stack>
// #include<unordered_map>
// #include<unordered_set>
// using namespace std;

// typedef long long ll;
// typedef pair<int,int> pii;
// typedef unsigned long long ull;

// int main() {
// 	ios::sync_with_stdio(false);
// 	auto check=[&](ll x,ll b)->bool{
// 		return x*(x-1)-2*b>=0;
// 	};
// 	int T;
// 	cin>>T;
// 	while(T--){
// 		ll n;
// 		cin>>n;
// 		ll l=2,r=2e9+1;
// 		ll res=-1;
// 		while(l<=r){
// 			ll mid=(l+r)>>1;
// 			if(check(mid,n)) {res=mid;r=mid-1;}
// 			else l=mid+1;
// 		}
// 		res--;
// 		res=res+n-res*(res-1)/2;
// 		cout<<res<<endl;
// 	}
//     return 0;
// }


// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<stack>
// #include<unordered_map>
// #include<unordered_set>
// using namespace std;

// typedef long long ll;
// typedef pair<int,int> pii;
// typedef unsigned long long ull;
// void solve(){
// 	 ll n;cin>>n;
//     ll q=sqrt(2*n);q++;
//     while(q*(q-1)/2>n)q--;
//     cout<<q+n-q*(q-1)/2<<"\n";
// }
// int main() {
// 	ios::sync_with_stdio(false);
// 	int T;
// 	cin>>T;
// 	while(T--){
// 	solve();
// 	}
//     return 0;
// }

#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<stack>
#include<unordered_map>
#include<unordered_set>
using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef unsigned long long ull;

int main() {
	// ios::sync_with_stdio(false);
	int T;
	cin>>T;
	while(T--){
		int a[2],b[2],c[2];
		for(int i=0;i<2;i++)
		cin>>a[i]>>b[i]>>c[i];
		int x=a[0]*a[1]+b[0]*b[1]+c[0]*c[1];
		if(x==100) cout<<"Sorry,NoBruteForce"<<endl;
		else{
			printf("%.6lf\n",100./(100-x));
		}
	}
    return 0;
}