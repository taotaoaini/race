#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<stack>
using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef unsigned long long ull;

int main() {
	ios::sync_with_stdio(false);
	int n;
	cin>>n;
	while(n--){
		int a,b,c;
		cin>>a>>b>>c;
		if(a+b>=10||a+c>=10||b+c>=10) cout<<"YES"<<endl;
		else cout<<"NO"<<endl;
	}
    return 0;
}