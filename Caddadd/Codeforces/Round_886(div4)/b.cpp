#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<stack>
using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef unsigned long long ull;

int main() {
	ios::sync_with_stdio(false);
	int t;
	cin>>t;
	while(t--){
		int n;
		int res[]{-1,0};
		cin>>n;
		for(int i=1;i<=n;i++){
			int a,b;
			cin>>a>>b;
			if(a<=10&&b>res[1]) res[0]=i,res[1]=b; 
		}
		cout<<res[0]<<endl;
	}
    return 0;
}