#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<stack>
using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef unsigned long long ull;

int main() {
	ios::sync_with_stdio(false);
	int T;
	cin>>T;
	while(T--){
	vector<vector<char>> a(8,vector<char>(8));
	for(int i=0;i<8;i++){
		for(int j=0;j<8;j++){
			cin>>a[i][j];
		}
	}
	string s;
	for(int i=0;i<8;i++){
		for(int j=0;j<8;j++){
			if(a[i][j]!='.'){
				s+=a[i][j];
			for(int k=j+1;k<8;k++){
				if(a[i][k]!='.') s+=a[i][k];
				else break;
			}	
			}
		}
	}
	cout<<s<<endl;
}
return 0;
}