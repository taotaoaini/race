#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<stack>
using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef unsigned long long ull;

int main() {
	ios::sync_with_stdio(false);
	int t;
	cin>>t;
	while(t--){
		int a,b,c;
		cin>>a>>b>>c;
		if(c&1){
			if(a<b) cout<<"Second"<<endl;
			else cout<<"First"<<endl;
		}else{
			if(a>b) cout<<"First"<<endl;
			else cout<<"Second"<<endl;
		}
	}
    return 0;
}