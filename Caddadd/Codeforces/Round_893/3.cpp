#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<stack>
using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef unsigned long long ull;
const int N=100010;
int main() {
	ios::sync_with_stdio(false);
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		vector<int> res;
		bool vis[N]{0};
		for(int i=1;i<=n;i++){
			if(vis[i]) continue;
			res.push_back(i);
			vis[i]=1;l;
			int x=i;
			while(x*2<=n) res.push_back(x*2),vis[x*2]=1,x*=2;
		}
		for(int i=0;i<n;i++){
			cout<<res[i]<<" ";
		}
		cout<<endl;
	}
    return 0;
}

