// /*
// ll=9.22*10^18 ((1LL << 63) - 1)
// int=2.1*10^9 ((1<<32)-1)
// ull=(1.844*10^19)

// double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
// double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
// double round(double x);//四舍五入,-2.7->-3,-2.2->-2

// fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
// cout<<fixed<<setprecision(3)<<1.2000;//->1.2

// 二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
// 三行四列的数组:arr[1][2]=arr[1*4+2]

// % + -
// << >>
// > >= < <=
// != & ^ | && ||

// string 从下标1开始读入
// char s[N];
// cin>>s+1;
// */
// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题

// #define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1
// #define fi first
// #define se second

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;

// /*
// 快读代码模板
// a=read<int>(),b=read<ll>(),c=read<__int128>();
// */
// template<typename T>
// void read(T &x) {
//     x = 0; bool flag(0); char ch = getchar();
//     while (!isdigit(ch)) flag = ch == '-', ch = getchar();
//     while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
//     flag ? x = -x : 0;
// }
// template<typename T>
// void write(T x, bool mode = 1) {
//     //mode=1为换行，0为空格
//     x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
//     do stk[++top] = x % 10, x /= 10; while (x);
//     while (top) putchar(stk[top--] | 48);
//     mode ? putchar('\n') : putchar(' ');
// }
// const int inf=0x3f3f3f3f;
// const ll dinf=0x7f7f7f7f;
// const int N=50010;
// struct edge{
//  int v,ne;
// }e[N*2];
// int h[N],idx;
// int f[N][510];
// int n,m;
// ll res=0;
// void add(int u,int v){
//  e[++idx]={v,h[u]};
//  h[u]=idx;
// }
// void dfs(int u,int fa){
//  f[u][0]=1;
//  for(int i=h[u];i;i=e[i].ne){
//      int v=e[i].v;
//      if(v==fa) continue;
//      dfs(v,u);
//      for(int j=0;j<m;j++){
//          res+=f[v][j]*f[u][m-j-1];
//      }
//      for(int j=0;j<m;j++){
//          f[u][j+1]+=f[v][j];
//      }
//  }
// }
// void solve() {
//     //freopen("a.in","r",stdin);
//     //freopen("a.out","w",stdout);
//     cin>>n>>m;
//     for(int i=1,x,y;i<n;i++){
//      cin>>x>>y;
//      add(x,y);
//      add(y,x);
//     }
//     dfs(1,0);
//     cout<<res<<endl;
// }

// int main() {
//     ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
//     int t = 1;
//     //cin>>t;
//     while (t--) {
//         solve();
//     }

//     return 0;
// }



/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/

/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
    x = 0; bool flag(0); char ch = getchar();
    while (!isdigit(ch)) flag = ch == '-', ch = getchar();
    while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
    flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
    //mode=1为换行，0为空格
    x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
    do stk[++top] = x % 10, x /= 10; while (x);
    while (top) putchar(stk[top--] | 48);
    mode ? putchar('\n') : putchar(' ');
}
const int inf = 0x3f3f3f3f;
const ll dinf = 0x7f7f7f7f;
//const int N=100010;


const int N = 10010;
const int INF = 10000010;
struct edge {
    int v, w, ne;
} e[N * 2];
int h[N], idx;
int del[N], sz[N], mxs, sum, root;

int dis[N], d[N], cnt;
int ans[N], q[INF], f[INF];
int n, m;

void add(int u, int v, int w) {
    e[++idx] = {v, w, h[u]};
    h[u] = idx;
}
//寻找重心
void getroot(int u, int fa) {
    sz[u] = 1;
    int s = 0;
    for (int i = h[u]; i; i = e[i].ne) {
        int v = e[i].v;
        if (v == fa || del[v]) continue;
        getroot(v, u);
        sz[u] += sz[v];
        s = max(s, sz[v]);
    }
    s = max(s, sum - sz[u]);
    if (s < mxs) mxs = s, root = u;
}
//记录各个点到根节点的距离
void getdis(int u, int fa) {
    dis[++cnt] = d[u];
    for (int i = h[u]; i; i = e[i].ne) {
        int v = e[i].v;
        if (v == fa || del[v]) continue;
        d[v] = d[u] + e[i].w;
        getdis(v, u);
    }
}
void calc(int u) {
    del[u] = judge[0] = 1;
    int p = 0;
    for (int i = h[u]; i; i = e[i].ne) {
        int v = e[i].v;
        if (del[v]) continue;
        cnt = 0;
        d[v] = e[i].w;
        //求出子树v的各点到u的距离
        getdis(v, u);
        for (int j = 1; j <= cnt; j++) {
            for (int k = 1; k <= m; k++) {
                if (k >= dis[j])
                    f[k] += f[k - dis[j]];
            }
        }
        //记录合法路径
        for (int j = 1; j <= cnt; j++) {
            if (dis[j] < INF)
                q[++p] = dis[j], judge[dis[j]] = 1;
        }
    }
    for (int i = 1; i <= p; i++) judge[q[i]] = 0;
}
void divide(int u) {
    calc(u);
    for (int i = h[u]; i; i = e[i].ne) {
        int v = e[i].v;
        if (del[v]) continue;
        mxs = sum = sz[v];
        getroot(v, 0);
        divide(root);
    }
}

void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    cin >> n >> m;
    for (int i = 1, x, y, w; i < n; i++) {
        cin >> x >> y >> w;
        add(x, y, w);
        add(y, x, w);
    }
    mxs = sum = n;
    getroot(1, 0);
    getroot(root, 0);
    divide(root);
    cout << res << endl;
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    //cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}