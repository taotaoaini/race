#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define ls u<<1
#define rs u<<1|1

using namespace std;

priority_queue<int,vector<int>,less<int> > mx_hp;//大顶堆
priority_queue<int,vector<int>,greater<int> > mi_hp;//小顶堆

//i64=9.22*10^18 ((1LL << 63) - 1)
//int=2.1*10^9 ((1<<32)-1)
//int[]:6.7e7
//64mb:4*1e6 (64*1024*1024)/4
//1 bytes=8 bit

// queue:push,pop,front,back
// deque:push_back,push_front,front,back,pop_front,pop_back
// priority_queue: top,push,pop
// stack: push,top,pop
// <algorithm> fill_n    fill_n(a,0);   fill fill(a.begin(),a.end(),1); [:)
// emplace()
//1e8:5.7e6 1e7:6.6e5 1e6:7.8e4 1e4:1229
//2^30:1e9
//    int b=__builtin_popcount(7); //7(111):3，1的个数 
//    int c=__builtin_ctz(8);  //8(1000):3，末尾0 
//    int d=__builtin_clz(8); //8(1000):28,一共32位,前导0 
//    int x=stoi(string s)
const int M=100010;
const int N=100010;

// --start--
#define fi first
#define se second
using pii = std::pair<int,int>;
using pll = std::pair<long long,long long>;
using pci=std::pair<char,int>;
using i64 = signed long long;
const int mod=1e9+7;
const int inf=0x3f3f3f3f;// 10^9级别的
const i64 dinf=0x3f3f3f3f3f3f3f3f;//4.62*10^18级别的


class Solution {
public:
    long long countAlternatingSubarrays(vector<int>& a) {
        i64 res=0;
        int n=a.size();
        int l=0,r=0;
        while(r<n){
            while(r<n&&a[r]!=a[r-1]) r+=1;
            // r=n || !r
            res+=(r-l);
            l=r;
        }
        return res;
    }
};