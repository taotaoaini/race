#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//const int inf=0x3f3f3f3f;
const int N=1000010;
int a[3]{1,5,11};
int f[N];
void solve(){
	int n;
	cin>>n;
	//f[i]:表示拼凑出i元，需要的最少票数
	memset(f,-1,sizeof(f));
	f[0]=0;
	for(int i=1;i<=n;i++){
		for(int j=0;j<3;j++){
			if(i-a[j]>=0&&f[i-a[j]]!=-1){
				f[i]=f[i]==-1?f[i-a[j]]+1:min(f[i],f[i-a[j]]+1);
				// f[i]=min(f[i],f[i-a[j]]+1);
			}
		}
		// cout<<f[i]<<endl;
	}
	cout<<f[n]<<endl;
}

int main(){
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	
	int t=1;
	//cin>>t;
	while(t--){
		solve();
	}

	return 0;
}