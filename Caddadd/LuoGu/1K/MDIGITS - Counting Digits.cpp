#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
string s;
ll ans[10];
ll f[50][20];
int n;
ll dfs(int p,ll cnt,int k,bool is_limit,bool is_num){
	if(p==n){
		// cout<<"#sum:"
		return cnt;
	}
	if(is_num&&!is_limit&&f[p][cnt]!=-1){
		return f[p][cnt];
	}
	ll res=0;
	if(!is_num) res+=dfs(p+1,cnt,k,false,is_num);
	int up=is_limit?s[p]-'0':9;
	int low=is_num?0:1;
	for(int i=low;i<=up;i++){
		res+=dfs(p+1,cnt+(k==i),k,is_limit&&i==up,true);
	}
	if(!is_limit&&is_num) f[p][cnt]=res;
	return res; 
}
ll sol(ll x,int k){
	s=to_string(x);
	n=s.size();
	memset(f,-1,sizeof(f));
	return dfs(0,0,k,true,false);
}
void solve(){
	ll a,b;
	while(cin>>a>>b&&(a||b)){
	if(a>b) swap(a,b);
	for(int i=0;i<=9;i++){
	 ans[i]=sol(b,i)-sol(a-1,i);
	}
	for(int i=0;i<=9;i++) cout<<ans[i]<<" ";
	cout<<endl;
	}
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}
