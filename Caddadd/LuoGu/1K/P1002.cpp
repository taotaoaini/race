/*DFS*/
// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题

// #define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1
// #define fi first
// #define se second

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;
// //__int128的输入
// inline __int128 read(){
//     __int128 x=0,f=1;
//     char ch=getchar();
//     while(ch<'0'||ch>'9'){
//         if(ch=='-') f=-1;
//         ch=getchar();
//     }
//     while(ch>='0'&&ch<='9'){
//         x=x*10+ch-'0';
//         ch=getchar();
//     }
//     return x*f;
// }
// //__int128的输出
// inline void print(__int128 x){
//     if(x<0){
//         putchar('-');
//         x=-x;
//     }
//     if(x>9)
//         print(x/10);
//     putchar(x%10+'0');
// }
// ll f[30][30];
// int dis[9][2]{{0,0},{2,1},{1,2},{-1,2},{-2,1},{-2,-1},{-1,-2},{1,-2},{2,-1}};
// int n,m,c,d;
// bool check(int i,int j){
// 	return i>=0&&i<=n&&j>=0&&j<=m;
// }
// ll dfs(int i,int j){
//     // cout<<i<<" "<<j<<endl;
// 	if(i==n&&j==m) return 1;
// 	if(f[i][j]!=-1) return f[i][j];
// 	for(auto &[x,y]:dis){
// 		int x1=c+x;
// 		int y1=d+y;
// 		if(check(x1,y1)&&x1==i&&y1==j) return f[i][j]=0;
// 	}
// 	ll res=0;
// 	if(i+1<=n) res+=dfs(i+1,j);
// 	if(j+1<=m) res+=dfs(i,j+1);
//     // cout<<res<<endl;
// 	return f[i][j]=res;
// }
// void solve(){
// 		cin>>n>>m>>c>>d;
// 		if(n==c&&m==d) {cout<<0<<endl;return;}
// 		memset(f,-1,sizeof(f));
// 		cout<<dfs(0,0)<<endl;
//         // cout<<f[1][0]<<endl;
// }
// int main() {
//     ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
//     int t=1;
//     //cin>>t;
//     while(t--){
//     	solve();
//     }
    
//     return 0;
// }



#include<iostream>
#include<cstring>
#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;

const int fx[] = {0, -2, -1, 1, 2, 2, 1, -1, -2};
const int fy[] = {0, 1, 2, 2, 1, -1, -2, -2, -1};
//马可以走到的位置

int bx, by, mx, my;
ll f[40][40];
bool s[40][40]; //判断这个点有没有马拦住
int main(){
    scanf("%d%d%d%d", &bx, &by, &mx, &my);
    bx += 2; by += 2; mx += 2; my += 2;
    //坐标+2以防越界
    f[2][1] = 1;//初始化
    s[mx][my] = 1;//标记马的位置
    for(int i = 1; i <= 8; i++) s[mx + fx[i]][my + fy[i]] = 1;
    for(int i = 2; i <= bx; i++){
        for(int j = 2; j <= by; j++){
            if(s[i][j]) continue; // 如果被马拦住就直接跳过
            f[i][j] = f[i - 1][j] + f[i][j - 1];
            //状态转移方程
        }
    }
    printf("%lld\n", f[bx][by]);
    return 0;
} 