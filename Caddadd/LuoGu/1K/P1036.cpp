/*175ms*/
/*#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
int n,k;
ll f[21][524290];
int a[21];
bool check(ll x){
	if(x==1) return false;
	for(int i=2;i<=x/i;i++){
		if(x%i==0) return false;
	}
	return true;
}
ll dfs(int i,int s){
	// cout<<i<<endl;
	if(i>n){
		ll x=0;
		int cnt=0;
		for(int j=1;j<=n;j++){
			if((s>>j)&1) {x+=a[j];cnt++;}
		}
		if(cnt==k&&check(x)) {return 1;}
		else return 0;
	}
	if(f[i][s]!=-1) return f[i][s];
	ll res=dfs(i+1,s|(1<<i))+dfs(i+1,s);
	return f[i][s]=res;
}
void solve(){
	cin>>n>>k;
	memset(f,-1,sizeof(f));
	for(int i=1;i<=n;i++) cin>>a[i];
	cout<<dfs(1,0)<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}*/


/*19ms*/
// #include <iostream>
// #include <cstdio>
// using namespace std;

// bool isprime(int a){//判断素数
//     /*0和1特判真的没啥用对这题
//     吐槽：题中n的数据范围很奇怪，
//     n还有可能=1.....那k<n......
//     */
//     for(int i = 2;i * i <= a; i++)//不想用sqrt，还要头文件
//         if(a % i == 0)//如果整除
//             return false;//扔回false
//     //程序都到这里的话就说明此为素数
//     //否则就被扔回了
//     return true;//扔回true
// }

// int n,k;
// int a[25];
// long long ans;

// void dfs(int m, int sum, int startx){//最重要的递归
// //m代表现在选择了多少个数
// //sum表示当前的和
// //startx表示升序排列，以免算重
//     if(m == k){//如果选完了的话
//         if(isprime(sum))//如果和是素数
//             ans++;//ans加一
//         return ;
//     }
//     for(int i = startx; i < n; i++)
//         dfs(m + 1, sum + a[i], i + 1);//递归
//         //步数要加一，和也要加
//         //升序起始值要变成i+1,以免算重
//     return ;//这一个步骤下，所有的都枚举完了
//     //直接返回去
// }

// int main(){
//     scanf("%d%d",&n,&k);//输入
    
//     for(int i = 0; i < n; i++)
//         scanf("%d",&a[i]);//循环读入
//     dfs(0,0,0);//调用函数
//     printf("%d\n",ans);//输出答案
//     return 0;//结束程序
// }