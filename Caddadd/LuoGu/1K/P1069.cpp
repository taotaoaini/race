#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
typedef struct Node{	
 int num;	// 质因数
 int sum;	// 次幂
}node;

int pr[5000];
bool flag[30005];
int num = 0;
// 任意的大于1 的整数都一定可以分解成质数幂乘积的形式
void Init(){            // 筛选质数 
	for(int i = 2; i < 30000; i++){
		if(!flag[i]) pr[num++] = i;
		for(int j = 0; j < num; j++){
			if(pr[j] * i > 30005) break;
			flag[pr[j] * i] = true;
			if(i % pr[j] == 0) break;
		}
	}
}
void solve(){
	Init();
	ll n;
 	cin >> n;
 	ll m1, m2;
	cin >> m1 >> m2;
 	queue<ll>v;           //存待选的细菌 
 	ll b;
 	for(int i = 1; i <= n; i++){
  	cin >> b;
  	v.push(b);
 	}
	vector<node>v1;      // 存 m1^m2 的质分解的结果 
 	for(int i = 0; m1 != 1; i++){
  		if(m1 % pr[i] == 0){        //如果 pr[i] 是 m1^m2 的质因子就存起来 
  			node a = {pr[i], 0};
   			while(m1 % pr[i] == 0){  // 算一下是多少次幂 
   				m1 /= pr[i];
    				a.sum++;
   			}
   			a.sum *= m2;             // 别忘了 m2 
   			v1.push_back(a);
   			int len = v.size();
   			for(int j = 1; j <= len; j++){    // 顺便判断待选细菌符合不符合 
   				int c = v.front();	//从队列中拿出来
    				v.pop();
    				if(c % pr[i] == 0) v.push(c);  //符合就再放回去
   			}
   		}
   	}
	
	if(v.empty()) cout << -1 << endl;        // 如果已经没有待选的答案，就输出 -1  
	else{
  		ll res = 0x3f3f3f3f;
  		int len = v.size();
  		for(int i = 1; i <= len; i++){       // 寻找最佳答案 
   			int c = v.front();
   			ll temp = 0;
   			for(int j = 0; j < v1.size(); j++){   // 遍历 m1^m2 的质因子
    				ll sum = 0;
    				while(c % v1[j].num == 0){
     					sum++;
     					c /= v1[j].num;
    				}
    				temp = max(temp, (v1[j].sum + sum - 1) / sum);
   			}
   			res = min(temp, res);
   			v.pop();
   		}
  	cout << res << endl;
  	}
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}