#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
const int N=1000010,M=2000010,inf=0x3f3f3f3f,mod=100003;
vector<int>e[N];
int dep[N];
bool vis[N];
int cnt[N];
int n,m;
void solve(){
     cin>>n>>m;
    for(int i=1;i<=m;i++){
        int x,y;
        cin>>x>>y;
        e[x].push_back(y);
        e[y].push_back(x);
    }
    queue<int> q;
    dep[1]=0;
    vis[1]=1;
    q.push(1);
    cnt[1]=1;
    while(!q.empty()){
        int x=q.front();q.pop();
        for(int i=0;i<e[x].size();i++){
            int t=e[x][i];
            if(!vis[t]){vis[t]=1;dep[t]=dep[x]+1;q.push(t);}
            if(dep[t]==dep[x]+1){cnt[t]=(cnt[t]+cnt[x])%mod;}
        }
    }
    for(int i=1;i<=n;i++){
        cout<<cnt[i]<<endl;
    }
}
int main(){
    solve();
    return 0;
}