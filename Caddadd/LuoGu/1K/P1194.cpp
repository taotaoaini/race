#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
int m, n;
const int N = 250010;
struct edge {
	int u, v, w;
} e[N];
int idx=0;
int a[510][510];
int fa[510];
void init(int n) {
	for (int i = 1; i <= n; i++) {
		fa[i] = i;
	}
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			if(a[i][j]==0) continue;
			e[++idx].u = i;
			e[idx].v = j;
			e[idx].w = a[i][j];
		}
	}
}
bool cmp(const edge& a, const edge& b) {
	return a.w < b.w;
}
int find(int x) {
	while (x != fa[x]) x = fa[x] = fa[fa[x]];
	return x;
}
void solve() {
	cin >> m >> n;
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= n; j++) {
			cin>>a[i][j];
		}
	}
	init(n);
	sort(e + 1, e + idx + 1,cmp);
	ll res = 0;
	int cnt = 0;
	for (int i = 1; i<=idx&&e[i].w < m; i++) {
		int fx = find(e[i].u);
		int fy = find(e[i].v);
		if (fx == fy) continue;
		res += e[i].w;
		cnt++;
		fa[fx] = fy;
		if(cnt==n-1) break;
	}
	for (int i = 1; i <= n; i++) {if (find(i) == i) res += m;}
	cout << res<<endl;
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);

	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}