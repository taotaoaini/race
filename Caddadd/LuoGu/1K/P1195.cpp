#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
int read(){
	char c=getchar();
	int res=0,f=1;
	while(c<'0'||c>'9'){
		if(c=='-') f=-1;
		c=getchar();
	}
	while(c>='0'&&c<='9'){
		res=res*10+(c-'0');
		c=getchar();
	}
	return res*f;
}
const int N=1010,M=10010;
struct edge{
	int u,v,w;
}e[M];	
int idx=0;
bool cmp(const edge& a,const edge& b){
	return a.w<b.w;
}
int n,m,k;
int fa[N];
int find(int x){
	while(x!=fa[x]) x=fa[x]=fa[fa[x]];
	return x;
}
void init(int n){
	for(int i=1;i<=n;i++) fa[i]=i;
}
void solve(){
	int n=read(),m=read(),k=read();
	for(int i=1;i<=m;i++){
		int a=read(),b=read(),c=read();
		e[++idx]={a,b,c};
	}
	init(n);
	if(n<k) {cout<<"No Answer"<<endl;return;}
	sort(e+1,e+idx+1,cmp);
	ll res=0;
	int cnt=0;
	for(int i=1;i<=idx;i++){
		int fx=find(e[i].u);
		int fy=find(e[i].v);
		if(fx==fy) continue;
		res+=e[i].w;
		fa[fx]=fy;
		cnt++;
		if(n-cnt==k) break;
	}
	printf("%lld\n",res);
}

int main(){
	int t=1;
	//cin>>t;
	while(t--){
		solve();
	}

	return 0;
}