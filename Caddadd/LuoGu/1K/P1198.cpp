#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read() {
	__int128 x = 0, f = 1;
	char ch = getchar();
	while (ch < '0' || ch > '9') {
		if (ch == '-') f = -1;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9') {
		x = x * 10 + ch - '0';
		ch = getchar();
	}
	return x * f;
}
//__int128的输出
inline void print(__int128 x) {
	if (x < 0) {
		putchar('-');
		x = -x;
	}
	if (x > 9)
		print(x / 10);
	putchar(x % 10 + '0');
}

const int N = 200010;
int m, p;
struct Tree {
	int l, r, mx;
} tr[N * 4];
void pushup(int u) {
	tr[u].mx = max(tr[lc].mx, tr[rc].mx);
}
void build(int u, int l, int r) {
	tr[u] = {l, r};
	if (l == r) return ;
	int m = l + r >> 1;
	build(lc, l, m); build(rc, m + 1, r);
}
void change(int u, int x, int v) {
	if (tr[u].l == x && tr[u].r == x) {tr[u].mx = v; return;}
	int m = tr[u].l + tr[u].r >> 1;
	if (x <= m) change(lc, x, v);
	else change(rc, x, v);
	pushup(u);
}
int query(int u, int l, int r) {
	if (l <= tr[u].l && r >= tr[u].r) return tr[u].mx;
	int m = tr[u].l + tr[u].r >> 1;
	int res = -2e9;
	if (l <= m) res = max(res, query(lc, l, r));
	if (r > m) res = max(res, query(rc, l, r));
	return res;
}
void solve() {
	cin >> m >> p;
	//先为m次操作预留空间
	build(1, 1, m);
	char c;
	ll x;
	//单点修改
	ll n = 0, t = 0;
	while (m--) {
		cin >> c >> x;
		if (c == 'A') change(1, ++n, (x + t) % p);
		else {t = query(1, n - x + 1, n); cout << t << endl;}
	}
}
int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}
	return 0;
}