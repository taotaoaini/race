#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls p<<1
#define rs p<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 20;

int a[N];
bool vis[N]; //统计每列
bool s[N * N], t[N * N * 2]; // +N*N进行偏移
int base = N * N;
int n;
int res;
int ans[5][N];
void dfs(int x) {
	if (x > n) {
		res++;
		if (res <= 3) {
			for (int i = 1; i <= n; i++) ans[res][i] = a[i];
			return;
		}
	}
	for (int i = 1; i <= n; i++) {
		if (!vis[i] && !s[x + i] && !t[base + x - i]) {
			a[x] = i;
			vis[i] = true;
			s[x + i] = true;
			t[base + x - i] = true;
			dfs(x + 1);
			a[x] = 0;
			vis[i] = false;
			s[x + i] = false;
			t[base + x - i] = false;
		}
	}
}
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	cin >> n;
	dfs(1);
	for (int i = 1; i <= 3; i++) {
		for (int j = 1; j <= n; j++) {
			cout << ans[i][j] << " ";
		}
		cout << endl;
	}
	cout << res << endl;
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	//cin>>t;

	while (t--) {
		solve();
		//init();
	}

	return 0;
}