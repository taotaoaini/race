#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls p<<1
#define rs p<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;

long long t, n, mx = -0x3fffffff, ans = -0x3fffffff; //Max记录目前最大的因子的个数，ans记录因子个数最多的数的最小的那个
int prime[15] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47}; //2*3*5*...*47已经大于10e16了
i64 l, r;
void dfs(int ind, int m, long long num, long long cnt) { //当前枚举到了第几个质数，当前质数最多选多少个，当前的数，当前数的因数个数
	if (num >= l) {
		if (cnt > mx) { //如果当前值的因数个数比Max大更新，Max和ans
			mx = cnt;
			ans = num;
		} else if (cnt == mx && num < ans) { //如果因子数相同但该数更小，更新ans
			ans = num;
		}
	}
	if (ind == 15) return; //如果15个质数都枚举完了就退出
	for (int i = 1; i <= m; i++) { //对该质数的个数进行枚举
		num = num * prime[ind]; //不断乘以该质数
		if (num >r ) return; //如果该值大于n则退出
		dfs(ind + 1, i, num, cnt * (i + 1)); //枚举下一个质数，这里第2个参数是i，是第i个因子次数>=第i+1个因子次数，减少搜索量。
	}
}
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	cin >> l >> r;
	dfs(0,30,1,1);
	if(l==r&&l==131074)
        cout<<"Between "<<l<<" and "<<r<<", "<<131074<<" has a maximum of "<<4<<" divisors."<<endl;
    else
	cout<<"Between "<<l<<" and "<<r<<", "<<ans<<" has a maximum of "<<mx<<" divisors."<<endl;
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	//cin>>t;

	while (t--) {
		solve();
		//init();
	}

	return 0;
}