/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;

string+string
to_string(int)
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second
#define mid ((l+r)>>1)

using i8 = signed char;
using u8 = unsigned char;
using i16 = signed short int;
using u16 = unsigned short int;
using i32 = signed int;
using u32 = unsigned int;
using f32 = float;
using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using i128 = __int128_t;
using u128 = __uint128_t;
using f128 = long double;

using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<int128>();
*/

template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}

template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 1000010;
int a[N];
struct Segtree {
	i64 sum[N * 4], add[N * 4], tag[N * 4];
	void pushup(int u) {
		sum[u] = max(sum[lc] , sum[rc]);
	}
	void build(int u, int l, int r) {
		tag[u] = dinf;
		if (l == r) {
			sum[u] = a[l];
			return;
		}
		build(lc, l, mid);
		build(rc, mid + 1, r);
		pushup(u);
	}
	void downupd(int u) {
		if (tag[u] != dinf) {
			tag[lc] = tag[u];
			tag[rc] = tag[u];
			sum[lc] = tag[u];
			sum[rc] = tag[u];
			tag[u] = dinf;
			add[lc] = add[rc] = 0;
		}
	}
	void downadd(int u) {
		if (add[u]) {
			add[lc] += add[u];
			add[rc] += add[u];
			sum[lc] += add[u];
			sum[rc] += add[u];
			add[u] = 0;
		}
	}
	void change(int u, int l, int r, int ql, int qr, int x, int k) {
		// if (ql > r || qr < l) return;
		if (ql <= l && qr >= r) {
			if (k == 1) {
				add[u] = 0;
				tag[u] = x;
				sum[u] = x;
			} else {
				add[u] += x;
				sum[u] += x;
			}
			return;
		}
		downupd(u);
		downadd(u);
		if (ql <= mid)
			change(lc, l, mid, ql, qr, x, k);
		if (qr > mid)
			change(rc, mid + 1, r, ql, qr, x, k);
		pushup(u);
	}
	i64 query(int u, int l, int r, int ql, int qr) {
		if (ql <= l && qr >= r) {
			return sum[u];
		}
		i64 res = -dinf;
		downupd(u);
		downadd(u);
		if (ql <= mid)
			res = max(res, query(lc, l, mid, ql, qr));
		if (qr > mid)
			res = max(res, query(rc, mid + 1, r, ql, qr));
		return res;
	}
} st;
int n, q;
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	cin >> n >> q;
//    Segtree st;
	for (int i = 1; i <= n; i++) cin >> a[i];
	st.build(1, 1, n);
	for (int i = 1, op, l, r, x; i <= q; i++) {
		cin >> op;
		if (op == 1) {
			cin >> l >> r >> x;
			st.change(1, 1, n, l, r, x, 1);
		} else if (op == 2) {
			cin >> l >> r >> x;
			st.change(1, 1, n, l, r, x, 2);
		} else {
			cin >> l >> r;
			cout << st.query(1, 1, n, l, r) << endl;
		}
		// cout<<"cnt:::"<<st.query(1,1,n,1,n)<<endl;
	}
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	//cin>>t;

	while (t--) {
		solve();
	}

	return 0;
}