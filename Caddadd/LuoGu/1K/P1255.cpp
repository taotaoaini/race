#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const int LEN=2010;
void clear(int a[]){
	for(int i=0;i<LEN;i++) a[i]=0;
}
void add(int a[],int b[],int c[]){
	clear(c);
	for(int i=0;i<LEN-1;i++){
		c[i]+=a[i]+b[i];
		if(c[i]>=10){
			c[i+1]+=1;
			c[i]-=10;
		}
	}
}
int a[LEN],b[LEN],c[LEN];
void print(int a[]){
	int i;
	for(i=LEN-1;i>=1;i--){
		if(a[i]!=0) break;
	}
	for(;i>=0;i--)  putchar(a[i]+'0');
	putchar('\n');
}
void fuzhi(int a[],int b[]){
	for(int i=0;i<LEN;i++){
		a[i]=b[i];
	}
}
void solve(){
	int n;
	cin>>n;
	if(n==1) {cout<<1<<endl;return;}
	if(n==2) {cout<<2<<endl;return;}
	a[0]=1;
	b[0]=2;
	for(int i=3;i<=n;i++){
		add(a,b,c);
		// print(c);
		fuzhi(a,b);
		fuzhi(b,c);
	}
	print(c);
}
int main(){
	// ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	
	int t=1;
	//cin>>t;
	while(t--){
		solve();
	}

	return 0;
}