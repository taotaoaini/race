#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
int f[20][20];
//jc函数求得是r个盒子的全排列数目：因为盒子不同
int jc(int k)
{
    int ans=1;
    for(int i=2;i<=k;i++)
    {
        ans*=i;
    }
    return ans;
}
void solve(){
	int n,k;
	cin>>n>>k;
	f[0][0]=1;//目前最后一个球放置的方案数
	for(int i=1;i<=n;i++)
		for(int j=1;j<=k;j++){
			f[i][j]=j*f[i-1][j]+f[i-1][j-1];
		}
	cout<<f[n][k]*jc(k)<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}

