#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls p<<1
#define rs p<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 40;

// 0,1,2,3
int f[N][N][4];
string ss = "NSWE";
int dx[4] { -1, 1, 0, 0}, dy[4] {0, 0, -1, 1};
string a[N];
int n, m;
int st[3], en[2];
int res = inf;
int dfs(int i, int j, int dir) {
    // if (i < 0 && i >= n && j < 0 && j >= m) return inf;
    if (i == en[0] && j == en[1]) {
        return 0;
    }
    if (f[i][j][dir] != -1) return f[i][j][dir];
    int res = inf;
    bool flag = true;
    int ndir = ((dir & 1) ? dir - 1 : dir + 1);
    // cout<<dir<<" "<<ndir<<":::::"<<endl;
    for (int k = 0; k < 4; k++) {
        if (k != ndir) {
            int ni = i + dx[k];
            int nj = j + dy[k];
            if (a[ni][nj] != '.') {flag = false; break;}
        }
    }
    // cout << flag << endl;
    if (flag) {
        // int ndir = ((dir & 1) ? dir - 1 : dir + 1);
        int x = dx[ndir], y = dy[ndir];
        res = min(res, dfs(i + x, j + y, ndir) + 10);
    } else {
        int x = dx[dir], y = dy[dir];
        if (a[i + x][j + y] != '.') {
            res = min(res, dfs(i + x, j + y, dir));
        }
        if (dir == 0) {
            x = dx[2], y = dy[2];
            if (a[i + x][j + y] != '.') {
                res = min(res, dfs(i + x, j + y, 2) + 1);
                // cout<<a[i+x][j+y]<<endl;
                cout<<i+x<<" "<<j+y<<"::"<<a[i+x][j+y]<<endl;
            }
            x = dx[3], y = dy[3];
            if (a[i + x][j + y] != '.') {
                res = min(res, dfs(i + x, j + y, 3) + 5);
            }
            cout<<"LL"<<x<<" "<<y<<endl;
            cout<<i+x<<" "<<j+y<<endl;
        }
        if (dir == 1) {
            x = dx[3], y = dy[3];
            if (a[i + x][j + y] != '.') {
                res = min(res, dfs(i + x, j + y, 3) + 1);
            }
            x = dx[2], y = dy[2];
            if (a[i + x][j + y] != '.') {
                res = min(res, dfs(i + x, j + y, 2) + 5);
            }
        }
        if (dir == 2) {
            x = dx[1], y = dy[1];
            if (a[i + x][j + y] != '.') {
                res = min(res, dfs(i + x, j + y, 1) + 1);
            }
            x = dx[0], y = dy[0];
            if (a[i + x][j + y] != '.') {
                res = min(res, dfs(i + x, j + y, 0) + 5);
            }
        }
        // cout<<"::"<<dir<<endl;
        if (dir == 3) {
            x = dx[0], y = dy[0];
            if (a[i + x][j + y] != '.') {
                res = min(res, dfs(i + x, j + y, 0) + 1);
            }
            x = dx[1], y = dy[1];
            if (a[i + x][j + y] != '.') {
                res = min(res, dfs(i + x, j + y, 1) + 5);
            }
        }
    }
    return f[i][j][dir] = res;
}
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    cin >> n >> m;
    for (int i = 0; i < n; i++) {
        cin >> a[i],a[i]+="00000000000000000000000000000";
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (ss.find(a[i][j]) != -1) st[0] = i, st[1] = j, st[2] = ss.find(a[i][j]);
            if (a[i][j] == 'F') en[0] = i, en[1] = j;
        }
    }
    memset(f, -1, sizeof(f));
    // cout<<st[0]<<"::"<<st[1]<<endl;
    // cout << st[2] << endl;
    dfs(st[0], st[1], st[2]);
    cout << res << endl;
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}