#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const int N = 10010, M = 40010;
struct edge {
	int v, w, ne;
} e[M];
int idx = 0, h[N];
void add(int u, int v, int w) {
	e[++idx].v = v;
	e[idx].w = w;
	e[idx].ne = h[u];
	h[u] = idx;
}
int n, m, s, t;
int dis[N];
bool vis[N];
priority_queue<pii, vector<pii>, greater<pii>> q;
void dijkstra(int s) {
	memset(dis, 0x3f, sizeof(dis));
	dis[s] = 0;
	q.push({0, s});
	while (!q.empty()) {
		auto now = q.top();
		q.pop();
		int u = now.second, d = now.first;
		if (vis[u]) continue;
		vis[u] = true;
		for (int i = h[u]; i; i = e[i].ne) {
			int v = e[i].v, w = e[i].w;
			if (dis[v] > max(dis[u], w)) {
				dis[v] = max(dis[u], w);
				q.push({dis[v], v});
			}
		}
	}
	cout << dis[t] << endl;
}
void solve() {
	cin >> n >> m >> s >> t;
	for (int i = 1; i <= m; i++) {
		int x, y, w;
		cin >> x >> y >> w;
		add(x, y, w);
		add(y, x, w);
	}
	dijkstra(s);
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);

	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}