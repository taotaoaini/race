#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
const int N=1000010;
int a[N],ans[N];
void f(int n){
	for(int i=2;i<=sqrt(n);i++){
		if(n%i==0){
			if(i==sqrt(n)&&n/i==i){
				a[i]+=1;
			}else{
				a[i]+=1;
				a[n/i]+=1;
			}
		}
	}
}
void solve(){
	int n;
	cin>>n;
	int x;
	for(int i=1;i<=n;i++) cin>>x,a[x]+=1,f(x);
	int cnt=0;
	for(int i=N-1;i>=1;i--){
		if(a[i]!=0&&cnt<a[i]){
			while(cnt<a[i]){
				ans[cnt++]=i;
			}
		}
	}
	for(int i=0;i<n;i++) if(ans[i]) cout<<ans[i]<<endl;else cout<<1<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}