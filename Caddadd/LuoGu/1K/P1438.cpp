#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
	//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}
const int N = 100010;
int a[N];
ll sum[N * 4], tag[N * 4];
void pushup(int u) {
	sum[u] = sum[lc] + sum[rc];
}
void pushdown(int u, int l, int r) {
	int mid = (l + r) >> 1;
	sum[lc] += tag[u] * (mid - l + 1);
	sum[rc] += tag[u] * (r - mid);
	tag[lc] += tag[u];
	tag[rc] += tag[u];
	tag[u] = 0;
}
void build(int u, int l, int r) {
	sum[u] = a[l]; tag[u] = 0;
	if (l == r) return;
	int mid = (l + r) >> 1;
	build(lc, l, mid);
	build(rc, mid + 1, r);
	pushup(u);
}

void change(int u, int l, int r, int x, int y, ll k) {
	if (x <= l && y >= r) {
		sum[u] += (r - l + 1) * k;
		tag[u] += k;
		return;
	}
	pushdown(u, l, r);
	int mid = (l + r) >> 1;
	if (x <= mid) change(lc, l, mid, x, y, k);
	if (y > mid) change(rc, mid + 1, r, x, y, k);
	pushup(u);
}

ll query(int u, int l, int r, int x, int y) {
	if (x <= l && y >= r) return sum[u];
	pushdown(u, l, r);
	ll s = 0;
	int mid = (l + r) >> 1;
	if (x <= mid) s += query(lc, l, mid, x, y);
	if (y > mid) s += query(rc, mid + 1, r, x, y);
	return s;
}
void solve() {
	int n, m, op, l, r, k, d, p;
	cin >> n >> m;
	for (int i = 1; i <= n; i++) cin >> a[i];
	for (int i = n; i > 1; i--) a[i] -= a[i - 1];
	build(1, 1, n);
	for (int i = 1; i <= m; i++) {
		cin >> op;
		if (op == 1) {
			cin >> l >> r >> k >> d;
			change(1, 1, n, l, l, k);
			if (l + 1 <= r) change(1, 1, n, l + 1, r, d);
			if (r < n) change(1, 1, n, r + 1, r + 1, -(k + d * (r - l)));
		} else {
			cin >> p;
			cout << query(1, 1, n, 1, p) << endl;
		}
	}
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}