#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
ll a = 0, b = 0, f;
string s;
ll gcd(ll a, ll b) {
	return b == 0 ? a : gcd(b, a % b);
}
void solve() {
	cin >> s;
	int i = 0;
	if (s[i] == '-') f = -1, i++;
	else f = 1;
	while (s[i] >= '0' && s[i] <= '9') {
		a = a * 10 + s[i] - '0';
		i += 1;
	}
	i += 1;
	while (s[i] >= '0' && s[i] <= '9') {
		b = b * 10 + s[i] - '0';
		i += 1;
	}
	for (; i < s.size();) {
		ll x = 0, y = 0, f1;
		if (s[i] == '-') f1 = -1;
		else f1 = 1;
		i += 1;
		while (s[i] >= '0' && s[i] <= '9') {
			x = x * 10 + s[i] - '0';
			i += 1;
		}
		i += 1;
		while (s[i] >= '0' && s[i] <= '9') {
			y = y * 10 + s[i] - '0';
			i += 1;
		}
		ll t = b * y / gcd(b, y);
		a = f * a * t / b + f1 * x * t / y;
		if (a < 0) f = -1, a = -a;
		else if (a == 0) {cout << 0 << endl; break;}
		else f = 1;
		b = t;
		t = gcd(a, b);
		a /= t;
		b /= t;
	}
	if(f==-1) cout<<"-";
	if (a % b == 0) cout << a << endl;
	else
		cout << a << "/" << b << endl;
}
int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t =1;
	// cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}