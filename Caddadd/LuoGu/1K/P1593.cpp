#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<functional>
#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
const ll mod=1e9+7;
using namespace std;


struct power {//a^b的因子和
	ll f[10010][2], cnt = 0;
	const ll mod = 9901;//mod需要根据题目要求进行更改。
	ll qmi(ll a, ll b) { //快速幂
		ll res = 1;
		while (b) {
			if (b & 1) {
				res = res * a % mod;
			}
			a = a * a % mod;
			b >>= 1;
		}
		return res % mod;
	}
	ll get(int a, int b) {//求a^b的因子和模mod
		a = a, b = b;
		if (a == 0) return 0;
		ll res = 1;
		for (int i = 2; 1LL * i * i <= a; i++) {//求质因子和数量
			if (a % i == 0) {
				cnt ++;
				f[cnt][0] = i;
				f[cnt][1] = 1;
				a = a / i;
				while (a % i == 0) {
					f[cnt][1]++;
					a = a / i;
				}
			}
		}
		if (a > 1) {
			cnt++;
			f[cnt][0] = a;
			f[cnt][1] = 1;
		}
		function<ll(ll, ll)> sum = [&](ll x, ll y) {
			ll res = 0;
			y *= b;
			if (x % mod == 1) {//(x-1)%mod==0：不存在乘法逆元，存在x>mod的情况
				res = (y + 1) % mod; //当逆元不存在时
			} else {
				res = (qmi(x % mod, y + 1) - 1) % mod * qmi((x - 1) % mod, mod - 2) % mod; //当逆元存在时
			}
			return res % mod;
		};
		for (int i = 1; i <= cnt; i++) {
			res = res * sum(f[i][0], f[i][1]) % mod;
		}
		return (res+mod)%mod;
	}
};
void solve() {
	int a, b;
	cin >> a >> b;
	power p;
	cout << p.get(a, b) << endl;
}
int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}