#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
struct A{
	int a,b;
	bool operator<(const A num1) const{
		return a<num1.a;
	} 
}nums[10010];
void solve(){
	int t,n;
	cin>>t>>n;
	vector<int> a(n);
	vector<int> b(n);
	for(int i=0;i<n;i++) cin>>nums[i].a>>nums[i].b;
	vector<ll> f(t+1);
	sort(nums,nums+n);
	for(int i=0;i<n;i++){
		for(int j=nums[i].a;j<=t;j++){
			f[j]=max(f[j],f[j-nums[i].a]+nums[i].b);
		}
	}
	cout<<f[t]<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);

    solve();
    return 0;
}