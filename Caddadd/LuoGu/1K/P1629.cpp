#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const int INF=0x3f3f3f3f;
const int N=1010;
struct node{
	int v;
	int w;
};
bool operator<(const node &a,const node &b){
	return a.w>b.w;
}
vector<node> a[N];
vector<node> a1[N];
int dis[N];
int n,m;
void dij(int u){
	priority_queue<node> q;
	node now,ne;
	now.v=1,now.w=0;
	q.push(now);
	dis[1]=0;
	while(!q.empty()){
		now=q.top();
		q.pop();
		int len=a[now.v].size();
		for(int i=0;i<len;i++){
			ne.v=a[now.v][i].v;
			ne.w=now.w+a[now.v][i].w;
			if(ne.w<dis[ne.v]){
				dis[ne.v]=ne.w;
			}
			q.push(ne);
		}
		a[now.v].clear();
	}
}
void dij1(int u){
	priority_queue<node> q;
	node now,ne;
	now.v=1,now.w=0;
	dis[1]=0;
	q.push(now);
	while(!q.empty()){
		now=q.top();
		q.pop();
		int len=a1[now.v].size();
		for(int i=0;i<len;i++){
			ne.v=a1[now.v][i].v;
			ne.w=now.w+a1[now.v][i].w;
			if(ne.w<dis[ne.v]){
				dis[ne.v]=ne.w;
			}
			q.push(ne);
		}
		a1[now.v].clear();
	}
}

void solve(){
	cin>>n>>m;
	node e;
	int x,y,w;
	for(int i=1;i<=m;i++){
		cin>>x>>y>>w;
		e.v=y;e.w=w;
		a[x].push_back(e);
		e.v=x;e.w=w;
		a1[y].push_back(e);
	}
	ll res=0;
	memset(dis,0x3f,sizeof(dis));
	dij(1);
	for(int i=1;i<=n;i++) res+=dis[i];
	// for(int i=1;i<=n;i++) cout<<dis[i]<<" ";
	// cout<<endl;
	memset(dis,0x3f,sizeof(dis));
	dij1(1);
	for(int i=1;i<=n;i++) res+=dis[i];
	cout<<res<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    return 0;
}