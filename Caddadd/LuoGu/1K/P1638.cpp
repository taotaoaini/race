#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

void solve(){
	int n,m;
	cin>>n>>m;
	unordered_map<int,int> p;
	int res[3]={1,n,n};
	vector<int> a(n+1);
	int cnt=0;
	for(int i=1;i<=n;i++) cin>>a[i];
	int l=1,r=1;
	while(r<=n){
		p[a[r]]+=1;
		if(p[a[r]]==1)  cnt++;
		while(cnt==m){
			if(res[2]>r-l+1){
				res[2]=r-l+1;
				res[0]=l;
				res[1]=r;
			}
			p[a[l]]-=1;
			if(p[a[l]]==0) cnt--;
			l++;
		}
		r++;
	}
	cout<<res[0]<<" "<<res[1]<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}