// /*
// ll=9.22*10^18 ((1LL << 63) - 1)
// int=2.1*10^9 ((1<<32)-1)
// ull=(1.844*10^19)

// double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
// double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
// double round(double x);//四舍五入,-2.7->-3,-2.2->-2

// fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
// cout<<fixed<<setprecision(3)<<1.2000;//->1.2

// 二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
// 三行四列的数组:arr[1][2]=arr[1*4+2]

// % + -
// << >>
// > >= < <=
// != & ^ | && ||

// string 从下标1开始读入
// char s[N];
// cin>>s+1;
// */
// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题

// #define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1
// #define fi first
// #define se second

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;

// /*
// 快读代码模板
// a=read<int>(),b=read<ll>(),c=read<__int128>();
// */
// template<typename T>
// void read(T &x) {
//     x = 0; bool flag(0); char ch = getchar();
//     while (!isdigit(ch)) flag = ch == '-', ch = getchar();
//     while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
//     flag ? x = -x : 0;
// }
// template<typename T>
// void write(T x, bool mode = 1) {
//     //mode=1为换行，0为空格
//     x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
//     do stk[++top] = x % 10, x /= 10; while (x);
//     while (top) putchar(stk[top--] | 48);
//     mode ? putchar('\n') : putchar(' ');
// }
// const int inf=0x3f3f3f3f;
// const ll dinf=0x7f7f7f7f;
// const int N=110;
// int v,w,s;
// int vv[100010],ww[100010],num=0;
// int f[40010];
// int n,m;
// void solve() {
// 	cin>>n>>m;
// 	for(int i=1;i<=n;i++){
// 		cin>>v>>w>>s;
// 		for(int j=1;j<=s;j<<=1){
// 			vv[++num]=j*v;
// 			ww[num]=j*w;
// 			s-=j;
// 		}if(s){
// 			vv[++num]=s*v;
// 			ww[num]=s*w;
// 		}
// 	}
// 	for(int i=1;i<=num;i++){
// 		for(int j=m;j>=ww[i];j--){
// 			f[j]=max(f[j],f[j-ww[i]]+vv[i]);
// 		}
// 	}
// 	cout<<f[m]<<endl;
// }

// int main() {
//     ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
//     int t = 1;
//     //cin>>t;
//     while (t--) {
//         solve();
//     }

//     return 0;
// }




/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
	//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}
const int inf = 0x3f3f3f3f;
const ll dinf = 0x7f7f7f7f;
const int N = 110;
const int M = 40010;
int f[M];
int g[M];
int n, m;
int v, w, s;
int q[M*2];
void solve() {
	cin >> n >> m;
	for (int i = 1; i <= n; i++) {
		memcpy(g, f, sizeof(f));//f备份到g中
		cin >> w>>v >> s;//价值，体积，数量
		for (int j = 0; j < v; j++) {//拆分成v个类
			int h = 0, t = -1;
			for (int k = j; k <= m; k += v) {//对每个类使用单调队列
				if (h <= t && q[h] < k - s * v) h++;//q[h]不在窗口[k-s*v,s-v]内，队头出队
				if (h <= t) f[k] = max(g[k], g[q[h]] + (k - q[h]) / v * w);
				//(k-q[h])/v:表示还能放入的物品个数
				//使用对头最大值跟新f
				while (h <= t && g[k] >= g[q[t]] + (k - q[t]) / v * w) t--;
				//如果g[k]比用g[q[t]]更新f[x]，获得更大的价值，则头出队
				//下标入队
				q[++t] = k;
			}
		}
	}
	cout << f[m] << endl;
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}