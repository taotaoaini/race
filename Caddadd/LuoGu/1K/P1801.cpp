/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
    x = 0; bool flag(0); char ch = getchar();
    while (!isdigit(ch)) flag = ch == '-', ch = getchar();
    while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
    flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
    //mode=1为换行，0为空格
    x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
    do stk[++top] = x % 10, x /= 10; while (x);
    while (top) putchar(stk[top--] | 48);
    mode ? putchar('\n') : putchar(' ');
}
const int inf = 0x3f3f3f3f;
const ll dinf = 0x7f7f7f7f;
const int N = 200010;
int a[N], u[N];
priority_queue<int> mx_hp;//大顶堆
priority_queue<int, vector<int>, greater<int>> mi_hp; //小顶堆，存放最大的k个元素，以至于可以O(1)找到第k大的值。
int n, k=1, num; //n:元素个数，动态k，num：需要加入的数
int m;
//调整堆元素个数
void qwq() {
    //调整小顶堆里的元素个数
    if (mx_hp.size() < k) {
        mx_hp.push(mi_hp.top());
        mi_hp.pop();
    }
    if (mx_hp.size() > k) {
        mi_hp.push(mx_hp.top());
        mx_hp.pop();
    }
}
//添加元素
void push(int num) {
    //如果新加入的元素小于mi_hp的堆顶的最小元素，可以直接加入大顶堆，
    //否则需要先将mi_hp的堆顶加入到mx_hp中，将num加入到小顶堆中
    //维护大顶堆一定是存放最小的k个数
    if (num <= mi_hp.top()) mx_hp.push(num);
    else mi_hp.push(num);
    qwq();
}
void solve() {
    cin >> n >> m;
    mi_hp.push(dinf);//避免边界判断，存放不可能出现的最大值
    for (int i = 1; i <= n; i++) cin >> a[i];
    for (int i = 1; i <= m; i++) cin >> u[i];
    for (int i = 1, j = 1; i <= n; i++) {
        //k=pass
        push(a[i]);
        while (j<=m&&i == u[j]) {
            cout << mx_hp.top() << endl; j++;
            k+=1;
        }
    }
}
int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    //cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}
