#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
int n,m;
const int N=1510;
vector<int> e[N],d[N];
int w[N],din[N],f[N];
queue<int> q;
void solve(){
	cin>>n>>m;
    for(int i=1;i<=m;i++){
        int x,y,w;
        cin>>x>>y>>w;
        e[x].push_back(y);
        d[x].push_back(w);
        din[y]++;
    }
    for(int i=2;i<=n;i++){
        f[i]=-1e9;
        if(!din[i]) q.push(i);
    }
    while(!q.empty()){
        int x=q.front();
        q.pop();
        for(int i=0;i<e[x].size();i++){
            if(!--din[e[x][i]]) q.push(e[x][i]);
        }
    }
    q.push(1);
    while(!q.empty()){
        int x=q.front();
        q.pop();
        for(int i=0;i<e[x].size();i++){
            int v=e[x][i];
            if(f[v]<f[x]+d[x][i]) f[v]=f[x]+d[x][i];
            if(!--din[v]) q.push(v);
        }
    }
    if(f[n]==-1e9) cout<<"-1";
    else cout<<f[n];
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}