#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
int l,r,cnt;
int pr[50000],ispr[50010];
void init(){
    ispr[1]=1;
    for(int i=2;i<50000;i++){
        if(!ispr[i]){
            pr[++cnt]=i;
        }
        for(int j=1;j<=cnt&&i*pr[j]<50000;j++){
            ispr[i*pr[j]]=1;
            if(i%pr[j]==0) break;
        }
    }
}
int ans[1000010],res=0;
void solve(){
    cin>>l>>r;
    init();
    for(int i=1;i<cnt&&pr[i]<=r;i++){
        for(ll j=max(2,(l+pr[i]-1)/pr[i]);j*pr[i]<=(ll)r;j++){
            ans[j*pr[i]-l]=1;
        }
    }
    if(l<=1) ans[1-l]=1;
    for(int i=0;i<=r-l;i++){
        if(!ans[i]) res++;
    }
    cout<<res;
}

int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
        solve();
    }
    
    return 0;
}