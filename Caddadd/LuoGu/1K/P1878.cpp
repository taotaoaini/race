/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
	//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}
const int inf = 0x3f3f3f3f;
const ll dinf = 0x7f7f7f7f;
const int N = 200100;
int n;
string s;
int idx;
int pos[N * 2][2];
int a[N];
bool vis[N];
int ans[N][2];
int cnt;
int res;
int faL[N], faR[N];
int findL(int x) {
	while (x != faL[x]) x = faL[x] = faL[faL[x]];
	return x;
}
int findR(int x) {
	while (x != faR[x]) x = faR[x] = faR[faR[x]];
	return x;
}
void init(int n) {
	for (int i = 1; i <= n + 1; i++) {
		faL[i] = i;
		faR[i] = i;
	}
}
struct cmp {
	bool operator()(pii a, pii b) {
		if (a.fi == b.fi) {
			if (pos[a.se][0] == pos[b.se][0]) {
				return pos[a.se][1] > pos[b.se][1];
			}
			return pos[a.se][0] > pos[b.se][0];
		}
		return a.fi > b.fi;
	}
};
priority_queue<pii, vector<pii>, cmp> mi_hp;
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	cin >> n;
	cin >> s;
	init(n);
	for (int i = 1; i <= n; i++) cin >> a[i];
	for (int i = 2; i <= s.size(); i++) {
		if (s[i - 1] != s[i - 2]) {
			pos[++idx][0] = i - 1;
			pos[idx][1] = i;
			mi_hp.push({abs(a[i] - a[i - 1]), idx});
		}
	}
	while (!mi_hp.empty()) {
		auto mi = mi_hp.top();
		mi_hp.pop();
		int val = mi.fi;
		int id = mi.se;
		int x = pos[id][0];
		int y = pos[id][1];
		if (vis[x] || vis[y]) continue;
		vis[x] = 1;
		vis[y] = 1;
		res += 1;
		ans[++cnt][0] = x;
		ans[cnt][1] = y;
		faL[x] = x - 1;
		faR[x] = y + 1;
		faL[y] = x - 1;
		faR[y] = y + 1;
		int fax = findL(x);
		int fay = findR(y);
		if (fax >= 1 && !vis[fax] && fay <= n && !vis[fay] && s[fax - 1] != s[fay - 1]) {
			pos[++idx][0] = fax;
			pos[idx][1] = fay;
			mi_hp.push({abs(a[fax] - a[fay]), idx});
		}
	}
	cout << res << endl;
	for (int i = 1; i <= cnt; i++) {
		cout << ans[i][0] << " " << ans[i][1] << endl;
	}
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}