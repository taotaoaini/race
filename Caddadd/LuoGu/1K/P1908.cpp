#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
    x = 0; bool flag(0); char ch = getchar();
    while (!isdigit(ch)) flag = ch == '-', ch = getchar();
    while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
    flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
    //mode=1为换行，0为空格
    x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
    do stk[++top] = x % 10, x /= 10; while (x);
    while (top) putchar(stk[top--] | 48);
    mode ? putchar('\n') : putchar(' ');
}
const int N=500010;
int a[N],b[N];
int n;
int sum[N*4];
void pushup(int u){
	sum[u]=sum[lc]+sum[rc];
}
void change(int u,int l,int r,int x){
	if(l==r) {sum[u]+=1;return;}
	int mid=(l+r)>>1;
	if(x<=mid) change(lc,l,mid,x);
	else change(rc,mid+1,r,x);
	pushup(u);
}
ll query(int u,int l,int r,int x,int y){
	if(x<=l&&y>=r) return sum[u];
	ll res=0;
	int mid=(l+r)>>1;
	if(x<=mid) res+=query(lc,l,mid,x,y);
	if(y>mid) res+=query(rc,mid+1,r,x,y);
	return res;
}
void solve() {
	cin>>n;
	for(int i=1;i<=n;i++){
		cin>>a[i];
		b[i]=a[i];
	}
	sort(b+1,b+n+1);
	ll res=0;
	for(int i=1;i<=n;i++){
		int id=lower_bound(b+1,b+n+1,a[i])-b;
		// cout<<id<<endl;
		change(1,1,n,id);
		// cout<<query(1,1,n,id+1,n)<<endl;
		res+=query(1,1,n,id+1,n);
	}
	cout<<res<<endl;
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    //cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}