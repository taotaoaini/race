#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
string res="";
string s;
int i;
string dfs(int& i){
	string res="";
	int x=0;
	for(;s[i]>='0'&&s[i]<='9';i++) x=x*10+s[i]-'0';
	string s1="";
	for(;s[i]!=']'&&i<s.size();i++){
		if(s[i]=='[') {i+=1;s1+=dfs(i);}
		else s1+=s[i];
	}
	for(int j=1;j<=x;j++){
		res+=s1;
	}
	return res;
}
void solve() {
	cin >> s;
	int slen=s.size();
	for (i = 0; i < slen;i++) {
		if (s[i] != '[') {
			res+=s[i];
		} else {
			i+=1;
			res+=dfs(i);
		}
	}
	cout<<res<<endl;
}
int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}





