#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const int N = 5010;
int n, m, h[N], idx;
struct edge {
	int v, ne, w;
}
e[N * 3];
int dis[N], cnt[N];
bool vis[N];
queue<int> q;
void add(int u, int v, int w) {
	e[++idx].v = v;
	e[idx].ne = h[u];
	e[idx].w = w;
	h[u] = idx;
}
bool spfa(int s) {
	dis[s] = 0;
	vis[s] = 1;
	q.push(s);
	while (!q.empty()) {
		int u = q.front();
		q.pop();
		vis[u] = 0;
		for (int i = h[u]; i; i = e[i].ne) {
			int v = e[i].v;
			if (dis[v] > dis[u] + e[i].w) {
				dis[v] = dis[u] + e[i].w;
				if (vis[v] == 0) {
					cnt[v]++;
					if (cnt[v] >= n + 1) return false;
					//此时就有n个点
					vis[v] = 1;
					q.push(v);
				}
			}
		}
	}
	return true;
}
void solve() {
	scanf("%d%d", &n, &m);
	int s = n + 1;
	for (int i = 1; i <= m; i++) {
		int t, u, v, w;
		scanf("%d", &t);
		switch (t) {
		case 1: {
			scanf("%d%d%d", &u, &v, &w);
			add(u, v, -w);
			break;
		}
		case 2: {
			scanf("%d%d%d", &u, &v, &w);
			add(v, u, w);
			break;
		}
		case 3: {
			scanf("%d%d", &u, &v);
			add(u, v, 0);
			add(v, u, 0);
			break;
		}
		}
	}
	for (int i = 1; i <= n; i++) {
		add(s, i, 0);
	}
	memset(dis, 0x3f, sizeof(dis));
	if (!spfa(s)) {
		printf("No\n");
		return;
	}
	printf("Yes\n");
}
int main() {
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}
	return 0;
}