#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const int maxn = 1e6+10;

typedef struct Node{   // 定义一个数据类型，具有二次函数的系数、自变量和函数值
	int a;
	int b;
	int c;
	int x;            // 分析中提到的指针
	int y;			  // 指针对应的函数值
} node;
bool operator < (const node &A, const node &B){    // 重载 < 号，让优先队列按照 y 的值进行排序
  	return A.y > B.y;
}

int f(node z, int x){  // 计算 x 对应的函数值
	return z.a * x * x + z.b * x + z.c;
}
void solve(){
	int n, m;
	cin >> n >> m;
	node v;
	priority_queue<node>qu;     // 优先队列
	for(int i = 1; i <= n; i++){
		cin >> v.a >> v.b >> v.c;
		v.x = 1;			   // 默认 x = 1
		v.y = v.a + v.b +  v.c;;
		qu.push(v);  		   // 把这个二次函数加到队列中
	} 
		
	vector<int>res;            // 放答案
	int ans = 0;			   // 答案个数
	while(ans <= m){           
		node now = qu.top();   // 取出队首元素
		qu.pop();			   // 因为要更新这个函数，把原来的函数删去
		res.push_back(now.y);  // 记录答案
		ans++;				   // 更新答案个数
		now.x++;               // 指针右移
		now.y = f(now, now.x); // 更新函数
		qu.push(now);		   // 更新队列，重新排序
	}
	
	for(int i = 0; i < m; i++) cout << res[i] << " ";
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}