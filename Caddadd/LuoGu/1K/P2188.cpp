#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
ll f[30][30];
string s1,s2;
string s;
int k;
ll dp(int i,int pre,bool is_limit,bool is_num){
    if(i>=s.size()) return is_num;
    ll res=0;
    if(!is_num) res+=dp(i+1,pre,false,false);
    if(is_num&&!is_limit&&f[i][pre]!=-1) return f[i][pre]; 
    int up=is_limit?s[i]-'0':9;
    int low=is_num?0:1;
    for(int d=low;d<=up;d++){
        //1：当是首位数字，2：当符合条件
        if(!is_num||abs(d-pre)<=k){
            res+=dp(i+1,d,is_limit&&d==up,true);
        }
    }
    if(is_num&&!is_limit)  f[i][pre]=res;
    return res;
}
bool check(string s){
    for(int i=1;i<s.size();i++){
        if(abs(s[i]-s[i-1])>k) return false;
    }
    return true;
}
void solve(){
	 cin>>s1>>s2>>k;
    s=s2;
    memset(f,-1,sizeof(f));
    ll res=dp(0,0,true,false);
    s=s1;
    memset(f,-1,sizeof(f));
    res-=dp(0,0,true,false);
    res+=check(s1);
    cout<<res<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}