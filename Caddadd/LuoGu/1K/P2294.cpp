#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
int fa[1010],sum[1010],n,m,flag;
int find(int x){
	if(fa[x]==x) return fa[x];
	int tmp=find(fa[x]);
	sum[x]+=sum[fa[x]];
	return fa[x]=tmp;
}
void merge(int x,int y,int z){
	int fx,fy;
	fx=find(x);
	fy=find(y);
	if(fx!=fy){
		fa[fy]=fx;
		sum[fy]=sum[x]-sum[y]+z;
	}else{
		if(sum[y]-sum[x]!=z)
			flag=true;
	}
}
void init(int n){
	for(int i=0;i<=n;i++){
		fa[i]=i;
		sum[i]=0;
	}
	flag=false;
}
void solve(){
	int x,y,z;
	cin>>n>>m;
	init(n);
	for(int i=1;i<=m;i++){
		cin>>x>>y>>z;
		x--;
		merge(x,y,z);
	}
	if(flag) cout<<"false"<<endl;
	else cout<<"true"<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    cin>>t;
    while(t--){

    	solve();
    }
    
    return 0;
}








