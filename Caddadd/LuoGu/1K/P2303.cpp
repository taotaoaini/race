// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>

// #define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;
// //__int128的输入
// inline __int128 read(){
//     __int128 x=0,f=1;
//     char ch=getchar();
//     while(ch<'0'||ch>'9'){
//         if(ch=='-') f=-1;
//         ch=getchar();
//     }
//     while(ch>='0'&&ch<='9'){
//         x=x*10+ch-'0';
//         ch=getchar();
//     }
//     return x*f;
// }
// //__int128的输出
// inline void print(__int128 x){
//     if(x<0){
//         putchar('-');
//         x=-x;
//     }
//     if(x>9)
//         print(x/10);
//     putchar(x%10+'0');
// }
// // unordered_map<int,int> up;
// int a[100010],idx;
// int b[100010];
// void init(int n){
// 	for(int i=2;i<=n/i;i++){
// 		if(n%i==0){
// 			a[idx]=i;
// 			while(n%i==0){
// 				b[idx]+=1;
// 				n/=i;
// 			}
// 			idx+=1;
// 		}
// 	}
// 	if(n>1) a[idx]=n,b[idx++]+=1;
// }
// void solve(){
// 	int n;
// 	cin>>n;
// 	init(n);
// 	ll res=0;
// 	for(int i=1;i<=n;i++){
// 		int x=i;
// 		int y=1;
// 		// if(n%i==0) {res+=i;continue;}
// 		for(int j=0;j<idx;j++){
// 			int cnt=b[j];
// 			while(x%a[j]==0&&cnt>0){
// 				y*=a[j];
// 				x/=a[j];
// 				cnt-=1;
// 			}
// 		}
// 		// cout<<y<<endl;
// 		res+=y;
// 	}
// 	cout<<res<<endl;
// }
// int main() {
//     ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
//     int t=1;
//     //cin>>t;
//     while(t--){
//     	solve();
//     }
    
//     return 0;
// }

#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
ll n;
ll phi(ll n){
	ll res=n;
	for(ll i=2;i<=n/i;i++){
		if(n%i==0){
			res=res/i*(i-1);
			while(n%i==0) n/=i;
		}
	}
	if(n>1) res=res/n*(n-1);
	return res;
}
void solve(){
	cin>>n;
	ll res=0;
	for(ll i=1;i<=n/i;i++){
		if(n%i==0){
			res+=i*phi(n/i);
			if(i!=n/i) res+=(n/i)*phi(i);

		}
	}
	cout<<res<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}


