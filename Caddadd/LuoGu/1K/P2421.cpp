#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}

const int N=30;
ll C[N],P[N],L[N];
int n;
ll mx;
ll gcd(ll a,ll b){
	return b==0?a:gcd(b,a%b);
}
ll exgcd(ll a,ll b,ll &x,ll &y){
  if(b == 0) {x=1, y=0; return a;}
  ll d = exgcd(b, a%b, y, x);
  y-=a/b*x;
  return d;
}
bool check(ll m){
    for(int i=1;i<=n;i++){
        for(int j=i+1;j<=n;j++){
            ll p=P[i]-P[j];
            ll c=C[j]-C[i];
            if(p<0) {p=P[j]-P[i],c=C[i]-C[j];}
            if(c%gcd(p,m)) continue;//若无解，不需要下次操作了
            ll x,y;
            ll d=exgcd(p,m,x,y);
            ll t=abs(m/d);
            x=((x*c/d)%t+t)%t;
            cout<<y*c/d<<endl;
            if(x<=min(L[i],L[j])) return false;
        }
    }
    return true;
}
void solve(){
	cin>>n;
    for(int i=1;i<=n;i++) cin>>C[i]>>P[i]>>L[i],mx=max(C[i],mx);
    for(int i=mx;;i++){
        if(check(i)) {cout<<i<<endl;break;}
    }
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}