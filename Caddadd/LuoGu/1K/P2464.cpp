/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
	//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}
const int inf = 0x3f3f3f3f;
const ll dinf = 0x7f7f7f7f;
const int N = 100010;
int root[N*2], ls[N * 40], rs[N * 40], tot, cnt;  //tot：开点个数，cnt：书编码的个数，sum：区间数的个数 
struct node {
	int op, l, r, id;  //id:书的编号
} q[N]; //保存操作
int n, m, a[N], b[N * 2],sum[N*40];
void pushup(int u) {
	sum[u] = sum[ls[u]] + sum[rs[u]];
}
void change(int &u, int l, int r, int p, int k) {
	if (!u) u = ++tot;
	if (l == r) {sum[u] += k; return;}
	int mid = (l + r) >> 1;
	if (p <= mid) change(ls[u], l, mid, p, k);
	else change(rs[u], mid + 1, r, p, k);
	pushup(u);
}
int query(int u, int l, int r, int x, int y) {
	if (x <= l && y >= r) return sum[u];
	int s = 0;
	int mid=(l+r)>>1;
	if (x <= mid) s += query(ls[u], l, mid, x, y);
	if (y > mid) s += query(rs[u], mid + 1, r, x, y);
	return s;
}
void solve() {
	cin >> n >> m;
	for (int i = 1; i <= n; i++) {
		cin >> a[i]; b[++cnt] = a[i];
	}
	for (int i = 1; i <= m; i++) {
		char s[2];
		cin >> s;
		if (s[0] == 'C') q[i].op = 0, cin >> q[i].l >> q[i].id;
		else q[i].op = 1, cin >> q[i].l >> q[i].r >> q[i].id;
		b[++cnt] = q[i].id;
	}
	sort(b + 1, b + cnt + 1);
	int bn = unique(b + 1, b + cnt + 1) - b - 1;
	// cout<<bn<<endl;
	for (int i = 1; i <= n; i++) {
		a[i] = lower_bound(b + 1, b + bn + 1, a[i]) - b;
		change(root[a[i]], 1, n, i, 1);
	}
	for (int i = 1; i <= m; i++) {
		if (!q[i].op) {
			change(root[a[q[i].l]], 1, n, q[i].l, -1);
			a[q[i].l] = lower_bound(b + 1, b + bn + 1, q[i].id) - b;
			change(root[a[q[i].l]], 1, n, q[i].l, 1);
		} else {
			int id = lower_bound(b + 1, b + bn + 1, q[i].id) - b;
			cout << query(root[id], 1, n, q[i].l, q[i].r) << endl;
		}
	}
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}