/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
	//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}
const int inf = 0x3f3f3f3f;
const ll dinf = 0x7f7f7f7f;
const int N = 500005;

int n, m, q;
int a[210][210], f[210][210][1010], g[210][210][1010];
//f[i][j][k]:区间[(1,1),(i,j)]内>=k的书的总页数
//f[i][j][k]:区间[(1,1),(i,j)]内>=k的总个数
int getsum(int x1, int y1, int x2, int y2, int k, int t) {
	if (t) return f[x2][y2][k] - f[x2][y1 - 1][k] - f[x1 - 1][y2][k] + f[x1 - 1][y1 - 1][k];
	else return g[x2][y2][k] - g[x2][y1 - 1][k] - g[x1 - 1][y2][k] + g[x1 - 1][y1 - 1][k];
}
void work1() {
	int mx = -1e9;
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++) {
			cin >> a[i][j];
			mx = max(a[i][j], mx);
		}
	}
	for (int k = 1; k <= mx; k++) {
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++) {
				f[i][j][k] = f[i - 1][j][k] + f[i][j - 1][k] - f[i - 1][j - 1][k] + (a[i][j] >= k) * a[i][j];
				g[i][j][k] = g[i - 1][j][k] + g[i][j - 1][k] - g[i - 1][j - 1][k] + (a[i][j] >= k);
			}
		}
	}
	// cout<<getsum(1,1,2,2,1,0)<<endl;
	int x1, y1, x2, y2, h;
	for (int i = 1; i <= q; i++) {
		cin >> x1 >> y1 >> x2 >> y2 >> h;
		if (getsum(x1, y1, x2, y2, 1, 1) < h) {cout << "Poor QLW" << endl; continue;}
		int l = 0, r = mx + 1;
		while (l + 1 < r) {
			int mid = (l + r) >> 1; //二分书页范围[1,mx]
			if (getsum(x1, y1, x2, y2, mid, 1) >= h) {
				l = mid;
			} else {
				r = mid;
			}
		}
		cout << getsum(x1, y1, x2, y2, l, 0) - (getsum(x1, y1, x2, y2, l, 1) - h) / l << endl;
	}
}

int b[N], root[N], tot; //root根节点,tot节点个数
int ls[N * 25], rs[N * 25], sum[N * 25], siz[N * 25];
//sum:区间内书的总页数
//siz:区间内书的总个数
void change(int &u, int v, int l, int r, int x) { //点修
	u = ++tot; //动态开点
	ls[u] = ls[v]; rs[u] = rs[v]; sum[u] = sum[v] + x; siz[u] = siz[v] + 1;
	if (l == r)return; //二分书页范围[1,mx]
	int mid = (l + r) >> 1;
	if (x <= mid)change(ls[u], ls[u], l, mid, x);
	else change(rs[u], rs[u], mid + 1, r, x);
}
int query(int u, int v, int l, int r, int h) { //点查
	if (l == r)return (h + l - 1) / l;
	int s = sum[rs[u]] - sum[rs[v]]; //二分书页范围[1,mx]
	int mid = (l + r) >> 1;
	if (h <= s)return query(rs[u], rs[v], mid + 1, r, h);
	else return query(ls[u], ls[v], l, mid, h - s) + siz[rs[u]] - siz[rs[v]];
}
void work2() { //可持久化线段树
	int mx = -1e9;
	for (int i = 1; i <= m; i++) cin >> b[i], mx = max(mx, b[i]);
	for (int i = 1; i <= m; i++)change(root[i], root[i - 1], 1, mx, b[i]);
	for (int i = 1, x1, y1, x2, y2, h; i <= q; i++) {
		cin >> x1; cin >> y1, y1 = root[y1-1]; cin >> x2; cin >> y2, y2 = root[y2]; cin >> h;
		if (sum[y2] - sum[y1] < h) {cout << "Poor QLW" << endl; continue;}
		cout << query(y2, y1, 1, mx, h) << endl;
	}
}

void solve() {
	cin >> n >> m >> q;
	if (n > 1) {
		work1();
	} else {
		work2();
	}
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}