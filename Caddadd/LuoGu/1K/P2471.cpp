#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read() {
	__int128 x = 0, f = 1;
	char ch = getchar();
	while (ch < '0' || ch > '9') {
		if (ch == '-') f = -1;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9') {
		x = x * 10 + ch - '0';
		ch = getchar();
	}
	return x * f;
}
//__int128的输出
inline void print(__int128 x) {
	if (x < 0) {
		putchar('-');
		x = -x;
	}
	if (x > 9)
		print(x / 10);
	putchar(x % 10 + '0');
}

const int N = 100010;
struct Tree {
	//线段树
	int l, r;
	int mx;
} tr[N * 4];
int n, m;
int ye[N], ra[N];
int idl, idr;
bool bl, br;
//
void pushup(int u) {
	tr[u].mx = max(tr[lc].mx, tr[rc].mx);
}
//
void build(int u, int l, int r) {
	tr[u] = {l, r, ra[l]};
	if (l == r) return;
	int mid = l + r >> 1;
	build(lc, l, mid);
	build(rc, mid + 1, r);
	pushup(u);
}
//
int query(int u, int l, int r) {
	if (l <= tr[u].l && r >= tr[u].r) {return tr[u].mx;}
	int mid = tr[u].l + tr[u].r >> 1;
	int res = 0;
	if (l <= mid) res = max(res,query(lc, l, r));
	if (r > mid) res = max(res, query(rc, l, r));
	return res;
}
//
void solve() {
	cin >> n;
	int y, x;
	for (int i = 1; i <= n; i++) {
		cin >> y >> x;
		ye[i] = y; ra[i] = x;
	}
	build(1, 1, n);
	cin >> m;
	while (m--) {
		cin >> y >> x;//y,x年
		idl = lower_bound(ye + 1, ye + n + 1, y) - ye;
		idr = lower_bound(ye + 1, ye + n + 1, x) - ye;
		bl = ye[idl] == y;
		br = ye[idr] == x;
		if (!bl) idl--;//若左端年份不存在,则修正下标
		int mx = query(1, idl + 1, idr - 1);
		if (bl && br && x - y == idr - idl && ra[idr] <= ra[idl] && ra[idr] > mx) cout << "true" << endl;
		else if ((bl && mx >= ra[idl]) || (br && mx >= ra[idr]) || (bl && br && ra[idr] > ra[idl])) cout << "false" << endl;
		else cout << "maybe" << endl;
	}
}
int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}