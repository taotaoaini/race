#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;


// 空间计算
// 只需要计算更新时候的节点变化所需空间
// 每次修改需要改变 log n*log n
// m*log (n+m) * log (n*m)
const int N = 1e5 + 10;
int n, m;
int a[N], b[N * 2], len;
struct Query {
    int op;
    int l, r, val;
} q[N];

#define mid ((l+r)/2)
#define sum(p) tr[p].sum
#define ls(p) tr[p].left
#define rs(p) tr[p].right

struct node {
    int sum;
    int left, right;
} tr[N * 20*20]; 
int root[N],tot;

int lowbit(int x) {
    return x & -x;
}

void pull(int p) {
    sum(p) = sum(ls(p)) + sum(rs(p));
}

void update_st(int &p, int l, int r, int x, int val) {
    if (!p) p = ++tot;
    if (l == r) {
        sum(p) += val;
        return;
    }
    if (x <= mid) update_st(ls(p), l, mid, x, val);
    else update_st(rs(p), mid + 1, r, x, val);
    pull(p);
}

void update_bt(int pos, int x, int val) {
    for (int i = pos; i <= n; i += lowbit(i))
        update_st(root[i], 1, len, x, val);
}

// 提取区间线段树的根节点
int rt1[N], rt2[N], cnt1, cnt2;

void locate(int l, int r) {
    cnt1 = cnt2 = 0;
    for (int i = l - 1; i; i -= lowbit(i))
        rt1[++cnt1] = root[i];
    for (int i = r; i; i -= lowbit(i))
        rt2[++cnt2] = root[i];
}

int ask(int l, int r, int k) { // O(nlogn)
    if (l == r) return l;
    int sumL = 0;
    for (int i = 1; i <= cnt1; i++) // O(n)
        sumL -= sum(ls(rt1[i]));
    for (int i = 1; i <= cnt2; i++)
        sumL += sum(ls(rt2[i]));
    if (sumL >= k) {
        for (int i = 1; i <= cnt1; i++)
            rt1[i] = ls(rt1[i]);
        for (int i = 1; i <= cnt2; i++)
            rt2[i] = ls(rt2[i]);
        return ask(l, mid, k);
    } else {
        for (int i = 1; i <= cnt1; i++)
            rt1[i] = rs(rt1[i]);
        for (int i = 1; i <= cnt2; i++)
            rt2[i] = rs(rt2[i]);
        return ask(mid + 1, r, k - sumL);
    }
}

void solve() {
    cin >> n >> m;
    for (int i = 1; i <= n; i++) {
        cin >> a[i];
        b[++len] = a[i];
    }
    for (int i = 1; i <= m; i++) {
        char op;
        cin >> op;
        if (op == 'Q') {
            q[i].op = 0;
            cin >> q[i].l >> q[i].r >> q[i].val;
        } else {
            q[i].op = 1;
            cin >> q[i].l >> q[i].val;
            b[++len] = q[i].val;
        }
    }
    //数值离散化
    sort(b + 1, b + len + 1);
    len = unique(b + 1, b + len + 1) - b - 1;
    for (int i = 1; i <= n; i++) a[i] = lower_bound(b + 1, b + len + 1, a[i]) - b;
    for (int i = 1; i <= m; i++)
        if (q[i].op) q[i].val = lower_bound(b + 1, b + len + 1, q[i].val) - b;
    // 建树（动态开点形式）
    for (int i = 1; i <= n; i++) {
        update_bt(i, a[i], 1);
    }
    for (int i = 1; i <= m; i++) {
        if (q[i].op) {
            update_bt(q[i].l, a[q[i].l], -1);
            a[q[i].l] = q[i].val;
            update_bt(q[i].l, q[i].val, 1);
        } else {
            locate(q[i].l, q[i].r);
            int res = b[ask(1, len, q[i].val)];
            cout << res << endl;
        }
    }
}
int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}