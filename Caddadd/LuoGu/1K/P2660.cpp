#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//例如 10000000000 1 之类的，调用的次数为maxn/minn，也就是10000000000次，自然也就爆空间了。
//那能不能，专门对这种数据进行优化呢？这种数据的特点就是重复调用。
//辗转相减法
ll res=0;
ll gcd(ll a,ll b){
	if(a>b) swap(a,b);
	if(a==b){
		res+=a*4;
		return a;
	}
	res+=a*4*(b/a);
	if(b%a==0) return a;
	return gcd(a,b%a);
}
void solve(){
	ll a,b;
	cin>>a>>b;
	if(a>b) swap(b,a);
	ll x=gcd(a,b);
	cout<<res<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}
