#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1
using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll,																										 ll>;
using namespace std;
struct edge {
	int u, v, w;
} e[100008];
int n, k;
int fa[100008];
bool vis[1000008];
bool cmp(const edge& a, const edge& b) {
	return a.w > b.w;
}
void init(int n) {
	for (int i = 0; i <= n; i++) fa[i] = i;
}
int find(int x){
	while(x!=fa[x]) x=fa[x]=fa[fa[x]];
	return x;
}
void solve() {
	cin>>n>>k;
	ll res = 0;
	int x;
	init(n);
	for (int  i = 1; i <= k; i++) cin>>x, vis[x] = 1;
	for (int i = 1; i <= n - 1; i++) {
		cin>>e[i].u>>e[i].v>>e[i].w;
		res += e[i].w; 
	}
	sort(e + 1, e + n, cmp);
	for (int i = 1; i <= n - 1; i++) {
		int fx = find(e[i].u);
		int fy = find(e[i].v);
		if (vis[fx] && vis[fy]) continue;
		res -= e[i].w;
		fa[fx] = fy;
		if (vis[fx]) vis[fy] = 1;
	}
	cout<<res<<endl;
}

int main() {
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	int t = 1;
	while (t--) {
		solve();
	}

	return 0;
}
