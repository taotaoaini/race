#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
int n;
bool f[30][900];
void solve(){
	 cin>>n;
    for(int i=1;i<=n;i++) f[i][0]=1;//初始化
    for(int i=2;i<=n;i++)//枚举i
    {
        for(int j=i-1;j>=1;j--)//枚举j
        {
            for(int k=0;k<=(i-j)*(i-j-1)/2;k++)//枚举k，枚举到(i-j)*(i-j-1)/2是因为结论2
                        f[i][j*(i-j)+k]=f[i][j*(i-j)+k]||f[i-j][k];
        }
    }
    int ans=0;
    for(int i=0;i<=n*(n-1)/2;i++) if(f[n][i]) ans++;//计算方案数
    cout<<ans;//输出
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}