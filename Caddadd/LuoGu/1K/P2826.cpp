#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const int N = 80010;
int n;
int ans[N];
int st[N], idx = 0;
ll a[N];
void solve() {
	cin >> n;
	int x;
	ll res = 0;
	for (int i = 1; i <= n; i++) {cin >> a[i];}

	a[n + 1] = 0x7f7f7f7f;
	for (int i = 1; i <= n + 1; i++) {
		while (idx > 0 && a[i] >= a[st[idx]]) {
			res += i - st[idx] - 1;
			idx -= 1;
		}
		st[++idx] = i;
	}
	cout << res << endl;
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);

	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}



// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>

// #define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
// #define endl '\n'
// #define lson pos<<1
// #define rson pos<<1|1

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;
// stack<int> st;
// int n;
// ll res=0;
// void solve(){
// 	cin>>n;
// 	int x=0;
// 	for(int i=1;i<=n;i++){
// 		cin>>x;
// 		while(!st.empty()&&x>=st.top()){
// 			st.pop();
// 		}
// 		st.push(x);
// 		res+=st.size()-1;
// 	}
// 	cout<<res<<endl;
// }

// int main(){
// 	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);

// 	int t=1;
// 	//cin>>t;
// 	while(t--){
// 		solve();
// 	}

// 	return 0;
// }