#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
int a[310][310];
void solve(){
	memset(a,0x3f,sizeof(a));
	int n,m,t;
	cin>>n>>m>>t;
	int x,y,h;
	for(int i=1;i<=n;i++) a[i][i]=0;
	for(int i=1;i<=m;i++){
		cin>>x>>y>>h;
		a[x][y]=h;
	}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++){
				a[i][j]=min(a[i][j],max(a[i][k],a[k][j]));
			}
	while(t--){
		cin>>x>>y;
		if(a[x][y]==0x3f3f3f3f) cout<<-1<<endl;
		else cout<<a[x][y]<<endl;
	}
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}