/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
	//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}
const int inf = 0x3f3f3f3f;
const ll dinf = 0x7f7f7f7f;
const int N = 100010;
vector<int> e[N];
int n, m; //节点数和操作数
int w[N];//节点权值
//dep:深度，sz：子树大小，fa：父亲节点，son：重儿子
int dep[N], sz[N], fa[N], son[N];//dfs1
//id:节点新id，nw：映射，id[u]=cnt,nw[cnt]=w[u]
int id[N], nw[N], top[N], cnt;//dfs2
void dfs1(int u, int father) {
	fa[u] = father, dep[u] = dep[father] + 1, sz[u] = 1;
	for (int v : e[u]) {
		if (v == father) continue;
		dfs1(v, u);
		sz[u] += sz[v];
		if (sz[v] > sz[son[u]]) son[u] = v;
	}
}
void dfs2(int u, int t) {
	id[u] = ++cnt; nw[cnt] = w[u];
	top[u] = t;
	if (!son[u]) return;
	dfs2(son[u], t);
	for (int v : e[u]) {
		if (v == fa[u] || v == son[u]) continue;
		dfs2(v, v);
	}
}
struct Tree {
	int l, r;
	ll add, sum;
} tr[N * 8];
void pushup(int u) {
	tr[u].sum = tr[lc].sum + tr[rc].sum;
}
void pushdown(int u) {
	if (tr[u].add) {
		tr[lc].sum += tr[u].add * (tr[lc].r - tr[lc].l + 1);
		tr[rc].sum += tr[u].add * (tr[rc].r - tr[rc].l + 1);
		tr[lc].add += tr[u].add;
		tr[rc].add += tr[u].add;
		tr[u].add = 0;
	}
}
void build(int u, int l, int r) {
	tr[u] = {l, r, 0, nw[l]};
	if (l == r) return;
	int mid = l + r >> 1;
	build(lc, l, mid), build(rc, mid + 1, r);
	pushup(u);
}
void update(int u, int l, int r, int k) {
	if (l <= tr[u].l && r >= tr[u].r) {
		tr[u].add += k;
		tr[u].sum += (ll)k * (tr[u].r - tr[u].l + 1);
		return;
	}
	pushdown(u);
	int mid = tr[u].l + tr[u].r >> 1;
	if (l <= mid) update(lc, l, r, k);
	if (r > mid) update(rc, l, r, k);
	pushup(u);
}
//修改[u,v]路径
void update_path(int u, int v, int k) {
	while (top[u] != top[v]) {//每次让u是深节点
		if (dep[top[u]] < dep[top[v]]) swap(u, v);
		update(1, id[top[u]], id[u], k);
		u = fa[top[u]];
	}
	//最后可能一个是另一个的祖先节点
	if (dep[u] < dep[v]) swap(u, v);
	update(1, id[v], id[u], k);
}
//修改以u为根的子树区间
void update_tree(int u, int k) {
	update(1, id[u], id[u] + sz[u] - 1, k);
}
//修改u的单点
void update_node(int u, int k) {
	update(1, id[u], id[u], k);
}

ll query(int u, int l, int r) {
	if (l <= tr[u].l && r >= tr[u].r) return tr[u].sum;
	pushdown(u);
	int mid = (tr[u].l + tr[u].r) >> 1;
	ll res = 0;
	if (l <= mid) res += query(lc, l, r);
	if (r > mid) res += query(rc, l, r);
	return res;
}
//查询[u,v]路径
ll query_path(int u, int v) {
	ll res = 0;
	while (top[u] != top[v]) {
		if (dep[top[u]] < dep[top[v]]) swap(u, v);
		res += query(1, id[top[u]], id[u]);
		u = fa[top[u]];
	}
	if (dep[u] < dep[v]) swap(u, v);
	res += query(1, id[v], id[u]);
	return res;
}
//查询u为根的子树区间
ll query_tree(int u) {
	return query(1, id[u], id[u] + sz[u] - 1);
}
void solve() {
	cin >> n >> m;
	// cout<<n<<m<<endl;
	for (int i = 1; i <= n; i++) cin >> w[i];
	for (int i = 1, x, y; i < n; i++) {
		cin >> x >> y;
		e[x].push_back(y);
		e[y].push_back(x);
	}
	dfs1(1, -1);
	dfs2(1, 1);
	build(1, 1, cnt);
	int op, x, k;
	for (int i = 1; i <= m; i++) {
		cin >> op;
		if (op == 1) {
			cin >> x >> k;
			update_node(x, k);
		} else if (op == 2) {
			cin >> x >> k;
			update_tree(x, k);
		} else {
			cin >> x;
			cout << query_path(1, x) << endl;
		}
	}

}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}