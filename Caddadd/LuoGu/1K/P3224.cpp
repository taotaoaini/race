/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second
#define mid ((l+r)>>1)

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}
const int inf = 0x3f3f3f3f;
const ll dinf = 0x7f7f7f7f;
//const int M=100010;
const int N = 100010;
int n, m, q, fa[N]; //f:并查集
int root[N], tot; //根节点,开点个数
int ls[N * 20], rs[N * 20], id[N * 20], sum[N * 20];
//id:节点编号，sum:重要度的出现次数之和
int find(int x) { //找根
	while (x != fa[x]) x = fa[x] = fa[fa[x]];
	return x;
}
void pushup(int u) { //上传
	sum[u] = sum[ls[u]] + sum[rs[u]];
}
int change(int u, int l, int r, int p, int i) { //点修
	if (!u) u = ++tot;
	if (l == r) {id[u] = i; sum[u]++; return u;}
	if (p <= mid) ls[u] = change(ls[u], l, mid, p, i);
	else rs[u] = change(rs[u], mid + 1, r, p, i);
	pushup(u); return u;
}
int merge(int x, int y) { //合并
	if (!x || !y) return x + y;
	ls[x] = merge(ls[x], ls[y]);
	rs[x] = merge(rs[x], rs[y]);
	pushup(x); return x;
}
int query(int u, int l, int r, int k) { //点查
	if (l == r) return id[u];
	int ans = 0;
	if (k <= sum[ls[u]]) ans = query(ls[u], l, mid, k);
	else ans = query(rs[u], mid + 1, r, k - sum[ls[u]]);
	return ans;
}
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	cin >> n >> m;
	int x, y;
	for (int i = 1; i <= n; i++) {
		fa[i] = i;
		cin >> x;
		root[i] = change(root[i], 1, n, x, i);
	}
	for (int i = 1; i <= m; i++) {
		cin >> x >> y;
		x = find(x);
		y = find(y);
		fa[y] = x;
		root[x] = merge(root[x], root[y]);
	}
	cin >> q;
	while (q--) {
		char ch;
		cin >> ch;
		if (ch== 'B') {
			cin >> x >> y;
			x = find(x); y = find(y);
			if (x == y) continue;
			fa[y] = x;
			root[x] = merge(root[x], root[y]);
		} else {
			cin >> x >> y;
			x = find(x);
			int res = query(root[x], 1, n, y);
			res = res ? res : -1;
			cout << res << endl;
		}
	}
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}