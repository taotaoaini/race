// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>

// #define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;
// const int N = 100010;
// int n, m, h[N], idx;
// struct edge {
// 	int v, ne, w;
// } e[300010];
// int dis[N], cnt[N];
// bool vis[N];
// queue<int> q;
// void add(int u, int v, int w) {
// 	e[++idx].v = v;
// 	e[idx].ne = h[u];
// 	e[idx].w = w;
// 	h[u] = idx;
// }
// bool spfa(int s) {
// 	dis[s] = 0;
// 	vis[s] = 1;
// 	q.push(s);
// 	while (!q.empty()) {
// 		int u = q.front(); q.pop();
// 		vis[u] = 0;
// 		for (int i = h[u]; i; i = e[i].ne) {
// 			int v = e[i].v;
// 			if (dis[v] < dis[u] + e[i].w) {
// 				dis[v] = dis[u] + e[i].w;
// 				if (vis[v] == 0) {
// 					cnt[v]++;
// 					if (cnt[v] > n + 1) return false; //此时就有n个点
// 					vis[v] = 1;
// 					q.push(v);
// 				}

// 			}
// 		}
// 	}
// 	return true;
// }
// void solve() {
// 	cin >> n >> m;
// 	int s = n + 1;
// 	for (int i = 1; i <= m; i++) {
// 		int t, u, v;
// 		cin >> t >> u >> v;
// 		switch (t) {
// 		case 1: {
// 			add(u, v, 0);
// 			add(v, u, 0);
// 			break;
// 		}
// 		case 2: {
// 			add(u, v, 1);
// 			break;
// 		}
// 		case 3: {
// 			add(v, u, 0);
// 			break;
// 		} case 4: {
// 			add(v, u, 1);
// 			break;
// 		} case 5: {
// 			add(u, v, 0);
// 			break;
// 		}
// 		}
// 	}
// 	for (int i = 1; i <= n; i++) {
// 		add(s, i, 0);
// 	}
// 	memset(dis, -1, sizeof(dis));
// 	if (!spfa(s)) {
// 		cout << -1<< endl;
// 		return;
// 	}
// 	int res=0;
// 	bool flag=false;
// 	for (int i = 1; i <= n; i++){
// 		// cout<<dis[i]<<endl;
// 		if(dis[i]==0) flag=true;
// 		res+=dis[i];
// 	}
// 	if(flag) res+=n;
// 	cout << res<<endl;
// }
// int main() {
// 	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
// 	int t = 1;
// 	//cin>>t;
// 	while (t--) {
// 		solve();
// 	}

// 	return 0;
// }








// 这题可以用缩点的方法做。首先我们只加上1、3、5三种情况的边，如果a<=b那么a向b连边，如果相等就两边都连。
// 后我们用Tarjan缩一遍环，然后可以构出一个DAG(有向无环图)。由于缩掉的都是1、3、5情况的边，那么他们构成的环就意味着环上的点必须相等。
// 缩环之后重构图，加上2、4情况的边，这个时候要特判：如果此时有一条2、4的边构成自环（意味着它在原图中连接了两个必须相等的点），所以直接输出-1结束。
// 然后我们给图来一遍拓扑排序，如果此时出现环（拓扑排序有些点没有访问到），必然意味着后面新加的2、4情况的边构成了环（1、3、5情况的边已经没有环了），此时也要直接输出-1结束。最后按照拓扑序来一遍dp就可以得出结果。（统计结果记得用long long）
// 对于X=1、X=3、X=5时，我们不难知道，想要使用最少的糖果数量，那么最好的方式就是两个人糖果数量一样。
// 对于X=2，那么A=B-1最优
// 对于X=4，那么B=A-1最优



// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>

// #define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;
// // A要少于B，则建一条A->B的边
// const int N = 100010;
// int n, k;
// /*
// scc:每个节点的归属新编号
// sum:缩点个数
// low：最小可到达时间戳
// dfn:每个节点的深度优先搜索时间戳
// cnt:
// */
// int scc[N], cnt, low[N], dfn[N], idx, tot[N];
// //scc：新的编号
// int dp[N];
// int in[N];
// ll ans;
// struct node {
// 	int ne;
// 	int v;
// };
// vector<node> e[N];
// vector<node> newe[N];
// bool ins[N];
// stack<int> s;

// void tarjan(int u) {
// 	low[u] = dfn[u] = ++idx;
// 	ins[u] = true;
// 	s.push(u);
// 	int len = e[u].size();
// 	for (int i = 0; i < len; i++) {
// 		int v = e[u][i].ne;
// 		if (dfn[v] == 0) {
// 			tarjan(v);
// 			low[u] = min(low[v], low[u]);
// 		} else {
// 			if (ins[v])
// 				low[u] = min(low[u], dfn[v]);
// 		}
// 	}
// 	if (dfn[u] == low[u]) {
// 		cnt++;
// 		scc[u] = cnt;
// 		ins[u] = false;
// 		tot[cnt]++;
// 		while (s.top() != u) {
// 			int t = s.top();
// 			ins[t] = false;
// 			scc[t] = cnt;
// 			s.pop();
// 			tot[cnt]++;
// 		}
// 		s.pop();
// 	}
// }
// void solve() {
// 	cin >> n >> k;
// 	for (int i = 1; i <= k; i++) {
// 		int t, x, y;
// 		cin >> t >> x >> y;
// 		switch (t) {
// 		case 1: {
// 			e[x].push_back((node) {y, 0});
// 			e[y].push_back((node) {x, 0});
// 			break;
// 		}
// 		case 2: {
// 			e[x].push_back((node) {y, 1});
// 			break;
// 		}
// 		case 3: {
// 			e[y].push_back((node) {x, 0});
// 			break;
// 		}
// 		case 4: {
// 			e[y].push_back((node) {x, 1});
// 			break;
// 		}
// 		case 5: {
// 			e[x].push_back((node){y, 0});
// 			break;
// 		}
// 		}
// 	}
// 		for(int i=1;i<=n;i++){
// 			if(dfn[i]==0) tarjan(i);
// 		}
// 		for(int i=1;i<=n;i++){
// 			int len=e[i].size();
// 			for(int j=0;j<len;j++){
// 				int v=e[i][j].ne;
// 				int xx=scc[i];
// 				int yy=scc[v];
// 				if(xx==yy&&e[i][j].v==1){
// 					cout<<-1<<endl;
// 					return;
// 				}
// 				if(xx!=yy){
// 					newe[xx].push_back((node){yy,e[i][j].v});
// 					in[yy]++;
// 					//会有重边
// 				}
// 			}
// 		}
// 		queue<int> q;
// 		for(int i=1;i<=cnt;i++){
// 			if(!in[i]){
// 				q.push(i);
// 				dp[i]=1;
// 			}
// 			while(!q.empty()){
// 				int cur=q.front();
// 				q.pop();
// 				int len=newe[cur].size();
// 				for(int i=0;i<len;i++){
// 					int v=newe[cur][i].ne;
// 					in[v]--;
// 					dp[v]=max(dp[v],dp[cur]+newe[cur][i].v);
// 					if(!in[v]) q.push(v);
// 				}
// 			}
// 		}
// 		for(int i=1;i<=cnt;i++){
// 			ans+=(ll)dp[i]*tot[i];
// 		}
// 		cout<<ans<<endl;
// 	}
// int main() {
// 	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
// 	int t = 1;
// 	//cin>>t;
// 	while (t--) {
// 		solve();
// 	}
// 	return 0;
// }


