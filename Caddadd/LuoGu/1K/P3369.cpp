#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
	//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}
const int N = 100010;
int op[N], a[N];
int sum[N * 4];
int b[N];
int m;
int n;


void pushup(int u) {
	sum[u] = sum[lc] + sum[rc];
}

void change(int u, int l, int r, int x, int k) {
	if (l == r) {sum[u] += k; return;}
	int mid = (l + r) >> 1;
	if (x <= mid) change(lc, l, mid, x, k);
	else change(rc, mid + 1, r, x, k);
	pushup(u);
}
//排名即为前缀和
int q_rank(int u, int l, int r, int x, int y) {
	if (x <= l && y >= r) return sum[u];
	int s = 0;
	int mid = (l + r) >> 1;
	if (x <= mid) s += q_rank(lc, l, mid, x, y);
	if (y > mid) s += q_rank(rc, mid + 1, r, x, y);
	return s;
}

//查询排名为x的数
int q_num(int u, int l, int r, int x) {
	if (l == r) return l;
	int mid = (l + r) >> 1;
	if (x <= sum[lc]) return q_num(lc, l, mid, x);
	else return q_num(rc, mid + 1, r, x - sum[lc]);
}

void solve() {
	cin >> n;
	for (int i = 1; i <= n; i++) {
		cin >> op[i] >> a[i];
		if (op[i] == 4) continue;
		b[++m] = a[i];
	}
	sort(b + 1, b + m + 1);
	m = unique(b + 1, b + m + 1) - b - 1;
	int id;
	for (int i = 1; i <= n; i++) {
		if (op[i] != 4) id = lower_bound(b + 1, b + m + 1, a[i]) - b;
		if (op[i] == 1) change(1, 1, m, id, 1);
		else if (op[i] == 2) change(1, 1, m, id, -1);
		else if (op[i] == 3) {int kth = id > 1 ? q_rank(1, 1, m, 1, id - 1) + 1 : 1; cout << kth << endl;}
		else if (op[i] == 4) {
			int idx = q_num(1, 1, m, a[i]);
			cout << b[idx] << endl;
		} else if (op[i] == 5) {
			int rk = q_rank(1, 1, m, 1, id - 1);	
			cout << b[q_num(1, 1, m, rk)] << endl;
		} else {
			//可能此时x还没有放入线段树中，但又是最大的。
			int rk = q_rank(1, 1, m, 1, id)+1;
			cout << b[q_num(1, 1, m, rk)] << endl;
		}
	}
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}