// /*
// ll=9.22*10^18 ((1LL << 63) - 1)
// int=2.1*10^9 ((1<<32)-1)
// ull=(1.844*10^19)

// double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
// double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
// double round(double x);//四舍五入,-2.7->-3,-2.2->-2

// fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
// cout<<fixed<<setprecision(3)<<1.2000;//->1.2

// 二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
// 三行四列的数组:arr[1][2]=arr[1*4+2]

// % + -
// << >>
// > >= < <=
// != & ^ | && ||

// string 从下标1开始读入
// char s[N];
// cin>>s+1;
// */
// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题

// #define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1
// #define fi first
// #define se second
// #define mid ((l+r)>>1)

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;

// /*
// 快读代码模板
// a=read<int>(),b=read<ll>(),c=read<__int128>();
// */
// template<typename T>
// void read(T &x) {
// 	x = 0; bool flag(0); char ch = getchar();
// 	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
// 	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
// 	flag ? x = -x : 0;
// }
// template<typename T>
// void write(T x, bool mode = 1) {//mode=1为换行，0为空格
// 	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
// 	do stk[++top] = x % 10, x /= 10; while (x);
// 	while (top) putchar(stk[top--] | 48);
// 	mode ? putchar('\n') : putchar(' ');
// }
// const int inf = 0x3f3f3f3f;
// const ll dinf = 0x7f7f7f7f;
// //const int M=100010;
// const int N = 100010;
// ll a[N];//初始化数组
// struct SegTree {
// 	ll sum[N * 4], add[N * 4];

// 	void pushup(int u) {//向上传
// 		sum[u] = sum[lc] + sum[rc];
// 	}
// 	void build(int u, int l, int r) {//初始化
// 		sum[u] = a[l];
// 		if (l == r) {
// 			return;
// 		}
// 		build(lc, l, mid);
// 		build(rc, mid + 1, r);
// 		pushup(u);
// 	}
// 	void pushdown(int u, int l, int r) { //向下传
// 		if (add[u]) {
// 			sum[lc] += add[u] * (mid - l + 1);
// 			sum[rc] += add[u] * (r - mid);
// 			add[lc] += add[u];
// 			add[rc] += add[u];
// 			add[u] = 0;
// 		}
// 	}
// 	void change(int u, int l, int r, int x, int y, int k) {//区间修改
// 		if (x > r || y < l) return;
// 		if (x <= l && y >= r) {
// 			sum[u] += (r - l + 1) * k;
// 			add[u] += k;
// 			return;
// 		}
// 		pushdown(u, l, r);
// 		change(lc, l, mid, x, y, k);
// 		change(rc, mid + 1, r, x, y, k);
// 		pushup(u);
// 	}
// 	ll query(int u, int l, int r, int x, int y) {//区间查询
// 		if (x > r || y < l) return 0;
// 		if (x <= l && r <= y) return sum[u];
// 		pushdown(u, l, r);
// 		return query(lc, l, mid, x, y) + query(rc, mid + 1, r, x, y);
// 	}
// } tr;

// int n, m;
// void solve() {
// 	//freopen("a.in","r",stdin);
// 	//freopen("a.out","w",stdout);
// 	cin >> n >> m;
// 	for (int i = 1; i <= n; i++) {
// 		cin >> a[i];
// 	}
// 	tr.build(1, 1, n);
// 	for (int i = 1, op, x, y, k; i <= m; i++) {
// 		cin >> op;
// 		if (op == 1) {
// 			cin >> x >> y >> k;
// 			tr.change(1, 1, n, x, y, k);
// 		} else {
// 			cin >> x >> y;
// 			cout << tr.query(1, 1, n, x, y) << endl;
// 		}
// 	}
// }

// int main() {
// 	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
// 	int t = 1;
// 	//cin>>t;
// 	while (t--) {
// 		solve();
// 	}

// 	return 0;
// }

/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;

string+string
to_string(int)
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls u<<1
#define rs u<<1|1
#define fi first
#define se second
#define mid ((l+r)/2)

using i8 = signed char;
using u8 = unsigned char;
using i16 = signed short int;
using u16 = unsigned short int;
using i32 = signed int;
using u32 = unsigned int;
using f32 = float;
using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using i128 = __int128_t;
using u128 = __uint128_t;
using f128 = long double;

using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<int128>();
*/

template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}

template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;
int a[N];
struct Solution2 {
	i64 sum[N * 4], tag[N * 4];
	void build(int u, int l, int r) {
		sum[u] = a[l];
		if (l == r) return;
		build(ls, l, mid);
		build(rs, mid + 1, r);
		sum[u] = sum[ls] + sum[rs];
	}
	void change(int u, int l, int r, int ql, int qr, i64 x) {
		sum[u]+=(min(r,qr)-max(ql,l)+1)*x;
		if (ql <= l && qr >= r) {
			tag[u] += x; return;}
		if (ql <= mid) change(ls, l, mid, ql, qr, x);
		if (qr > mid) change(rs, mid + 1, r, ql, qr, x);
	}
	i64 query(int u, int l, int r, int ql, int qr, i64 s) {
		if (ql <= l && qr >= r) return sum[u] + (min(r, qr) - max(ql, l)+1) * s;
		i64 res = 0; s += tag[u];
		if (ql <= mid) res += query(ls, l, mid, ql, qr, s);
		if (qr > mid) res += query(rs, mid + 1, r, ql, qr, s);
		return res;
	}
} st;
void solve() {
	int n, m;
	cin >> n>>m;
	for (int i = 1; i <= n; i++) cin >> a[i];
	st.build(1, 1, n);
	for (int i = 1, op, l, r, x; i <= m; i++) {
		cin >> op;
		// cout<<"op:::"<<op<<endl;
		if (op == 1) {
			cin >> l >> r >> x;
			st.change(1, 1, n, l, r, x);
		} else {
			cin >> l >> r;
			cout << st.query(1, 1, n, l, r, 0) << endl;
		}
	}
}
int main() {
	solve();
}