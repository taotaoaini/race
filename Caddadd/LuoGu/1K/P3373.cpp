#include<iostream>
#include<vector>

using namespace std;

// (1) https://www.luogu.com.cn/problem/P3373
// 同时维护乘法和加法
// 在下传tag的时候，乘法优先
// 父节点的懒标记为m和a，子节点的懒标记为mul和add
// (x*mul+add)*m+a ==>x*(mul*m)+(add*m+a)
// 子节点新的mul=(mul*m),add=(add*m+a)
#define lc p*2
#define rc p*2+1
using i64 = long long;
const int N = 100010;
int a[N]; //数组
int n, q, mod;
struct node { //存储
    i64 sum, mul, add;
    int l, r;
};

struct SegmentTree {
    int n;
    vector<node> tr;

    SegmentTree(int n_) : n(n_), tr(4 * n + 10) {}

    void pull(int p) { //上传
        tr[p].sum = tr[lc].sum + tr[rc].sum;
    }

    void calc(node &t, i64 m, i64 a) { //计算
        t.sum = (t.sum * m + (t.r - t.l + 1) * a) % mod;
        t.mul = t.mul * m % mod;
        t.add = (t.add * m + a) % mod;
    }

    void push(int p) { //下传
        calc(tr[lc], tr[p].mul, tr[p].add);
        calc(tr[rc], tr[p].mul, tr[p].add);
        tr[p].add = 0;
        tr[p].mul = 1;
    }

    void build(int p, int l, int r) {  //建树
        tr[p] = {a[l], 1, 0, l, r}; //节点初始化
        if (l == r) return;
        int mid = (l + r) / 2;
        build(lc, l, mid);
        build(rc, mid + 1, r);
        pull(p);
    }

    void change(int p, int ql, int qr, int m, int a) { //区改m：mul,a:add
        if (ql > tr[p].r || qr < tr[p].l) return;
        if (ql <= tr[p].l && tr[p].r <= qr) {
            calc(tr[p], m, a);
            return;
        }
        push(p);
        change(lc, ql, qr, m, a);
        change(rc, ql, qr, m, a);
        pull(p);
    }

    i64 query(int p, int ql, int qr) { //区查
        if (ql > tr[p].r || qr < tr[p].l) return 0;
        if (ql <= tr[p].l && tr[p].r <= qr) return tr[p].sum;
        push(p);
        return (query(lc, ql, qr) + query(rc, ql, qr)) % mod;
    }

    i64 query(int p, int x) { //点查
        if (tr[p].l == tr[p].r) return tr[p].sum;
        push(p);
        if (x <= tr[lc].r) return query(lc, x);
        else return query(rc, x);
    }
};
int main() {
    cin >> n >> q >> mod;
    for (int i = 1; i <= n; i++) cin >> a[i];
    SegmentTree *tr = new SegmentTree(n);
    tr->build(1, 1, n);
    for (int i = 1, op, l, r, x; i <= q; i++) {
        cin >> op;
        if (op == 1) {
            cin >> l >> r >> x;
            tr->change(1, l, r, x, 0);
        } else if (op == 2) {
            cin >> l >> r >> x;
            tr->change(1, l, r, 1, x); //默认mul为1
        } else {
            cin >> l >> r;
            cout << tr->query(1, l, r) << endl;
        }
    }
}