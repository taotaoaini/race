/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题

// #define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1
// #define fi first
// #define se second

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;

// /*
// 快读代码模板
// a=read<int>(),b=read<ll>(),c=read<__int128>();
// */
// template<typename T>
// void read(T &x) {
// 	x = 0; bool flag(0); char ch = getchar();
// 	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
// 	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
// 	flag ? x = -x : 0;
// }
// template<typename T>
// void write(T x, bool mode = 1) {
// 	//mode=1为换行，0为空格
// 	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
// 	do stk[++top] = x % 10, x /= 10; while (x);
// 	while (top) putchar(stk[top--] | 48);
// 	mode ? putchar('\n') : putchar(' ');
// }
// const int inf = 0x3f3f3f3f;
// const ll dinf = 0x7f7f7f7f;
// const int N = 1000010;

// int m, n;
// string s, p;
// int ne[N];
// void solve() {
// 	cin >> s >> p;
// 	m = s.size(), n = p.size();
// 	s=" "+s;
// 	p=" "+p;
// 	// cout<<m<<endl;
// 	ne[1]=0;
// 	//i扫描模式串，j扫描前缀
// 	//初始化ne[1]=0,i=2,j=0
// 	//
// 	for(int i=2,j=0;i<=n;i++){
// 		while(j&&p[i]!=p[j+1]) j=ne[j];
// 		if(p[i]==p[j+1]) j++;
// 		ne[i]=j;
// 	}
// 	for(int i=1,j=0;i<=m;i++){
// 		while(j&&s[i]!=p[j+1]) j=ne[j];
// 		if(s[i]==p[j+1]) j++;
// 		if(j==n) cout<<i-n+1<<endl;
// 	}
// 	for(int i=1;i<=n;i++) cout<<ne[i]<<" ";
// 	cout<<endl;
// }

// int main() {
// 	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
// 	int t = 1;
// 	//cin>>t;
// 	while (t--) {
// 		solve();
// 	}

// 	return 0;
// }



// /*
// ll=9.22*10^18 ((1LL << 63) - 1)
// int=2.1*10^9 ((1<<32)-1)
// ull=(1.844*10^19)

// double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
// double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
// double round(double x);//四舍五入,-2.7->-3,-2.2->-2

// fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
// cout<<fixed<<setprecision(3)<<1.2000;//->1.2

// 二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
// 三行四列的数组:arr[1][2]=arr[1*4+2]

// % + -
// << >>
// > >= < <=
// != & ^ | && ||

// string 从下标1开始读入
// char s[N];
// cin>>s+1;
// */
// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题

// #define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1
// #define fi first
// #define se second

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;

// /*
// 快读代码模板
// a=read<int>(),b=read<ll>(),c=read<__int128>();
// */
// template<typename T>
// void read(T &x) {
// 	x = 0; bool flag(0); char ch = getchar();
// 	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
// 	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
// 	flag ? x = -x : 0;
// }
// template<typename T>
// void write(T x, bool mode = 1) {
// 	//mode=1为换行，0为空格
// 	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
// 	do stk[++top] = x % 10, x /= 10; while (x);
// 	while (top) putchar(stk[top--] | 48);
// 	mode ? putchar('\n') : putchar(' ');
// }
// const int inf = 0x3f3f3f3f;
// const ll dinf = 0x7f7f7f7f;
// const int N = 1000010;
// int n, m;
// char s[N], p[N];
// string s,p;
// int ne[N];



// void solve() {
// 	scanf("%s%s", s + 1, p + 1);
// 	n = strlen(s + 1), m = strlen(p + 1);
// 	ne[1] = 0;
// 	for (int i = 2, j = 0; i <= m; i++) {
// 		while (j && p[i] != p[j + 1]) j = ne[j];
// 		if (p[i] == p[j + 1]) j++;
// 		ne[i] = j;
// 	}
// 	for (int i = 1, j = 0; i <= n; i++) {
// 		while (j && s[i] != p[j + 1]) j = ne[j];
// 		if (s[i] == p[j + 1]) j++;
// 		if (j == m) printf("%d\n", i - m + 1);
// 	}
// 	for (int i = 1; i <= m; i++) printf("%d ", ne[i]);
// 	puts("");
// }

// int main() {
// 	// ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
// 	int t = 1;
// 	//cin>>t;
// 	while (t--) {
// 		solve();
// 	}

// 	return 0;
// }







#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls p<<1
#define rs p<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;

string s, p;
int n, m;
int ne[N];
void init() {
	ne[1] = 0;
	for (int i = 2,j = 0; i <= m; i++) {
		while(j&&p[i]!=p[j+1]) j=ne[j];
		if(p[i]==p[j+1]) j++;
		ne[i]=j;
	}
}
void find(){
	for(int i=1,j=0;i<=n;i++){
		while(j&&s[i]!=p[j+1]) j=ne[j];
		if(s[i]==p[j+1]) j++;
		if(j==m) cout<<i-m+1<<endl;
	}
}
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	cin >> s >> p;
	n = s.length(), m = p.length();
	s = " " + s;
	p = " " + p;
	init();
	find();
	for(int i=1;i<=m;i++) cout<<ne[i]<<" ";
	cout<<endl;
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	//cin>>t;

	while (t--) {
		solve();
		//init();
	}

	return 0;
}