/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

// /*
// 快读代码模板
// a=read<int>(),b=read<ll>(),c=read<__int128>();
// */
// template<typename T>
// void read(T &x) {
// 	x = 0; bool flag(0); char ch = getchar();
// 	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
// 	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
// 	flag ? x = -x : 0;
// }
// template<typename T>
// void write(T x, bool mode = 1) {
// 	//mode=1为换行，0为空格
// 	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
// 	do stk[++top] = x % 10, x /= 10; while (x);
// 	while (top) putchar(stk[top--] | 48);
// 	mode ? putchar('\n') : putchar(' ');
// }
// const int inf = 0x3f3f3f3f;
// const ll dinf = 0x7f7f7f7f;
// const int N = 500010;
// const int M=N*2;
/*倍增算法*/
// int dep[N],pa[N][22];
// vector<int> e[N];
// int n,m,s,a,b;
// void dfs(int x,int fa){
// 	dep[x]=dep[fa]+1;
// 	pa[x][0]=fa;
// 	for(int i=1;i<=20;i++){
// 		pa[x][i]=pa[pa[x][i-1]][i-1];
// 	}
// 	for(int y:e[x]){
// 		if(y!=fa) dfs(y,x);
// 	}
// }
// int lca(int x,int y){
// 	if(dep[x]<dep[y]) swap(x,y);
// 	for(int i=20;i>=0;i--){
// 		if(dep[pa[x][i]]>=dep[y]) x=pa[x][i];
// 	}
// 	if(x==y) return y;
// 	for(int i=20;i>=0;i--){
// 		if(pa[x][i]!=pa[y][i]) x=pa[x][i],y=pa[y][i];
// 	}
// 	return pa[x][0];
// }

// void solve() {
// 	cin>>n>>m>>s;
// 	for(int i=1,x,y;i<n;i++){
// 		cin>>x>>y;
// 		e[x].push_back(y);
// 		e[y].push_back(x);
// 	}
// 	dfs(s,0);
// 	for(int i=1,x,y;i<=m;i++){
// 		cin>>x>>y;
// 		cout<<lca(x,y)<<endl;
// 	}
// }

/*******************************/
const int N=500005,M=2*N;
vector<int> e[N];
vector<pair<int, int>> query[N];
int fa[N], vis[N], ans[M];
int n, m, s;
int find(int x){
  if(x==fa[x]) return x;
  return fa[x]=find(fa[x]);
}
// // void tarjan(int x) {
// // 	vis[x] = true;
// // 	for (auto v : e[x]) {
// // 		if (!vis[v]) {
// // 			tarjan(v);
// // 			fa[v] = x;
// // 		}
// // 	}
// // 	for (auto q : query[x]) {
// // 		int v = q.first, i = q.second;
// // 		if (vis[v]) ans[i] = find(v);
// // 	}
// // }
void tarjan(int x){
  vis[x]=true;//标记x已访问
  for(auto y : e[x]){
    if(!vis[y]){
      tarjan(y);
      fa[y]=x;//回到x时指向x
    }        
  }
  //离开x时找LCA
  for(auto q : query[x]){
    int y=q.first,i=q.second;
    if(vis[y])ans[i]=find(y);
  }
}
void solve() {
   freopen("./input/P3379_1.in","r",stdin);   //从文件mid.in里读取数据
   freopen("./output/1.out","w",stdout); 
	cin>>n>>m>>s;
	for (int i = 1, x, y; i < n; i++) {
		cin>>x>>y;
		e[x].push_back(y);
		e[y].push_back(x);
	}
	for(int i=1,x,y;i<=m;i++){
		cin>>x>>y;
		query[x].push_back({y,i});
		query[y].push_back({x,i});
	}
	for(int i=1;i<N;i++) fa[i]=i;
	tarjan(s);
	for(int i=1;i<=m;i++){
		cout<<ans[i]<<endl;
	}
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}
// 2.0s
// #include <iostream>
// #include <algorithm>
// #include <cstring>
// #include <vector>
// using namespace std;

// const int N=500005,M=2*N;
// int n,m,s,a,b;
// vector<int> e[N];
// vector<pair<int,int>>query[N];
// int fa[N],vis[N],ans[M]; 

// int find(int x){
//   if(x==fa[x]) return x;
//   return fa[x]=find(fa[x]);
// }
// void tarjan(int x){
//   vis[x]=true;//标记x已访问
//   for(auto y : e[x]){
//     if(!vis[y]){
//       tarjan(y);
//       fa[y]=x;//回到x时指向x
//     }        
//   }
//   //离开x时找LCA
//   for(auto q : query[x]){
//     int y=q.first,i=q.second;
//     if(vis[y])ans[i]=find(y);
//   }
// }
// int main(){
// 	   freopen("./input/P3379_1.in","r",stdin);   //从文件mid.in里读取数据
//    freopen("./output/1.out","w",stdout); 
//   scanf("%d%d%d", &n,&m,&s);
//   for(int i=1; i<n; i++){
//     scanf("%d%d",&a,&b);
//     e[a].push_back(b);
//     e[b].push_back(a);
//   }
//   for(int i=1;i<=m;i++){
//     scanf("%d%d",&a,&b);
//     query[a].push_back({b,i});
//     query[b].push_back({a,i});
//   }
  
//   for(int i=1;i<=N;i++)fa[i]=i;
//   tarjan(s);
//   for(int i=1; i<=m; i++)
//     printf("%d\n",ans[i]);
//   return 0;
// }