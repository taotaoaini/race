#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
int n1,n2,m;
const int N=510;
const int M=50010;
struct edge{
    int v,ne;
}e[M];
int h[N],idx;
int match[N];
bool vis[N];
void add(int u,int v){
    e[++idx]={v,h[u]};
    h[u]=idx;
}
int find(int x){
    for(int i=h[x];i;i=e[i].ne){
        int y=e[i].v;
        if(!vis[y]){
            vis[y]=1;
            if(!match[y]||find(match[y])){
                match[y]=x;
                return true;
            }
        }
    }
    return false;
}
void solve(){
cin>>n1>>n2>>m;
    for(int i=1;i<=m;i++){
        int x,y;
        cin>>x>>y;
        add(x,y);
    }
    int res=0;
    for(int i=1;i<=n1;i++){
        memset(vis,0,sizeof(vis));
        if(find(i)) res++;
    }
    cout<<res<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}