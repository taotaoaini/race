// // #include<iostream>
// // #include<cstring>
// // #include<algorithm>
// // #include<map>
// // #include<set>
// // #include<vector>
// // #include<queue>
// // #include<sstream>
// // #include<cmath>
// // #include<list>
// // #include<bitset>
// // #include<unordered_map>
// // #include<unordered_set>
// // #include<stack>

// // #define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
// // #define endl '\n'
// // #define lc u<<1
// // #define rc u<<1|1

// // using ll = long long;
// // using ull = unsigned long long;
// // using pii = std::pair<int, int>;
// // using pll = std::pair<ll, ll>;
// // using namespace std;
// // const int N=1e4+5;
// // //f[i]意义同low[i]。
// // // /u[i]记录原图每个点的点权。
// // //w[k]记录第k个强连通分量的联合点权。
// // //ans[i]记录从i点出发的路径最大点权（记忆化搜索）。
// // //mp为某个点i属于哪个强连通分量的索引(mp{f[i] -> k})。
// // int f[N], dfn[N], inStack[N], w[N], ans[N], u[N];
// // int n, m, idx=0, cnt=0;
// // vector <int> e[N], g[N];
// // // 记录从i点出发的边集，g[k]记录第k个强连通分量的点集。
// // map <int, int> mp;
// // stack <int> stk;
// // int tarjan(int x){
// // 	dfn[x]=f[x]=++idx;
// // 	stk.push(x);
// // 	instack[x]=1;
// // 	for(int i=0;i<e[x].size();i++){
// // 		int v=e[x][i];
// // 		if(!dfn[v]){
// // 			tarjan(v);
// // 			f[x]=min(f[x],f[v]);
// // 		}else if(inStack[v]){
// // 			f[x]=min(f[x],f[v]);
// // 		}
// // 	}
// // 	if(dfn[x]==f[x]){
// // 		mp.insert(pair<int,int>(f[x],++cnt));
// // 		w[cnt]=0;
// // 		do{
// // 			int t=stk.top();
// // 			stk.pop();
// // 			inStack[t]=0;
// // 			g[cnt].push_back(t);
// // 			f[t]=f[x];
// // 			w[cnt]+=u[t];
// // 		}while(x!=t);
// // 	}
// // }
// // void solve(){
	
// // }
// // int main() {
// //     ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
// //     int t=1;
// //     //cin>>t;
// //     while(t--){
// //     	solve();
// //     }
    
// //     return 0;
// // }




// #include <cstdio>
// #include <cstring>
// #include <iostream>
// #include <algorithm>
// #include <vector>
// using namespace std;

// const int N=10010;
// int n,m,a,b;
// vector<int> e[N],ne[N]; 
// int dfn[N],low[N],tot;
// int stk[N],top;
// bool cut[N];
// vector<int> dcc[N];
// int root,cnt,num,id[N];

// void tarjan(int x){
//   dfn[x]=low[x]=++tot;
//   stk[++top]=x;
//   if(x==root&&!e[x].size()){//孤立点
//     dcc[++cnt].push_back(x);
//     return;
//   }
//   int child=0;  
//   for(int y : e[x]){
//     if(!dfn[y]){//若y尚未访问
//       tarjan(y);////
//       low[x]=min(low[x],low[y]); 
//       if(low[y]>=dfn[x]){
//         child++;
//         if(x!=root||child>1)
//           cut[x]=true; 
//         int z; cnt++;
//         printf("vDCC:");
//         do{ //记录vDCC
//           z=stk[top--];
//           dcc[cnt].push_back(z);
//           printf("%d ",z);
//         }while(z!=y);
//         dcc[cnt].push_back(x);
//         printf("%d\n",x);
//       }
//     }
//     else //若y已经访问
//       low[x]=min(low[x],dfn[y]);
//   }
// }
// int main(){
//   cin>>n>>m;
//   while(m --){
//     cin>>a>>b;
//     e[a].push_back(b),
//     e[b].push_back(a);
//   }
//   for(root=1;root<=n;root++)
//     if(!dfn[root])
//       tarjan(root);
      
//   //给每个割点一个新编号（cnt+1开始）
//   num=cnt;
//   for(int i=1;i<=n;i++)
//     if(cut[i])id[i]=++num;
//   //建新图，从每个vDCC向对应割点连边
//   for(int i=1;i<=cnt;i++){
//     for(int j=0;j<dcc[i].size();j++){
//       int x=dcc[i][j];
//       if(cut[x]){
//         ne[i].push_back(id[x]),
//         ne[id[x]].push_back(i);        
//       }
//     }
//   }
//   return 0;
// }


// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>

// #define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;
// const int N=10010;
// it n,m,a,b;
// vector<int> e[N],ne[N];
// int dfn[N],low[N],idx;
// int stk[N],top;
// bool cut[N];
// vector<int> dcc[N];
// void tarjan(int x){
// 	dfn[x]=low[x]=++idx;
// 	stk[++top]=x;
// 	if(x==root&&!e[x].size()){
// 		dcc[++cnt].push_back(x);
// 		return;
// 	}
// 	int ch=0;
// 	for(int y:e[x]){
// 		if(!dfn[y]){
// 			tarjan(y);
// 			low[x]=min(low[x],low[y]);
// 			if(low[y]>=dfn[x]){
// 				ch++;
// 			if(x!=root||child>1) cut[x]=true;
// 			int z;cnt++;
// 			do{
// 				z=stk[top--];
// 				dcc[cnt].push_back(z);
// 			}while(z!=y);
// 			dcc[cnt].push_back(x);
// 			}
// 		}else
// 			low[x]=min(low[x],dfn[y]);
// 	}
// }
// void solve(){
// 		cin>>n>>m;
// 		while(m--){
// 			cin>>a>>b;
// 			e[a].push_back(b);
// 			e[b].push_back(a);
// 		}
// 		for(root=1;root<=n;root++){
// 			if(!dfn[root])
// 				tarjan(root);
// 		}
// 		num=cnt;
// 		for(int i=1;i<=n;i++){
// 			if(cut[i]) id[i]=++num;
// 		}
// 		for(int i=1;i<=cnt;i++){
// 			for(int j=0;j<dcc[i].size();j++){
// 				int x=dcc[i][j];
// 				if(cut[x]){
// 					ne[i].push_back(id[x]);
// 					ne[id[x]].push_back(i);
// 				}
// 			}
// 		}
// }
// int main() {
//     ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
//     int t=1;
//     //cin>>t;
//     while(t--){
//     	solve();
//     }
    
//     return 0;
// }





