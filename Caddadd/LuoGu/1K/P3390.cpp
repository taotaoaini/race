#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
    x = 0; bool flag(0); char ch = getchar();
    while (!isdigit(ch)) flag = ch == '-', ch = getchar();
    while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
    flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
    //mode=1为换行，0为空格
    x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
    do stk[++top] = x % 10, x /= 10; while (x);
    while (top) putchar(stk[top--] | 48);
    mode ? putchar('\n') : putchar(' ');
}
const int mod=1e9+7;
struct matrix{
	ll c[110][110];
	matrix(){memset(c,0,sizeof(c));}
}A,res;
ll n,k;

matrix operator*(matrix &x,matrix &y){
	matrix t;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			for(int k=1;k<=n;k++)
				t.c[i][j]=(t.c[i][j]+x.c[i][k]*y.c[k][j])%mod;
			//常数优化：调换j，k的循环次序，提高空间访问效率
	return t;
}
void qmi(ll k){
	for(int i=1;i<=n;i++) res.c[i][i]=1;
	while(k){
		if(k&1) res=res*A;
		A=A*A;
		k>>=1;
	}
}
void solve() {
	cin>>n>>k;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++) cin>>A.c[i][j];
	qmi(k);
	for(int i=1;i<=n;i++){
		{for(int j=1;j<=n;j++) cout<<res.c[i][j]<<" ";}
		cout<<endl;
	}
}
int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    //cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}