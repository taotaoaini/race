#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls p<<1
#define rs p<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;

int n, m;
struct node {
	int a, b, c;
} v[N];
int P[N];
int s[N];
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	cin >> n >> m;
	for (int i = 1; i <= m; i++) {
		cin >> P[i];
	}
	for (int i = 1; i <= n - 1; i++) {
		auto& p = v[i];
		cin >> p.a >> p.b >> p.c;
	}
	for (int i = 1; i <= m - 1; i++) {
		int x = P[i], y = P[i + 1];
		if (x > y) swap(x, y);
		s[x] += 1;
		s[y] -= 1;
	}
	i64 res = 0;
	for (int i = 1; i <= n - 1; i++) {
		s[i] += s[i - 1];
		if (!s[i]) continue;
		auto& p = v[i];
		res += min(p.c + 1LL * s[i] * p.b, 1LL * p.a * s[i]); // 差分贪心
	}
	cout << res << endl;
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	//cin>>t;

	while (t--) {
		solve();
		//init();
	}

	return 0;
}