/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
	//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}
const int inf = 0x3f3f3f3f;
const ll dinf = 0x7f7f7f7f;
const int N = 500010;
int ls[N * 25], rs[N * 25], sum[N * 25];
int root[N], tot;
int n, m;
void change(int& u, int v, int l, int r, int x) {
	u = ++tot;
	ls[u] = ls[v]; rs[u] = rs[v]; sum[u] = sum[v] + 1;
	if (l == r) return;
	int mid = (l + r) >> 1;
	if (x <= mid) change(ls[u], ls[v], l, mid, x);
	if (x > mid) change(rs[u], rs[v], mid + 1, r, x);
}
int query(int u, int v, int l, int r, int len) {
	if (l == r) return l;
	int mid = (l + r) >> 1;
	if (sum[ls[u]] - sum[ls[v]] > len / 2) return query(ls[u], ls[v], l, mid, len);
	if (sum[rs[u]] - sum[rs[v]] > len / 2) return query(rs[u], rs[v], mid + 1, r, len);
	return 0;
}
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	cin >> n >> m;
	for (int i = 1, x; i <= n; i++) {
		cin >> x;
		change(root[i], root[i - 1], 1, n, x);
	}
	for (int i = 1, l, r; i <= m; i++) {
		cin >> l >> r;
		cout << query(root[r], root[l - 1], 1, n, r - l + 1) << endl;
	}
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}