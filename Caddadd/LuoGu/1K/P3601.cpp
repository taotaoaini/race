#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read() {
    __int128 x = 0, f = 1;
    char ch = getchar();
    while (ch < '0' || ch > '9') {
        if (ch == '-') f = -1;
        ch = getchar();
    }
    while (ch >= '0' && ch <= '9') {
        x = x * 10 + ch - '0';
        ch = getchar();
    }
    return x * f;
}
//__int128的输出
inline void print(__int128 x) {
    if (x < 0) {
        putchar('-');
        x = -x;
    }
    if (x > 9)
        print(x / 10);
    putchar(x % 10 + '0');
}
const int N = 1e6 + 10;
const ll mod = 666623333;
int pr[N], cnt;
bool vis[N];
void get_pr(int n) {
    for (int i = 2; i<= n; i++) {
        if (!vis[i]) pr[cnt++] = i;
        for (int j = 0; pr[j] <= n / i; j++) {
            vis[pr[j]*i] = true;
            if (i % pr[j] == 0) break;
        }
    }
}
// N=1e6 + 10;
vector<int> ans[N];
ll phi[N];
void solve() {
    ll l, r, res=0;
    cin>>l>>r;
    get_pr((int)sqrt(r));
    for(int i=0;i<cnt;i++){
        int p=pr[i];
        for(ll j=((l-1)/p+1)*p;j<=r;j+=p)
            ans[j-l].push_back(p);
    }
    for(ll i=l;i<=r;i++){
        ll tmp=i;
        phi[i-l]=i;
        for(int p:ans[i-l]){
            phi[i-l]=phi[i-l]/p*(p-1);
            while(tmp%p==0) tmp/=p;
        }
        if(tmp>1) phi[i-l]=phi[i-l]/tmp*(tmp-1);
        //phi:互质的数
        //i-phi：不互质的数
        res=(res+i-phi[i-l])%mod;
    }
    cout<<res<<endl;
}
int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    //cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}