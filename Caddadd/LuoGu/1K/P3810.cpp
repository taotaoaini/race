// CDQ分治(归并排序)+树状数组 O(nlognlogn)
#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;

#define lowbit(x) x&-x
#define mid (l+r>>1)
const int N = 100010, M = 200010;
int n, m;
int s[M], ans[N];
struct node {
  int x, y, z;
  int cnt, s; //cnt:重复元素个数,s:≤ai的个数
  bool operator==(const node& t) const {
    return x == t.x && y == t.y && z == t.z;
  }
} a[N];

bool cmpx(node &a, node &b) { //按x排序
  if (a.x != b.x) return a.x < b.x;
  if (a.y != b.y) return a.y < b.y;
  return a.z < b.z;
}
bool cmpy(node &a, node &b) { //按y排序
  return a.y < b.y;
}
void change(int x, int k) { //向后修
  while (x <= M) s[x] += k, x += lowbit(x);
}
int query(int x) { //向前查
  int t = 0;
  while (x) t += s[x], x -= lowbit(x);
  return t;
}
void CDQ(int l, int r) { //CDQ分治(归并排序)
  if (l == r) return;
  CDQ(l, mid);
  CDQ(mid + 1, r); //按x分裂
  int i = l, j = mid + 1;
  while (i <= mid && j <= r) {
    if (a[i].y <= a[j].y)
      change(a[i].z, a[i].cnt), i++; //zi加入树状数组
    else a[j].s += query(a[j].z), j++; //累计<=zj的个数
  }
  while (i <= mid) change(a[i].z, a[i].cnt), i++;
  while (j <= r) a[j].s += query(a[j].z), j++;

  for (j = l; j <= mid; j++)change(a[j].z, -a[j].cnt); //清空树状数组
  sort(a + l, a + r + 1, cmpy); //按y排序
}

void solve1() {
  cin >> n >> m;
  for (int i = 1; i <= n; i++) {
    cin >> a[i].x >> a[i].y >> a[i].z;
    a[i].cnt = 1;
  }
  sort(a + 1, a + n + 1, cmpx);
  int k = 1;
  for (int i = 2; i <= n; i++) {
    if (a[i] == a[k]) a[k].cnt++;
    else a[++k] = a[i]; //去重
  }
  CDQ(1, k); //CDQ分治
  for (int i = 1; i <= k; i++) //统计答案
    ans[a[i].s + a[i].cnt - 1] += a[i].cnt;
  for (int i = 0; i < n; i++) cout << ans[i] << endl;
}
int main() {
  solve1();
}