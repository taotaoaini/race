// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>

// #define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;
// //__int128的输入
// inline __int128 read(){
//     __int128 x=0,f=1;
//     char ch=getchar();
//     while(ch<'0'||ch>'9'){
//         if(ch=='-') f=-1;
//         ch=getchar();
//     }
//     while(ch>='0'&&ch<='9'){
//         x=x*10+ch-'0';
//         ch=getchar();
//     }
//     return x*f;
// }
// //__int128的输出
// inline void print(__int128 x){
//     if(x<0){
//         putchar('-');
//         x=-x;
//     }
//     if(x>9)
//         print(x/10);
//     putchar(x%10+'0');
// }
// ll n,p;
// ll qmi(ll a,ll b){
// 	ll res=1;
// 	while(b){
// 		if(b&1) res=res*a%p;
// 		b>>=1;
// 		a=a*a%p;
// 	}
// 	return res;
// }
// void solve(){
// 	cin>>n>>p;
// 	for(ll i=1;i<=n;i++){
// 		cout<<qmi(i,p-2)<<endl;
// 	}
// }
// int main() {
//     ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
//     int t=1;
//     //cin>>t;
//     while(t--){
//     	solve();
//     }
    
//     return 0;
// }




#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
int inv[3000010];
int n,p;
void solve(){
	cin>>n>>p;
	inv[1]=1;
	for(int i=2;i<=n;i++){
		inv[i]=((ll)p-p/i)*inv[p%i]%p;
	}	
	for(int i=1;i<=n;i++) cout<<inv[i]<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}