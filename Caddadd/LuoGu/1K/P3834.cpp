/*
double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;

str+str
to_string(int)
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls u<<1
#define rs u<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

using i8 = signed char;
using u8 = unsigned char;
using i16 = signed short int;
using u16 = unsigned short int;
using i32 = signed int;
using u32 = unsigned int;
using f32 = float;
using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
//using i128 = __int128_t;
//using u128 = __uint128_t;
using f128 = long double;

using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<int128>();
*/

template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}

template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

//i64=9.22*10^18 ((1LL << 63) - 1)
//int=2.1*10^9 ((1<<32)-1)
//64=(1.844*10^19)
//int[]:6.7e7
//64mb:4*1e6 (64*1024*1024)/4
//1 bytes=8 bit

// queue:push,pop,front,back
// deque:push_back,push_front,front,back,pop_front,pop_back
// priority_queue: top,push,pop
// stack: push,top,pop
// <algorithm> fill_n    fill_n(a,0);   fill fill(a.begin(),a.end(),1); [:)
// emplace()
//1e8:5.7e6 1e7:6.6e5 1e6:7.8e4 1e4:1229
//2^30:1e9
const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 200010;
struct node{
    // 节点值域中有多少个数
    int l,r,s;
};
struct Solution1 {
	vector<node> tr;
	int idx = 0;
	void init(int n) {
		idx = 0;
		tr.assign(n*25,node());
	}
	void build(int &x, int L, int R) {
		x = ++idx;
		if (L == R) return;
		int mid = (L + R) / 2;
		build(lc(x), L, mid);
		build(rc(x), mid + 1, R);
	}
	void add(int x, int &y, int L, int R, int k) {
		y = ++idx;
		tr[y] = tr[x]; //先复制一遍
		tr[y].s += 1;
		if (L == R) return;
		int mid = (L + R) / 2;
		if (k <= mid) add(lc(x), lc(y), L, mid, k);
		else add(rc(x), rc(y), mid + 1, R, k);
	}
	int query(int x, int y, int L, int R, int k) {
		if (L == R) return L;
		int mid = (L + R) / 2;
		int s = tr[lc(y)].s - tr[lc(x)].s;
		if (k <= s) return query(lc(x), lc(y), L, mid, k);
		else return query(rc(x), rc(y), mid + 1, R, k - s);
	}
};
void solve1() {
	int n, m;
	cin >> n >> m;
	vector<int> a(n + 1, 0);
	vector<int> b; //离散化数组
	vector<int> root(n + 1, 0);
	for (int i = 1; i <= n; i++) cin >> a[i];
	b = a;
	sort(b.begin()+1,b.end());
	b.erase(unique(b.begin() + 1, b.end()), b.end());
	int bn = b.size() - 1;
	Solution1 *tr = new Solution1();
	tr->init(n);
	tr->build(root[0], 1, bn); //创建空壳
	for (int i = 1; i <= n; i++) {
		int id = lower_bound(b.begin() + 1, b.end(), a[i]) - b.begin();
		tr->add(root[i - 1], root[i], 1, bn, id);
	}
	for (int i = 1, l, r, k; i <= m; i++) {
		cin >> l >> r >> k;
		int id = tr->query(root[l - 1], root[r], 1, bn, k);
		cout << b[id] << endl;
	}
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	//cin>>t;

	while (t--) {
		solve1();
		//init();
	}

	return 0;
}