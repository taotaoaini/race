#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const int MOD = 998244353;
const int N = 1000005;
ll a[N],idx=0,pos1[N],pos2[N],pos;
ll f[6721][6721];
void init(ll n){
    idx=0;
    for(ll i=1;i<=n/i;i++){
        if(n%i==0){
            a[++idx]=i;
            if(i*i!=n) a[++idx]=n/i;
        }
    }
}
void solve(){
    ll n;
    cin>>n;
    init(n);
    sort(a+1,a+idx+1);
     for (int i = 1; 2 * i <= idx + 1; i++) {
            pos1[a[i]] = i;
            pos2[a[i]] = idx - i + 1;
        }
      ll sqrtn=sqrt(n);
      for (ll i = 1; i <= idx; i++) {
            if (i == 1) f[i][1] = 1;
            else f[i][1] = 0;
            for (ll j = 2; j <= idx; j++) {
                f[i][j] = f[i][j - 1];
                if (j > i) continue;
                if (a[i] % a[j] == 0) {
                    ll tmp = a[i] / a[j];
                    pos = (tmp <= sqrtn ? pos1[tmp] : pos2[n / tmp]);
                    f[i][j] = (f[i][j] + f[pos][j - 1]) % MOD;
                }
            }
        }
        // cout<<idx<<endl;
        cout << f[idx][idx] - 1 << "\n";

}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    cin>>t;
    while(t--){
        solve();
    }
    return 0;
}
