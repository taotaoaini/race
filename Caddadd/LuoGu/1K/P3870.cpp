#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second
#define ls tr[u].l
#define rs tr[u].r
using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
    x = 0; bool flag(0); char ch = getchar();
    while (!isdigit(ch)) flag = ch == '-', ch = getchar();
    while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
    flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
    //mode=1为换行，0为空格
    x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
    do stk[++top] = x % 10, x /= 10; while (x);
    while (top) putchar(stk[top--] | 48);
    mode ? putchar('\n') : putchar(' ');
}
const int N=100010;
struct Tree{
	int l,r,x,y,flag;
	//flag=1:开，flag=-1：关
}tr[N*4];
int n,m;
void pushup(int u){
	tr[u].x=tr[lc].x+tr[rc].x;
	tr[u].y=tr[lc].y+tr[rc].y;
}
void pushdown(int u){
	int f1=tr[u].flag;
	if(f1&1) {swap(tr[lc].x,tr[lc].y);swap(tr[rc].x,tr[rc].y);}
	tr[lc].flag+=tr[u].flag;
	tr[rc].flag+=tr[u].flag;
	tr[u].flag=0;
}
void build(int u,int l,int r){
	tr[u]={l,r,0,r-l+1,0};
	if(l==r) return;
	int mid=(l+r)>>1;
	build(lc,l,mid);
	build(rc,mid+1,r);
}
void change(int u,int x,int y,int k){
	if(x<=tr[u].l&&y>=tr[u].r){
		swap(tr[u].x,tr[u].y);
		// cout<<tr[u].x<<" "<<tr[u].y<<endl;
		tr[u].flag+=k;
		return;
	}
	pushdown(u);
	int mid=(tr[u].l+tr[u].r)>>1;
	// cout<<mid<<endl;
	if(x<=mid) change(lc,x,y,k);
	if(y>mid)change(rc,x,y,k);
	pushup(u);
}
int query(int u,int x,int y){
	if(x<=tr[u].l&&y>=tr[u].r) return tr[u].x;
	pushdown(u);
	int mid=(tr[u].l+tr[u].r)>>1;
	int res=0;
	if(x<=mid) res+=query(lc,x,y);
	if(y>mid) res+=query(rc,x,y);
	return res;
}
void solve() {
	cin>>n>>m;
	build(1,1,n);
	for(int i=1;i<=m;i++){
		int c,a,b;
		cin>>c>>a>>b;
		if(c==0){
			change(1,a,b,1);
			// cout<<"a:b:"<<query(1,a,b)<<endl;
		}else{
			cout<<query(1,a,b)<<endl;
		}
		// cout<<query(1,1,n,1,n)<<endl;
	}
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    //cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}