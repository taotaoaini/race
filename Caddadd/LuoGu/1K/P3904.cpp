#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const int N = 55;
int S[N][N][100], L[N][N];

void add(int x, int y) {
	L[x][y] = max(L[x - 1][y - 1], L[x - 1][y]);
	for (int i = 0; i < L[x][y]; i++) {
		S[x][y][i] += S[x - 1][y - 1][i] + y * S[x - 1][y][i];
		S[x][y][i + 1] += S[x][y][i] / 10;
		S[x][y][i] %= 10;
	}
	while (S[x][y][L[x][y]]) {
		S[x][y][L[x][y] + 1] += S[x][y][L[x][y]] / 10;
		S[x][y][L[x][y]] %= 10;
		L[x][y]++;
	}
}
void solve() {
	int n, m;
	cin >> n >> m;
	S[0][0][0] = 1; L[0][0] = 1;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= i; j++)
			add(i, j);
	if (!L[n][m]) cout << 0;
	for (int i = L[n][m] - 1; i >= 0; i--)
		cout << S[n][m][i];
}
int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}



//字符串模拟大数
// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>

// #define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;
// const int N = 55;
// string S[N][N];

// string add(string a, string b) {
// 	string res;
// 	int l = a.size(), lb = b.size();
// 	int u[N * 10] = {0}, v[N * 10] = {0};
// 	for (int i = 0; i < l; i++)u[l - i - 1] = a[i] - '0';
// 	for (int i = 0; i < lb; i++)v[lb - i - 1] = b[i] - '0';
// 	l = max(l, lb);
// 	for (int i = 0; i < l; i++)
// 		u[i] += v[i], u[i + 1] += u[i] / 10, u[i] %= 10;
// 	while (u[l])l++;
// 	for (int i = l - 1; i >= 0; i--)res += u[i] + '0';
// 	return res;
// }
// string mul(string a, int b) {
// 	string res;
// 	int l = a.size();
// 	int u[N * 10] = {0}, v = 0;
// 	for (int i = l - 1; i >= 0; i--)u[l - i - 1] = a[i] - '0';
// 	for (int i = 0; i < l; i++)
// 		u[i] = u[i] * b + v, v = u[i] / 10, u[i] %= 10;
// 	while (v)u[l++] = v % 10, v /= 10;
// 	for (int i = l - 1; i >= 0; i--)res += u[i] + '0';
// 	return res;
// }
// void solve() {
// 	int n, m;
// 	cin >> n >> m;
// 	for (int i = 0; i < N; i++)
// 		for (int j = 0; j < N; j++)
// 			S[i][j] = "0";
// 	S[0][0] = "1";
// 	for (int i = 1; i <= n; i++)
// 		for (int j = 1; j <= i; j++)
// 			S[i][j] = add(S[i - 1][j - 1], mul(S[i - 1][j], j));
// 	cout << S[n][m] << endl;
// }
// int main() {
// 	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
// 	int t = 1;
// 	//cin>>t;
// 	while (t--) {
// 		solve();
// 	}

// 	return 0;
// }