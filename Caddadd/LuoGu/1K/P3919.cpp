#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const int N=1e6+10;
struct node{
	int l,r,v;
}e[N*45];
//1e5开*40,1e6开*45
//其实开(log(maxn)*(n+m)*maxn)就好
int n,m,cnt;
int ver,op,pos,v;//版本,操作类型,位置,值 
int a[N];
int root[N];
void build(int &p,int l,int r){
	p=++cnt;
	if(l==r){
		e[p].v=a[l];
		return;
	}
	int mid=l+r>>1;
	build(e[p].l,l,mid);
	build(e[p].r,mid+1,r);
	// cout<<p<<endl;
}
void update(int l,int r,int &cur,int las,int pos,int v){
	cur=++cnt;
	e[cur]=e[las];
	if(l==r){
		e[cur].v=v;
		return;
	}
	int mid=l+r>>1;
	if(pos<=mid) update(l,mid,e[cur].l,e[las].l,pos,v);
	else update(mid+1,r,e[cur].r,e[las].r,pos,v);
}
int query(int p,int l,int r,int pos){
	if(l==r) return e[p].v;
	int mid=l+r>>1;
	if(pos<=mid) return query(e[p].l,l,mid,pos);
	else return query(e[p].r,mid+1,r,pos);
}
int ask(int cur,int las,int pos){
	root[cur]=++cnt;
	e[root[cur]]=e[root[las]];
	return query(root[cur],1,n,pos);
}
void solve(){
	cin>>n>>m;
	for(int i=1;i<=n;i++) cin>>a[i];
	build(root[0],1,n);
	for(int i=1;i<=m;i++){
		cin>>ver>>op;
		if(op==1){
			cin>>pos>>v;
			update(1,n,root[i],root[ver],pos,v);
		}else{
			cin>>pos;
			cout<<ask(i,ver,pos)<<endl;
		}
	}
}
int main(){
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	
	solve();
	return 0;
}