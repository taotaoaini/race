#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
const int N=1010;
char a[N][N];
int h[N][N],l[N][N],r[N][N];//矩阵向上延申，向左延申，向右延申。
int n,m;
void solve(){
	cin>>n>>m;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			cin>>a[i][j];
			if(a[i][j]=='F') h[i][j]=1;
			l[i][j]=j;
			r[i][j]=j;
		}
		for(int j=2;j<=m;j++){
			if(a[i][j]=='F'&&a[i][j-1]=='F') l[i][j]=l[i][j-1];//处理单行向左延申
		}
		for(int j=m-1;j>=1;j--){
			if(a[i][j]=='F'&&a[i][j+1]=='F') r[i][j]=r[i][j+1];//处理单行向右延申
		} 
	}
	int res=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++){
			if(a[i][j]=='F'&&a[i-1][j]=='F'){
				h[i][j]=h[i-1][j]+1;//
				l[i][j]=max(l[i][j],l[i-1][j]);
				r[i][j]=min(r[i][j],r[i-1][j]);
			}
			if(a[i][j]=='F') res=max(res,h[i][j]*(r[i][j]-l[i][j]+1));
		}
	cout<<res*3<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}