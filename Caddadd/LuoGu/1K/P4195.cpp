#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
	//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}
ll a, p, b;
ll gcd(ll a, ll b) {
	return b == 0 ? a : gcd(b, a % b);
}
ll exbsgs(ll a, ll b, ll p) {
	a %= p; b %= p;
	if (b == 1 || p == 1) return 0;
	ll d, k = 0, A = 1;
	while (true) {
		d = gcd(a, p);
		if (d == 1) break;
		if (b % d) return -1;
		k++; b /= d; p /= d;
		A = A * (a / d) % p;
		if (A == b) return k;
	}
	ll m = ceil(sqrt(p));
	ll t = b;
	unordered_map<int, int> up;
	up[b] = 0;
	for (int j = 1; j < m; j++) {
		t = t * a % p;
		up[t] = j;
	}
	ll x = 1;
	for (int i = 1; i <= m; i++)
		x = x * a % p;
	t = A;
	for (int i = 1; i <= m; i++) {
		t = t * x % p;
		if (up.count(t)) return i * m - up[t] + k;
	}
	return -1;
}
void solve() {
	while (cin >> a >> p >> b&&a) {
		ll res = exbsgs(a, b, p);
		if (res == -1) cout << "No Solution" << endl;
		else cout << res << endl;
	}
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}