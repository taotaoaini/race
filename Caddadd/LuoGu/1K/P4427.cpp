// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题

// #define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1
// #define fi first
// #define se second

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;
// //__int128的输入
// inline __int128 read() {
//   __int128 x = 0, f = 1;
//   char ch = getchar();
//   while (ch < '0' || ch > '9') {
//     if (ch == '-') f = -1;
//     ch = getchar();
//   }
//   while (ch >= '0' && ch <= '9') {
//     x = x * 10 + ch - '0';
//     ch = getchar();
//   }
//   return x * f;
// }
// //__int128的输出
// inline void print(__int128 x) {
//   if (x < 0) {
//     putchar('-');
//     x = -x;
//   }
//   if (x > 9)
//     print(x / 10);
//   putchar(x % 10 + '0');
// }
// // typedef long long ll;
// const ll mod = 998244353;
// const int N = 3e5 + 10, M = 2 * N;
// int n, m;
// int h[N], to[M], ne[M], idx;
// int fa[N][22];//fa[u][i]表示从u向上跳2^i层的祖先结点
// int dep[N];   //dep[v]表示v的深度
// ll mi[60];   //mi[j]表示dep[v]的j次幂
// ll s[N][60]; //s[v][j]表示从根到v的路径节点的深度的j次幂之和

// void add(int u, int v) {
//   to[++idx] = v, ne[idx] = h[u], h[u] = idx;
// }

// void dfs(int u, int f) { //预处理节点信息
//   for (int i = 1; i <= 20; i++)
//     fa[u][i] = fa[fa[u][i - 1]][i - 1];
//   for (int i = h[u]; i; i = ne[i]) {
//     int v = to[i];
//     if (v == f) continue;
//     fa[v][0] = u; dep[v] = dep[u] + 1;
//     for (int j = 1; j <= 50; j++) mi[j] = mi[j - 1] * dep[v] % mod;
//     for (int j = 1; j <= 50; j++) s[v][j] = (mi[j] + s[u][j]) % mod;
//     dfs(v, u);
//   }
// }
// int lca(int u, int v) { //倍增法求lca
//   if (dep[u] < dep[v])swap(u, v);//保证u节点的深度比v节点的深度深
//   for (int i = 20; i >= 0; i--)
//     if (dep[fa[u][i]] >= dep[v]) u = fa[u][i];
//   if (u == v) return v;
//   for (int i = 20; i >= 0; i--)
//     if (fa[u][i] != fa[v][i]) u = fa[u][i], v = fa[v][i];
//   return fa[u][0];
// }
// void solve() {
//   cin >> n;
//   for (int i = 1, a, b; i < n; i++) {
//     cin >> a >> b;
//     add(a, b);
//     add(b, a);
//   }
//   mi[0] = 1; dfs(1, 0);
//   cin >> m;
//   for (int i = 1, u, v, k; i <= m; i++) {
//     cin >> u >> v >> k;
//     int l = lca(u, v);
//     ll res = (s[u][k] + s[v][k] - s[l][k] - s[fa[l][0]][k] + 2 * mod) % mod;
//     //减去两个小于mod的数，可能造成负数，最坏的情况会有res<(-mod)的情况，（res+2*mod）%mod可以转化为正整数
//     cout << res << endl;
//   }
// }
// int main() {
//   ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
//   int t = 1;
//   //cin>>t;
//   while (t--) {
//     solve();
//   }

//   return 0;
// }



#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
const int N=300010,M=600010;
struct edge{
  int v,ne;
}e[M];
int h[N],idx=0;
const int mod=998244353;
int dep[N];
ll mi[60];
ll s[N][60];
int fa[N][25];
int n,m;
void add(int u,int v){
  e[++idx]={v,h[u]};
  h[u]=idx;
}
void dfs(int x,int f){
  for(int i=1;i<=20;i++){
    fa[x][i]=fa[fa[x][i-1]][i-1];
  }
  for(int i=h[x];i;i=e[i].ne){
    int v=e[i].v;
    if(v==f) continue;
    fa[v][0]=x;
    dep[v]=dep[x]+1;
    for(int j=1;j<=50;j++) mi[j]=mi[j-1]*dep[v]%mod;
    for(int j=1;j<=50;j++) s[v][j]=(mi[j]+s[x][j])%mod;
    dfs(v,x);
  }
}
 int lca(int u,int v){
  if(dep[u]<dep[v]) swap(u,v);
  for(int i=20;i>=0;i--){
    if(dep[fa[u][i]]>=dep[v]) u=fa[u][i];
  }
  if(u==v) return v;
  for(int i=20;i>=0;i--){
    if(fa[u][i]!=fa[v][i]) u=fa[u][i],v=fa[v][i];
  }
  return fa[u][0];
 }
void solve(){
    cin>>n;
    for(int i=1,a,b;i<n;i++){
      cin>>a>>b;
      add(a,b);
      add(b,a);
    }
    mi[0]=1;
    dfs(1,0);
    cin>>m;
    for(int i=1,u,v,k;i<=m;i++){
        cin>>u>>v>>k;
        int l=lca(u,v);
        ll res=(s[u][k]+s[v][k]-s[l][k]-s[fa[l][0]][k]+2*mod)%mod;
        cout<<res<<endl;
    }

}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
      solve();
    }
    
    return 0;
}