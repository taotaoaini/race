// /*
// ll=9.22*10^18 ((1LL << 63) - 1)
// int=2.1*10^9 ((1<<32)-1)
// ull=(1.844*10^19)

// double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
// double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
// double round(double x);//四舍五入,-2.7->-3,-2.2->-2

// fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
// cout<<fixed<<setprecision(3)<<1.2000;//->1.2

// 二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
// 三行四列的数组:arr[1][2]=arr[1*4+2]

// % + -
// << >>
// > >= < <=
// != & ^ | && ||

// string 从下标1开始读入
// char s[N];
// cin>>s+1;
// */
// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题

// #define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1
// #define fi first
// #define se second
// #define mid ((l+r)>>1)

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;

// /*
// 快读代码模板
// a=read<int>(),b=read<ll>(),c=read<__int128>();
// */
// template<typename T>
// void read(T &x) {
//     x = 0; bool flag(0); char ch = getchar();
//     while (!isdigit(ch)) flag = ch == '-', ch = getchar();
//     while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
//     flag ? x = -x : 0;
// }
// template<typename T>
// void write(T x, bool mode = 1) {
//     //mode=1为换行，0为空格
//     x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
//     do stk[++top] = x % 10, x /= 10; while (x);
//     while (top) putchar(stk[top--] | 48);
//     mode ? putchar('\n') : putchar(' ');
// }
// const int inf = 0x3f3f3f3f;
// const ll dinf = 0x7f7f7f7f;
// const int N = 100010;
// int n, m, ans[N];
// vector<int> e[N];
// int fa[N][20], dep[N];
// int root[N], tot;
// int ls[N * 50], rs[N * 50], sum[N * 50], typ[N * 50];
// //sum:某种救济粮的数量
// //typ:救济粮的类型

// void pushup(int u) {//上传
//     if (sum[ls[u]] >= sum[rs[u]]) {
//         sum[u] = sum[ls[u]], typ[u] = typ[ls[u]];
//     } else {
//         sum[u] = sum[rs[u]], typ[u] = typ[rs[u]];
//     }
// }
// void change(int &u, int l, int r, int p, int k) {//点修
//     if (!u) u = ++tot;
//     if (l == r) {sum[u] += k; typ[u] = p; return;}
//     if (p <= mid) change(ls[u], l, mid, p, k);
//     else change(rs[u], mid + 1, r, p, k);
//     pushup(u);
// }
// void dfs(int u, int father) {
//     dep[u] = dep[father] + 1;
//     fa[u][0] = father;
//     for (int i = 1; i <= 18; i++) {
//         fa[u][i] = fa[fa[u][i - 1]][i - 1];
//     }
//     for (auto v : e[u]) {
//         if (v == father) continue;
//         dfs(v, u);
//     }
// }
// int lca(int u, int v) {//lca板子
//     if (dep[u] < dep[v]) swap(u, v);
//     // if(u==v) return v;
//     for (int i = 18; i >= 0; i--) {
//         if (dep[fa[u][i]] >= dep[v]) u = fa[u][i];
//     }
//     if (u == v) return v;
//     for (int i = 18; i >= 0; i--) {
//         if (fa[u][i] != fa[v][i]) {
//             u = fa[u][i];
//             v = fa[v][i];
//         }
//     }
//     return fa[u][0];
// }
// int merge(int x, int y, int l, int r) {//合并
//     if (!x || !y) return x + y;//一个为空，就返回另一个
//     if (l == r) {sum[x] += sum[y]; return x;}
//     ls[x] = merge(ls[x], ls[y], l, mid);
//     rs[x] = merge(rs[x], rs[y], mid + 1, r);
//     pushup(x);
//     return x;
// }
// void calc(int u, int father) {//统计
//     for (int v : e[u]) {
//         if (v == father) continue;
//         calc(v, u);
//         root[u] = merge(root[u], root[v], 1, N);
//     }
//     ans[u] = sum[root[u]] ? typ[root[u]] : 0;
// }

// void solve() {
//     //freopen("a.in","r",stdin);
//     //freopen("a.out","w",stdout);
//     cin >> n >> m;
//     for (int i = 1, x, y; i < n; i++) {
//         cin >> x >> y;
//         e[x].push_back(y);
//         e[y].push_back(x);
//     }
//     dfs(1, 0);//树上倍增
//     for (int i = 1, x, y, z; i <= m; i++) {//差分
//         cin >> x >> y >> z;
//         change(root[x], 1, N, z, 1);
//         change(root[y], 1, N, z, 1);
//         int t = lca(x, y);
//         // cout<<t<<endl;
//         change(root[t], 1, N, z, -1);
//         change(root[fa[t][0]], 1, N, z, -1);
//     }
//     calc(1, 0);//统计
//     for (int i = 1; i <= n; i++) {
//         cout << ans[i] << endl;
//     }
// }

// int main() {
//     ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
//     int t = 1;
//     //cin>>t;
//     while (t--) {
//         solve();
//     }

//     return 0;
// }

//空间：nlogn*4
// 时间：nlogn*4

/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second
#define mid ((l+r)>>1)
using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
    x = 0; bool flag(0); char ch = getchar();
    while (!isdigit(ch)) flag = ch == '-', ch = getchar();
    while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
    flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
    x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
    do stk[++top] = x % 10, x /= 10; while (x);
    while (top) putchar(stk[top--] | 48);
    mode ? putchar('\n') : putchar(' ');
}
const int inf=0x3f3f3f3f;
const ll dinf=0x7f7f7f7f;
const int N=100010;
int n,m;
int fa[N][20],dep[N];
int ls[N*50],rs[N*50],sum[N*50],typ[N*50],tot,root[N];
int ans[N];
vector<int> e[N];
void pushup(int u){
    if(sum[ls[u]]>=sum[rs[u]]){
        sum[u]=sum[ls[u]];
        typ[u]=typ[ls[u]];
    }else{
        sum[u]=sum[rs[u]];
        typ[u]=typ[rs[u]];
    }
}
void dfs(int u,int father){
    dep[u]=dep[father]+1;
    fa[u][0]=father;
    for(int i=1;i<=18;i++){
        fa[u][i]=fa[fa[u][i-1]][i-1];
    }
    for(auto v:e[u]){
        if(v==father) continue;
        dfs(v,u);
    }
}
int lca(int u,int v){
    if(dep[u]<dep[v]) swap(u,v);
    for(int i=18;i>=0;i--){
        if(dep[fa[u][i]]>=dep[v]) u=fa[u][i];
    }
    if(u==v) return u;
    for(int i=18;i>=0;i--){
        if(fa[u][i]!=fa[v][i]) u=fa[u][i],v=fa[v][i];
    }
    return fa[u][0];
}
void change(int &u,int l,int r,int p,int k){
    if(!u) u=++tot;
    if(l==r) {sum[u]+=k;typ[u]=p;return;}
    if(p<=mid) change(ls[u],l,mid,p,k);
    else change(rs[u],mid+1,r,p,k);
    pushup(u);
}
int merge(int x,int y,int l,int r){
    if(!x||!y) return x+y;
    if(l==r) {sum[x]+=sum[y];return x;}
    ls[x]=merge(ls[x],ls[y],l,mid);
    rs[x]=merge(rs[x],rs[y],mid+1,r);
    pushup(x);
    return x;
}
void calc(int u,int father){
    for(auto v:e[u]){
        if(v==father) continue;
        calc(v,u);
        root[u]=merge(root[u],root[v],1,N);
    }
    ans[u]=sum[root[u]]?typ[root[u]]:0;
}
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout); 
    cin>>n>>m;
    for(int i=1,x,y;i<n;i++){
        cin>>x>>y;
        e[x].push_back(y);
        e[y].push_back(x);
    }  
    dfs(1,0);
    for(int i=1,x,y,z;i<=m;i++){
        cin>>x>>y>>z;
        int t=lca(x,y);
        // cout<<t<<endl;
        change(root[x],1,N,z,1);
        change(root[y],1,N,z,1);
        change(root[t],1,N,z,-1);
        change(root[fa[t][0]],1,N,z,-1);
    }
    calc(1,0);
    for(int i=1;i<=n;i++) cout<<ans[i]<<endl;
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    //cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}