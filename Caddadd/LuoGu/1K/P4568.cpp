#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read() {
	__int128 x = 0, f = 1;
	char ch = getchar();
	while (ch < '0' || ch > '9') {
		if (ch == '-') f = -1;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9') {
		x = x * 10 + ch - '0';
		ch = getchar();
	}
	return x * f;
}
//__int128的输出
inline void print(__int128 x) {
	if (x < 0) {
		putchar('-');
		x = -x;
	}
	if (x > 9)
		print(x / 10);
	putchar(x % 10 + '0');
}

const int N = 1e4 + 5, M = 5e4 + 5, K = 11;
struct edge {
	int u, v, w, ne;
} e[M * (K + 1) * 4];
int n, m, k, s, t, idx = 1, h[N * (K + 1)], d[N * (K + 1)], vis[N * (K + 1)];
struct node {int u, dis;};
bool operator<(node a, node b) {return a.dis > b.dis;}
priority_queue<node> q;
void add(int u, int v, int w) {
	e[++idx] = {u, v, w, h[u]};
	h[u] = idx;
}
void dijkstra(int st) {
	memset(d, 0x3f, sizeof(d));
	q.push({st, 0}); d[st] = 0;
	while (!q.empty()) {
		int u = q.top().u; q.pop();
		if (vis[u])continue;
		vis[u] = 1;
		for (int i = h[u]; i; i = e[i].ne) {
			int v = e[i].v, w = e[i].w;
			if (d[v] > d[u] + w) {
				d[v] = d[u] + w;
				q.push({v, d[v]});
			}
		}
	}

}
void solve() {
	cin >> n >> m >> k >> s >> t; ++s; ++t;
	for (int i = 1, u, v, w; i <= m; i++) {
		cin >> u >> v >> w; ++u; ++v;
		add(u, v, w); add(v, u, w);
		for (int j = 1; j <= k; j++) {
			add(j * n + u, j * n + v, w);
			add(j * n + v, j * n + u, w);
			add((j - 1)*n + u, j * n + v, 0);
			add((j - 1)*n + v, j * n + u, 0);
		}
	}
	//当s->t的最短路径的节点小于k
	for (int i = 1; i <= k; i++)
		add((i - 1)*n + t, i * n + t, 0);
	dijkstra(s);
	cout << d[k * n + t] << '\n';
}
int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}