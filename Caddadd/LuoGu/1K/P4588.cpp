#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read() {
	__int128 x = 0, f = 1;
	char ch = getchar();
	while (ch < '0' || ch > '9') {
		if (ch == '-') f = -1;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9') {
		x = x * 10 + ch - '0';
		ch = getchar();
	}
	return x * f;
}
//__int128的输出
inline void print(__int128 x) {
	if (x < 0) {
		putchar('-');
		x = -x;
	}
	if (x > 9)
		print(x / 10);
	putchar(x % 10 + '0');
}

/*
1.对Q次操作，建一颗叶子节点的值均为1的线段树。
2.对Q次操作看做对Q个叶子节点的单点修改，操作1把第i个叶子节点修改为m，
操作2把pos位置的叶子节点修改为。
3.线段树维护区间积，每次操作后，根节点的值即答案。
*/

const int N = 100005;
int M;
struct Tree {
	int l, r;
	int mul;
} tr[N * 4];
void pushup(int u) {
	tr[u].mul = (1ll * tr[lc].mul * tr[rc].mul) % M;
}
void build(int u, int l, int r) {
	tr[u] = {l, r, 1};
	if (l == r) return;
	int mid = (l + r) >> 1;
	build(lc, l, mid);
	build(rc, mid + 1, r);
	pushup(u);
}
void change(int u, int x, int c) { //点修
	if (x == tr[u].l && x == tr[u].r) {
		tr[u].mul = c; return;
	}
	int mid = tr[u].l + tr[u].r >> 1;
	if (x <= mid) change(lc, x, c);
	if (x > mid) change(rc, x, c);
	pushup(u);
}
void solve() {
	int t; cin >> t;
	for (int i = 1; i <= t; i++) {
		int q, op, m, pos;
		cin >> q >> M;
		build(1, 1, q);
		for (int i = 1; i <= q; i++) {
			cin >> op;
			if (op == 1) {
				cin >> m, change(1, i, m);
				cout << tr[1].mul << endl;
			} else
			{cin >> pos, change(1, pos, 1); cout << tr[1].mul << endl;}
		}
	}
}
int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}