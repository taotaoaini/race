/*
ll=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
ull=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

fixed有个地方需要注意的是可以保存的有效数字包括0，不开fixed的话，即使保存3个有效数字，小数点后的0也会自动省略。
cout<<fixed<<setprecision(3)<<1.2000;//->1.2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
	//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}
const int inf = 0x3f3f3f3f;
const ll dinf = 0x7f7f7f7f;
const int N = 600010, len = 23;
int n, m, s[N];
int ch[N * 25][2], ver[N * 25];
int root[N], idx;
int a[N];
void insert(int x, int y, int i) {
	ver[x] = i;
	for (int k = len; k >= 0; k--) {
		int c = s[i] >> k & 1;
		ch[x][!c] = ch[y][!c];
		ch[x][c] = ++idx;
		x = ch[x][c]; y = ch[y][c];
		ver[x] = i;
	}
}
int query(int x, int L, int v) {
	int res = 0;
	for (int k = len; k >= 0; k--) {
		int c = v >> k & 1;
		if (ver[ch[x][!c]] >= L)
			x = ch[x][!c], res += 1 << k;
		else x = ch[x][c];
	}
	return res;
}
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	int l, r, x, res;
	char op;
	cin >> n >> m;
	ver[0] = -1;//ver[0]=-1:因为走到空节点，可能造成影响
	root[0] = ++idx;
	insert(root[0], 0, 0);
	for (int i = 1; i <= n; i++) {
		cin >> x;
		root[i] = ++idx;
		s[i] = s[i - 1] ^ x;
		insert(root[i], root[i - 1], i);
	}
	while (m--) {
		cin >> op;
		if (op == 'A') {
			cin >> x;
			root[++n] = ++idx;
			s[n] = s[n - 1] ^ x;
			insert(root[n], root[n - 1], n);
		} else {
			cin >> l >> r >> x;
			res = query(root[r - 1], l - 1, s[n] ^ x);
			cout << res << endl;
		}
	}
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}