#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;

typedef __int128 ll;
const int N = 100005;
ll n, m[N], r[N];

ll exgcd(ll a, ll b, ll &x, ll &y) {
  if (b == 0) {x = 1, y = 0; return a;}
  ll d = exgcd(b, a % b, y, x);
  y -=(a / b) * x;
  return d;
}
ll EXCRT(ll m[], ll r[]) {
  ll m1, m2, r1, r2, p, q;
  m1 = m[1], r1 = r[1];
  for (int i = 2; i <= n; i++) {
    m2 = m[i], r2 = r[i];
    ll d = exgcd(m1, m2, p, q);
    if ((r2 - r1) % d) {return -1;}
    p = p * (r2 - r1) / d; //特解
    p = (p % (m2 / d) + m2 / d) % (m2 / d);
    r1 = m1 * p + r1;
    m1 = m1 * m2 / d;
  }
  return (r1 % m1 + m1) % m1;
}
int main() {
  scanf("%lld", &n);
  for (int i = 1; i <= n; ++i)
    scanf("%lld%lld", m + i, r + i);
  printf("%lld\n", EXCRT(m, r));
  return 0;
}


