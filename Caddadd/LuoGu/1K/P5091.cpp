#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
ll a,b,m,phi;
bool flag;
string s;
int get_phi(int n){//求欧拉函数
  int res = n;
  for(int i=2; i*i<=n; i++){
    if(n%i == 0){
      res = res/i*(i-1);
      while(n%i == 0) n /= i;
    }
  }
  if(n>1) res = res/n*(n-1);
  return res;
}
int depow(ll phi){//降幂
  ll b = 0;
  int len=s.size();
    for(int i=0;i<len; i++){
      b = b*10+(s[i]-'0');
      if(b>=phi) flag=1, b%=phi;
    }
    if(flag) b += phi;
    return b;
}
int qmi(ll a, ll b){//快速幂
    int res = 1;
    while(b){
      if(b&1) res = res*a%m;
      a = a*a%m;
      b >>= 1;
    }
    return res;
}
void solve(){
	cin>>a>>m>>s;
    phi = get_phi(m);
    b = depow(phi);
    cout<<qmi(a,b)<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}