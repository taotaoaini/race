#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read() {
    __int128 x = 0, f = 1;
    char ch = getchar();
    while (ch < '0' || ch > '9') {
        if (ch == '-') f = -1;
        ch = getchar();
    }
    while (ch >= '0' && ch <= '9') {
        x = x * 10 + ch - '0';
        ch = getchar();
    }
    return x * f;
}
//__int128的输出
inline void print(__int128 x) {
    if (x < 0) {
        putchar('-');
        x = -x;
    }
    if (x > 9)
        print(x / 10);
    putchar(x % 10 + '0');
}
const int N = 5000010;
int a[N];
int b[N];
int n, p, k;
int pre[N], suf[N];
void read(int& x) {
    int f = 1;
    char c = getchar();
    while (c < '0' || c > '9') {
        if (c == '-') f = -1;
        c = getchar();
    }
    while (c >= '0' && c <= '9') {
        x = x * 10 + c - '0';
        c = getchar();
    }
    x = x * f;
}
void print(ll &x) {
    if (x < 0) {
        putchar('-');
        x = -x;
    }
    if (x > 9) print(x / 10);
    putchar(x % 10 + '0');

}
ll qmi(int a, int b) {
    ll res = 1ll;
    a %= p;
    while (b) {
        if (b & 1) res = res * a % p;
        b >>= 1;
        a = (1ll)*a * a % p;
    }
    return res;
}
void init() {
    b[0] = 1;
    k %= p;
    for (int i = 1; i <= n; i++) {
        b[i] = (1ll)*b[i - 1] * k % p;
    }
}
void solve() {
    read(n), read(p), read(k);
    init();
    for (int i = 1; i <= n; i++) read(a[i]);
    ll res = 0;
    pre[0] = 1;
    suf[n + 1] = 1;
    for (int i = 1; i <= n; i++) {
        pre[i] = (1ll)*pre[i - 1] * a[i] % p;
    }
    for (int i = n; i >= 1; i--) {
        suf[i] = (1ll)*suf[i + 1] * a[i] % p;
    }
    for (int i = 1; i <= n; i++) {
        res = (res + ((1ll) * pre[i - 1] * suf[i + 1] % p) * b[i]) % p;
    }
    res = (res * qmi(pre[n], p - 2)) % p;
    print(res);
    putchar('\n');
}
int main() {
    int t = 1;
    while (t--) {
        solve();
    }

    return 0;
}