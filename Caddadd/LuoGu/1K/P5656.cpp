#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

const int inf=0x3f3f3f3f;
const ll dinf=0x7f7f7f7f;
const int N=100010;
/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<__int128>();
*/
template<typename T>
void read(T &x) {
    x = 0; bool flag(0); char ch = getchar();
    while (!isdigit(ch)) flag = ch == '-', ch = getchar();
    while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
    flag ? x = -x : 0;
}
template<typename T>
void write(T x, bool mode = 1) {
    //mode=1为换行，0为空格
    x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
    do stk[++top] = x % 10, x /= 10; while (x);
    while (top) putchar(stk[top--] | 48);
    mode ? putchar('\n') : putchar(' ');
}
ll a,b,c;
ll gcd(ll a,ll b){
	return b==0?a:gcd(b,a%b);
}
ll exgcd(ll a,ll b,ll& x,ll& y){
	if(b==0){
		x=1;y=0;
		return a;
	}
	ll d=exgcd(b,a%b,y,x);
	y-=(a/b)*x;
	return d;
}
void solve() {
	cin>>a>>b>>c;
	ll x,y;
	if(c%gcd(a,b)) {cout<<"-1"<<endl;return;}
	ll d=exgcd(a,b,x,y);
	//特解：x1，y1
	ll x1=x*c/d,y1=y*c/d;
	ll dx=b/d,dy=a/d;
	//取值范围
	ll L=(ll)ceil((-x1+1)*1.0/dx);
	ll R=(ll)floor((y1-1)*1.0/dy);
	ll xmin=x1+L*dx;
	ll ymin=y1-R*dy;
	//当k=L时，x最小，y最大，当k=R时，x最大，y最小
	if(L>R){
		cout<<xmin<<" "<<ymin<<endl;
		return;
	}
	ll xmax=x1+R*dx,ymax=y1-L*dy;
	cout<<R-L+1<<" "<<xmin<<" "<<ymin<<" "<<xmax<<" "<<ymax<<endl;
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}