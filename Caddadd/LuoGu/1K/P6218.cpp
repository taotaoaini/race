#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

int a[50], len, f[50][65], vis[50][65];

int dfs(bool limit, bool is_num, int pos, int cha) {
    if(pos==0) return cha>=30;
    if(!limit&&is_num&&vis[pos][cha]) return f[pos][cha];
    int res=0;
    int up=limit?a[pos]:1;
    for(int i=0;i<=up;i++) {
        res+=dfs(limit&(i==a[pos]), is_num||(i!=0), pos-1, cha+(i==0?(is_num?1:0):-1));
    }
    if(!limit&&is_num) vis[pos][cha]=1, f[pos][cha]=res;
    return res;
}
int sol(int x) {
    len=0;
    while(x) {
        a[++len]=x%2;
        x/=2;
    }
    return dfs(1, 0, len, 30);
}
void solve(){
    int l,r;
    cin>>l>>r;
    cout<<sol(r)-sol(l-1);
}

int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
        solve();
    }
    
    return 0;
}