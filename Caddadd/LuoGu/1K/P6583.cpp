//80分
// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题

// #define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;
// //__int128的输入
// inline __int128 read(){
//     __int128 x=0,f=1;
//     char ch=getchar();
//     while(ch<'0'||ch>'9'){
//         if(ch=='-') f=-1;
//         ch=getchar();
//     }
//     while(ch>='0'&&ch<='9'){
//         x=x*10+ch-'0';
//         ch=getchar();
//     }
//     return x*f;
// }
// //__int128的输出
// inline void print(__int128 x){
//     if(x<0){
//         putchar('-');
//         x=-x;
//     }
//     if(x>9)
//         print(x/10);
//     putchar(x%10+'0');
// }
// ll get(ll x){
// 	while(x%2==0) x/=2;
// 	while(x%5==0) x/=5;
// 	return x;
// }	
// ll n;
// void solve(){
// 	cin>>n;
// 	ll res=0;
// 	for(int i=1;i<=n;i++){
// 		ll x=get(i);
// 		res+=n/x;
// 	}
// 	cout<<res<<endl;
// }
// int main() {
//     ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
//     int t=1;
//     //cin>>t;
//     while(t--){
//     	solve();
//     }
    
//     return 0;
// }
//100分
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
ll f(ll x){
    if(x==0) return 0;
    ll res=0;
    ll pow2=1;
    for(ll a=0;;a++){
        res+=ll(log(x/pow2)/log(5))+1;
        pow2*=2;
        if(pow2>x) break;
    }
    return res;
}
ll g(ll x){
    return x-x/2-x/5+x/10;
}
void solve(){
    ll n;
    cin>>n;
    ll res=0;
    for(ll l=1,r;l<=n;l=r+1){
        ll val=n/l;
        r=n/val;
        ll cur=val*f(val);
        res+=(g(r)-g(l-1))*cur;
    }
    cout<<res<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
        solve();
    }
    
    return 0;
}