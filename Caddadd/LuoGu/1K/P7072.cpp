/*桶排序*/
// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题

// #define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1
// #define fi first
// #define se second

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;
// //__int128的输入
// inline __int128 read(){
//     __int128 x=0,f=1;
//     char ch=getchar();
//     while(ch<'0'||ch>'9'){
//         if(ch=='-') f=-1;
//         ch=getchar();
//     }
//     while(ch>='0'&&ch<='9'){
//         x=x*10+ch-'0';
//         ch=getchar();
//     }
//     return x*f;
// }
// //__int128的输出
// inline void print(__int128 x){
//     if(x<0){
//         putchar('-');
//         x=-x;
//     }
//     if(x>9)
//         print(x/10);
//     putchar(x%10+'0');
// }
// int n,w;
// int a[610];
// void solve(){
// 	cin>>n>>w;
// 	int x;
// 	for(int i=1;i<=n;i++){
// 		cin>>x;
// 		a[x]++;
// 		int t=max(1,i*w/100);
// 		int sum=0;
// 		for(int j=600;j>=0;j--){
// 			sum+=a[j];
// 			if(sum>=t){
// 				cout<<j<<" ";
// 				break;
// 			}
// 		}
// 	}
// 	cout<<endl;
// }
// int main() {
//     ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
//     int t=1;
//     //cin>>t;
//     while(t--){
//     	solve();
//     }
    
//     return 0;
// }


/*对顶堆*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}

priority_queue<int> mx_hp;//大顶堆
priority_queue<int,vector<int>,greater<int>> mi_hp;//小顶堆，存放最大的k个元素，以至于可以O(1)找到第k大的值。
int n,w,k,num;
void qwq(){
    //调整小顶堆里的元素个数
    if(mi_hp.size()<k){
        mi_hp.push(mx_hp.top());
        mx_hp.pop();
    }
    if(mi_hp.size()>k){
        mx_hp.push(mi_hp.top());
        mi_hp.pop();
    }
}
void push(int num){
    //如果新加入的元素大于mx_hp的堆顶的最大元素，可以直接加入小顶堆，
    //否则需要先将mx_hp的堆顶加入到mi_hp中，将num加入到大顶堆中
    //维护小顶堆一定是存放最大的k个数
    if(num>=mx_hp.top()) mi_hp.push(num);
    else mx_hp.push(num);
    qwq();
}
void solve(){
    cin>>n>>w;
    mx_hp.push(0);//避免边界判断
    for(int i=1;i<=n;i++){
        k=max(1,i*w/100);
        cin>>num;
        push(num);
        cout<<mi_hp.top()<<" ";
    }
    cout<<endl;

}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
        solve();
    }
    
    return 0;
}