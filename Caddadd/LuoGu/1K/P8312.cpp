#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const int N=80,INF=0x3f3f3f3f;
int dis[N][N],e[N][N],f[N][N];
int n,m,q,t,x,y,k;
void solve(){
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			if(i==j) {e[i][j]=0;continue;}
			e[i][j]=INF;
		}
	for(int i=1;i<=m;i++){
		cin>>x>>y>>t;
		e[x][y]=min(e[x][y],t);
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			dis[i][j]=e[i][j];
		}
	}
	cin>>k>>q;
	k=min(k,n);
	for(int p=2;p<=k;p++){
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++) f[i][j]=dis[i][j];
		for(int kk=1;kk<=n;kk++)
			for(int i=1;i<=n;i++)
				for(int j=1;j<=n;j++)
					f[i][j]=min(f[i][j],dis[i][kk]+e[kk][j]);
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				dis[i][j]=f[i][j];
	}
	while(q--){
		cin>>x>>y;
		if(x==y) cout<<0<<endl;
		else if(dis[x][y]==INF) cout<<-1<<endl;
		else cout<<dis[x][y]<<endl;
	}

}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}