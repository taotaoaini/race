#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
int n,k;
int f(int n,int m){
	int x,y;
	if(n<=1||m<=1) return -1;
	if(m%2==0) y=1;
	else y=2;
	for(int i=3;i<=n;i++){
		x=(y-1+m)%i+1;
		y=x;
	}
	return y;
}
void solve(){
	cin>>n>>k;
	int x=f(n,k);
	cout<<x<<endl;
}

int main(){
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	
	int t=1;
	//cin>>t;
	while(t--){
		solve();
	}

	return 0;
}