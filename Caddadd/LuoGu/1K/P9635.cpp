#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

void solve(){
	int n;
	cin>>n;
	ll x=0;
	vector<int> res;
	for(int i=31;i>=0;i--){
		if(n>>i&1) {x+=(1<<i);res.push_back(1<<i);}
	}
	if(x<=(ll)n*n){
		for(int i=0;i<res.size();i++) cout<<res[i]<<" ";
		cout<<endl;
	}else{
		cout<<-1<<endl;
	} 
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
 	int t;
 	cin>>t;
 	while(t--){
 		solve();
 	}
 	// solve();   
    return 0;
}