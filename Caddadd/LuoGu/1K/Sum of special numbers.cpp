#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
string s="2019";
void solve(){
	int n;
	cin>>n;
	ll res=0;
	for(int i=1;i<=n;i++){
		int x=i;
		while(x){
			int p=x%10;
			x/=10;
			if(s.find(p+'0')!=string::npos){
				// cout<<p<<endl;
				res+=i;
				break;
			}
		}
	}
	cout<<res<<endl;
	
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}