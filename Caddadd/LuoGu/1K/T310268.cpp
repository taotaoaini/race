#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

int a[50010],ft[50010],n;
int lowbit(int x){
	return x&-x;
}
void update(int x,int k){
	for(int i=x;i<=n;i+=lowbit(i)){
		ft[i]+=k;
	}
}
int query(int x){
	int res=0;
	for(int i=x;i>=1;i-=lowbit(i)){
		res+=ft[i];
	}
	return res;
}
int query(int l,int r){
	return query(r)-query(l-1);
}
void solve(){
	cin>>n;
	for(int i=1;i<=n;i++) cin>>a[i],update(i,a[i]);
	string s="";
	int l,r;
	while(true){
		cin>>s;
		if(s=="End") break;
		cin>>l>>r;
		if(s=="Query"){
			cout<<query(l,r)<<endl;
		}else if(s=="Add"){
			update(l,r);
		}else if(s=="Sub"){
			update(l,-r);
		}
	}
}
int main(){
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	solve();		

	return 0;
}