#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const int N=10010;
struct edge{
	int v,ne;
}e[20010];
int h[N],idx;
int f[N],in[N],cnt;
int n,m;
queue<int> q;
void add(int u,int v){
	e[++idx]={v,h[u]};
	h[u]=idx;
}
void solve(){
	cin>>n>>m;
	for(int i=1;i<=m;i++){
		int x,y;
		cin>>x>>y;
		add(y,x);
		in[x]++;
	}
	for(int i=1;i<=n;i++){
		if(!in[i]) {q.push(i);f[i]=100;cnt++;}
	}
	while(!q.empty()){
		int u=q.front();q.pop();
		for(int i=h[u];i;i=e[i].ne){
			int v=e[i].v;
			in[v]--;
			f[v]=max(f[u]+1,f[v]);
			if(!in[v])
				{q.push(v);cnt++;}
		}
	}
	if(cnt!=n) {cout<<"Poor Xed"<<endl;return;}
	ll res=0;
	for(int i=1;i<=n;i++){
		res+=f[i];
	}
	cout<<res<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
    	solve();
    }
    
    return 0;
}