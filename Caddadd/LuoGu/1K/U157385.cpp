#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
int a[3010][2];
int n;
vector<int> e[3010];
int in[3010];
int f[3010];
void solve(){
	cin>>n;
	int x,y;
	for(int i=1;i<=n;i++) cin>>a[i][0]>>a[i][1];
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			if(a[i][0]>a[i][1]) swap(a[i][0],a[i][1]);
			if(a[j][0]>a[j][1]) swap(a[j][0],a[j][1]);
			if(a[i][0]>a[j][0]&&a[i][1]>a[j][1]) {e[i].push_back(j);in[j]++;}
			else if(a[i][0]<a[j][0]&&a[i][1]<a[j][1]) {e[j].push_back(i);in[i]++;} 
		}
	}
	queue<int> q;
	for(int i=1;i<=n;i++){
		if(in[i]==0) {f[i]=1;q.push(i);}
	}
	while(!q.empty()){
		int u=q.front();
		q.pop();
		for(auto v:e[u]){
			in[v]-=1;
			f[v]=max(f[v],f[u]+1);
			if(in[v]==0) q.push(v);
		}
	}
	int res=0;
	for(int i=1;i<=n;i++) res=max(res,f[i]);
	cout<<res<<endl;
}

int main(){
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	
	int t=1;
	//cin>>t;
	while(t--){
		solve();
	}

	return 0;
}