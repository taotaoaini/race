#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//const int inf=0x3f3f3f3f;
const int N = 210;
const int M = 10010;
int a[N];
int f[M];
int n, m;
void solve() {
	cin >> n >> m;
	for (int i = 0; i < n; i++) cin >> a[i];
	//f[i]:拼凑i元最小花费票子
	memset(f, -1, sizeof(f));
	f[0] = 0;
	for (int i = 1; i <= m; i++) {
		for (int j = 0; j < n; j++) {
			if (i - a[j] >= 0 && f[i - a[j]] != -1) {
				f[i] = f[i]!=-1 ? min(f[i], f[i - a[j]] + 1) : f[i - a[j]] + 1;
			}
		}
	}
	for(int i=1;i<=m;i++) cout<<f[i]<<endl;
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);

	int t = 1;
	//cin>>t;
	while (t--) {
		solve();
	}

	return 0;
}