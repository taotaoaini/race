#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;


int main() {
    ios::sync_with_stdio(false);
    int n,m,k;
    cin>>n>>m>>k;
    vector<int> a(n+2,0);
   	for(int i=0;i<m;i++){
   		int x,y;
   		cin>>x>>y;
   		a[x]++;
   		a[y]--;
   	}
   	ll res=0;
   	ll sum=0;
   	for(int i=1;i<=n;i++){
   		sum+=a[i];
//    		cout<<sum<<endl;
   		res=max(res,(sum+k-1)/k);
   	}
   	cout<<res<<endl;
    return 0;
}