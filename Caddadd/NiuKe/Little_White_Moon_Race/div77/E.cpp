#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

//cpp
int n,m,
ll k;
const int N=50010;
const int M=100100;
bool vis[N];
struct edge{
    int v,w,ne;
}e[M];
int h[N],idx;
int dist[N];
priority_queue<pii,vector<pii>,greater<pii>> hp;
void dijkstra(){
    memset(dist,0x3f,sizeof(dist));
    dist[1]=0;
    hp.push({0,1});
    while(hp.size()){
        auto t=hp.top();
        hp.pop();
        int v=t.second,d=t.first;
        if(vis[v]) continue;
        vis[v]=true;
        for(int i=h[v];i;i=e[i].ne){
            int j=e[i].v;
            if(dist[j]>dist[v]+e[j].w){
                dist[j]=dist[v]+e[j].w;
                hp.push({dist[j],j});
            }
        }
    }
}
void add(int a,int b,int w){
    e[++idx]={b,w,h[a]};
    h[a]=idx;
}
int main() {
    ios::sync_with_stdio(false);
    cin>>n>>m>>k;
    for(int i=0;i<m;i++){
        int a,b,c,d;
        cin>>a>>b>>c>>d;
        if(a==0){
           add(b,c,d);
           add(c,b,d);
        }else{
            add(c,b,d);
        }
    }
    
    return 0;
}