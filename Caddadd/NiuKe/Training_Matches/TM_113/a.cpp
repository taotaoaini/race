#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<stack>
using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef unsigned long long ull;

int main() {
	ios::sync_with_stdio(false);
	int n;
	cin>>n;
	if(n==3) cout<<1<<endl;
	else cout<<2<<endl;
    return 0;
}