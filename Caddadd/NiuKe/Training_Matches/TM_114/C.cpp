#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

void solve(){
	int n,m;
	cin>>n>>m;
	vector<int> a(n);
	for(int i=0;i<n;i++) cin>>a[i];
	vector<pii> b;
	for(int i=0;i<n;i++){
		int j=i;
		while(j<n-1&&a[j+1]==a[j]+1) j++;
		b.push_back({a[i],a[j]});
		i=j;
	}
	sort(b.begin(),b.end());
	// for(auto& [x,y]:b){
	// 	cout<<x<<" "<<y<<endl;
	// }
	int cnt=0;
	int l=1,r=0;
	bool f=false;
	for(int i=0;i<b.size();i++){
		auto &[l1,r1]=b[i];
		// cout<<"l1:"<<l1<<endl;
		if(l1>l) {f=true;break;}
		cnt++;
		int j=i;
		while(j<b.size()&&b[j].first<=l) {r=max(r,b[j].second);j++;}
		if(r>=m) break;
		l=r+1;
		
	} 
	if(f||r<m) cout<<-1<<endl;
	else cout<<cnt<<endl;
}
/*贪心,区间覆盖问题，特殊1-m*/
int main() {
    ios::sync_with_stdio(false);
    int T;
    cin>>T;
    while(T--){
    	solve();
    }
    return 0;
}