#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

using ll = long long;
using ull = unsigned long long;
using Pii = std::pair<int, int>;
using Pll = std::pair<ll, ll>;
using namespace std;
int a[200010],n;
void work()
{
	cin >> n;
    for(int i = 1; i <= n; i++)
        num[i] = 0;
    for(int i = 1; i <= n; i++)
    {
        int x = read();
        num[x]++;
    }
	for(int i = n; i >= 1; i--)
    {
       while(num[i])
       {
        	int cnt = 0;
        	for(int j = i; j >= 1; j--)
        	{
            	num[j]--;
      	      	cnt++;
            	if(num[j - 1] < num[j] + 1)
                	break;
        	}
        	if(cnt < 5)
        	{
            	printf("NO\n");
            	return;
        	}
       	}
    }
    printf("YES\n");
	return;
}
int main() {
    ios::sync_with_stdio(false);
    int T;
    cin>>T;
    while(T--){
    	work();
    }
    return 0;
}