#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

char a[30][50];
bool check(int i,int j){
    return i>=0&&i<30&&j>=0&&j<50;
}
int d[3][2]{{1,0},{0,1},{1,1}};
ll dfs(int i,int j){
    ll cnt=0;
    for(auto&[x,y]:d){
        // int i1=x+i;
        // int j1=y+j;
        // if(check(i1,j1)&&a[i1][j1]>a[i][j]) cnt+=dfs(i1,j1)+1;
        int i1=i,j1=j;
        while(check(i1,j1)){
            if(a[i1][j1]>a[i][j]) cnt++;
            i1+=x;
            j1+=y;
        }
    }
    int i1=i+1,j1=j-1;
    while(check(i1,j1)){
        if(a[i1][j1]!=a[i][j]) cnt++;
        i1+=1;
        j1-=1;
    }
    return cnt;
    }
void solve(){
    for(int i=0;i<30;i++){
        for(int j=0;j<50;j++){
            cin>>a[i][j];
        }
    }
    ll res=0;
    for(int i=0;i<30;i++)
        for(int j=0;j<50;j++){
            res+=dfs(i,j);
        }
        cout<<res<<endl;
}
int main() {
    ios::sync_with_stdio(false);
    solve();
    return 0;
}



