#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;
map<int,int> mp1;
map<int,int> mp2;
int n; 
int a[200010];
char c[200010];
int main() {
    cin>>n;
    for(int i=0;i<n;i++) cin>>a[i];
    for(int i=0;i<n;i++) cin>>c[i];
    ll res=0;
    for(int i=0;i<n;i++){
        if(c[i]=='R'){
            if(mp1.find(a[i])!=mp1.end()){
                res+=mp1[a[i]];
            }
            mp2[a[i]]+=1;
        }else{
            if(mp2.find(a[i])!=mp2.end()){
                res+=mp2[a[i]];
            }
            mp1[a[i]]+=1;
        }
    }
    cout<<res<<endl;
    return 0;
}