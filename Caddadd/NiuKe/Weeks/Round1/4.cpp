#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;
string s;
// int d[200010];
const int mod=1e9+7;
int f[200010][10];
int main() {
    cin>>s;
    f[0][0]=1;
    int n=s.length();
    for(int i=1;i<=n;i++){
        int x=s[i-1]-'0';
        for(int j=0;j<9;j++){
           f[i][j]=f[i-1][j];
        int d=(j-x+9)%9;
        f[i][j]=(f[i][j]+f[i-1][d])%mod;
        }
    }
    cout<<f[n][0]-1<<endl;
}