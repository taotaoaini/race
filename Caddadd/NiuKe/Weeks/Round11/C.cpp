#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

void solve(){
	string s;
	cin>>s;
	int a=0,b=0;
	for(int i=0;i<s.size();i++){ 
		if(s[i]=='0'&&(i&1)==0){
			b+=1;
		}
		if(s[i]=='0'&&(i&1)==1){
			a+=1;
		}
		if(s[i]=='1'&&(i&1)==0){
			a+=1;
		}
		if(s[i]=='1'&&(i&1)==1){
			b+=1;
		}
}
	cout<<min(a,b)<<endl;  
	}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    solve();

 	   
    return 0;
}