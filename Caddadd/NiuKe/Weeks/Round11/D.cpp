/*
线性DP
f[i][k1]:前i个数构造k1个数的方案数
累加f[i][k]
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>

#define bitcnt(x) (__builtin_popcountll(x))//ll二进制数中1的个数
#define endl '\n'
#define lson pos<<1
#define rson pos<<1|1

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
const ll mod=1e9+7;
void solve(){
	int n,m;
	cin>>n>>m;
	m=n-m;
//     cout<<"#k:"<<k<<endl;
	vector<int>a(n+1);
	ll res=0;
	vector<vector<int>> f(n+1,vector<int>(m+1));
	for(int i=1;i<=n;i++) cin>>a[i],f[i][1]=1;
    sort(a.begin()+1,a.end());
	for(int i=1;i<=n;i++){
		for(int j=1;j<i;j++){
			if(a[i]%a[j]==0){
				for(int k=2;k<=m;k++){
					f[i][k]=(f[i][k]+f[j][k-1])%mod;}
	}
		}
        res=(res+f[i][m])%mod;}
		cout<<res<<endl;
}
int main(){
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	solve();	

	return 0;
}
