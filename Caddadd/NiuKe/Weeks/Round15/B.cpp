// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题

// #define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1
// #define fi first
// #define se second

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;
// //__int128的输入
// inline __int128 read(){
//     __int128 x=0,f=1;
//     char ch=getchar();
//     while(ch<'0'||ch>'9'){
//         if(ch=='-') f=-1;
//         ch=getchar();
//     }
//     while(ch>='0'&&ch<='9'){
//         x=x*10+ch-'0';
//         ch=getchar();
//     }
//     return x*f;
// }
// //__int128的输出
// inline void print(__int128 x){
//     if(x<0){
//         putchar('-');
//         x=-x;
//     }
//     if(x>9)
//         print(x/10);
//     putchar(x%10+'0');
// }
// int res=0x3f3f3f3f;
// int get(int x,int y) {
//     if(x<y) swap(x,y);
//     return min(('z'-x)+1+(y-'a'), abs(y-x));
// }
// void solve(){
// 	string s;
//     cin>>s;
    
// 	for(int i='a';i<='z';i++){
// 		int t=0;
// 		for(int j=0;j<s.size();j++){
// 			// int x=s[j]-'a';
//             // cout<<abs(x-i)<<" "<<abs(26-x+i)<<endl;
// 			// t+=min(abs(x-i),abs(26-x+i));
//             t+=get(s[j],i);
// 		}
// 		res=min(res,t);
// 	}
// 	cout<<res<<endl;
// }
// int main() {
//     ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
//     int t=1;
//     //cin>>t;
//     while(t--){
//     	solve();
//     }
    
//     return 0;
// }




#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}
int res=0x3f3f3f3f;
void solve(){
    string s;
    cin>>s;
    
    for(int i=0;i<26;i++){
        int t=0;
        for(int j=0;j<s.size();j++){
            int x=s[j]-'a';
//             cout<<abs(x-i)<<" "<<abs(26-x+i)<<endl;
            t+=min(abs(x-i),26-abs(i-x));
        }
        res=min(res,t);
    }
    cout<<res<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    int t=1;
    //cin>>t;
    while(t--){
        solve();
    }
    
    return 0;
}