// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>
// #include<bitset>
// #include<unordered_map>
// #include<unordered_set>
// #include<stack>
// #include<iomanip>//设置浮点数精度问题

// #define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
// #define endl '\n'
// #define lc u<<1
// #define rc u<<1|1
// #define fi first
// #define se second

// using ll = long long;
// using ull = unsigned long long;
// using pii = std::pair<int, int>;
// using pll = std::pair<ll, ll>;
// using namespace std;
// //__int128的输入
// inline __int128 read() {
// 	__int128 x = 0, f = 1;
// 	char ch = getchar();
// 	while (ch < '0' || ch > '9') {
// 		if (ch == '-') f = -1;
// 		ch = getchar();
// 	}
// 	while (ch >= '0' && ch <= '9') {
// 		x = x * 10 + ch - '0';
// 		ch = getchar();
// 	}
// 	return x * f;
// }
// //__int128的输出
// inline void print(__int128 x) {
// 	if (x < 0) {
// 		putchar('-');
// 		x = -x;
// 	}
// 	if (x > 9)
// 		print(x / 10);
// 	putchar(x % 10 + '0');
// }
// bool f(int a, int b, int c) {
// 	if (a == b || b == c) return true;
// 	int x = a * 3 * 3 + b * 3 + c;
// 	// cout << x << endl;
// 	return (x & 1);
// }
// int a[1010];
// int ans[1010];
// int n;
// string s;
// bool dfs(int i, int k) {
// 	if (i >= n) {
// 		for (int i = 0; i < n; i++) ans[i] = a[i];
// 		return true;
// 	}
// 	bool res = false;
// 	if (a[i] == -1) {
// 		for (int j = 0; j <= 2; j++) {
// 			if(k>=2)
// 			if (k >= 2) if (f(a[i - 2], a[i - 1], j)) continue;
// 			a[i] = j;
// 			res = res || dfs(i + 1, k + 1);
// 			a[i] = -1;
// 		}
// 	} else {
// 		if (k >= 2) {if (f(a[i - 2], a[i - 1], a[i])) return false;}
// 		res = res || dfs(i + 1, k + 1);
// 	}
// 	return res;
// }
// void solve() {
// 	cin >> s;
// 	n = s.size();
// 	// memset(dp,-1,sizeof(dp));
// 	for (int i = 0; i < n; i++) {
// 		int x = s[i] - '0';
// 		if (x <= 1) a[i] = x;
// 		else a[i] = -1;
// 		// cout<<a[i]<<" "<<endl;
// 	}
// 	if (dfs(0, 0)) {
// 		for (int i = 0; i < n; i++) cout << ans[i] << " ";
// 		cout << endl;
// 	} else {
// 		cout << -1 << endl;
// 	}

// }
// int main() {
// 	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
// 	int t = 1;
// 	//cin>>t;
// 	while (t--) {
// 		solve();
// 	}

// 	return 0;
// }



#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;
//__int128的输入
inline __int128 read(){
    __int128 x=0,f=1;
    char ch=getchar();
    while(ch<'0'||ch>'9'){
        if(ch=='-') f=-1;
        ch=getchar();
    }
    while(ch>='0'&&ch<='9'){
        x=x*10+ch-'0';
        ch=getchar();
    }
    return x*f;
}
//__int128的输出
inline void print(__int128 x){
    if(x<0){
        putchar('-');
        x=-x;
    }
    if(x>9)
        print(x/10);
    putchar(x%10+'0');
}

string a[10];
int main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    string str;
    cin >> str;
    int len = str.size();
    if (len == 1) {
        if (str == "?") {
            cout << 0;
        } else {
            cout << str;
        }
        return 0;
    } else if (len < 3) {
        string a[6] = {"01", "02", "10", "12", "20", "21"};
        for (int k = 0 ; k < 6 ; k++) {
            bool flag = true;
            for (int i = 0 ; i < len ; i++) {
                if (str[i] == '?') continue;
                if (str[i] != a[k][i]) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                cout << a[k];
                return 0;
            }
        }
    } else if (len == 3) {
        string a[4] = {"020", "101", "121", "202"};
        for (int k = 0 ; k < 4 ; k++) {
            bool flag = true;
            for (int i = 0 ; i < len ; i++) {
                if (str[i] == '?') continue;
                if (str[i] != a[k][i]) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                cout << a[k];
                return 0;
            }
        }
    } else {
        string a[2];
        for (int i = 0 ; i < len ; i++) {
            if (i & 1) {
                a[0] += '2';
                a[1] += '0';
            } else {
                a[0] += '0';
                a[1] += '2';
            }
        }
        for (int k = 0 ; k < 2 ; k++) {
            bool flag = true;
            for (int i = 0 ; i < len ; i++) {
                if (str[i] == '?') continue;
                if (str[i] != a[k][i]) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                cout << a[k];
                return 0;
            }
        }
    }
    cout << -1;
    return 0;
}