#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;
int n;
int a[100010];
char s[100010];
int f[100010];
int main() {
    cin>>n;
    for(int i=1;i<=n;i++) cin>>a[i];
    for(int i=1;i<=n;i++) cin>>s[i];
    for(int i=2;i<=n;i++){
        f[i]=f[i-1];
        if(s[i]!=s[i-1]) f[i]=max(f[i],f[i-2]+a[i]+a[i-1]);
    }
    cout<<f[n]<<endl;
    return 0;
}