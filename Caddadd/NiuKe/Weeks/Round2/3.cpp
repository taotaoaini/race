#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;
int h[9];
int n;
void solve1(int x){
    int mx=0;
    for(int i=x;i<=x+1;i++){
        mx=max(mx,h[i]);
    }
    h[x]=mx+3;
    h[x+1]=mx+1;
}
void solve2(int x){
    int mx=-1;
    for(int i=x;i<=x+2;i++){
        if(mx==-1||h[mx]<h[i]) mx=i;
    }
    int hi=h[mx];
    if(mx==x){
        h[x]=h[x+1]=h[x+2]=hi+2;
    }else{
        h[x]=h[x+1]=h[x+2]=hi+1;
    }
}
void solve3(int x){
    if(h[x]<=h[x+1]){
        h[x]=h[x+1]=h[x+1]+3;
    }else if(h[x]==h[x+1]+1){
        h[x]=h[x+1]=h[x]+2;
    }else{
        h[x]=h[x+1]=h[x]+1;
    } 
}
void solve4(int x){
    int mx=0;
    for(int i=x;i<=x+2;i++){
        mx=max(mx,h[i]);
    }
    h[x]=h[x+1]=mx+1;
    h[x+2]=mx+2;
}
int main() {
    cin>>n;
    int a,b;
    for(int i=1;i<=n;i++){
        cin>>a>>b;
        if(a==0) solve1(b);
        else if(a==90) solve2(b);
        else if(a==180) solve3(b);
        else solve4(b);
    }
    for(int i=1;i<=8;i++){
        cout<<h[i]<<" ";
    }
    puts("");
    return 0;
}