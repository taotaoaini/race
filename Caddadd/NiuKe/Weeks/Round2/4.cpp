#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;
int n,h,k;
ll f[100010];
void solve(int x){
    ll hi=(ll)h+x*k;
    int cnt=0;
    for(int i=1;i<=n;i++){
        if(hi<=f[i]) break;
        hi-=f[i];
        cnt++;
    }
    cout<<cnt<<" ";
}
int main() {
    cin>>n>>h>>k;
    int a,b;
    for(int i=1;i<=n;i++){
    cin>>a>>b;
    int c=(a/4)*3;
    a%=4;
    if(a==3) c+=1;
    else if(a==0) c-=1;
    // cout<<":::"<<c<<endl;
    f[i]=(ll)c*b;
    }
    sort(f+1,f+n+1);
    // for(int i=1;i<=n;i++) cout<<"::"<<f[i]<<endl;
    int q,x;
    cin>>q;
    while(q--){
        cin>>x;
        solve(x);
    }
    puts("");
    return 0;
}