#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
#define endl '\n'
#define lc u<<1
#define rc u<<1|1
#define fi first
#define se second
#define mid ((l+r)>>1)

using ll = long long;
using ull = unsigned long long;
using pii = std::pair<int, int>;
using pll = std::pair<ll, ll>;
using namespace std;

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const ll dinf = 0x7f7f7f7f;
//const int M=100010;
const int N = 510;

char c[N][N];
int n, m, k;
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    cin >> n >> m >> k;
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++) c[i][j] = '.';
    char ch;
    for (int i = 1, x, y; i <= k; i++) {
        cin >> x >> y >> ch;
        c[x][y] = ch;
        for (int j = 1; j <= 2; j++) {
            int x1 = x + j;
            int x2 = x - j;
            if (x1 <= n) c[x1][y] = ch;
            if (x2 >= 1) c[x2][y] = ch;
            int y1 = y + j;
            int y2 = y - j;
            if (y1 <= m) c[x][y1] = ch;
            if (y2 >= 1) c[x][y2] = ch;
        }
    }
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            cout << c[i][j] << "";
        }
        cout << endl;
    }
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    int t = 1;
    //cin>>t;
    while (t--) {
        solve();
    }

    return 0;
}