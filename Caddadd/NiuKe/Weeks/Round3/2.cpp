// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>

// using namespace std;

// typedef long long ll;
// typedef pair<int,int> PII;
// int res=0x3f3f3f3f;
// string s;
// int main() {
//     cin>>s;
//     int len=s.length();
//     for(int i='a';i<='z';i++){
//             int x=0;
//         for(int j=0;j<len;j++){
//             x+=(s[j]-i+26)%26;
//         }
//         res=min(res,x);
//     }
//     cout<<res<<endl;
//     return 0;
// }

#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;
int res=0x3f3f3f3f;
string s;
int main() {
    cin>>s;
    int len=s.length();
    for(int i='a';i<='z';i++){
            int x=0;
        for(int j=0;j<len;j++){
           if(s[j]==i) continue;
           else if(s[j]<i){
            x+=min(i-s[j],26-(i-s[j]));
           }else{
            x+=min(s[j]-i,26-(s[j]-i));
           }
        }
        cout<<x<<endl;
        res=min(res,x);
    }
    cout<<res<<endl;
    return 0;
}