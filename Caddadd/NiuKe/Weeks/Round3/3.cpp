// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>

// using namespace std;

// typedef long long ll;
// typedef pair<int,int> PII;
// const int N=1000010;
// int dp[N][2][3];
// ll res=0;
// int n,m,a,b;
// int main() {
//     cin>>n>>m>>a>>b;
//     if(n<=1&&m<=1)  {cout<<0<<endl;return 0;}
//     else if(n==2&&m==1) {cout<<a<<endl;return 0;}
//     else if(n==1&&m==2) {cout<<b<<endl;return 0;}
//     dp[0][0][0]=a;
//     dp[0][0][1]=n-2;
//     dp[0][0][2]=m-1;
//     dp[1][0][0]=b;
//     dp[1][0][1]=n-1;
//     dp[1][0][2]=m-2;
//     for(int i=1;i<N;i++){
//         bool flag=true;
//     if(dp[i-1][0][1]+2<=n&&dp[i-1][0][2]+1<=m){
//         dp[i][0][0]=dp[i-1][0][0]+a;
//         dp[i][0][1]=dp[i-1][0][1]-2;
//         dp[i][0][2]=dp[i-1][0][2]-1;
//         flag=false;
//     }
//      if(dp[i-1][1][1]+1<=n&&dp[i-1][1][2]+2<=m){
//         dp[i][1][0]=dp[i-1][1][0]+b;
//         dp[i][0][1]=dp[i-1][0][1]-2;
//         dp[i][0][2]=dp[i-1][0][2]-1;
//         flag=false;
//     }
//     }
//     return 0;
// }

#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;
int n,m,a,b;
int main() {
    cin>>n>>m>>a>>b;
    int A=min(n/2,m);
    // int B=min(n,m/2);
    int res=0;
    for(int i=0;i<=A;i++){
        int x=n-i*2;
        int y=m-i;
        res=max(res,a*i+min(x,y/2));
    }
    cout<<res<<endl;
    return 0;
}