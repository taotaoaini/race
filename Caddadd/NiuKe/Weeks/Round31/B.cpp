/*
i64=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
u64=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;

str+str
to_string(int)
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls u<<1
#define rs u<<1|1
#define fi first
#define se second
#define mid ((l+r)/2)

using i8 = signed char;
using u8 = unsigned char;
using i16 = signed short int;
using u16 = unsigned short int;
using i32 = signed int;
using u32 = unsigned int;
using f32 = float;
using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
//using i128 = __int128_t;
//using u128 = __uint128_t;
using f128 = long double;

using pii = std::pair<int,int>;
using pll = std::pair<long long,long long>;
using pci=std::pair<char,int>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<int128>();
*/

template<typename T>
void read(T &x) {
    x = 0; bool flag(0); char ch = getchar();
    while (!isdigit(ch)) flag = ch == '-', ch = getchar();
    while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
    flag ? x = -x : 0;
}

template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
    x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
    do stk[++top] = x % 10, x /= 10; while (x);
    while (top) putchar(stk[top--] | 48);
    mode ? putchar('\n') : putchar(' ');
}

priority_queue<int,vector<int>,less<int> > mx_hp;//大顶堆
priority_queue<int,vector<int>,greater<int> > mi_hp;//小顶堆

//int[]:6.7e7
//64mb:4*1e6 (64*1024*1024)/4
//1 bytes=8 bit
const int inf=0x3f3f3f3f;//1061109567,是10^9级别的
const i64 dinf=0x3f3f3f3f3f3f3f3f;//4.62*10^18级别的
const int mod=1e9+7;
const int M=100010;
const int N=100010;


void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout); 
    i64 n;
   	int res=0;
   	for(int i=1;i<=n/i;i++){
   		if(n%i==0){
   			res+=1;
   			while(n%i==0) n/=i;
   		}
   	}
   	if(n>1) res++;
   	cout<<res<<endl;
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    cout << fixed << setprecision(12);
    
    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}