/*
double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;

str+str
to_string(int)
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls u<<1
#define rs u<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

using i8 = signed char;
using u8 = unsigned char;
using i16 = signed short int;
using u16 = unsigned short int;
using i32 = signed int;
using u32 = unsigned int;
using f32 = float;
using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
//using i128 = __int128_t;
//using u128 = __uint128_t;
using f128 = long double;

using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<int128>();
*/

template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}

template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

//i64=9.22*10^18 ((1LL << 63) - 1)
//int=2.1*10^9 ((1<<32)-1)
//64=(1.844*10^19)
//int[]:6.7e7
//64mb:4*1e6 (64*1024*1024)/4
//1 bytes=8 bit

// queue:push,pop,front,back
// deque:push_back,push_front,front,back,pop_front,pop_back
// priority_queue: top,push,pop
// stack: push,top,pop
// <algorithm> fill_n    fill_n(a,0);   fill fill(a.begin(),a.end(),1); [:)
// emplace()
//1e8:5.7e6 1e7:6.6e5 1e6:7.8e4 1e4:1229
//2^30:1e9
const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;

//线段树可做
template<typename T>
struct Fenwick {
	int n;
	std::vector<T> a;

	Fenwick(int n = 0) {
		init(n);
	}

	void init(int n) {//初始化空间大小
		this->n = n;
		a.assign(n, T());
	}

	void add(int x, T v) {//允许从0开始加
		for (int i = x + 1; i <= n; i += i & -i) {
			a[i - 1] += v;
		}
	}

	T sum(int x) {//算的是下标小于x的sum,统计[0,x)的个数
		auto ans = T();
		for (int i = x; i > 0; i -= i & -i) {
			ans += a[i - 1];
		}
		return ans;
	}

	T rangeSum(int l, int r) {//[l,r)
		return sum(r) - sum(l);
	}

	//树状数组二分
	int kth(T k) {// 返回第k小 权值树状数组的时候使用，从0下标开始，0下标最小的数，如果没有kth小的数存在返回Fenwick_MaxLength
		int x = 0;
		for (int i = 1 << std::__lg(n); i; i /= 2) {
			if (x + i <= n && k >= a[x + i - 1]) {
				x += i;
				k -= a[x - 1];
			}
		}
		return x;
	}
	int qeury(T s) { //查询最大T，满足sum(a1..ak)<=s 的k,从最大位开始贪心
		T sum = 0;
		int pos = 0;
		for (int j = 18; j >= 0; j--) {
			if (pos + (1 << j) <= n && sum + a[pos + (1 << j)] <= s) {
				pos += (1 << j);
				sum += a[pos];
			}
		}
		return pos;
	}
};
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	int n, m;
	cin >> n >> m;
	string s;
	cin >> s;
	s = "." + s + "..";
	// Fenwick<int> fk = Fenwick<int>(n + 10);
	int res = 0;
	for (int i = 1; i <= n; i++) {
		if (s[i] == s[i + 1]) res+=1;
		else if (s[i] == s[i + 2]) res+=1;
	}
	cout<<res<<endl;
	// auto get=[&](string t){
	// 	int res=0;
	// 	for(int i=1,j=1;i<=n;i++,j+=1){
	// 		if(j==4) j=1;
	// 		if(s[i]!=t[j]) res+=1;
	// 	}
	// 	return res;
	// };
	// vector<int> ans(3);
	// cout<<fk.sum(n)<<endl;
	// for (int i = 1, op; i <= m; i++) {
	// 	cin >> op;
	// 	if (op == 1) {
	// 		char c;
	// 		int x;
	// 		cin >> x >> c;
	// 		x += 1;
	// 		if (c == s[x]) continue;
	// 		if (s[x] == s[x - 1] && s[x - 1] != s[x + 1]) fk.add(x - 1, -1);
	// 		if (s[x] == s[x - 2] && s[x - 2] != s[x - 1]) fk.add(x - 2, -1);
	// 		if (s[x] == s[x + 1]) fk.add(x, -1);
	// 		else if (s[x] == s[x + 2]) fk.add(x, -1);
	// 		s[x] = c;
	// 		if (s[x] == s[x - 1] && s[x - 1] != s[x + 1]) fk.add(x - 1, 1);
	// 		if (s[x] == s[x - 2] && s[x - 2] != s[x - 1]) fk.add(x - 2, 1);
	// 		if (s[x] == s[x + 1]) fk.add(x, 1);
	// 		else if (s[x] == s[x + 2]) fk.add(x, 1);
	// 	} else {
	// 		int L = 0, R = 0;
	// 		cin >> L >> R;
	// 		L += 1, R += 1;
	// 		if (L == R)  {cout << 0 << endl; continue;}
	// 		int res = fk.rangeSum(L, R);
	// 		// cout << "#" << res << endl;
	// 		if (s[R - 1] == s[R + 1] && s[R - 1] != s[R]) {
	// 			res -= 1;
	// 			// cout << R - 1 << " " << s[R - 1] << " " << s[R] << endl;
	// 		}
	// 		// cout << s << endl;
	// 		cout << res << endl;
	// 	}
	// }

}

int main() {
	ios::sync_with_stdio(false); cin.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	//cin>>t;

	while (t--) {
		solve();
		//init();
	}

	return 0;
}