#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;
const int N=20010;
int n,m,k;
struct chess{
    string s;
    int cnt;
    int x,y;
}e[N];
int fa[N];
map<string,int> node;
map<char,PII> dis;
int a[510][510];

void init(){
    dis['W']={-1,0};
    dis['A']={0,-1};
    dis['D']={0,1};
    dis['S']={1,0};
    memset(a,-1,sizeof(a));
    for(int i=0;i<N;i++){
        fa[i]=i;
    }
}
int find(int x){
    while(x!=fa[x]) x=fa[x]=fa[fa[x]];
    return x;
}

bool check(int x,int y){
    if(x<=0 || x>n || y<=0 || y>m) return true;
    else return false;
}
int main() {
    init();
    cin>>n>>m>>k;
    string s;
    char c;
    int x,y;
    for(int i=0;i<k;i++) 
    { 
        cin>>s>>x>>y;
        if(a[x][y]==-1){
             a[x][y]=i;
         e[i]={s,1,x,y};
        // cout<<"?>?"<<e[i].s<<endl;
         node[s]=i;
        }else{
            int idx=a[x][y];
            if(e[idx].s>s){
                continue;
            }else{
               node.erase(e[idx].s);
               a[x][y]=i;
               e[i]={s,1,x,y};
            }
        }
    }
    int q;
    cin>>q;
    while(q--){
    cin>>s>>c;
    if(node.find(s)==node.end()) {cout<<"unexisted empire."<<endl;continue;}
    int idx=node[s];
    int x=e[idx].x+dis[c].first;
    int y=e[idx].y+dis[c].second;
    // cout<<":::"<<x<<","<<y<<"::"<<a[x][y]<<endl;
    if(check(x,y)) cout<<"out of bounds!"<<endl;
    // else if(e[idx].cnt==0) cout<<"unexisted empire."<<endl;
    else if(a[x][y]==-1) {cout<<"vanquish!"<<endl;a[x][y]=idx;e[idx].x=x;e[idx].y=y;e[idx].cnt++;}
    else if(find(a[x][y])==idx) cout<<"peaceful."<<endl;
    else{
        int idy=a[x][y];
        int fax=find(idx);
        int fay=find(idy);
        if(fax==fay) cout<<"peaceful."<<endl;
        else{
            if(e[fax].cnt>e[fay].cnt||(e[fax].cnt==e[fay].cnt&&e[fax].s>e[fay].s)){
                fa[fay]=fax;
                e[fax].cnt+=e[fay].cnt;
                e[fay].cnt=0;
                node.erase(e[fay].s);
                cout<<e[fax].s<<" wins!"<<endl;
            }else{
                fa[fax]=fay;
                e[fay].cnt+=e[fax].cnt;
                e[fax].cnt=0;
                // cout<<e[fa]
                node.erase(e[fax].s);
                // cout<<fax<<"dsdsdsd"<<endl;
                cout<<e[fay].s<<" wins!"<<endl;
            }
        }
    }
    }

    return 0;
}


