#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef unsigned long long ull;

int main() {
    string s;
    cin>>s;
    for(int i=0;i<s.size();i++){
        if(s[i]>=97&&s[i]<=122){
            if(s[i]=='a') s[i]='z';
            else s[i]-=1;
        }else if(s[i]>=65&&s[i]<=90){
            if(s[i]=='Z') s[i]='A';
            else s[i]+=1;
        }
    }
    cout<<s<<endl;
    return 0;
}