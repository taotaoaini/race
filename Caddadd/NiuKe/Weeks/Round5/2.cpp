#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef unsigned long long ull;
int n,k;
int main() {
    cin>>n>>k;
    int x=1;
    int y=n-k+1;
    for(int i=1;i<=n;i++){
        if(y==n) {cout<<x++<<" ";continue;}
        if(i&1) cout<<y++<<" ";
        else cout<<x++<<" ";
    }
    puts("");
    return 0;
}