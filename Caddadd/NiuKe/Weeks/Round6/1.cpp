// #include<iostream>
// #include<cstring>
// #include<algorithm>
// #include<map>
// #include<set>
// #include<vector>
// #include<queue>
// #include<sstream>
// #include<cmath>
// #include<list>

// using namespace std;

// typedef long long ll;
// typedef pair<int,int> pii;
// typedef unsigned long long ull;

// int main() {
// 	string s;
// 	cin>>s;
// 	ll res=0;
// 	for(char c:s){
// 		if(c=='0'|| c=='6'||c=='9') res++;
// 		else if(c=='8') res+=2;
// 	}
// 	cout<<res<<endl;
//     return 0;
// }




/*mask是已经加入子集里的，sub是 全集除去mask 剩余并且不重复的*/
class Solution {
public:
    int minimumIncompatibility(vector<int>& nums, int k) {
        int n = nums.size();
        vector<int> dp(1 << n, INT_MAX);
        dp[0] = 0;
        int group = n / k;
        unordered_map<int, int> values;

        for(int mask = 1; mask < (1 << n); mask++){
            // 先要遍历n的全部子集合，由于题目说明1 <= nums[i] <= n
            // 所以n的全部子集合一定包含nums的全部子集合
            if(__builtin_popcount(mask) != group){
                continue;
            }
            // 由于nums[i]最大也才16，差值必不可能大于20，其实这个20也可以换成更大的数字
            int mn = 20, mx = 0;
            unordered_set<int> cur;
            for(int i = 0; i < n; i++){
                // 判断该集合内是否存在某个数字，例如如果i=5，那么就会往左移5位，看当前mask内是否存在5这个数字，但这里的目的，并不在于查看mask内是否有某个数字，而是看mask是否在一个范围内。
                // 由于最外面的大循环依次看子集合的顺序为0,1,10,11,100,110
                // 类比为取nums的数字就是：空集，取下标为0的子集，取下标为1的子集，取下标为0和1的子集，取下标为2的子集，取下标为2和1的子集
                // 到这里的小循环的时候，如果外面的大循环才到110，最大位也不过是1往左移动2位，但是i的范围是0~n，就代表2~n后面的就不用继续看了，算是节省了时间
                if(mask & (1 << i)){
                    // 判断当前状态内（分组）是否已经有这个数字了，如果有，直接跳出小循环，说明该组不可用，后面的数字都不用看了。
                    // 拿[1,2,3,1]举例，如果此时我们的mask=(1101)2，说明我们取到了[1,3,1]这个子集,而当循环i=3时，由于cur已经存了一个1，所以直接break了
                    // 需要注意的是，不用担心是否会影响下面的cur.size()==group这段判断代码，由于之前的代码已经初判断过一次，所以这里的所有mask的内部1的数量都是等于group的，一旦break就会缺少至少一个二进制1的数量，也就无法达到下面代码的判断条件。
                    if(cur.count(nums[i]) > 0){
                        break;
                    }
                    cur.insert(nums[i]);
                    // 取子集合内最大值与最小值
                    mn = min(mn, nums[i]);
                    mx = max(mx, nums[i]);
                }
            }
            if(cur.size() == group){
                values[mask] = mx - mn;
            }
        }

        // 前面的代码简单来说就做了两个工作
        // 1.获取数字1-n的全部子集合
        // 2.获取这些子集合中，长度为k且不含重复数字的子集合，并且记录他们的包含值到values中

        // 该循环开始是动态规划，从空集开始、划分1个数据的子集最优方案，划分2个子集数据的最优方案...划分n个数字的最佳方案
        // mask代表当前所选取的数字组合，sub代表还未被mask选择的数字，遍历sub的子集nxt，nxt若符合values的条件，则代表可以加入当前状态，如果当前状态已经有了dp值，那就可以进行比对。
        // 例如(110011)2,如果k=2
        // 这个组合可以是(110000)2 + (000011)2
        // 也可以是(100010)2 + (010001)2等等
        // 所以可以用dp来存储，然后比较
        for(int mask = 0; mask < (1 << n); mask ++){
            // mask从0开始，而dp[0] = 0;
            if(dp[mask] == INT_MAX){
                continue;
            }
           	//不合法的 mask直接跳过
            unordered_map<int, int> seen;
            for(int i = 0; i < n; i++){
                // 这里的判断目的与上面是一致的，如果外层循环还没有取到mask=(100)2，那说明数组内i=2还没有被选取过，因此将数组内的值以及下标存入seen内。
                // 例如当mask=0时，seen内会存入所有的nums数字以及其下标。
                // 当mask=1时，seen内则会存入除了下标为0的数字外所有的数字。
                // 如果存在重复值，则只会存一个
                // 例如[1,2,1,4]
                // 原本seen[1] == 0
                // 之后seen[1] == 2
                if((mask & (1 << i)) == 0){
                    seen[nums[i]] = i;
                }
            }
            // 随着大循环往后面走，seen能取的范围逐步变小，后面就不符合k的要求了，也就不用往后面看了，节省时间
            if(seen.size() < group){
                continue;
            }
            // 获取当前seen,也即是还有多少没被选择的数字
            int sub = 0;
            for(auto& pair : seen){
                sub |= (1 << pair.second);
            }
            int nxt = sub;
            while(nxt > 0){
                // 是否存在该子集，也即是该子集是否符合之前的条件（存入了values的子集都是符合题目条件的子集）
                if(values.count(nxt) > 0){
                    // 判断下一个状态（加入该子集后）是否和原本的状态+该子集的包含值的大小
                    dp[mask | nxt] = min(dp[mask|nxt], dp[mask] + values[nxt]);
                }
                // 获取sub的子集
                nxt = (nxt - 1) & sub;
            }
        }
        // 如果最后发现全取状态的dp值为INT_MAX（我们设置的初始值），说明该状态不存在合理的分法，则返回-1
        return (dp[(1 << n) - 1] < INT_MAX) ? dp[(1 << n) - 1] : -1;
    }
};