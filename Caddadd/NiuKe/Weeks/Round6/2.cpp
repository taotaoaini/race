#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef unsigned long long ull;

int main() {	
	int t;
	cin>>t;
	while(t--){
	int a,b;
	ull res=0;
	cin>>a>>b;
	string s=to_string(b);
	for(auto x:s){
		res+=a*(x-'0');
	}
	// cin>>a>>b;
	cout<<res<<endl;
	}
    return 0;
}