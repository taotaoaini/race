#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef unsigned long long ull;

int main() {
	ll n;
	cin>>n;
	ll x=1,y=1;
	ll res[3]{n,1,1};
	for(int i=2;i<=n;i++){
		x*=i;
		if(i==2) continue;
		if(x>=n+1){
		if(x-1-n<res[0]) res[0]=x-1-n,res[1]=i,res[2]=1;
			break;
		}
		int z=n/(x-1);
		if(z!=2&&abs((x-1)*z-n)<res[0]){
			res[0]=abs((x-1)*z-n),res[1]=i,res[2]=z;
		}
		z+=1;
		if(z!=2&&abs((x-1)*z-n)<res[0]){
			res[0]=abs((x-1)*z-n),res[1]=i,res[2]=z;
		}
	}
	cout<<res[1]<<" "<<res[2]<<endl;
    return 0;
}