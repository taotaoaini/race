#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<stack>
using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef unsigned long long ull;
char a[1010][1010];
int main() {
	ios::sync_with_stdio(false);
	int n,m;
	cin>>n>>m;
	for(int i=0;i<n;i++){
		for(int j=0;j<m;j++) cin>>a[i][j];
	}
	auto check=[&](int x,int y)->bool{
		map<char,int> mp;
		for(int i=0;i<2;i++){
			for(int j=0;j<2;j++){
				mp[a[x+i][y+j]]++;
			}
		}
		return mp['u']>=1&&mp['y']>=1&&mp['o']>=1;
	};
	int res=0;
	for(int i=0;i<n-1;i++){
		for(int j=0;j<m-1;j++){
			if(check(i,j)) res+=1;
		}
	}
	cout<<res<<endl;
    return 0;
}