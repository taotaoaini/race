#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<stack>
using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef unsigned long long ull;
int a,b,l,r;
int main() {
	ios::sync_with_stdio(false);
	int t;
	cin>>t;
	while(t--){
		cin>>a>>b>>l>>r;
		int p1=(b-a)%r;
		if(p1!=0&&p1<l&&(r+p1)<l*2) cout<<-1<<endl;
		else{
			int mi=((b-a)/r)+(p1!=0);
			int mx=((b-a)/l)+((b-a)%l!=0);
			cout<<mi<<" "<<mx<<endl;
		}
	}
    return 0;
}