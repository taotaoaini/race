/*
i64=9.22*10^18 ((1LL << 63) - 1)
int=2.1*10^9 ((1<<32)-1)
u64=(1.844*10^19)

double floor(double x);//把一个小数向下取整,返回不大于x的最大整数
double ceil(double x);//把一个小数向上取整,返回不小于x的最大整数
double round(double x);//四舍五入,-2.7->-3,-2.2->-2

二维索引转为一维索引,一维索引 = 行号 * 列数 + 列号
三行四列的数组:arr[1][2]=arr[1*4+2]

% + -
<< >>
> >= < <=
!= & ^ | && ||

string 从下标1开始读入
char s[N];
cin>>s+1;

str+str
to_string(int)
*/
#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls u<<1
#define rs u<<1|1
#define fi first
#define se second
#define mid ((l+r)/2)

using i8 = signed char;
using u8 = unsigned char;
using i16 = signed short int;
using u16 = unsigned short int;
using i32 = signed int;
using u32 = unsigned int;
using f32 = float;
using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
//using i128 = __int128_t;
//using u128 = __uint128_t;
using f128 = long double;

using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

/*
快读代码模板
a=read<int>(),b=read<ll>(),c=read<int128>();
*/

template<typename T>
void read(T &x) {
	x = 0; bool flag(0); char ch = getchar();
	while (!isdigit(ch)) flag = ch == '-', ch = getchar();
	while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
	flag ? x = -x : 0;
}

template<typename T>
void write(T x, bool mode = 1) {//mode=1为换行，0为空格
	x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
	do stk[++top] = x % 10, x /= 10; while (x);
	while (top) putchar(stk[top--] | 48);
	mode ? putchar('\n') : putchar(' ');
}

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

//int[]:6.7e7
//64mb:4*1e6 (64*1024*1024)/4
//1 bytes=8 bit
const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;

int n, m;
int res = n;
// vector<pii> c;
// int dfs(int x,int k){
// 	if(x>=c.size()){
// 		int t=1;
// 		for(int i=2;i<=n;i++) if(a[i]>a[1]) t++;
// 		return t;
// 	}
// 	int res=n;
// 	res=min(res,dfs(x+1,))
// }
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	cin >> n >> m;
	vector<int> a(n + 10, 0);
	for (int i = 1; i <= n; i++) cin >> a[i];
	vector<int> b(n + 1, 0);
	vector<pii> c;
	for (int i = 1, x, y; i <= m; i++) {
		cin >> x >> y;
		if (x > y) swap(x, y);
		if (x == 1 || y == 1) {
			a[1] += 3;
		} else {
			c.push_back({x, y});
		}
	}
	vector<int> tmp;
	for (int i = 0; i < c.size(); i++) {
		int x = c[i].fi, y = c[i].se;
		if (a[x]>a[1]) a[x]+=3;
		else if(a[y]>a[1]) a[y]+=3;
		else if(a[])
			// b[x]--;
			// b[y]--;
			// if (a[x] + 3 + b[x] <= a[i]) {
			// 	a[x] += 3;
			// } else if (a[y] + 3 + b[y] <= a[i]) {
			// 	a[y]+=3;
			// }else{
			// 	a[x]+=1;
			// 	a[y]+=1;
			// }
			if (a[x] + 3 <= a[y]) a[x] += 3;
			else if (a[y] + 3 <= a[x]) a[y] += 3;
			else a[x] += 1, a[y] += 1;
	}
	int res = 1;
	for (int i = 2; i <= n; i++) {
		if (a[i] > a[1]) res++;
	}
	cout << res << endl;
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	cin >> t;

	while (t--) {
		solve();
		//init();
	}

	return 0;
}