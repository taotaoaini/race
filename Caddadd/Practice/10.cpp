#include<iostream>
#include <vector>
#include<queue>
#include<array>
using namespace std;
using pii = pair<int, int>;
#define fi first
#define se second

const int N = 1010;
const int inf = 0x3f3f3f3f;

int n, m, K;
vector<pii> e[N];
vector<vector<pii>> get(int md) { // O(n+m)二分check预处理出>md:1,<=md:0的边
	vector<vector<pii>> g(n + 1);
	for (int i = 1; i <= n; i++) {
		for (auto& p : e[i]) {
			if (p.fi <= md) {
				g[i].push_back({0, p.se});
			} else {
				g[i].push_back({1, p.se});
			}
		}
	}
	return g;
}

bool check(int md) { //O(n+m) 01最短路
	deque<pii> q;  //双端队列可以优化log的时间,0放前，1放后，就是模拟的优先队列的操作
	vector<vector<pii>> g = get(md);
	vector<int> dis(n + 1, inf);
	dis[1] = 0;
	q.push_front({0, 1});
	while (q.size()) {
		auto p = q.front();
		q.pop_front();
		int  w = p.fi, u = p.se;
		if (dis[u] != w) continue;
		for (auto &edge : g[u]) { 
			int c = edge.fi; //边权
			int v = edge.se; //节点
			int d = w + c;
			if (dis[v] > d) {
				dis[v] = d;
				if (c) q.push_back({d, v}); //边权为1：放尾
				else q.push_front({d, v}); //边权为0：放首
			}
		}
	}
	return dis[n] <= K;
}
void solve() {
	cin >> n >> m >> K;
	for (int i = 1; i <= m; i++) {
		int a, b, w;
		cin >> a >> b >> w;
		e[a].push_back({w, b}); //做图的题目，喜欢把权值放在前面，点放在后面存
		e[b].push_back({w, a});
	}
	const int MxRight = 1e6 + 10;
	int l = -1, r = MxRight;
	while (l + 1 < r) { //O(log(1e6+10)*(n+m))
		int mid = (l + r) >> 1;
		if (check(mid)) r = mid;
		else l = mid;
	}
	cout << (r == MxRight ? -1 : r) << endl;
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0);
	int t = 1;
	solve();
}