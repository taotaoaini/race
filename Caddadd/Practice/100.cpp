#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define mid ((l+r)/2)

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;

const int N = 1e5 + 10;
// 2022:01:01:0:0:0
i64 yy[13] {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

vector<i64> a;
void init(string s) {
	vector<i64> b(10, 0);
	b[0] = stoll(s.substr(5, 2));
	b[1] = stoll(s.substr(8, 2));
	b[2] = stoll(s.substr(11, 2));
	b[3] = stoll(s.substr(14, 2));
	b[4] = stoll(s.substr(17, 2));
	i64 sum = (yy[b[0] - 1] + b[1]-1) * 24 * 60 * 60 + b[2] * 3600 + b[3] * 60 + b[4];
	a.push_back(sum);
}
void solve() {
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	for (int i = 1; i <= 12; i++) {
		yy[i]+=yy[i - 1];
	}
//    for(int i=1;i<=12;i++) cout<<yy[i]<<" ";
//    cout<<endl;
	string s;
//	while(getline(cin,s)) {
//		getline(cin,s);
//		init(s);
//	}
	for(int i=0;i<520;i++){
		getline(cin,s);
		init(s);
	}
	sort(a.begin(),a.end());
//    cout<<a.size()<<endl;
	i64 sum=0;
//	for(int i=0; i<a.size(); i++) {
//		cout<<"::"<<a[i]<<endl;
//	}
	cout<<a.size()<<endl;
	for (int i = 1; i < a.size(); i += 2) {
		sum += a[i] - a[i-1];
	}
	cout<<sum<<endl;
}
int main() {
//    ios::sync_with_stdio(false); cin.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	//cin>>t;

	while (t--) {
		solve();
		//init();
	}

	return 0;
}
