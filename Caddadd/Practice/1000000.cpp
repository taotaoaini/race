	#include<iostream>
	#include<cstring>
	#include<algorithm>
	#include<map>
	#include<set>
	#include<vector>
	#include<queue>
	#include<sstream>
	#include<cmath>
	#include<list>
	#include<bitset>
	#include<unordered_map>
	#include<unordered_set>
	#include<stack>
	#include<iomanip>//设置浮点数精度问题
	#include<functional>//编写内部函数

	#define bitcnt(x) (__builtin_popcountll(x))//LL进制数中1的个数
	#define endl '\n'
	#define lc u<<1
	#define rc u<<1|1
	#define fi first
	#define se second
	#define mid ((l+r)>>1)

	using i8 = signed char;
	using u8 = unsigned char;
	using i16 = signed short int;
	using u16 = unsigned short int;
	using i32 = signed int;
	using u32 = unsigned int;
	using f32 = float;
	using i64 = signed long long;
	using u64 = unsigned long long;
	using f64 = double;

	using pii = std::pair<int, int>;
	using pll = std::pair<long long, long long>;
	using pci = std::pair<char, int>;
	using namespace std;

	/*
	快读代码模板
	a=read<int>(),b=read<ll>(),c=read<int128>();
	*/

	template<typename T>
	void read(T &x) {
		x = 0; bool flag(0); char ch = getchar();
		while (!isdigit(ch)) flag = ch == '-', ch = getchar();
		while (isdigit(ch)) x = (x << 1) + (x << 3) + (ch ^ 48), ch = getchar();
		flag ? x = -x : 0;
	}

	template<typename T>
	void write(T x, bool mode = 1) {//mode=1为换行，0为空格
		x < 0 ? x = -x, putchar('-') : 0; static short stk[50], top(0);
		do stk[++top] = x % 10, x /= 10; while (x);
		while (top) putchar(stk[top--] | 48);
		mode ? putchar('\n') : putchar(' ');
	}

	priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
	priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

	const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
	const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
	const int mod = 1e9 + 7;
	const int M = 100010;
	const int N = 300010;
	int n, q;
	vector<int> e[N];
	int sz[N], son[N];
	void dfs(int u, int fa) {
		sz[u] = 1;
		for (auto v : e[u]) {
			if (v == fa) continue;
			dfs(v, u);
			if (sz[v] > sz[son[u]]) son[u] = v;
			sz[u] += sz[v];
		}
	}
	void solve() {
		//freopen("a.in","r",stdin);
		//freopen("a.out","w",stdout);
		cin >> n >> q;
		for (int i = 2; i <= n; i++) {
			int x;
			cin >> x;
			e[x].push_back(i);
		}
		dfs(1, 0);
		// for(int i=1;i<=n;i++) cout<<":::"<<son[i]<<endl;
		for (int i = 1; i <= q; i++) {
			int x;
			cin >> x;
			if(son[x]==0) cout<<x<<endl;
			else if(sz[son[x]]<(sz[x]+1)/2) cout<<x<<endl;
			else cout<<son[x]<<endl;
		}
	}

	int main() {
		ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
		cout << fixed << setprecision(12);

		//init();//全局初始化

		int t = 1;
		//cin>>t;

		while (t--) {
			solve();
		}

		return 0;
	}