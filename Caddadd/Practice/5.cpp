#include<iostream>
#include <vector>
using namespace std;
using pii = pair<int, int>;
#define fi first
#define se second
// (1)给一棵树，每条树边都有权值c，问从每个顶点出发，经过的路径权值之和最大为多少？(n<=10000)
const int N = 10010;
vector<pii> e[N];
int n;
int dp[3][N]; //记录的是最大路径的子树节点位置
int id[N];

void dfs1(int u, int fa) {  //第一个dfs更新子树的最大跟次大和s
    for (auto &p: e[u]) {
        int v = p.fi;
        int w = p.se;
        if (v == fa) continue;
        dfs1(v, u);
        if (dp[0][u] < dp[0][v] + w) { //这里是更新最大和，记住经过哪个儿子最大
            dp[0][u] = dp[0][v] + w;
            id[u] = v; //最长路径来自于v孩子节点
        }
    }
    for (auto &p: e[u]) {
        int v = p.fi;
        int w = p.se;
        if (v == fa || v == id[u]) continue; //跳过这个儿子，再剩下点里面找一个最大的，就是这个点次大的
        dp[1][u] = max(dp[1][u], dp[0][v] + w);
    }
}

void dfs2(int u, int fa) { //这个是更新先往父亲节点走一步的最大和
    for (auto &p: e[u]) {
        int v = p.fi;
        int w = p.se;
        if (v == fa) continue;
        //每个父亲都有两种方式，一个是再往父亲走一步，一个是走父亲的子树，max(dp[2][x], dp[1][x])，这个就体现出这两部了，注意经不经过这个点直接走子树最大和的那个点
        if (v == id[u]) dp[2][v] = max(dp[2][u], dp[1][u]) + w;
        else dp[2][v] = max(dp[2][u], dp[0][u]) + w;
        //因为dfs1更新了所有子树的特点，子树的信息可以直接用了，父节点的信息从一步步dfs下去都已经更新好了，上面的也是可以直接用，每一步都看看是不是走父亲的父亲更好，一直更新最优
        dfs2(v,u);
    }
}

void solve1() {
    cin >> n;
    for (int i = 2, x, w; i <= n; i++) {
        cin >> x >> w;
        e[i].push_back({x, w});
        e[x].push_back({i, w});
    }
    dfs1(1, 0);
    dfs2(1, 0);
    for(int i=1;i<=n;i++) cout<<max(dp[0][i],dp[2][i])<<endl;
}
int main(){
    solve1();
    return 0;
}