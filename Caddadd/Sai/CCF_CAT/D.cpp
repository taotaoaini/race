#include <bits/stdc++.h>
using namespace std;
using ll = long long ;
const int N = 2e7 + 5 ;

int pr[N], d[N], v[N], num = 0 ;
bool isnp[N] ;

void get_primes(int n)
{
    isnp[1] = 1 ; d[1] = 1 ;
    for(int i = 2; i <= n; ++ i)
    {
        if(!isnp[i])
        {
            d[i] = 2 ;
            pr[++ num] = i ;
            v[i] = i ;
        }
        for(int j = 1; j <= num && pr[j] <= n / i; ++ j)
        {
            isnp[i * pr[j]] = 1 ;
            if(i % pr[j] == 0)
            {
                v[i * pr[j]] = v[i] * pr[j] ;
                if(v[i * pr[j]] == i * pr[j])
                {
                    d[i * pr[j]] = d[i] + 1 ;
                }
                else
                {
                    d[i * pr[j]] = d[i / v[i]] * d[v[i] * pr[j]] ;
                }
                
                break ;
            }
            else d[i * pr[j]] = d[i] * d[pr[j]], v[i * pr[j]] = pr[j] ;
        }
    }
}

void solve()
{
    int n ; cin >> n ;

    get_primes(n) ;

    long long ans = 0 ;

    for(int i = 1; i <= num && pr[i] < n; ++ i)
    {
        if(isnp[n - pr[i]]) ans += d[n - pr[i]] ;
    }
    cout << ans << '\n' ;
}

int main()
{
    //ios::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    long long t=1;
    //cin>>t;
    while(t--)
        solve();
}