#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls p<<1
#define rs p<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 10010;
const int N = 1010;
// https://pintia.cn/problem-sets/994805046380707840/exam/problems/994805068539215872?type=7&page=1
struct node {
	int id, fa, mo, k;
	vector<int> ch;
	double x, s;
} tr[N];
int fa[M][5]; //0:编号，1：房产套数，2：房产面积，3：人数，4：最小编号
int vis[M]; //记录存在的编号
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	int n;
	cin >> n;
	auto find = [&](int x)->int{
		while (x != fa[x][0]) x = fa[fa[x][0]][0];
		return x;
	};
	auto get = [&](int x, int y) {
		int fax = find(x);
		int fay = find(y);
		if (fax != fay) {
			fa[fax][1] += fa[fay][1];
			fa[fay][1] = 0;
			fa[fax][2] += fa[fay][2];
			fa[fay][2] = 0;
			fa[fax][3] += fa[fay][3];
			fa[fay][3] = 0;
			fa[fax][4] = min(fa[fax][4], fa[fay][4]);
			fa[fay][0] = fax;
		}
		return 1;
	};
	for (int i = 0; i < M; i++) fa[i][0] = i, fa[i][3] = 1, fa[i][4] = i;
	for (int i = 1; i <= n; i++) {
		auto& p = tr[i];
		cin >> p.id >> p.fa >> p.mo >> p.k;
		vis[p.id] = 1;
		if (p.fa != -1) vis[p.fa] = 1;
		if (p.mo != -1) vis[p.mo] = 1;
		for (int j = 0, c; j < p.k; j++) cin >> c, p.ch.push_back(c) , vis[c] = 1;
		cin >> p.x >> p.s;
		fa[p.id][1] = p.x;
		fa[p.id][2] = p.s;
	}
	for (int i = 1; i <= n; i++) {
		auto& p = tr[i];
		if (p.fa != -1) {
			get(p.id, p.fa);
		}
		if (p.mo != -1) {
			get(p.id, p.mo);
		}
		for (int j = 0; j < (int)p.ch.size(); j++) {
			get(p.id, p.ch[j]);
		}
	}
	int res = 0;
	vector<int> ans;
	for (int i = 0; i < M; i++) {
		if (vis[i]) {
			int fax = find(i);
			if (fax == i) {
				res++; ans.push_back(i);
			}
		}
	}
	cout << res << endl;

	sort(ans.begin(), ans.end(), [&](int x, int y) {
		double a = 1.0 * fa[x][2] / fa[x][3];
		double b = 1.0 * fa[y][2] / fa[y][3];
		if (fabs(a - b) < 1e-6) {
			return fa[x][4] < fa[y][4];
		}
		return a > b;
	});
	for (int i = 0; i < (int)ans.size(); i++) {
		int x = ans[i];
		cout << setfill('0') << setw(4) << fa[x][4] << " ";
		cout << fa[x][3] << " " << (1.0 * fa[x][1] / fa[x][3]) << " " << (1.0 * fa[x][2] / fa[x][3]) << endl;
	}
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0);
	cout << fixed << setprecision(3);

	//init();//全局初始化

	int t = 1;
	//cin>>t;

	while (t--) {
		solve();
		//init();
	}

	return 0;
}
