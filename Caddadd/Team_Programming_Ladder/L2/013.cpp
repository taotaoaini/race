#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls p<<1
#define rs p<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 510;

int n, m, k;
int a[N][N];
void solve() {
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	//环内的，叶子节点
	cin >> n >> m;
	vector<int> fa(n + 10);
	for (int i = 0; i < n; i++) fa[i] = i;
	auto find = [&](int x) {
		while (x != fa[x]) x = fa[x] = fa[fa[x]];
		return x;
	};
	for (int i = 1; i <= m; i++) {
		int x, y;
		cin >> x >> y;
		a[x][y] = a[y][x] = 1;
	}
	set<int> d;
	cin >> k;
	vector<int> b(k + 1);
	for (int i = 1; i <= k; i++) cin >> b[i], d.insert(b[i]);
	for (int i = 0; i < n; i++) {
		if (d.count(i) >= 1) continue;
		for (int j = i + 1; j < n; j++) {
			if (d.count(j)>=1 || a[i][j] == 0) continue;
			int fax = find(i);
			int fay = find(j);
			fa[fax] = fay;
		}
	}
	vector<bool> ans(k + 1);
	for (int i = k; i >= 1; i--) {
		bool flag = false;
		set<int> st;
		for (int j = 0; j < n; j++) {
			if (j == b[i] || a[b[i]][j] == 0 || d.count(j) >= 1) continue;
			int fay = find(j);
			if (st.size() && st.count(fay) <= 0) {
				flag = true;
			}
			st.insert(fay);
			fa[find(b[i])] = fay;
		}
		d.erase(b[i]);
		ans[i] = flag;
	}
	for (int i = 1; i <= k; i++) {
		if (ans[i]) {
			cout << "Red Alert: City " << b[i] << " is lost!" << endl;
		} else {
			cout << "City " << b[i] << " is lost." << endl;
		}
	}
	if (k == n) {
		cout << "Game Over." << endl;
	}
}

int main() {
	ios::sync_with_stdio(false); cin.tie(0);
	cout << fixed << setprecision(12);

	//init();//全局初始化

	int t = 1;
	//cin>>t;

	while (t--) {
		solve();
		//init();
	}

	return 0;
}