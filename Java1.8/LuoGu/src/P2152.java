import java.io.BufferedInputStream;
import java.math.BigInteger;
import java.util.Scanner;

public class P2152 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(new BufferedInputStream(System.in));
        BigInteger a=sc.nextBigInteger();
        BigInteger b=sc.nextBigInteger();
        System.out.println(a.gcd(b).toString());
    }
}
