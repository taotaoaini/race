import java.io.BufferedInputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.util.Scanner;

public class P5714 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(new BufferedInputStream(System.in));
        double m=sc.nextDouble();
        double h=sc.nextDouble();
        double res=m/h/h;
        if(res<18.5) System.out.println("Underweight");
        else if(res>=24){
//            Double x= res;
            BigDecimal x=new BigDecimal(res);
            MathContext mc=new MathContext(6);
            res=x.divide(BigDecimal.ONE,mc).doubleValue();
            System.out.println(res);
            System.out.println("Overweight");
        }
        else System.out.println("Normal");
    }
}
