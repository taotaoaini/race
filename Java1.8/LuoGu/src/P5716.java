import javax.swing.plaf.synth.SynthUI;
import java.io.BufferedInputStream;
import java.util.Scanner;

public class P5716 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(new BufferedInputStream(System.in));
        int y=sc.nextInt();
        int m=sc.nextInt();
        int[] a={0,31,28,31,30,31,30,31,31,30,31,30,31};//直接初始化创建数组
        if(m==2){
             if(y%4==0&&y%100!=0||y%400==0){
                 System.out.println(a[m]+1);
             }else{
                 System.out.println(a[m]);
             }
        }else{
            System.out.println(a[m]);
        }
    }
}
