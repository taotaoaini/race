import java.util.LinkedList;

class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
  }

public class Main{
    // 代表分隔符的字符
    String SEP=",";
    // 代表 null 空指针的字符
    String NULL="#";
    TreeNode deserialize(String data){
        LinkedList<String> nodes=new LinkedList<>();
        for(String s:data.split(SEP)){
            nodes.addLast(s);
        }
        return dfs(nodes);
    }
    TreeNode dfs(LinkedList<String> nodes){
        if(nodes.isEmpty()) return null;
        String node=nodes.removeFirst();
        if(node.equals(NULL)) return null;
        TreeNode root=new TreeNode(Integer.parseInt(node));
        root.left=dfs(nodes);
        root.right=dfs(nodes);
        return root;
    }
}